<?php
/*
Plugin Name: Corporate Toolkit
Plugin URI: http://kutethemes.net
Description: This Toolkit is used for themes that are developed by Kutethemes.
Version: 1.0.7
Author: Kutethemes
Author URI: http://kutethemes.net
*/

// don't load directly
if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

if( !class_exists('Coporate_Toolkit') ){
	class Coporate_Toolkit{
		public $plugin_uri ='';
		/**
		 * The single instance of the class.
		 *
		 * @var Coporate_Toolkit
		 * @since 1.0
		 */
		protected static $_instance = null;

        public $tabs = array();

		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}

		/**
		 * Coporate_Toolkit Constructor.
		 */
		public function __construct() {
			$this->plugin_uri = trailingslashit(plugin_dir_url(__FILE__) );

			$this->includes();
			$this->define_constants();
			add_action( 'admin_menu', array( $this, 'admin_menu' ),9 );
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_script' ) );
            $this->set_tabs();
			do_action( 'coporate_toolkit_loaded' );
			add_filter( 'user_contactmethods', array(&$this,'author_info'));
		}

		public function admin_script(){
			wp_enqueue_style( 'kt-backend',$this->plugin_uri . 'assets/css/style.css', array(), false );
			wp_enqueue_style( 'cmb2-style',$this->plugin_uri . 'includes/vendor/CMB/css/style.css', array(), false );
			wp_enqueue_script( 'kt-theme-option-js', $this->plugin_uri . 'includes/vendor/CMB/js/custom.js', array( 'jquery' ), '1.0', true );
		}
		
		/**
		 * Coporate_Toolkit admin menu.
		 */
		public function admin_menu(){
			add_menu_page( 'Coporate', 'Coporate', 'manage_options', 'corporatepro', array( &$this, 'wellcome' ), $this->plugin_uri .'assets/images/menu-icon.png', 2 );
			add_submenu_page( 'corporatepro', 'Coporate Dashboard', 'Dashboard', 'manage_options', 'corporatepro',  array( $this, 'wellcome' ) );
		}

        public function set_tabs(){
            $this->tabs = array(
                'demos' => esc_html__('Sample Data','corporatepro'),
                'support' => esc_html__('Support','corporatepro')
            );

        }
		/**
		 * Coporate_Toolkit cpanel page.
		 */
        public function wellcome(){

            /* deactivate_plugin */
            if( isset($_GET['action']) && $_GET['action'] == 'deactivate_plugin'){
                $this->deactivate_plugin();
            }
            /* deactivate_plugin */
            if( isset($_GET['action']) && $_GET['action'] == 'active_plugin'){
                $this->active_plugin();
            }

            $tab = 'demos';
            if( isset($_GET['tab'])){
                $tab = $_GET['tab'];
            }
            ?>
            <div class="wrap redapple-wrap">
                <div class="welcome-panel">
                    <div class="welcome-panel-content">
                        <h2><?php esc_html_e('Welcome to Linda!','corporatepro');?></h2>
                        <p class="about-description"><?php esc_html_e('We\'ve assembled some links to get you started','corporatepro');?></p>
                        <div class="welcome-panel-column-container">
                            <div class="welcome-panel-column">
                                <h3><?php esc_html_e('Get Started','corporatepro')?></h3>
                                <a href="http://kutethemes.net/wordpress/corporate/" target="_blank" class="button button-primary button-hero trigger-tab"><?php esc_html_e('View demos','corporatepro');?></a>
                                <p class="small-text">or, <a href="http://localhost/wp-test/wp-admin/customize.php"><?php echo esc_html_e('Customize your site','corporatepro');?></a></p>
                            </div>
                            <div class="welcome-panel-column">
                                <h3><?php echo esc_html_e('Next Steps','corporatepro');?></h3>
                                <ul>
                                    <li><a target="_blank" href="#" class="welcome-icon welcome-widgets-menus">View Header Builder</a></li>
                                    <li><a target="_blank" href="#" class="welcome-icon dashicons-media-document"><?php esc_html_e('Read Documentation','corporatepro')?></a></li>
                                    <li><a target="_blank" href="http://support.ovicsoft.com/support-system" class="welcome-icon dashicons-editor-help"><?php esc_html_e('Request Support','corporatepro');?></a></li>
                                    <li><a target="_blank" href="http://kutethemes.net/wordpress/corporate/changelog.txt" class="welcome-icon dashicons-backup"><?php esc_html_e('View Changelog Details','corporatepro');?></a></li>
                                </ul>
                            </div>
                            <div class="welcome-panel-column">
                                <h3><?php esc_html_e('Keep in Touch','corporatepro');?></h3>
                                <ul>
                                    <li><a target="_blank" href="#" class="welcome-icon dashicons-email-alt"><?php esc_html_e('Newsletter','corporatepro');?></a></li>
                                    <li><a target="_blank" href="#" class="welcome-icon dashicons-twitter"><?php esc_html_e('Twitter','corporatepro');?></a></li>
                                    <li><a target="_blank" href="#" class="welcome-icon dashicons-facebook"><?php esc_html_e('Facebook','corporatepro');?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- .welcome-panel-content -->
                </div>
                <div id="tabs-container" role="tabpanel">
                    <div class="nav-tab-wrapper">
                        <?php foreach ($this->tabs as $key => $value ):?>
                            <a class="nav-tab corporatepro-nav <?php if( $tab == $key ):?> active<?php endif;?>" href="admin.php?page=corporatepro&tab=<?php echo esc_attr($key);?>"><?php echo esc_html($value);?></a>
                        <?php endforeach;?>
                    </div>
                    <div class="tab-content">
                        <?php $this->$tab();?>
                    </div>
                </div>
            </div>
            <?php
        }
        // Author Social Links
		
		public function author_info(){
            $contactmethods             = array();
            $contactmethods['company']	= esc_html__('Company','corporatepro');
            $contactmethods['twitter']  = esc_html__('Link Twitter','corporatepro');
            $contactmethods['facebook'] = esc_html__('Link Facebook','corporatepro');
            $contactmethods['google']   = esc_html__('Link Google Plus','corporatepro');
		    
		    return $contactmethods;
		}
        public static function demos(){
            if( class_exists('CORPORATE_IMPORTER')){
               $corporate_importer = new CORPORATE_IMPORTER();
               $corporate_importer->importer_page_content();
            }
        }
        public static function support(){
            ?>
            <div class="rp-row">
                <div class="rp-col">
                    <div class="suport-item">
                        <h3><?php esc_html_e('Documentation','linda');?></h3>
                        <p><?php esc_html_e('Here is our user guide for Linda, including basic setup steps, as well as Linda features and elements for your reference.','linda');?></p>
                        <a target="_blank" href="#" class="button button-primary"><?php esc_html_e('Read Documentation','linda');?></a>
                    </div>
                </div>
                <div class="rp-col closed">
                    <div class="suport-item">
                        <h3><?php esc_html_e('Video Tutorials','linda');?></h3>
                        <p class="coming-soon"><?php esc_html_e('Video tutorials is the great way to show you how to setup Linda theme, make sure that the feature works as it\'s designed.','linda');?></p>
                        <a href="#" class="button button-primary disabled"><?php esc_html_e('See Video','linda');?></a>
                    </div>
                </div>
                <div class="rp-col">
                    <div class="suport-item">
                        <h3><?php esc_html_e('Forum','linda');?></h3>
                        <p><?php esc_html_e('Can\'t find the solution on documentation? We\'re here to help, even on weekend. Just click here to start 1on1 chatting with us!','linda');?></p>
                        <a target="_blank" href="http://support.ovicsoft.com/support-system" class="button button-primary"><?php esc_html_e('Request Support','linda');?></a>
                    </div>
                </div>
            </div>
            <?php
        }
		/**
		 * Define Coporate_Toolkit Constants.
		 */
		private function define_constants() {
			
			$this->define( 'CORPORATEPRO_VERSION', '1.0' );
			$this->define( 'CORPORATEPRO_BASE_URL', trailingslashit( plugins_url( 'corporate-toolkit' ) ) );
			$this->define( 'CORPORATEPRO_DIR_PATH', plugin_dir_path( __FILE__ ) );
			$this->define( 'CORPORATEPRO_LIBS', CORPORATEPRO_DIR_PATH . '/libs/' );
			$this->define( 'CORPORATEPRO_LIBS_URL', CORPORATEPRO_BASE_URL . '/libs/' );
			$this->define( 'CORPORATEPRO_CORE', CORPORATEPRO_DIR_PATH . '/core/' );
			$this->define( 'CORPORATEPRO_CSS_URL', CORPORATEPRO_BASE_URL . 'assets/css/' );
			$this->define( 'CORPORATEPRO_JS', CORPORATEPRO_BASE_URL . 'assets/js/' );
			$this->define( 'CORPORATEPRO_VENDORS_URL', CORPORATEPRO_BASE_URL . 'assets/vendors/' );
			$this->define( 'CORPORATEPRO_IMG_URL', CORPORATEPRO_BASE_URL . 'assets/images/' );
			$this->define( 'CORPORATEPRO_SHORTCODE_IMG_URL', CORPORATEPRO_BASE_URL . 'assets/shortcode-images/' );
			$this->define( 'CORPORATEPRO_TEMPLATES_PATH','templates/');
		}
				
				
				
		/**
		 * Define constant if not already set.
		 *
		 * @param  string $name
		 * @param  string|bool $value
		 */

		private function define( $name, $value ) {
			if ( ! defined( $name ) ) {
				define( $name, $value );
			}
		}

		/**
		 * Include required core files used in admin and on the frontend.
		 */
		public function includes() {
			include_once( 'includes/vendor/ReduxCore/framework.php' );
			include_once( 'includes/vendor/CMB/init.php' );
			include_once( 'includes/post-types.php' );
            include_once( 'includes/widgets/widget_instagram.php' );

			/*MAILCHIP*/
			include_once( 'includes/classes/MCAPI/MCAPI.class.php' );
			include_once( 'includes/classes/MCAPI/mailchimp-settings.php' );
			include_once( 'includes/classes/MCAPI/mailchimp.php' );
			/* SHORTCODE */
			if (class_exists('Vc_Manager')) {
			    include_once( 'shortcodes/shortcodes.php' );
				include_once( 'shortcodes/newsletter.php' );
				include_once( 'shortcodes/contactinfo.php');
				include_once( 'shortcodes/custommenu.php');
				include_once( 'shortcodes/socials.php');
				include_once( 'shortcodes/counters.php');
				include_once( 'shortcodes/process_bar.php');
				include_once( 'shortcodes/pie_chart.php');
				include_once( 'shortcodes/icon_with_text.php');
				include_once( 'shortcodes/pricing.php');
				include_once( 'shortcodes/custombox.php');
				include_once( 'shortcodes/whatwedo.php');
				include_once( 'shortcodes/team.php');
				include_once( 'shortcodes/aboutbox.php');
				include_once( 'shortcodes/button.php');
				include_once( 'shortcodes/list.php');
				include_once( 'shortcodes/projects.php');
				include_once( 'shortcodes/team.php');
				include_once( 'shortcodes/tabs.php');
				include_once( 'shortcodes/sectiontitle.php');
				include_once( 'shortcodes/video.php');
				include_once( 'shortcodes/partner.php');
				include_once( 'shortcodes/accordion.php');
				include_once( 'shortcodes/feature.php');
				include_once( 'shortcodes/maps.php');
				include_once( 'shortcodes/banner.php');
				include_once( 'shortcodes/wellcome.php');
				include_once( 'shortcodes/blogs.php');
                include_once( 'shortcodes/instagram.php');
				include_once( 'shortcodes/service.php');
                include_once( 'shortcodes/simple_slide.php');
			}
			
			if ( class_exists( 'WooCommerce' ) ){
				include_once( 'shortcodes/functions.php');
			    include_once( 'shortcodes/products.php');
			}
			
		}
		/**
		 * Include required frontend files.
		 */
		public function frontend_includes() {
			
		}
	}
}

/**
 * Main instance of Coporate_Toolkit.
 *
 * Returns the main instance of WC to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return Coporate_Toolkit
 */
if( !function_exists( 'Coporate_Toolkit' ) ){
	function Coporate_Toolkit() {
		return Coporate_Toolkit::instance();
	}
}


add_action( 'plugins_loaded', 'Coporate_Toolkit' ,9999);
// Global for backwards compatibility.
// $GLOBALS['coporate_toolkit'] = Coporate_Toolkit();
