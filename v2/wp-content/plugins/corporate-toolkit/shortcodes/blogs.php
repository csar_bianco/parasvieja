<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}
        
add_action( 'vc_before_init', 'corporatepro_blogs_settings' );
function corporatepro_blogs_settings() {
    /* CATEGORY DRROPDOW */
    $categories_array = array(
        __('All', 'corporate') => ''
    );
    $args = array(
        'taxonomy' => 'category'
    );
    $categories = get_categories($args);
    foreach ($categories as $category) {
        //kt_print($category);
        $categories_array[$category->name] = $category->slug;
    }
    
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'corporatepro' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'blogs/default.jpg'
                ),
                'layout1'=>array(
                    'alt'=>'layout 01',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'blogs/layout1.jpg'
                ),
                'layout2'=>array(
                    'alt'=>'layout 02',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'blogs/layout2.jpg'
                ),
                'layout3'=>array(
                    'alt'=>'layout 03',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'blogs/layout3.jpg'
                ),
                'layout4'=>array(
                    'alt'=>'layout 04',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'blogs/layout4.jpg'
                ),
                'layout5'=>array(
                    'alt'=>'layout 05',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'blogs/layout5.jpg'
                ),
                'layout6'=>array(
                    'alt'=>'layout 06',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'blogs/layout6.jpg'
                ),
                'layout7'=>array(
                    'alt'=>'layout 07',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'blogs/layout7.jpg'
                ),
                'layout8'=>array(
                    'alt'=>'layout 08',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'blogs/layout8.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'corporatepro' ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'corporatepro' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'corporatepro' ),
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Date Start: ', 'corporatepro' ),
            'param_name'    =>  'd_start',
            'admin_label'   =>  true,
            'std'           =>  '',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout8'),
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Date End:', 'corporatepro' ),
            'param_name'    =>  'd_end',
            'admin_label'   =>  true,
            'std'           =>  '',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout8'),
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Month', 'corporatepro' ),
            'description'   =>  __( 'Only use full month by string,ex: February', 'corporatepro' ),
            'param_name'    =>  'month',
            'admin_label'   =>  true,
            'std'           =>  '',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout8'),
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Year', 'corporatepro' ),
            'param_name'    =>  'year',
            'admin_label'   =>  true,
            'std'           =>  '',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout8'),
            ),
        ),

        array(
            'type'        => 'textfield',
            'heading'     => __('Number Post', 'corporatepro'),
            'param_name'  => 'per_page',
            'std'         => 3,
            'admin_label' => false,
            'description' => __('Number post in a slide', 'corporatepro'),
        ),
        // responsive ---------------------------------------------------
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( '1 Row', 'corporatepro' )  => '1',
                __( '2 Rows', 'corporatepro' ) => '2',
                __( '3 Rows', 'corporatepro' ) => '3',
                __( '4 Rows', 'corporatepro' ) => '4',
                __( '5 Rows', 'corporatepro' ) => '5'
            ),
            'std'         => '1',
            'heading'     => __( 'The number of rows which are shown on block', 'corporatepro' ),
            'param_name'  => 'owl_number_row',
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'Yes', 'corporatepro' ) => 'true',
                __( 'No', 'corporatepro' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => __( 'AutoPlay', 'corporatepro' ),
            'param_name'  => 'owl_autoplay',
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'No', 'corporatepro' )  => 'false',
                __( 'Yes', 'corporatepro' ) => 'true'
            ),
            'std'         => false,
            'heading'     => __( 'Navigation', 'corporatepro' ),
            'param_name'  => 'owl_navigation',
            'description' => __( "Show buton 'next' and 'prev' buttons.", 'corporatepro' ),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'No', 'corporatepro' )  => 'false',
                __( 'Yes', 'corporatepro' ) => 'true'
            ),
            'std'         => false,
            'heading'     => __( 'Dots', 'corporatepro' ),
            'param_name'  => 'owl_dots',
            'description' => __( "Show dots.", 'corporatepro' ),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'Yes', 'corporatepro' ) => 'true',
                __( 'No', 'corporatepro' )  => 'false'
            ),
            'std'         => false,
            'heading'     => __( 'Loop', 'corporatepro' ),
            'param_name'  => 'owl_loop',
            'description' => __( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'corporatepro' ),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Slide Speed", 'corporatepro'),
            "param_name"  => "owl_slidespeed",
            "value"       => "200",
            "suffix"      => __("milliseconds", 'corporatepro'),
            "description" => __('Slide speed in milliseconds', 'corporatepro'),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Margin", 'corporatepro'),
            "param_name"  => "owl_margin",
            "value"       => "0",
            "description" => __('Distance( or space) between 2 item', 'corporatepro'),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),

        array(
            "type"        => "textfield",
            "heading"     => __("The items on desktop (Screen resolution of device >= 1200px )", 'corporatepro'),
            "param_name"  => "owl_lg_items",
            "value"       => "4",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on desktop (Screen resolution of device >= 992px < 1200px )", 'corporatepro'),
            "param_name"  => "owl_md_items",
            "value"       => "3",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on tablet (Screen resolution of device >=768px and < 992px )", 'corporatepro'),
            "param_name"  => "owl_sm_items",
            "value"       => "3",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on mobile landscape(Screen resolution of device >=480px and < 768px)", 'corporatepro'),
            "param_name"  => "owl_xs_items",
            "value"       => "2",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on mobile (Screen resolution of device < 480px)", 'corporatepro'),
            "param_name"  => "owl_ts_items",
            "value"       => "1",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        // responsive ---------------------------------------------------

        /* group posts */
        array(
            'param_name'  => 'category_slug',
            'type'        => 'dropdown',
            'value'       => $categories_array, // here I'm stuck
            'heading'     => __('Category filter:', 'corporatepro'),
            "admin_label" => false,
        ),
        array(
            "type"        => "dropdown",
            "heading"     => __("Order by", 'corporatepro'),
            "param_name"  => "orderby",
            "value"       => array(
                __('None', 'corporatepro')     => 'none',
                __('ID', 'corporatepro')       => 'ID',
                __('Author', 'corporatepro')   => 'author',
                __('Name', 'corporatepro')     => 'name',
                __('Date', 'corporatepro')     => 'date',
                __('Modified', 'corporatepro') => 'modified',
                __('Rand', 'corporatepro')     => 'rand',
            ),
            'std'         => 'date',
            "description" => __("Select how to sort retrieved posts.", 'corporatepro'),
        ),
        array(
            "type"        => "dropdown",
            "heading"     => __("Order", 'corporatepro'),
            "param_name"  => "order",
            "value"       => array(
                __('ASC', 'corporatepro')  => 'ASC',
                __('DESC', 'corporatepro') => 'DESC'
            ),
            'std'         => 'DESC',
            "description" => __("Designates the ascending or descending order.", 'corporatepro'),
        ),

        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "corporatepro"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro"),
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'corporatepro' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'corporatepro' ),
        )
    );

    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Blogs', 'corporatepro' ),
        'base'     => 'corporatepro_blogs', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'corporatepro' ),
        'description'   =>  __( 'Display a Posts.', 'corporatepro' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_blogs( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_blogs', $atts ) : $atts;

    $default_atts = array(
        'layout'            => 'default',
        'title'             => '',
        'target'            => '',
        'category_slug'     => '',
        'per_page'          => 10,
        'orderby'           => 'date',
        'order'             => 'desc',
        'status'            => '',
        'css'               => '',
        'items'             => '',
        'el_class'          => '',
        'owl_number_row'    => 1,
        'owl_rows_space'    => 'rows-space-0',
        'owl_autoplay'      => false,
        'owl_navigation'    => false,
        'owl_dots'          => false,
        'owl_loop'          => false,
        'owl_slidespeed'    => 200,
        'owl_margin'        => 0,
        'owl_lg_items'      => 4,
        'owl_md_items'      => 3,
        'owl_sm_items'      => 3,
        'owl_xs_items'      => 2,
        'owl_ts_items'      => 1,
        'd_start'           => '',
        'd_end'             => '',
        'month'             => '',
        'year'              => '',
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' ' .$layout.' '.$status;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $owl_settings = '';

    $owl_settings = corporatepro_generate_carousel_data_attributes('owl_', $atts);
    
    $args = array(
        'post_type'           => 'post',
        'post_status'         => 'publish',
        'ignore_sticky_posts' => 1,
        'posts_per_page'      => $per_page,
        'suppress_filter'     => true,
        'orderby'             => $orderby,
        'order'               => $order,    
    );

    if ( $category_slug ) {
        $idObj = get_category_by_slug($category_slug);
        if (is_object($idObj)) {
            $args['cat'] = $idObj->term_id;
        }
    }

    $loop_posts = new WP_Query(apply_filters('corporatepro_shortcode_posts_query', $args, $atts));

    $template_args = array(
        'atts'          => $atts,
        'css_class'     => $css_class,
        'loop_posts'    => $loop_posts,
        'owl_carousel'  => $owl_settings,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/blogs/temp',$layout, $template_args );

    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_blogs', 'corporatepro_blogs' );
