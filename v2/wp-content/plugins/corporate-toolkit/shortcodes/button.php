<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_button_settings' );
function corporatepro_button_settings() {
    $socials = array();
    $all_socials = corporatepro_get_all_social();
    if( $all_socials ){
        foreach ($all_socials as $key =>  $social)
            $socials[$social['name']] = $key;
    }
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'button/default.jpg'
                ),
                'outline-button'=>array(
                    'alt'=>'Outline button',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'button/layout1.jpg'
                ),
                'layout2'=>array(
                    'alt'=>'Layout2',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'button/layout2.jpg'
                ),
                'layout3'=>array(
                    'alt'=>'Layout3',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'button/layout3.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        array(
            "type" => "textfield",
            "heading" => __("Title", "trueshop"),
            "param_name" => "title",
            'admin_label' => true,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout2'),
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Style button', 'trueshop'),
            'value'       => array(
                __('Button outline', 'trueshop')        => 'outline-button',
                __('Button background', 'trueshop')     => 'cp-button',
            ),
            'admin_label' => true,
            'param_name'  => 'style_btn',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout3',
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Icon library', 'trueshop'),
            'value'       => array(
                __('Font Awesome', 'trueshop')     => 'fontawesome',
            ),
            'admin_label' => true,
            'param_name'  => 'icon_lib',
            'description' => __('Select icon library.', 'trueshop'),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout3',
            ),
        ),
        array(
            'type'        => 'iconpicker',
            'heading'     => __('Icon', 'trueshop'),
            'param_name'  => 'icon_fontawesome',
            'value'       => 'fa fa-adjust', // default value to backend editor admin_label
            'settings'    => array(
                'emptyIcon'    => false,
                'iconsPerPage' => 4000,
            ),
            'dependency'  => array(
                'element' => 'icon_lib',
                'value'   => 'fontawesome',
            ),
            'description' => __('Select icon from library.', 'trueshop'),
        ),
        array(
            'type' => 'vc_link',
            'heading' => __( 'URL (Link)', 'kute-toolkit' ),
            'param_name' => 'link',
            'description' => __( 'Add link.', 'kute-toolkit' ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Button align', 'trueshop' ),
            'param_name'  => 'button_align',
            'value'       => array(
                'Default'   => '',
                'Center'    => 'button-align center',
                'Right'     => 'button-align right',
                'Left'      => 'button-align left',
            ),
            'std'=>''
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Button Size', 'trueshop' ),
            'param_name'  => 'button_size',
            'value'       => array(
                'Default' => '',
                'Medium'  => 'button-medium',
                'Darger'  => 'button-larger',
            ),
            'std'=>''
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "trueshop"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'trueshop' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'trueshop' ),
        )
    );
    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Button', 'trueshop' ),
        'base'     => 'corporatepro_button', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        'description'   =>  __( 'Display a button.', 'trueshop' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_button( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_button', $atts ) : $atts;

    $default_atts = array(
        'layout'      => 'default',
        'link'        => '',
        'icon_lib'    => '',
        'layout2'     => '',
        'button_size' => '',
        'button_align'=> '',
        'style_btn'   => '',
        'css'         => '',
        'el_class'    => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' cp-button '.$layout.' '.$button_size .' '.$button_align;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/button/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_button', 'corporatepro_button' );
