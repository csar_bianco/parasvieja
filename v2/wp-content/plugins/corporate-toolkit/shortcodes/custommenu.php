<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepre_custommenu_settings' );
function corporatepre_custommenu_settings() {
    $all_menu = array();
    $menus =  get_terms( 'nav_menu', array( 'hide_empty' => false ));
    if( $menus && count($menus) > 0 ){
        foreach ($menus as $m){
            $all_menu[$m->name] = $m->slug;
        }
    }
    $params  = array(
        array(
            'type'        => 'textfield',
            'heading'     => __('Title', 'trueshop'),
            'param_name'  => 'title',
            'description' => __('The title of shortcode', 'trueshop'),
            'admin_label' => true,
            'std'         => '',
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Menu', 'trueshop' ),
            'param_name'  => 'menu',
            'value'       => $all_menu,
            'description' => __( 'Select menu to display.', 'trueshop' )
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", "trueshop"),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__('Css', 'trueshop'),
            'param_name' => 'css',
            'group'      => esc_html__('Design options', 'trueshop'),
        )
    );
    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Custom menu', 'trueshop' ),
        'base'     => 'corporatepre_custommenu', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        'description'   =>  __( 'Display a menu.', 'trueshop' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepre_custommenu( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepre_custommenu', $atts ) : $atts;

    $default_atts = array(
        'title'    => '',
        'menu'     => '',
        'css'      => '',
        'el_class' => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' corporatepre_custommenu ';
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $nav_menu = get_term_by('slug', $menu,'nav_menu');
    ob_start();
    ?>
    <div class="<?php echo esc_attr( $css_class );?>">
        <?php if( is_object($nav_menu) && count($nav_menu) == 1 ):?>
            <?php
            $instance = array();
            if( $title ){
                $instance['title'] = $title;
            }
            if( $nav_menu->term_id ){
                $instance['nav_menu'] = $nav_menu->term_id;
            }
            the_widget('WP_Nav_Menu_Widget',$instance);
            ?>
        <?php endif;?>
    </div>
    <?php
    $html = ob_get_clean();
    return balanceTags( $html );

}

add_shortcode( 'corporatepre_custommenu', 'corporatepre_custommenu' );
