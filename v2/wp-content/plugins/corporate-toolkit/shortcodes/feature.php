<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'feature_settings' );
function feature_settings() {
    $socials = array();
    $all_socials = corporatepro_get_all_social();
    if( $all_socials ){
        foreach ($all_socials as $key =>  $social)
            $socials[$social['name']] = $key;
    }
    $params  = array(
        array(
            'type'      => 'kt_select_preview',
            'heading'   => __( 'Layout', 'corporatepro' ),
            'value'     => array(
                'default'   =>array(
                    'alt'   =>'Default',
                    'img'   =>CORPORATEPRO_SHORTCODE_IMG_URL.'feature/default.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label'   => true,
            'param_name'    => 'layout',
            'description'   => __( 'Select a layout.', 'corporatepro' ),
        ),
        array(
            "type"          => "textfield",
            "heading"       => __("Title", "corporatepro"),
            "param_name"    => "title",
            "description"   => __("title shortcode.", "corporatepro")
        ),
        array(
            "type"          => "textarea",
            "heading"       => __("Description", "corporatepro"),
            "param_name"    => "description",
            "description"   => __("title shortcode.", "corporatepro")
        ),
        array(
            "type"          => "textfield",
            "heading"       => __("Number", "corporatepro"),
            "param_name"    => "number",
            "description"   => __("title shortcode.", "corporatepro")
        ),
        array(
            "type"          => "checkbox",
            "param_name"    => "light",
            'value' => array(
                __( 'Light', 'corporatepro' )  => 'light'
            ),
            "description"   => __("Style feature.", "corporatepro")
        ),
        array(
            'type'          => 'dropdown',
            'param_name'    => 'text_align',
            'value' => array(
                __( 'Center', 'corporatepro' )  => 'text-center',
                __( 'Left', 'corporatepro' )    => 'text-left',
                __( 'Right', 'corporatepro' )   => 'text-right',
            ),
            'std'           => '',
            'heading'       => __( 'Text Align', 'corporatepro' ),
            'description'   => __( 'Select style feature.', 'corporatepro' ),
        ),
        array(
            'type' => 'vc_link',
            'heading' => __( 'URL (Link)', 'corporatepro' ),
            'param_name' => 'link',
            'admin_label' => true,
            'description' => __( 'Add link.', 'corporatepro' ),
        ),

        array(
            "type"          => "textfield",
            "heading"       => __("Extra class name", "corporatepro"),
            "param_name"    => "el_class",
            "description"   => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'corporatepro' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'corporatepro' ),
        )
    );
    $map_settings = array(
        'name'          => esc_html__( 'Corporate: Feature', 'corporatepro' ),
        'base'          => 'feature', // shortcode
        'category'      => esc_html__( 'Corporate', 'corporatepro' ),
        'description'   =>  __( 'Display a feature.', 'corporatepro' ),
        'params'        => $params,
    );

    vc_map( $map_settings );
}

function feature( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'feature', $atts ) : $atts;

    $default_atts = array(
        'layout'            => 'default',
        'text_align'        => '',
        'description'       => '',
        'number'            => '',
        'title'             => '',
        'light'             => '',
        'link'              => '',
        'c_icon'            => '',
        'css'               => '',
        'el_class'          => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' cp-feature ' .$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/feature/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'feature', 'feature' );
