<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}
        
add_action( 'vc_before_init', 'corporatepro_icon_with_text_settings' );
function corporatepro_icon_with_text_settings() {
   
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'icon_with_text/default.jpg'
                ),
                'style1'=>array(
                    'alt'=>'Style 01',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'icon_with_text/layout1.jpg'
                ),
                'style2'=>array(
                    'alt'=>'Style 02',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'icon_with_text/layout2.jpg'
                ),
                'style3'=>array(
                    'alt'=>'Style 03',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'icon_with_text/layout3.jpg'
                ),
                'style4'=>array(
                    'alt'=>'Style 04',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'icon_with_text/layout4.jpg'
                ),
                'style5'=>array(
                    'alt'=>'Style 05',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'icon_with_text/layout5.jpg'
                ),
                'style6'=>array(
                    'alt'=>'Style 06',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'icon_with_text/layout6.jpg'
                ),
                'style7'=>array(
                    'alt'=>'Style 07',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'icon_with_text/layout7.jpg'
                ),
                'style8'=>array(
                    'alt'=>'Style 08',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'icon_with_text/layout8.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'trueshop' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
         array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Sub title', 'trueshop' ),
            'param_name'    =>  'subtitle',
            'description'   =>  __( 'The title of shortcode', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  '',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style4'),
            ),
        ),
        array(
            'type'          =>  'textarea',
            'heading'       =>  __( 'Description', 'trueshop' ),
            'param_name'    =>  'description',
            'description'   =>  __( 'The description of shortcode', 'trueshop' ),
            'admin_label'   =>  false,
            'std'           =>  '',
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Status', 'trueshop'),
            'value'       => array(
                __('None', 'trueshop')   => '',
                __('Active', 'trueshop') => 'active',
            ),
            'admin_label' => true,
            'param_name'  => 'status',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style4'),
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Icon library', 'trueshop'),
            'value'       => array(
                __('Font Awesome', 'trueshop')     => 'fontawesome',
            ),
            'admin_label' => true,
            'param_name'  => 'icon_lib',
            'description' => __('Select icon library.', 'trueshop'),
        ),
        array(
            'type'        => 'iconpicker',
            'heading'     => __('Icon', 'trueshop'),
            'param_name'  => 'icon_fontawesome',
            'value'       => 'fa fa-adjust', // default value to backend editor admin_label
            'settings'    => array(
                'emptyIcon'    => false,
                'iconsPerPage' => 4000,
            ),
            'dependency'  => array(
                'element' => 'icon_lib',
                'value'   => 'fontawesome',
            ),
            'description' => __('Select icon from library.', 'trueshop'),
        ),
        array(
            'type' => 'vc_link',
            'heading' => __( 'URL (Link)', 'xshop-core' ),
            'param_name' => 'link',
            'description' => __( 'Add link to custom heading.', 'trueshop' ),
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "trueshop"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'trueshop' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'trueshop' ),
        )
    );

    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Icon With Text', 'trueshop' ),
        'base'     => 'corporatepro_icon_with_text', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        'description'   =>  __( 'Display a Icon.', 'trueshop' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_icon_with_text( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_icon_with_text', $atts ) : $atts;

    $default_atts = array(
        'layout'      => 'default',
        'title'       => '',
        'subtitle'    =>'',
        'description' => '',
        'link'        => '',
        'icon_lib'    => '',
        'status'      => '',
        'css'         => '',
        'el_class'    => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' ' .$layout.' '.$status;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $icon = $atts['icon_'.$icon_lib];
    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
        'icon'      => $icon
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/icon_with_text/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_icon_with_text', 'corporatepro_icon_with_text' );
