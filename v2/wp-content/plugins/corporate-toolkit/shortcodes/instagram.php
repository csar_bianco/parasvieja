<?php

// Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }


    add_action('vc_before_init', 'dezilkInstagram');
    function dezilkInstagram()
    {
        vc_map(
            array(
                'name'     => esc_html__('Corporate: Instagram', 'corporatepro'),
                'base'     => 'dezi_core_instagram', // shortcode
                'class'    => '',
                'category' => esc_html__('Corporate', 'corporatepro'),
                'params'   => array(
                    array(
                        'type'       => 'textfield',
                        'holder'     => 'div',
                        'class'      => '',
                        'heading'    => esc_html__('Title', 'corporatepro'),
                        'param_name' => 'title',
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'holder'     => 'div',
                        'class'      => '',
                        'heading'    => esc_html__('Title Color', 'corporatepro'),
                        'param_name' => 'title_color',
                        'std'        => '#fff',
                    ),
                    array(
                        'type'       => 'textfield',
                        'holder'     => 'div',
                        'class'      => '',
                        'heading'    => esc_html__('User Name', 'ihosting-core'),
                        'param_name' => 'username',
                        'std'        => 'almostmakesperfect',
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder'      => 'div',
                        'class'       => '',
                        'heading'     => esc_html__('Limit items', 'ihosting-core'),
                        'param_name'  => 'limit_items',
                        'std'         => '12',
                        'description' => esc_html__('Enter number of limit item to display max = 12', 'ihosting-core'),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'class'      => '',
                        'heading'    => esc_html__('Image Link Target', 'corporatepro'),
                        'param_name' => 'target',
                        'value'      => array(
                            esc_html__('Open In New Tab', 'corporatepro')        => '_blank',
                            esc_html__('Open In Current Window', 'corporatepro') => '_self',
                        ),
                        'std'        => '_blank',
                    ),
                    vc_map_add_css_animation(),
                    array(
                        'type'       => 'css_editor',
                        'heading'    => esc_html__('Css', 'corporatepro'),
                        'param_name' => 'css',
                        'group'      => esc_html__('Design options', 'corporatepro'),
                    )
                )
            )
        );
    }

    function dezi_core_instagram($atts)
    {

        $atts = function_exists('vc_map_get_attributes') ? vc_map_get_attributes('dezi_core_instagram', $atts) : $atts;

        extract(
            shortcode_atts(
                array(
                    'title'               => '',
                    'title_color'         => '#fff',
                    'username'            => '',
                    'limit_items'         => '',
                    'target'              => '',
                    'css_animation'       => '',
                    'css'                 => '',
                ), $atts
            )
        );


        $html = '';
        if ($username != '') {

            $media_array = corporatepro_mr_instagram($username, $limit_items);

            if (is_wp_error($media_array)) {

                echo $media_array->get_error_message();

            } else {

                // filter for images only?
                if ($images_only = apply_filters('wpiw_images_only', false)) {
                    $media_array = array_filter($media_array, dezi_images_only());
                }
                $html .= '<ul class="instagram-pics">';
                $i = 0;
                foreach ($media_array as $item) {

                    $i++;
                    $img_url = $item['thumbnail'];
                    $img_name = basename($img_url);

                    $size_add = 's75x75';


                    $crop_str = '/sh0.08/c0.100.800.800/c0.135.1080.1080/c0.0.1079.1079/c107.0.865.865/c123.0.557.557/c234.0.611.611/';

                    if ($item['type'] == 'video') {
                        $crop_str = '/';
                    }

                    $img_url = str_replace($img_name, $size_add . $crop_str . $img_name, $img_url);

                    $html .= '<li><a href="' . esc_url($item['link']) . '" target="' . esc_attr($target) . '"><img src="' . esc_url($img_url) . '"  alt="' . esc_attr($item['description']) . '" title="' . esc_attr($item['description']) . '"/></a></li>';

                }

                $html .= '</ul><!-- /.photo-instagram -->';

            }
        } //if ($username != '')

        $html = '<div class="widget widget-instagram">
                    <h3 class="title-widget" style="color:'.esc_attr($title_color).';">'.esc_attr($title).'</h3>
				    ' . $html . '
			    </div>';

        return $html;

    }

    add_shortcode('dezi_core_instagram', 'dezi_core_instagram');
