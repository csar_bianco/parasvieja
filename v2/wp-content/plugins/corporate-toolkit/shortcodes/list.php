<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_list_settings' );
function corporatepro_list_settings() {
    
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'list/default.jpg'
                ),
                'layout1'=>array(
                    'alt'=>'Layout 01',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'list/layout1.jpg'
                ),
                'layout2'=>array(
                    'alt'=>'Layout 02',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'list/layout2.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        array(
            "type"          => "param_group",
            "heading"       => __( "Items", "corporatepro" ),
            "admin_label"   => false,
            "param_name"    => "items",
            "params"        => array(
                array(
                    "type"        => "textfield",
                    "heading"     => __( "Text", "corporatepro" ),
                    "param_name"  => "text",
                    "admin_label" => false
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => __('Icon library', 'trueshop'),
                    'value'       => array(
                        __('Font Awesome', 'trueshop')     => 'fontawesome',
                    ),
                    'admin_label' => true,
                    'param_name'  => 'icon_lib',
                    'description' => __('Select icon library.', 'trueshop'),
                ),
                array(
                    'type'        => 'iconpicker',
                    'heading'     => __('Icon', 'trueshop'),
                    'param_name'  => 'icon_fontawesome',
                    'value'       => 'fa fa-adjust', // default value to backend editor admin_label
                    'settings'    => array(
                        'emptyIcon'    => false,
                        'iconsPerPage' => 4000,
                    ),
                    'dependency'  => array(
                        'element' => 'icon_lib',
                        'value'   => 'fontawesome',
                    ),
                    'description' => __('Select icon from library.', 'trueshop'),
                ),  
            )
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "trueshop"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'trueshop' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'trueshop' ),
        )
    );
    $map_settings = array(
        'name'     => esc_html__( 'Corporate: List', 'trueshop' ),
        'base'     => 'corporatepro_list', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        'description'   =>  __( 'Display a about box.', 'trueshop' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_list( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_list', $atts ) : $atts;

    $default_atts = array(
        'layout'   => 'default',
        'text'     => '',
        'icon_lib' => '',
        'css'      => '',
        'el_class' => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' '.$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/list/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_list', 'corporatepro_list' );
