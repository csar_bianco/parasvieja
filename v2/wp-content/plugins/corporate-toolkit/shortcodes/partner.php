<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_partner_settings' );
function corporatepro_partner_settings() {
    $socials = array();
    $all_socials = corporatepro_get_all_social();
    if( $all_socials ){
        foreach ($all_socials as $key =>  $social)
            $socials[$social['name']] = $key;
    }
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'corporatepro' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'partner/default.jpg'
                ),
                'layout1'=>array(
                    'alt'=>'Layout 01',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'partner/layout1.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'corporatepro' ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Partner name", "trueshop"),
            "param_name"  => "title",
            'std'         => 'Lamar Joint Stock Company',
            'admin_label' => true,
        ),
        array(
            "type"        => "attach_image",
            "heading"     => __( "Background Image: ", 'xshop-core' ),
            "param_name"  => "bg_partner",
            "admin_label" => true,
        ),
        array(
            "type"        => "attach_image",
            "heading"     => __( "Logo Image: ", 'xshop-core' ),
            "param_name"  => "logo_partner",
            "admin_label" => true,
        ),
        array(
            'type' => 'vc_link',
            'heading' => __( 'URL (Link)', 'corporatepro' ),
            'param_name' => 'link',
            'admin_label' => true,
            'description' => __( 'Add link.', 'corporatepro' ),
        ),

        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "corporatepro"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'corporatepro' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'corporatepro' ),
        )
    );
    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Partner', 'corporatepro' ),
        'base'     => 'corporatepro_partner', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'corporatepro' ),
        'description'   =>  __( 'Display a partner.', 'corporatepro' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_partner( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_partner', $atts ) : $atts;

    $default_atts = array(
        'layout'        => 'default',
        'link'          => '',
        'logo_partner'  => '',
        'bg_partner'    => '',
        'title'         => '',
        'css'           => '',
        'el_class'      => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' ' .$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/partner/temp',$layout, $template_args );
    $html = ob_get_clean();
   
    return balanceTags( $html );

}

add_shortcode( 'corporatepro_partner', 'corporatepro_partner' );
