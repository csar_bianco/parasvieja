<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_pie_chart_settings' );
function corporatepro_pie_chart_settings() {
   
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'pie_chart/default.jpg'
                ),
                'style2'=>array(
                    'alt'=>'Style 02',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'pie_chart/layout2.jpg'
                ),
                'style3'=>array(
                    'alt'=>'Style 03',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'pie_chart/layout3.jpg'
                ),
                'style4'=>array(
                    'alt'=>'Style 04',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'pie_chart/layout4.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'trueshop' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Percent', 'trueshop' ),
            'param_name'    =>  'percent',
            'description'   =>  __( 'The Percent of shortcode', 'trueshop' ),
            'admin_label'   =>  false,
            'std'           =>  '',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('default','style2'),
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Number', 'trueshop' ),
            'param_name'    =>  'number',
            'description'   =>  __( 'The number of shortcode', 'trueshop' ),
            'admin_label'   =>  false,
            'std'           =>  '',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style3','style4'),
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Icon library', 'trueshop'),
            'value'       => array(
                __('Font Awesome', 'trueshop')     => 'fontawesome',
            ),
            'admin_label' => true,
            'param_name'  => 'icon_lib',
            'description' => __('Select icon library.', 'trueshop'),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style2'),
            ),
        ),
        array(
            'type'        => 'iconpicker',
            'heading'     => __('Icon', 'trueshop'),
            'param_name'  => 'icon_fontawesome',
            'value'       => 'fa fa-adjust', // default value to backend editor admin_label
            'settings'    => array(
                'emptyIcon'    => false,
                'iconsPerPage' => 4000,
            ),
            'dependency'  => array(
                'element' => 'icon_lib',
                'value'   => 'fontawesome',
            ),
            'description' => __('Select icon from library.', 'trueshop'),
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Bar Color", 'koolshop' ),
            "param_name" => "barcolor",
            "value" => '#333333', //Default Red color
            "description" => __( "Choose color", 'koolshop' ),
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Track Color", 'koolshop' ),
            "param_name" => "trackcolor",
            "value" => '#e5e5e5', //Default Red color
            "description" => __( "Choose color", 'koolshop' ),
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Box Color", 'koolshop' ),
            "param_name" => "boxcolor",
            "value" => '#ff4949', //Default Red color
            "description" => __( "Choose color", 'koolshop' ),
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "trueshop"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'trueshop' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'trueshop' ),
        )
    );

    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Pie Chart', 'trueshop' ),
        'base'     => 'corporatepro_pie_chart', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        'description'   =>  __( 'Display a Pie Chart.', 'trueshop' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_pie_chart( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_pie_chart', $atts ) : $atts;

    $default_atts = array(
        'layout'     => 'default',
        'percent'    => '50',
        'title'      => '',
        'trackcolor' => '#e5e5e5',
        'barcolor'   => '#333333',
        'boxcolor'   => '#ff4949',
        'icon_lib'   =>'',
        'number'     => '',
        'css'        => '',
        'el_class'   => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' ' .$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $icon = $atts['icon_'.$icon_lib];

    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
        'icon'      => $icon
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/pie_chart/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_pie_chart', 'corporatepro_pie_chart' );
