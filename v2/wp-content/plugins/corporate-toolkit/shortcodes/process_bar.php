<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_process_bar_settings' );
function corporatepro_process_bar_settings() {
   
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                ),
                'style2'=>array(
                    'alt'=>'Style 02',
                ),
                'style3'=>array(
                    'alt'=>'Style 03',
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'trueshop' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
        array(
            'type'          =>  'textarea',
            'heading'       =>  __( 'Short description', 'kute-toolkit' ),
            'param_name'    =>  'short_description',
            'description'   =>  __( 'The description of shortcode', 'kute-toolkit' ),
            'admin_label'   =>  false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style2'),
            ),
        ),
        array(
            "type"       => "dropdown",
            "heading"    => __("Skill align", 'kute-toolkit'),
            "param_name" => "skill_align",
            "value"      => array(
                __( 'Left', 'kute-toolkit' )      => 'left',
                __( 'Right', 'kute-toolkit' )      => 'right',
            ),
            'std'         => 'left',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style3'),
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Percent', 'trueshop' ),
            'param_name'    =>  'percent',
            'description'   =>  __( 'The Percent of shortcode', 'trueshop' ),
            'admin_label'   =>  false,
            'std'           =>  '',
        ),
         array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Background Skill Bar", 'koolshop' ),
            "param_name" => "bgskillbar",
            "value" => '#1b1d1f', //Default Red color
            "description" => __( "Choose color", 'koolshop' ),
        ),
         array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Background Skill", 'koolshop' ),
            "param_name" => "bgskill",
            "value" => '#ff4949', //Default Red color
            "description" => __( "Choose color", 'koolshop' ),
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "trueshop"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'trueshop' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'trueshop' ),
        )
    );

    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Process bar', 'trueshop' ),
        'base'     => 'corporatepro_process_bar', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        'description'   =>  __( 'Display a process bar.', 'trueshop' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_process_bar( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_process_bar', $atts ) : $atts;

    $default_atts = array(
        'layout'            => 'default',
        'percent'           => '50',
        'title'             => '',
        'bgskillbar '       =>'',
        'bgskill '          => '',
        'short_description' =>'',
        'skill_align'       =>'left',
        'css'               => '',
        'el_class'          => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' cp-skillbar ' .$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/process_bar/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_process_bar', 'corporatepro_process_bar' );
