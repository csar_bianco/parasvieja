<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_products_settings' );
function corporatepro_products_settings() {
    // CUSTOM PRODUCT SIZE
    $product_size_width_list  = array();
    $width = 300;
    $height = 300;
    $crop = 1;
    if( function_exists( 'wc_get_image_size' ) ){
        $size = wc_get_image_size( 'shop_catalog' );
        $width   = isset( $size[ 'width' ] ) ? $size[ 'width' ] : $width;
        $height  = isset( $size[ 'height' ] ) ? $size[ 'height' ] : $height;
        $crop    = isset( $size[ 'crop' ] ) ? $size[ 'crop' ] : $crop;
    }
    for( $i = 100; $i < $width; $i= $i+10){
        array_push($product_size_width_list, $i);
    }
    $product_size_list = array();
    $product_size_list[$width.'x'.$height] = $width.'x'.$height;
    foreach( $product_size_width_list as $k => $w ){
        $w = intval( $w );
        if(isset($width) && $width > 0){
            $h = round( $height * $w / $width ) ;
        }else{
            $h = $w;
        }
        $product_size_list[$w.'x'.$h] = $w.'x'.$h;
    }
    $product_size_list['Custom'] = 'custom';

    $attributes_tax = wc_get_attribute_taxonomies();
    $attributes = array();
    foreach ( $attributes_tax as $attribute ) {
        $attributes[ $attribute->attribute_label ] = $attribute->attribute_name;
    }
    $params   =  array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'corporatepro' ),
            'value' => array(
                'default'=>array(
                    'alt'=> __( 'Tab Category', 'corporatepro' ),
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'products/default.jpg'
                ),
                'style1'=>array(
                    'alt'=> __( 'Grid products 01', 'corporatepro' ),
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'products/style1.jpg'
                ),
                'style5'=>array(
                    'alt'=> __( 'Grid products 02', 'corporatepro' ),
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'products/style5.jpg'
                ),
                'style2'=>array(
                    'alt'=> __( 'owl products 01', 'corporatepro' ),
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'products/style2.jpg'
                ),
                'style3'=>array(
                    'alt'=> __( 'owl products 02', 'corporatepro' ),
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'products/style3.jpg'
                ),
                'style4'=>array(
                    'alt'=> __( 'owl products 03', 'corporatepro' ),
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'products/style4.jpg'
                ),
                'style6'=>array(
                    'alt'=> __( 'owl products 04', 'corporatepro' ),
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'products/style6.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'corporatepro' ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'corporatepro' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'corporatepro' ),
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
        array(
            "type"       => "dropdown",
            "heading"    => __("Show Gallerys", 'corporatepro'),
            "param_name" => "show_gallery",
            "value"      => array(
                __('Yes', 'corporatepro')   => 'product-brand',
                __('No', 'corporatepro')    => 'no'
            ),
            'std'         => 'no',
            "dependency"  => array("element" => "layout", "value" => array( 'style5' )),
        ),
        array(
            "type"       => "dropdown",
            "heading"    => __("Products list style", 'corporatepro'),
            "param_name" => "productsliststyle",
            "value"      => array(
                __('Carousel', 'corporatepro')  => 'owl',
                __('grid', 'corporatepro') => 'grid'
            ),
            'std'         => 'grid',
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Product image thumb size', 'corporatepro' ),
            'param_name'  => 'product_image_size',
            'value'       => $product_size_list,
            'description' => __( 'Select a size for product', 'corporatepro' )
        ),
        array(
            "type"        => "kt_number",
            "heading"     => __("Width", 'corporatepro'),
            "param_name"  => "product_custom_thumb_width",
            "value"       => $width,
            "suffix"      => __("px", 'corporatepro'),
            "dependency"  => array("element" => "product_image_size", "value" => array( 'custom' )),
        ),
        array(
            "type"        => "kt_number",
            "heading"     => __("Height", 'corporatepro'),
            "param_name"  => "product_custom_thumb_height",
            "value"       => $height,
            "suffix"      => __("px", 'corporatepro'),
            "dependency"  => array("element" => "product_image_size", "value" => array( 'custom' )),
        ),
        /*Products */
        array(
            "type"          => "kt_taxonomy",
            "taxonomy"      => "product_cat",
            "class"         => "",
            "heading"       => __("Category", 'corporatepro'),
            "param_name"    => "taxonomy",
            "value"         => '',
            'parent'        => '',
            'multiple'      => true,
            'hide_empty'    => false,
            'placeholder'   => __('Choose category', 'corporatepro'),
            "description"   => __("Note: If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'corporatepro'),
            'std'           =>  '',
            'group'      => esc_html__( 'Products options', 'corporatepro' ),
        ),
        array(
            'type'        =>    'dropdown',
            'heading'     =>    __( 'Target', 'corporatepro' ),
            'param_name'  =>    'target',
            'value'       => array(
                __( 'Best Selling Products', 'corporatepro' ) => 'best-selling',
                __( 'Top Rated Products', 'corporatepro' )    => 'top-rated',
                __( 'Recent Products', 'corporatepro' )       => 'recent-product',
                __( 'Product Category', 'corporatepro' )      => 'product-category',
                __( 'Products', 'corporatepro' )              => 'products',
                __( 'Featured Products', 'corporatepro' )     => 'featured_products',
                __( 'On Sale', 'corporatepro' )               => 'on_sale',
            ),
            'description'   =>  __( 'Choose the target to filter products', 'corporatepro' ),
            'std'           =>  'recent-product',
            'group'      => esc_html__( 'Products options', 'corporatepro' ),
        ),
        array(
            'type'           => 'dropdown',
            'heading'        => __( 'Attribute', 'corporatepro' ),
            'param_name'     => 'attribute',
            'value'          => $attributes,
            'save_always'    => true,
            'description'    => __( 'List of product taxonomy attribute', 'corporatepro' ),
            "dependency"     => array("element" => "target", "value" => array( 'product_attribute' )),
            'group'      => esc_html__( 'Products options', 'corporatepro' ),
        ),
        array(
            "type"       => "dropdown",
            "heading"    => __("Order by", 'corporatepro'),
            "param_name" => "orderby",
            "value"      => array(
                '',
                __( 'Date', 'corporatepro' )      => 'date',
                __( 'ID', 'corporatepro' )        => 'ID',
                __( 'Author', 'corporatepro' )    => 'author',
                __( 'Title', 'corporatepro' )     => 'title',
                __( 'Modified', 'corporatepro' )  => 'modified',
                __( 'Random', 'corporatepro' )    => 'rand',
                __( 'Comment count', 'corporatepro' ) => 'comment_count',
                __( 'Menu order', 'corporatepro' )    => 'menu_order',
                __( 'Sale price', 'corporatepro' )    => '_sale_price',
            ),
            'std'         => 'date',
            "description" => __("Select how to sort retrieved posts.",'corporatepro'),
            "dependency"  => array("element" => "target", "value" => array( 'top-rated', 'recent-product', 'product-category', 'featured_products', 'on_sale', 'product_attribute')),
            'group'      => esc_html__( 'Products options', 'corporatepro' ),
        ),
        array(
            "type"       => "dropdown",
            "heading"    => __("Order", 'corporatepro'),
            "param_name" => "order",
            "value"      => array(
                __('ASC', 'corporatepro')  => 'ASC',
                __('DESC', 'corporatepro') => 'DESC'
            ),
            'std'         => 'DESC',
            "description" => __("Designates the ascending or descending order.",'corporatepro'),
            "dependency"  => array("element" => "target", "value" => array( 'top-rated', 'recent-product', 'product-category', 'featured_products', 'on_sale', 'product_attribute')),
            'group'      => esc_html__( 'Products options', 'corporatepro' ),
        ),
        array(
            'type'        => 'kt_number',
            'heading'     => __( 'Max item', 'corporatepro' ),
            'param_name'  => 'per_page',
            'value'       => 6,
            "dependency"  => array("element" => "target", "value" => array( 'best-selling', 'top-rated', 'recent-product', 'product-category', 'featured_products', 'product_attribute', 'on_sale')),
            'group'      => esc_html__( 'Products options', 'corporatepro' ),
        ),
        array(
            'type'               => 'autocomplete',
            'heading'            => __( 'Products', 'corporatepro' ),
            'param_name'         => 'ids',
            'settings'           => array(
                'multiple'       => true,
                'sortable'       => true,
                'unique_values'  => true,
            ),
            'save_always' => true,
            'description' => __( 'Enter List of Products', 'corporatepro' ),
            "dependency"  => array("element" => "target", "value" => array( 'products')),
            'group'      => esc_html__( 'Products options', 'corporatepro' ),
        ),
        /* OWL Settings */
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( '1 Row', 'corporatepro' )  => '1',
                __( '2 Rows', 'corporatepro' ) => '2',
                __( '3 Rows', 'corporatepro' ) => '3',
                __( '4 Rows', 'corporatepro' ) => '4',
                __( '5 Rows', 'corporatepro' ) => '5'
            ),
            'std'         => '1',
            'heading'     => __( 'The number of rows which are shown on block', 'corporatepro' ),
            'param_name'  => 'owl_number_row',
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('owl')
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Rows space', 'corporatepro' ),
            'param_name'  => 'owl_rows_space',
            'value'       => array(
                __( 'Default', 'corporatepro' ) => 'rows-space-0',
                __( '10px', 'corporatepro' )    => 'rows-space-10',
                __( '20px', 'corporatepro' )    => 'rows-space-20',
                __( '30px', 'corporatepro' )    => 'rows-space-30',
                __( '40px', 'corporatepro' )    => 'rows-space-40',
                __( '50px', 'corporatepro' )    => 'rows-space-50',
                __( '60px', 'corporatepro' )    => 'rows-space-60',
                __( '70px', 'corporatepro' )    => 'rows-space-70',
                __( '80px', 'corporatepro' )    => 'rows-space-80',
                __( '90px', 'corporatepro' )    => 'rows-space-90',
                __( '100px', 'corporatepro' )   => 'rows-space-100',
            ),
            'std'=>'rows-space-0',
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            "dependency"  => array("element" => "owl_number_row", "value" => array( '2','3','4','5' )),
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'Yes', 'corporatepro' ) => 'true',
                __( 'No', 'corporatepro' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => __( 'AutoPlay', 'corporatepro' ),
            'param_name'  => 'owl_autoplay',
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('owl')
            ),
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'No', 'corporatepro' )  => 'false',
                __( 'Yes', 'corporatepro' ) => 'true'
            ),
            'std'         => false,
            'heading'     => __( 'Navigation', 'corporatepro' ),
            'param_name'  => 'owl_navigation',
            'description' => __( "Show buton 'next' and 'prev' buttons.", 'corporatepro' ),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('owl')
            ),
            'admin_label' => false,
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'No', 'corporatepro' )  => 'false',
                __( 'Yes', 'corporatepro' ) => 'true'
            ),
            'std'         => false,
            'heading'     => __( 'Dots', 'corporatepro' ),
            'param_name'  => 'owl_dots',
            'description' => __( "Show dots.", 'corporatepro' ),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('owl')
            ),
            'admin_label' => false,
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'Yes', 'corporatepro' ) => 'true',
                __( 'No', 'corporatepro' )  => 'false'
            ),
            'std'         => false,
            'heading'     => __( 'Loop', 'corporatepro' ),
            'param_name'  => 'owl_loop',
            'description' => __( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'corporatepro' ),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('owl')
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Slide Speed", 'corporatepro'),
            "param_name"  => "owl_slidespeed",
            "value"       => "200",
            "suffix"      => __("milliseconds", 'corporatepro'),
            "description" => __('Slide speed in milliseconds', 'corporatepro'),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('owl')
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Margin", 'corporatepro'),
            "param_name"  => "owl_margin",
            "value"       => "0",
            "description" => __('Distance( or space) between 2 item', 'corporatepro'),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('owl')
            ),
        ),

        array(
            "type"        => "textfield",
            "heading"     => __("The items on desktop (Screen resolution of device >= 1200px )", 'corporatepro'),
            "param_name"  => "owl_lg_items",
            "value"       => "4",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('owl')
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on desktop (Screen resolution of device >= 992px < 1200px )", 'corporatepro'),
            "param_name"  => "owl_md_items",
            "value"       => "3",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('owl')
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on tablet (Screen resolution of device >=768px and < 992px )", 'corporatepro'),
            "param_name"  => "owl_sm_items",
            "value"       => "3",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('owl')
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on mobile landscape(Screen resolution of device >=480px and < 768px)", 'corporatepro'),
            "param_name"  => "owl_xs_items",
            "value"       => "2",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('owl')
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on mobile (Screen resolution of device < 480px)", 'corporatepro'),
            "param_name"  => "owl_ts_items",
            "value"       => "1",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('owl')
            ),
        ),
        /* Bostrap setting */
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Rows space', 'corporatepro' ),
            'param_name'  => 'boostrap_rows_space',
            'value'       => array(
                __( 'Default', 'corporatepro' ) => 'rows-space-0',
                __( '10px', 'corporatepro' )    => 'rows-space-10',
                __( '20px', 'corporatepro' )    => 'rows-space-20',
                __( '30px', 'corporatepro' )    => 'rows-space-30',
                __( '40px', 'corporatepro' )    => 'rows-space-40',
                __( '50px', 'corporatepro' )    => 'rows-space-50',
                __( '60px', 'corporatepro' )    => 'rows-space-60',
                __( '70px', 'corporatepro' )    => 'rows-space-70',
                __( '80px', 'corporatepro' )    => 'rows-space-80',
                __( '90px', 'corporatepro' )    => 'rows-space-90',
                __( '100px', 'corporatepro' )   => 'rows-space-100',
            ),
            'std'=>'rows-space-0',
            'group'         =>  __( 'Boostrap settings', 'corporatepro' ),
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('grid')
            ),
        ),

        array(
            'type'          =>  'dropdown',
            'heading'       =>  __( 'Items per row on Desktop', 'corporatepro' ),
            'param_name'    =>  'boostrap_lg_items',
            'value'         =>  array(
                __( '1 item', 'corporatepro' ) => '12',
                __( '2 items', 'corporatepro' ) => '6',
                __( '3 items', 'corporatepro' ) => '4',
                __( '4 items', 'corporatepro' ) => '3',
                __( '5 items', 'corporatepro' ) => '15',
                __( '6 items', 'corporatepro' ) => '2',
            ),
            'description'   =>  __( '(Item per row on screen resolution of device >= 1200px )', 'corporatepro' ),
            'group'         =>  __( 'Boostrap settings', 'corporatepro' ),
            'std'           =>  '15',
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('grid')
            ),
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  __( 'Items per row on landscape tablet', 'corporatepro' ),
            'param_name'    =>  'boostrap_md_items',
            'value'         =>  array(
                __( '1 item', 'corporatepro' )  => '12',
                __( '2 items', 'corporatepro' ) => '6',
                __( '3 items', 'corporatepro' ) => '4',
                __( '4 items', 'corporatepro' ) => '3',
                __( '5 items', 'corporatepro' )  => '15',
                __( '6 items', 'corporatepro' )  => '2',
            ),
            'description'   =>  __( '(Item per row on screen resolution of device >=992px and < 1200px )', 'corporatepro' ),
            'group'         =>  __( 'Boostrap settings', 'corporatepro' ),
            'std'           =>  '3',
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('grid')
            ),
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  __( 'Items per row on portrait tablet', 'corporatepro' ),
            'param_name'    =>  'boostrap_sm_items',
            'value'         =>  array(
                __( '1 item', 'corporatepro' ) => '12',
                __( '2 items', 'corporatepro' ) => '6',
                __( '3 items', 'corporatepro' ) => '4',
                __( '4 items', 'corporatepro' ) => '3',
                __( '5 items', 'corporatepro' ) => '15',
                __( '6 items', 'corporatepro' ) => '2',
            ),
            'description'   =>  __( '(Item per row on screen resolution of device >=768px and < 992px )', 'corporatepro' ),
            'group'         =>  __( 'Boostrap settings', 'corporatepro' ),
            'std'           =>  '4',
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('grid')
            ),
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  __( 'Items per row on Mobile', 'corporatepro' ),
            'param_name'    =>  'boostrap_xs_items',
            'value'         =>  array(
                __( '1 item', 'corporatepro' ) => '12',
                __( '2 items', 'corporatepro' ) => '6',
                __( '3 items', 'corporatepro' ) => '4',
                __( '4 items', 'corporatepro' ) => '3',
                __( '5 items', 'corporatepro' ) => '15',
                __( '6 items', 'corporatepro' ) => '2',
            ),
            'description'   =>  __( '(Item per row on screen resolution of device >=480  add < 768px )', 'corporatepro' ),
            'group'         =>  __( 'Boostrap settings', 'corporatepro' ),
            'std'           =>  '6',
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('grid')
            ),
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  __( 'Items per row on Mobile', 'corporatepro' ),
            'param_name'    =>  'boostrap_ts_items',
            'value'         =>  array(
                __( '1 item', 'corporatepro' ) => '12',
                __( '2 items', 'corporatepro' ) => '6',
                __( '3 items', 'corporatepro' ) => '4',
                __( '4 items', 'corporatepro' ) => '3',
                __( '5 items', 'corporatepro' ) => '15',
                __( '6 items', 'corporatepro' ) => '2',
            ),
            'description'   =>  __( '(Item per row on screen resolution of device < 480px)', 'corporatepro' ),
            'group'         =>  __( 'Boostrap settings', 'corporatepro' ),
            'std'           =>  '12',
            "dependency"  => array(
                "element" => "productsliststyle", "value" => array('grid')
            ),
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "corporatepro"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'corporatepro' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'corporatepro' ),
        ),

    );


    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Products', 'corporatepro' ),
        'base'     => 'corporatepro_products', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'corporatepro' ),
        'description'   =>  __( 'Display a product list.', 'corporatepro' ),
        'params' => $params

    );


    vc_map( $map_settings );
}

function corporatepro_products( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_products', $atts ) : $atts;
    // $atts = apply_filters( 'corporatepro_atts_{shortcode_name}', $atts );
    $atts = apply_filters( 'corporatepro_atts_corporatepro_woocommerce_products', $atts );

    $default_atts = array(
        'layout'                      => 'default',
        'product_image_size'          => '',
        'product_custom_thumb_width'  => '',
        'product_custom_thumb_height' => '',
        'productsliststyle'           => 'grid',
        'product_style'               => 1,
        'owl_number_row'              => 1,
        'owl_rows_space'              => 'rows-space-0',
        'owl_autoplay'                => false,
        'owl_navigation'              => false,
        'owl_dots'                    => false,
        'owl_loop'                    => false,
        'owl_slidespeed'              => 200,
        'owl_margin'                  => 0,
        'owl_lg_items'                => 4,
        'owl_md_items'                => 3,
        'owl_sm_items'                => 3,
        'owl_xs_items'                => 2,
        'owl_ts_items'                => 1,
        'boostrap_rows_space'         => 'rows-space-0',
        'boostrap_lg_items'           => 15,
        'boostrap_md_items'           => 3,
        'boostrap_sm_items'           => 4,
        'boostrap_xs_items'           => 6,
        'boostrap_ts_items'           => 12,
        'taxonomy'                    => '',
        'target'                      => 'recent-product',
        'attribute'                   => '',
        'orderby'                     => 'date',
        'order'                       => 'DESC',
        'per_page'                    => 6,
        'ids'                         => '',
        'title'                       => '',
        'subtitle'                    => '',
        'show_gallery'                => 'no',
        'tab_effect'                  => '',
        'product_more_link'           => '',
        'product_more_link_text'      => '',
        'css'                         => '',
        'el_class'                    => ''
    );

    // $default_atts = apply_filters( 'corporatepro_default_atts_{shortcode_name}', $default_atts );
    $default_atts = apply_filters( 'corporatepro_default_atts_corporatepro_woocommerce_products', $default_atts );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' cp-woocommerce-products '.$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    /* Product Size */
    if ( $product_image_size ){
        if( $product_image_size == 'custom'){
            $thumb_width = $product_custom_thumb_width;
            $thumb_height = $product_custom_thumb_height;
        }else{
            $product_image_size = explode("x",$product_image_size);
            $thumb_width = $product_image_size[0];
            $thumb_height = $product_image_size[1];
        }
        if($thumb_width > 0){
            add_filter( 'corporatepro_shop_pruduct_thumb_width', create_function('','return '.$thumb_width.';') );
        }
        if($thumb_height > 0){
            add_filter( 'corporatepro_shop_pruduct_thumb_height', create_function('','return '.$thumb_height.';') );
        }
    }

    $products = corporatepro_getProducts($atts);
    $product_item_class = array('product-item product');
    $product_list_class = array();
    $owl_settings = '';
    if( $productsliststyle  == 'grid' ){
        $product_list_class[] = 'product-list-grid row auto-clear';
        $product_list_class[] = 'product-list-grid-with-product-style-'.$product_style;

        $product_item_class[] = $boostrap_rows_space;
        $product_item_class[] = 'col-lg-'.$boostrap_lg_items;
        $product_item_class[] = 'col-md-'.$boostrap_md_items;
        $product_item_class[] = 'col-sm-'.$boostrap_sm_items;
        $product_item_class[] = 'col-xs-'.$boostrap_xs_items;
        $product_item_class[] = 'col-ts-'.$boostrap_ts_items;
    }
    if( $productsliststyle  == 'owl' ){
        $product_list_class[] = 'product-list-owl owl-carousel';
        $product_list_class[] = 'product-list-owl-with-product-style-'.$product_style;

        $product_item_class[] = $owl_rows_space;

        $owl_settings = corporatepro_generate_carousel_data_attributes('owl_', $atts);
    }

    $template_args = array(
        'atts'              => $atts,
        'css_class'         => $css_class,
        'products'          => $products,
        'owl_carousel'      => $owl_settings,
        'grid_bootstrap'    => $product_item_class,
        'thumb_width'       => $thumb_width,
        'thumb_height'      => $thumb_height,
    );
    ob_start();
    corporatepro_get_template_part('shortcodes/products/temp',$layout, $template_args );

    $html = ob_get_clean();
    wp_reset_postdata();
    return balanceTags( $html );

}

add_shortcode( 'corporatepro_products', 'corporatepro_products' );
