<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_sectiontitle_settings' );
function corporatepro_sectiontitle_settings() {
    $socials = array();
    $all_socials = corporatepro_get_all_social();
    if( $all_socials ){
        foreach ($all_socials as $key =>  $social)
            $socials[$social['name']] = $key;
    }
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'sectiontitle/default.jpg'
                ),
                'layout2'=>array(
                    'alt'=>'Style 02',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'sectiontitle/layout2.jpg'
                ),
                'layout3'=>array(
                    'alt'=>'Style 03',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'sectiontitle/layout3.jpg'
                ),
                'layout4'=>array(
                    'alt'=>'Style 04',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'sectiontitle/layout4.jpg'
                ),
                'layout5'=>array(
                    'alt'=>'Style 05',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'sectiontitle/layout5.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        array(
            "type"        => "attach_image",
            "heading"     => __( "Image: ", "corporatepro" ),
            "param_name"  => "bg_title",
            "admin_label" => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout4'),
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'trueshop' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
        array(
            'type' => 'vc_link',
            'heading' => __( 'URL (Link)', 'kute-toolkit' ),
            'param_name' => 'link',
            'description' => __( 'Add link.', 'kute-toolkit' ),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout4'),
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Style title:', 'trueshop' ),
            'param_name'  => 'style_title',
            'value'       => array(
                'Style 01'   =>'title-style7',
                'Style 02'   =>'title-style9'
            ),
            'admin_label'   =>  true,
            'std'           =>  'title-style7',
        ),
        array(
            'type'          =>  'textarea',
            'heading'       =>  __( 'Description', 'trueshop' ),
            'param_name'    =>  'description',
            'description'   =>  __( 'The description of shortcode', 'trueshop' ),
            'std'           =>  '',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout3','layout4'),
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Number Slide', 'trueshop' ),
            'param_name'    =>  'number',
            'description'   =>  __( 'Ex: 01', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  '',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout4'),
            ),
        ),
        array(
            "type"       => "checkbox",
            "heading"    => __("hr title", 'kute-toolkit'),
            "param_name" => "hr_title",
            'description'   =>  __( 'border below title.', 'trueshop' ),
            "value"      => array(
                __( 'Yes', 'kute-toolkit' )      => 'hr',
            ),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout4'),
            ),
        ),
        array(
            "type"       => "checkbox",
            "heading"    => __("Light Title", 'kute-toolkit'),
            "param_name" => "light_bg",
            "value"      => array(
                __( 'Light', 'kute-toolkit' )      => 'light',
            ),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout3'),
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Text align', 'trueshop' ),
            'param_name'  => 'text_align',
            'value'       => array(
                'Left'   =>'text-left',
                'Center' =>'text-center',
                'Right'  =>'text-right'
            ),
            'admin_label'   =>  true,
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Text Color", 'koolshop' ),
            "param_name" => "textcolor",
            "value" => '#000000', //Default Red color
            "description" => __( "Choose color", 'koolshop' ),
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "trueshop"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'trueshop' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'trueshop' ),
        )
    );
    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Section Title', 'trueshop' ),
        'base'     => 'corporatepro_sectiontitle', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        'description'   =>  __( 'Display a about box.', 'trueshop' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_sectiontitle( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_sectiontitle', $atts ) : $atts;

    $default_atts = array(
        'layout'        => 'default',
        'title'         => '',
        'description'   => '',
        'light_bg'      => '',
        'style_title'   => '',
        'link'          => '',
        'number'        => '',
        'bg_title'      => '',
        'hr_title'      => '',
        'text_align'    => 'text-center',
        'textcolor'     => '#000000',
        'css'           => '',
        'el_class'      => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' title-section '.$layout.' '.$text_align;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $args_text = array(
        'a' => array(
            'href' => array(),
            'title' => array()
        ),
        'br' => array(),
        'em' => array(),
        'strong' => array(),
    );

    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
        'args_text' => $args_text,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/sectiontitle/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_sectiontitle', 'corporatepro_sectiontitle' );
