<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}
        
add_action( 'vc_before_init', 'corporatepro_service_settings' );
function corporatepro_service_settings() {
   
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'corporatepro' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'service/default.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'corporatepro' ),
        ),
        array(
            "type"          => "param_group",
            "heading"       => __( "Items", "corporatepro" ),
            "admin_label"   => false,
            "param_name"    => "items",
            "params"        => array(
                array(
                    "type"        => "textfield",
                    "heading"     => __( "Title", "corporatepro" ),
                    "param_name"  => "title",
                    "admin_label" => false
                ), 
                array(
                    "type"        => "textarea",
                    "heading"     => __( "Description", "corporatepro" ),
                    "param_name"  => "description",
                    "admin_label" => false
                ),
                array(
                    "type"        => "attach_image",
                    "heading"     => __( "Image", "corporatepro" ),
                    "param_name"  => "img",
                    "admin_label" => false
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => __('Icon library', 'trueshop'),
                    'value'       => array(
                        __('Font Awesome', 'trueshop')     => 'fontawesome',
                    ),
                    'admin_label' => true,
                    'param_name'  => 'icon_lib',
                    'description' => __('Select icon library.', 'trueshop'),
                ),
                array(
                    'type'        => 'iconpicker',
                    'heading'     => __('Icon', 'trueshop'),
                    'param_name'  => 'icon_fontawesome',
                    'value'       => 'fa fa-adjust', // default value to backend editor admin_label
                    'settings'    => array(
                        'emptyIcon'    => false,
                        'iconsPerPage' => 4000,
                    ),
                    'dependency'  => array(
                        'element' => 'icon_lib',
                        'value'   => 'fontawesome',
                    ),
                    'description' => __('Select icon from library.', 'trueshop'),
                ),
                array(
                    'type' => 'vc_link',
                    'heading' => __( 'URL (Link)', 'kute-toolkit' ),
                    'param_name' => 'link',
                    'description' => __( 'Add link.', 'kute-toolkit' ),
                ),
            )
        ),     

        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "corporatepro"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'corporatepro' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'corporatepro' ),
        )
    );

    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Service', 'corporatepro' ),
        'base'     => 'corporatepro_service', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'corporatepro' ),
        'description'   =>  __( 'Display a Service.', 'corporatepro' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_service( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_service', $atts ) : $atts;

    $default_atts = array(
        'layout'      => 'default',
        'items'       => '',
        'icon_lib'    => '',
        'status'      => '',
        'css'         => '',
        'el_class'    => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' ' .$layout.' '.$status;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/service/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_service', 'corporatepro_service' );
