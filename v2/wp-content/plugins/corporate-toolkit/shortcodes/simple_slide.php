<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

add_action( 'vc_before_init', 'corporatepro_slide_settings' );
function corporatepro_slide_settings() {
    /* CATEGORY DRROPDOW */

    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'corporatepro' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'simple_slide/default.jpg'
                ),
                'layout1'=>array(
                    'alt'=>'Layout 01',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'simple_slide/layout1.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'corporatepro' ),
        ),
        array(
            "type"          => "param_group",
            "heading"       => __( "Items Slide", "corporatepro" ),
            "admin_label"   => false,
            "param_name"    => "items",
            "params"        => array(
                array(
                    "type"        => "attach_image",
                    "heading"     => __( "Image", "corporatepro" ),
                    "param_name"  => "bg_img",
                    "admin_label" => true,
                ),
                array(
                    'type' => 'vc_link',
                    'heading' => __( 'URL (Link)', 'kute-toolkit' ),
                    'param_name' => 'link',
                    'description' => __( 'Add link.', 'kute-toolkit' ),
                ),
            )
        ),

        /* OWL Settings */
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'Yes', 'corporatepro' ) => 'true',
                __( 'No', 'corporatepro' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => __( 'AutoPlay', 'corporatepro' ),
            'param_name'  => 'owl_autoplay',
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'No', 'corporatepro' )  => 'false',
                __( 'Yes', 'corporatepro' ) => 'true'
            ),
            'std'         => false,
            'heading'     => __( 'Navigation', 'corporatepro' ),
            'param_name'  => 'owl_navigation',
            'description' => __( "Show buton 'next' and 'prev' buttons.", 'corporatepro' ),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'No', 'corporatepro' )  => 'false',
                __( 'Yes', 'corporatepro' ) => 'true'
            ),
            'std'         => false,
            'heading'     => __( 'Dots', 'corporatepro' ),
            'param_name'  => 'owl_dots',
            'description' => __( "Show dots.", 'corporatepro' ),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'Yes', 'corporatepro' ) => 'true',
                __( 'No', 'corporatepro' )  => 'false'
            ),
            'std'         => false,
            'heading'     => __( 'Loop', 'corporatepro' ),
            'param_name'  => 'owl_loop',
            'description' => __( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'corporatepro' ),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Slide Speed", 'corporatepro'),
            "param_name"  => "owl_slidespeed",
            "value"       => "200",
            "suffix"      => __("milliseconds", 'corporatepro'),
            "description" => __('Slide speed in milliseconds', 'corporatepro'),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Margin", 'corporatepro'),
            "param_name"  => "owl_margin",
            "value"       => "0",
            "description" => __('Distance( or space) between 2 item', 'corporatepro'),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),

        array(
            "type"        => "textfield",
            "heading"     => __("The items on desktop (Screen resolution of device >= 1200px )", 'corporatepro'),
            "param_name"  => "owl_lg_items",
            "value"       => "4",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on desktop (Screen resolution of device >= 992px < 1200px )", 'corporatepro'),
            "param_name"  => "owl_md_items",
            "value"       => "3",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on tablet (Screen resolution of device >=768px and < 992px )", 'corporatepro'),
            "param_name"  => "owl_sm_items",
            "value"       => "3",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on mobile landscape(Screen resolution of device >=480px and < 768px)", 'corporatepro'),
            "param_name"  => "owl_xs_items",
            "value"       => "2",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on mobile (Screen resolution of device < 480px)", 'corporatepro'),
            "param_name"  => "owl_ts_items",
            "value"       => "1",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
        ),

        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "corporatepro"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'corporatepro' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'corporatepro' ),
        )
    );
    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Simple slide', 'corporatepro' ),
        'base'     => 'corporatepro_slide', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'corporatepro' ),
        'description'   =>  __( 'Display a list img.', 'corporatepro' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_slide( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_slide', $atts ) : $atts;

    $default_atts = array(
        'layout'            => '',
        'items'             => '',
        'owl_autoplay'      => false,
        'owl_navigation'    => false,
        'owl_dots'          => false,
        'owl_loop'          => false,
        'owl_slidespeed'    => 200,
        'owl_margin'        => 20,
        'owl_lg_items'      => 4,
        'owl_md_items'      => 3,
        'owl_sm_items'      => 3,
        'owl_xs_items'      => 2,
        'owl_ts_items'      => 1,
        'css'               => '',
        'el_class'          => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' '.$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $owl_settings = corporatepro_generate_carousel_data_attributes('owl_', $atts);

    $template_args = array(
        'atts'              => $atts,
        'css_class'         => $css_class,
        'owl_carousel'      => $owl_settings,
    );

    ob_start();

    corporatepro_get_template_part('shortcodes/simple_slide/temp',$layout, $template_args );

    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_slide', 'corporatepro_slide' );
