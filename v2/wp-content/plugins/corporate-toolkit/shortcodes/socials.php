<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_socials_settings' );
function corporatepro_socials_settings() {
    $socials = array();
    $all_socials = corporatepro_get_all_social();
    if( $all_socials ){
        foreach ($all_socials as $key =>  $social)
            $socials[$social['name']] = $key;
    }
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'socials/default.jpg'
                ),
                'style1'=>array(
                    'alt'=>'Style 01',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'socials/style1.jpg'
                ),
                'style2'=>array(
                    'alt'=>'Style 02',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'socials/style2.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'trueshop' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
        array(
            'type'        => 'checkbox',
            'heading'     => __( 'No text', 'trueshop' ),
            'param_name'  => 'no_text',
            "description" => __("only show icon social.", "trueshop"),
            'value'       => array(
                __('Yes', 'trueshop')    => 'hiden-text-social',
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Text align', 'trueshop'),
            'value'       => array(
                __('center', 'trueshop')    => 'center',
                __('left', 'trueshop')      => 'left',
                __('right', 'trueshop')     => 'right',
            ),
            'admin_label' => true,
            'param_name'  => 'text_align',
            'std'         => '',
        ),
        array(
            'type'        => 'checkbox',
            'heading'     => __( 'Display on', 'trueshop' ),
            'param_name'  => 'use_socials',
            'class'         => 'checkbox-display-block',
            'value'       => $socials,
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "trueshop"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'trueshop' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'trueshop' ),
        )
    );
    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Socials', 'trueshop' ),
        'base'     => 'corporatepro_socials', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        'description'   =>  __( 'Display a social list.', 'trueshop' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_socials( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_socials', $atts ) : $atts;

    $default_atts = array(
        'layout'      => 'default',
        'use_socials' => '',
        'text_align'  => '',
        'title'       => '',
        'no_text'     => '',
        'css'         => '',
        'el_class'    => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' socials '.$layout.' '.$no_text;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    $socials = explode(',', $use_socials);
    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
        'socials'   => $socials
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/socials/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_socials', 'corporatepro_socials' );
