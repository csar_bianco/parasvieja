<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_video_settings' );
function corporatepro_video_settings() {
   
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Popup Video',
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Video link', 'trueshop' ),
            'param_name'    =>  'videolink',
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
        array(
            "type"        => "attach_image",
            "heading"     => __( "Video background", "corporatepro" ),
            "param_name"  => "videobackground",
            "admin_label" => false
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "trueshop"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'trueshop' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'trueshop' ),
        )
    );

    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Video', 'trueshop' ),
        'base'     => 'corporatepro_video', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        'description'   =>  __( 'Display a popup video.', 'trueshop' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_video( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_video', $atts ) : $atts;

    $default_atts = array(
        'layout'          => 'default',
        'videolink'       =>'',
        'videobackground' => '',
        'css'             => '',
        'el_class'        => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.'  ' .$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/video/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_video', 'corporatepro_video' );
