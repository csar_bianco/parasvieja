<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

add_action( 'vc_before_init', 'corporatepro_whatwedo_settings' );
function corporatepro_whatwedo_settings() {
    $params = array(
        'layout' => array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        'title' => array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'trueshop' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
        'el_class' => array(
            "type" => "textfield",
            "heading" => __("Extra class name", "trueshop"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        'css' => array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'trueshop' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'trueshop' ),
        ),
    );

    $map_settings = array(
        'name'                    => esc_html__('Corporate: What we do', 'trueshop'),
        'base'                    => 'corporatepro_whatwedo', // shortcode
        'class'                   => '',
        'category'                => esc_html__('Corporate', 'trueshop'),
        "content_element"         => true,
        "show_settings_on_create" => false,
        "is_container"            => true,
        "js_view"                 => 'VcColumnView',
        'description'             =>  __( 'Display a what we do block.', 'trueshop' ),
        "as_parent" => array('only' => 'corporatepro_whatwedo_item,corporatepro_whatwedo_item_space'),
        'params'                  => $params
    );

    // $map_settings = apply_filters( 'trueshop_vc_map_{shortcode_name}', $map_settings );
    $map_settings = apply_filters( 'trueshop_vc_map_trueshop_container', $map_settings );

    vc_map( $map_settings );
}

if ( class_exists( 'WPBakeryShortCodesContainer' )  && ! class_exists('WPBakeryShortCode_corporatepro_whatwedo')) {
    class WPBakeryShortCode_corporatepro_whatwedo extends WPBakeryShortCodesContainer {

        protected function content($atts, $content = null) {

            $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_whatwedo', $atts ) : $atts;

            $default_atts = array(
                'layout'   => '',
                'title'    => '',
                'css'      => '',
                'el_class' => ''
            );
            extract( shortcode_atts( $default_atts, $atts ) );

            $css_class = 'ss-we-do '.$el_class.' '.$layout;
            if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
                $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
            endif;
            // Get all url link
            $corporatepro_whatwedo_item = corporatepro_get_all_attributes('corporatepro_whatwedo_item', $content);
            $template_args = array(
                'atts'      => $atts,
                'css_class' => $css_class,
                'content'  => $content,
                'corporatepro_whatwedo_item' => $corporatepro_whatwedo_item

            );
            ob_start();
            corporatepro_get_template_part('shortcodes/whatwedo/temp',$layout, $template_args );
            $html = ob_get_clean();

            return balanceTags( $html );
        }
    }
}

/* Item */
add_action( 'vc_before_init', 'corporatepro_whatwedo_item_settings' );
function corporatepro_whatwedo_item_settings() {
    $params  = array(
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'trueshop' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Link', 'trueshop' ),
            'param_name'    =>  'link',
            'description'   =>  __( 'The link of title', 'trueshop' ),
            'admin_label'   =>  false,
            'std'           =>  '',
        ),
        array(
            'type'          =>  'textarea',
            'heading'       =>  __( 'Short description', 'kute-toolkit' ),
            'param_name'    =>  'short_description',
            'description'   =>  __( 'The description of shortcode', 'kute-toolkit' ),
            'admin_label'   =>  false,
        ),
    );
    $map_settings = array(
        'name'     => esc_html__( 'Item', 'trueshop' ),
        'base'     => 'corporatepro_whatwedo_item', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        "as_child"                =>    array( 'only' => 'corporatepro_whatwedo' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_whatwedo_item( $atts ) {

}

add_shortcode( 'corporatepro_whatwedo_item', 'corporatepro_whatwedo_item' );


