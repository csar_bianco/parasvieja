<?php
if( isset($atts) ){
	extract ( $atts );
}
if( $images ){
	$images = explode(',', $images);
}
if( !empty($link) ){
	$link = vc_build_link( $link );
}else{
	$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
}
?>
<div class="<?php echo esc_attr( $css_class );?> ss-ourvision">
	<?php if( $images && is_array($images) && count($images) >0 ):?>
		<div class="ourvision-slide owl-carousel dot-style1" data-dots="true" data-items="1" data-nav="false">
			<?php foreach( $images as $image):?>
				<div class="item-vision">
					<figure><?php echo wp_get_attachment_image($image,'full');?></figure>
				</div>
			<?php endforeach;?>	
		</div>
	<?php endif;?>	
	<div class="ourvision-text">
		<?php if( $title ):?>
		<div class="title-section light title-style2">
			<span class="hr"></span>
			<h2 class="title"><?php echo esc_html( $title );?></h2>
		</div>
		<?php endif;?>
		<?php if( $short_description ):?>
		<div class="text"><?php echo esc_html( $short_description );?></div>
		<?php endif;?>
		<?php if($link['url']): ?>
            <a class="cp-button outline-button button-medium" href="<?php echo esc_url($link['url']) ?>" <?php if($link['target']): ?> target="<?php echo esc_html($link['target']) ?>" <?php endif; ?>  <?php if($link['rel']): ?> rel="<?php echo esc_attr($link['rel']) ; ?>" <?php endif; ?>><?php echo esc_html($link['title']); ?></a>
        <?php endif; ?>
	</div>
</div>