<?php
if( isset($atts) ){
	extract ( $atts );
}
$items = vc_param_group_parse_atts( $items );
?>
<div class="cp-acordion <?php echo esc_attr($style_accordion) ?> <?php echo esc_attr( $css_class );?>" data-tab-active="<?php echo esc_attr($active_section) ?>">
	<?php foreach( $items as $item):?>
		<?php  
			if( $item['link'] ){
				$link = vc_build_link( $item['link'] );
			}else{
				$link = array('url'=>' ', 'title'=>'', 'target'=>'_self', 'rel'=>'') ;
			}
		?>
		<?php if ( !empty($item['title']) ) : ?>
			<h3><?php echo esc_html($item['title']) ?></h3>
		<?php endif; ?>
		<?php if ( !empty($item['description']) ) : ?>
			<div class="acordion-content">
				<p><?php echo esc_html($item['description']) ?></p>
				<?php if ( $item['link'] ) : ?>
					<a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>" class="cp-button">
						<?php echo esc_html($link['title']); ?>
					</a>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	<?php endforeach;?>	
</div>