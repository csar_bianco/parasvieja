<?php
if( isset($atts) ){
	extract ( $atts );
}
$items = vc_param_group_parse_atts( $items );
?>

<div class="cp-service-slide <?php echo esc_attr( $css_class );?>">
	<?php foreach( $items as $item):?>
		<?php  
			if( $item['link'] ){
				$link = vc_build_link( $item['link'] );
			}else{
				$link = array('url'=>'#', 'title'=>'', 'target'=>'_self', 'rel'=>'') ;
			}
		?>
		<div class="item-service">
			<div class="service-base">
				<?php if ( !empty($item['icon_fontawesome']) ) : ?>
					<div class="icon-service"><i class="<?php echo esc_html($item['icon_fontawesome']) ?>"></i></div>
				<?php endif; ?>
				<?php if ( !empty($item['title']) ) : ?>
					<h4 class="title-service"><?php echo esc_html($item['title']) ?></h4>
				<?php endif; ?>
			</div>
			<div class="service-full">
				<?php if ( !empty($item['icon_fontawesome']) ) : ?>
					<div class="icon-service"><i class="<?php echo esc_html($item['icon_fontawesome']) ?>"></i></div>
				<?php endif; ?>
				<div class="content-service">
					<?php if ( !empty($item['title']) ) : ?>
						<h4 class="title-service"><?php echo esc_html($item['title']) ?></h4>
					<?php endif; ?>
					<?php if ( !empty($item['description']) ) : ?>
						<div class="desc-service"><?php echo esc_html($item['description']) ?></div>
					<?php endif; ?>
					<?php if ( $item['link'] ) : ?>
						<a href="<?php echo esc_url($link['url']) ?>" <?php if( $link['target'] ):?>target="<?php echo esc_html($link['target']) ?>" <?php endif;?> class="bt-learnmore bt-link">
							<?php echo esc_html__('Learn more ','corporatepro') ?>
							<i class="fa fa-caret-right"></i>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php endforeach;?>	
</div>