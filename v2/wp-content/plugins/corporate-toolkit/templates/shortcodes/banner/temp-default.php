<?php
if( isset($atts) ){
	extract ( $atts );
}
$items = vc_param_group_parse_atts( $items );
?>

<div class="ss-statistics <?php echo esc_attr( $css_class );?>">

	<?php 
		$args = array();

		foreach( $items as $i => $item) {
			$args[] = $item;
		}
	?>

		<div class="row-statistics odd">
			<div class="container">
				<div class="col-md-5 col-sm-6">
					<div class="statistic-box light">
						<div class="statistic-content">
							<span class="number"><?php echo esc_html($args[0]['title']) ?></span>
							<div class="desc"><?php echo esc_html($args[0]['description']) ?></div>
						</div>
					</div>
				</div>
				<div class="col-md-5 col-md-offset-2 col-sm-6">
					<div class="statistic-box">
						<div class="statistic-content">
							<span class="number"><?php echo esc_html($args[1]['title']) ?></span>
							<div class="desc"><?php echo esc_html($args[1]['description']) ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row-statistics even">
			<div class="container">
				<div class="col-sm-6 col-md-5">
					<div class="statistic-box">
						<div class="statistic-content">
							<span class="number"><?php echo esc_html($args[2]['title']) ?></span>
							<div class="desc"><?php echo esc_html($args[2]['description']) ?></div>
						</div>
					</div>
				</div>
				<div class="col-md-5 col-md-offset-2 col-sm-6">
					<div class="statistic-box light">
						<div class="statistic-content">
							<span class="number"><?php echo esc_html($args[3]['title']) ?></span>
							<div class="desc"><?php echo esc_html($args[3]['description']) ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<?php if ($icon_fontawesome != '') : ?>
		<div class="icon-statistics"><i class="<?php echo esc_attr($icon_fontawesome) ?>"></i></div>
	<?php endif; ?>
</div>
