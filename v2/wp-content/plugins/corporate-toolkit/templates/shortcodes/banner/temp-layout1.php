<?php
if( isset($atts) ){
	extract ( $atts );
}
if( $link ){
	$link = vc_build_link( $link );
}else{
	$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
}
?>

<div class="ads-box ads-style2 <?php echo esc_attr( $css_class );?>">
	<div class="ads-box-info">
		<?php if ( $main_title ) : ?>
			<h3><?php echo esc_html( $main_title ) ?></h3>
		<?php endif; ?>
		<?php if ( $main_des ) : ?>
			<span class="number-sale"><?php echo esc_html( $main_des ) ?></span>
		<?php endif; ?>
		<a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>" class="outline-button">
			<?php echo esc_html($link['title']); ?>
		</a>
	</div>
</div>