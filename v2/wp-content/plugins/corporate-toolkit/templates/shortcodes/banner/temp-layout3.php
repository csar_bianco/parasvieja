<?php
if( isset($atts) ){
	extract ( $atts );
}
$lists = vc_param_group_parse_atts( $list );
?>
<div class="cp-brand-spot">
	<div class="brand-img">
		<figure>
			<?php $image_thumb = corporatepro_resize_image( $bg_banner, null, 330, 250, true, true, false ); ?>
        	<img  src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="">
		</figure>
		<?php if ( $main_title ) : ?>
			<h4 class="title-brand"><?php echo esc_html( $main_title ) ?></h4>
		<?php endif; ?>
	</div>
	<ul class="list-brand">
		<?php foreach ($lists as $value) : ?>
			<?php  
				if( !empty($value['links']) ){
					$link = vc_build_link( $value['links'] );
				}else{
					$link = array('url'=>' ', 'title'=>'', 'target'=>'', 'rel'=>'') ;
				}
			?>
			<li>
				<a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>">
					<?php echo esc_html( $value['title'] ) ?>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>