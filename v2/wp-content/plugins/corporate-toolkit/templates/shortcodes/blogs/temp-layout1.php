<?php
if ( isset( $atts ) ) {
	extract( $atts );
}
?>
<?php if ( $loop_posts->have_posts() ): ?>
    <div class="ss-blog <?php echo esc_attr( $css_class ); ?>">
        <div class="cp-latest-news latest-news-2 cp-height-equal">
			<?php while ( $loop_posts->have_posts() ): $loop_posts->the_post(); ?>
				<?php
				$gallery   = get_post_meta( get_the_ID(), '_format_gallery_images', true );
				$videos    = get_post_meta( get_the_ID(), '_format_video_embed', true );
				$thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full', true );
				?>
				<?php if ( empty( $gallery ) && empty( $videos ) ) : ?>
                    <div class="item-post post-standard height-equal">
                        <div class="post-format">
							<?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id( get_the_ID() ), null, 485, 250, true, true, false ); ?>
                            <img class="img-responsive" src="<?php echo esc_attr( $image_thumb[ 'url' ] ); ?>"
                                 width="<?php echo intval( $image_thumb[ 'width' ] ) ?>"
                                 height="<?php echo intval( $image_thumb[ 'height' ] ) ?>" alt="<?php the_title() ?>">
                        </div>
                        <div class="info-post">
                            <span class="date-post"><?php the_time( 'd F Y' ) ?></span>
                            <h3 class="title-post"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        </div>
                        <div class="link-more">
                            <span class="icon-before"></span>
                            <a href="<?php the_permalink(); ?>">
								<?php echo esc_html__( 'More', 'corporatepro' ) ?>
                            </a>
                        </div>
                    </div>
				<?php elseif ( !empty( $gallery ) ) : ?>
                    <div class="item-post post-image height-equal"
                         style="background-image: url(<?php echo esc_url( $thumb_url[ 0 ] ); ?>)">
                        <div class="icon-post">
                            <a href="<?php the_permalink(); ?>"
                               class="icon-link-post">
                                <i class="fa fa-camera"></i>
                            </a>
                        </div>
                        <div class="info-post">
                            <span class="date-post"><?php the_time( 'd F Y' ) ?></span>
                            <h3 class="title-post"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        </div>
                    </div>
				<?php elseif ( !empty( $videos ) ) : ?>
					<?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id( get_the_ID() ), null, 585, 498, true, true, false ); ?>
                    <div class="item-post post-video height-equal"
                         style="background-image: url(<?php echo esc_url( $thumb_url[ 0 ] ); ?>)">
                        <a href="<?php the_permalink(); ?>" class="icon-link-post">
                            <i class="fa fa-play"></i>
                        </a>
                    </div>
				<?php endif; ?>
			<?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>
<?php wp_reset_query(); ?>