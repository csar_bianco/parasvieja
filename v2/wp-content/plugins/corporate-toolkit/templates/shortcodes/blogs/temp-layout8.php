<?php
if( isset($atts) ){
    extract ( $atts );
}
?>
<div class="ss-upcoming-even ss-full">
    <div class="row">
        <div class="left-ss col-sm-12 col-md-3">
            <div class="title-section title-style1 text-center light">
                <span class="icon-title"><i class="fa fa-home"></i></span>
                <?php if ( $title ) : ?>
                    <h2 class="title"><?php echo esc_html($title) ?></h2>
                <?php endif; ?>
                <span class="sub-title">
                    <?php echo esc_html($month.' '.$d_start.'-'.$d_end.', '.$year) ?>
                </span>
            </div>
        </div>
        <div class="right-ss col-sm-12 col-md-9">
            <div class="cp-tab tab-style-1 tab-light">
                <?php
                    $arrdate = array();

                    for ($i = $d_start; $i <= $d_end; $i++) {
                        $arrdate[] = $i;
                    }

                    $full_time = '';
                ?>
                <ul>
                    <?php foreach ($arrdate as $key => $value) : ?>
                        <?php
                            $full_time = $year.'-'.$month.'-'.$value;

                            $tempDate = strtotime( $full_time);

                            if ( $year != '' && $month != '' && $value != '' ) {
                                $datetime       = DateTime::createFromFormat('Y-F-j', $full_time);
                                $format_date    = $datetime->format('l');
                            } else {
                                $format_date = '';
                            }
                        ?>
                        <li>
                            <a href="#date<?php echo esc_attr($value) ?>">
                                <?php echo esc_html($value) ?> <?php echo $format_date ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php foreach ($arrdate as $key => $value) : ?>
                    <?php
                    $full_time = $year.'-'.$month.'-'.$value;

                    $tempDate = strtotime( $full_time);

                    if ( $year != '' && $month != '' && $value != '' ) {

                        $datetime = DateTime::createFromFormat('Y-F-j', $full_time);

                        $archive_year  = $datetime->format('Y');
                        $archive_month = $datetime->format('m');
                        $archive_day   = $datetime->format('d');
                    } else {
                        $archive_year  = '';
                        $archive_month = '';
                        $archive_day   = '';
                    }

                    ?>
                    <div id="date<?php echo esc_attr($value) ?>">
                        <?php
                        if ( $year != '' && $month != '' && $value != '' ) {
                            $query_string = array(
                                'post_type'         => 'post',
                                'posts_per_page'    => $per_page,
                                'date_query'    => array(
                                    array(
                                        'year'  => $archive_year,
                                        'month' => $archive_month,
                                        'day'   => $archive_day,
                                    ),
                                ),
                                'post_status'       => 'publish'
                            );
                        } else {
                            $query_string = array();
                        }
                        $loop = new wp_query($query_string);

                        ?>

                        <div class="cp-list-even even-style1 row">
                            <?php while ($loop->have_posts()) : $loop->the_post() ?>
                                <?php $time_event = get_post_meta( get_the_ID(),'_corporatepro_time_event',true ); ?>
                                <div class="even-item col-sm-6">
                                    <div class="even-image">
                                        <?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, 150, 150, true, true, false ); ?>
                                        <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
                                    </div>
                                    <div class="even-info">
                                        <h3 class="even-title">
                                            <a href="<?php the_permalink() ?>">
                                                <?php the_title() ?>
                                            </a>
                                        </h3>
                                        <span class="even-date">
                                            <i class="fa fa-clock-o"></i>
                                            <?php echo esc_html($time_event) ?>
                                        </span>
                                        <div class="even-readmore">
                                            <a href="<?php the_permalink() ?>" class="readmore">
                                                <?php echo esc_html__('Read more','corporatepro')?>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
