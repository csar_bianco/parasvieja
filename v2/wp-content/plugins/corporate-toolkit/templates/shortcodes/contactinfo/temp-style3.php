<?php
if( isset($atts) ){
    extract ( $atts );
}
?>
<div class="contactinfo-v10 <?php echo esc_attr( $css_class );?>">
	<div class="widget widget-info-company">
		<a href="<?php echo esc_url( get_home_url() ) ?>" class="logo-footer">
			<img src="<?php echo esc_url( wp_get_attachment_url($atts['img_logo']) ) ?>" alt="">
		</a>
		<?php if( $description ):?>
			<div class="desc-company"><?php echo esc_html($description)?></div>
		<?php endif;?>
	</div>
	<div class="widget widget-contactinfo-2">
		<?php if( $title ):?>
	        <h3 class="title-widget"><?php echo esc_html($title)?></h3>
	    <?php endif;?>
		<div class="widget-content">
			<div class="contact-address">
				<?php if( $address ):?>
			        <div class="contactinfo">
			            <i class="fa fa-map-marker"></i>
			            <span class="text-info"><?php echo esc_html($address)?></span>
			        </div>
		        <?php endif;?>

		        <?php if( $phone ):?>
			        <div class="contactinfo">
			            <i class="fa fa-phone"></i>
			            <span class="text-info"><?php echo esc_html($phone)?></span>
			        </div>
		        <?php endif;?>

		        <?php if( $email ):?>
			        <div class="contactinfo">
			            <i class="fa fa-envelope-o"></i>
			            <a class="text-info" href="mailto:<?php echo esc_html($email)?>"><?php echo esc_html($email)?></a>
			        </div>
		        <?php endif;?>
			</div>
		</div>
	</div>
</div>
