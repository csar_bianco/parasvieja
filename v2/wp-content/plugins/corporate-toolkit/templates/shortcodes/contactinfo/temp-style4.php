<?php
if( isset($atts) ){
    extract ( $atts );
}
?>

<div class="cp-contact-info <?php echo esc_attr( $option_style ); ?> <?php echo esc_attr( $css_class );?>">
	<div class="icon-contact"><i class="<?php echo esc_attr( $icon_fontawesome ); ?>"></i></div>
	<?php if( $title ):?>
       	<h4 class="title-ct"><?php echo esc_html($title)?></h4>
    <?php endif;?>
    <?php  
        $lists = vc_param_group_parse_atts( $group_feild );
        if ( !empty($lists) ) :
    ?>
        <div class="info-ct">
            <?php 
                foreach ($lists as $key => $value) {
                    if ( $check_mail == true) {
                        printf('<a href="mailto: '.esc_attr($value['list_field']).'">'.$value['list_field'].'</a>');
                    } else {
                        printf('<span>'.$value['list_field'].'</span>');
                    }
                }
            ?>  
        </div>
    <?php endif; ?>
</div>