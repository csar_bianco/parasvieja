<?php
if( isset($atts) ){
    extract ( $atts );
}
?>
<div class="widget widget-contactinfo-2 <?php echo esc_attr( $css_class );?>">
	<?php if( $title ): ?>
		<h3 class="title-widget"><?php echo esc_html($title)?></h3>
	<?php endif;?>
	
	<div class="contact-info">
		<?php if( $address ): ?>
			<p><?php echo esc_html($address)?></p>
		<?php endif;?>
		<?php if( $phone ):?>
			<div class="contact-phone">
				<span class="title"><?php echo esc_html__('Call us now:','corporatepro') ?></span>
				<span class="info"><i class="fa fa-phone"></i><?php echo esc_html($phone)?></span>
			</div>
		<?php endif;?>

		<?php if( $email ): ?>
			<p><?php echo esc_html__('E-mail:','corporatepro') ?><a href="mailto:<?php echo esc_attr($email)?>">
				<?php echo esc_html($email) ?></a>
			</p>
		<?php endif;?>							
	</div>
</div>