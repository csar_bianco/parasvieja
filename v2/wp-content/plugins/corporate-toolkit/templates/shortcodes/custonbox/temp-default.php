<?php if( $atts ){
	extract($atts);
}?>
<div class="cp-custombox <?php echo esc_attr( $css_class );?>" style="background-color: <?php echo esc_attr( $boxbackground );?>;">
	<?php if($icon_type == 'image'):  ?>
		<div class="icon-box">
			<img src="<?php echo esc_url($icon[0]) ?>" width="<?php echo intval($icon[1]) ?>" height="<?php echo intval($icon[2]) ?>" alt="<?php echo esc_html($title); ?>">
		</div>
	<?php else:?>
		<div class="icon-box"><span class="<?php echo esc_attr($icon);?>"></span></div>
	<?php endif;?>	
	<?php if( $title ):?>
	<h4><a href="<?php echo esc_url( $link );?>"><?php echo esc_html( $title );?></a></h4>
	<?php endif;?>
</div>