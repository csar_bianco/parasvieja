<?php
if( isset($atts) ){
	extract ( $atts );
}
$link_default = array(
    'url'    => '',
    'title'  => '',
    'target' => '',
);

if (function_exists('vc_build_link')):
    $link = vc_build_link($link);
else:
    $link = $link_default;
endif;
?>

<div class="cp-iconbox iconbox-style4">
	<div class="iconbox"><i class="<?php echo esc_attr($icon);?>"></i></div>
	<div class="content-iconbox">
		<h4 class="title-iconbox">
			<a href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
				<?php echo esc_html( $title );?>
			</a>
		</h4>
		<span class="icon-text">
			<?php echo esc_html( $description );?>
		</span>
	</div>
</div>