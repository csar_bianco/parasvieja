<?php
if( isset($atts) ){
	extract ( $atts );
}
$link_default = array(
    'url'    => '',
    'title'  => '',
    'target' => '',
);

if (function_exists('vc_build_link')):
    $link = vc_build_link($link);
else:
    $link = $link_default;
endif;
?>

<div class="cp-feature feature-style2">
	<span class="icon-feature">
		<i class="<?php echo esc_attr($icon_fontawesome) ?>"></i>
	</span>
	<?php if ($title) : ?>
		<h4>
			<a href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
				<?php echo esc_html($title) ?>
			</a>
		</h4>
	<?php endif; ?>
</div>