<?php
if( $atts ){
	extract($atts);
}
$items = vc_param_group_parse_atts( $items );
?>
<?php if( is_array( $items ) && count( $items ) >0 ):?>
	<ul class="list-dots list-dots2">
		<?php foreach( $items as $item):?>
			<?php if( $item['text'] ):?>
			<li><?php echo balanceTags( $item['text']  );?></li>
			<?php endif;?>	
		<?php endforeach;?>	
	</ul>
<?php endif;?>	