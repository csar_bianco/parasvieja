<?php
if( $atts ){
	extract($atts);
}
$items = vc_param_group_parse_atts( $items );
?>

<?php if( is_array( $items ) && count( $items ) >0 ):?>
	<div class="cp-list-style list-icon <?php echo esc_attr( $css_class );?>">
		<ul>
		<?php foreach( $items as $item):?>
			<li>
			<?php if( !empty($item['icon_fontawesome']) ):?>
				<span class="icon"><i class="<?php echo esc_attr( $item['icon_fontawesome']  );?>"></i></span>
			<?php endif;?>	
			<?php if( !empty($item['text']) ):?>
				<span class="text"><?php echo balanceTags( $item['text']  );?></span>
			<?php endif;?>
			</li>
		<?php endforeach;?>	
		</ul>
	</div>
<?php endif;?>	