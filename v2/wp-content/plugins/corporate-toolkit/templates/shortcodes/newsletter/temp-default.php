<?php
if( isset($atts) ){
	extract ( $atts );
}
?>
<div class="newsletter-form-wrap widget widget-newsletter <?php echo esc_attr( $css_class );?>">
	<?php if( $title ):?>
	<h3 class="title-widget"><i class="fa fa-paper-plane"></i><?php echo esc_html($title);?></h3>
	<?php endif;?>
	<p>Get subscriber only insights &amp; news <br>delivered by <a href="#" target="_blank">kutethemes.com</a></p>
	<div class="form-newsletter">
		<input name="emailaddress" class="email" type="email"  placeholder="<?php echo esc_attr($placeholder_text);?>"/>

		<span><input class="submit-newsletter" type="submit" value="<?php echo balanceTags($button_text);?>"></span>
	</div>
</div>