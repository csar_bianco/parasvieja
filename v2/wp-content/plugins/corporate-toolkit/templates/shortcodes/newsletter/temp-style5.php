<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="newsletter-form-wrap widget widget-newsletter-2 <?php echo esc_attr( $css_class );?>">
	<form action="#" class="form-newsletter">
		<?php if( $title ):?>
			<h3><?php echo esc_html($title);?></h3>
		<?php endif;?>
		<input name="emailaddress" type="email" value="" placeholder="<?php echo esc_attr($placeholder_text);?>">
		<span><button type="submit" class="submit-newsletter"><i class="fa fa-key"></i></button></span>
	</form>
</div>