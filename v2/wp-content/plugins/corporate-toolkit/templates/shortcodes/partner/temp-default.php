<?php
if( $atts ){
    extract ( $atts );
}
$link_default = array(
    'url'    => '',
    'title'  => '',
    'target' => '',
);

if (function_exists('vc_build_link')):
    $link = vc_build_link($link);
else:
    $link = $link_default;
endif;
?>
<div class="cp-partner <?php echo esc_attr( $css_class );?>">
	<?php if ($bg_partner) : ?>
		<div class="img-partner">
			<a href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
				<img src="<?php echo esc_url( wp_get_attachment_url($atts['bg_partner']) ) ?>" alt="">
			</a>
		</div>
	<?php endif; ?>
	<div class="info-partner">
		<?php if ($logo_partner) : ?>
			<a href="<?php echo esc_url( $link['url']) ?>" class="logo-partner" target="<?php echo esc_attr( $link['target']) ?>">
				<img src="<?php echo esc_url( wp_get_attachment_url($atts['logo_partner']) ) ?>" alt="">
			</a>
		<?php endif; ?>

		<?php if ($title) : ?>
			<div class="desc">
				<a href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
					<?php echo esc_html($title); ?>
				</a>
			</div>
		<?php endif; ?>
	</div>
</div>