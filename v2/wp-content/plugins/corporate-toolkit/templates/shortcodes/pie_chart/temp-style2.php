<?php
if( isset($atts) ){
	extract ( $atts );
}
?>
<div class="<?php echo esc_attr( $css_class );?> cp-pie-charts pie-chart-2" data-strockwidth="0" data-goal="<?php echo esc_attr( $percent );?>" data-barcolor="<?php echo esc_attr( $barcolor );?>" data-trackcolor="<?php echo esc_attr( $trackcolor );?>">
	<div class="progress-label">
        <div class="percent-number">
        	<span style="color:<?php echo esc_attr( $barcolor );?>;" class="percent icon-pie">
        		<i class="<?php echo esc_attr( $icon ); ?>"></i>
        	</span>
        </div>
        <?php if( $title ):?>
        <div class="title-progress"><?php echo esc_html( $title );?></div>
    	<?php endif;?>
    </div>
</div>