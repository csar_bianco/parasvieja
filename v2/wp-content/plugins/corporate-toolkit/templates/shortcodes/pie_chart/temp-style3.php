<?php
if( isset($atts) ){
	extract ( $atts );
}
?>
<div class="<?php echo esc_attr( $css_class );?>  pie-chart-3" data-number="<?php echo esc_attr( $number );?>">
	<div class="pie-chart-bg" style="background-color: <?php echo esc_attr( $boxcolor );?>;">
		<div class="cp-pie-charts" data-strockwidth="0" data-strockwidthbar="3" data-barcolor="#ffffff" data-trackcolor="#e5e5e5">
		<!-- <div class="cp-pie-charts" data-strockwidth="0" data-strockwidthbar="3" data-barcolor="<?php echo esc_attr( $barcolor );?>" data-trackcolor="<?php echo esc_attr( $trackcolor );?>"> -->
			<div class="progress-label">
                <div class="percent-number">
                	<span class="percent piechart-number"><?php echo esc_attr( $number );?></span>
                </div>
            </div>
        </div>
    </div>
    <?php if( $title ):?>
    <div class="title-progress"><?php echo esc_html( $title );?></div>
	<?php endif;?>
</div>