<?php
if( isset($atts) ){
	extract ( $atts );
}
$link_default = array(
    'url'    => '',
    'title'  => '',
    'target' => '',
);

if (function_exists('vc_build_link')):
    $link = vc_build_link($link);
else:
    $link = $link_default;
endif;
				
?>

<div class="cp-pricing pricing-default <?php echo esc_attr( $css_class );?>">
	<div class="pricing-header">
		<h4 class="title-pricing">
			<a href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
				<?php echo esc_html( $title ); ?>
			</a>
		</h4>
		<span class="desc-title"><?php echo esc_html__('Starting at', 'corporatepro'); ?></span>
		<div class="price-unit">
			<div class="price">
				<span class="currency"><?php echo esc_html__('$','corporatepro') ?></span>
				<?php echo esc_html( $price ); ?>
			</div>
			<div class="unit"><?php echo esc_html__('/','corporatepro') ?><?php echo esc_html( $per ); ?></div>
		</div>
	</div>
	<div class="pricing-feature" style="text-align: <?php echo esc_attr( $text_align ); ?>">
		<ul>
		<?php  
			$lists = vc_param_group_parse_atts( $group_feild );
			if ( !empty($lists) ) {
				foreach ($lists as $key => $value) {
					printf('<li>'.$value['list_field'].'</li>');
				}	
			}	
		?>
		</ul>
	</div>
	<div class="pricing-button">
		<a class="outline-button bt-recent" href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
			<?php echo esc_html( $link['title']) ?>	
		</a>
	</div>
</div>