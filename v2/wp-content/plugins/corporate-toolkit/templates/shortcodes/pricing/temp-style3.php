<?php
if( isset($atts) ){
	extract ( $atts );
}
$link_default = array(
    'url'    => '',
    'title'  => '',
    'target' => '',
);

if (function_exists('vc_build_link')):
    $link = vc_build_link($link);
else:
    $link = $link_default;
endif;
?>

<div class="cp-pricing pricing-bphone <?php echo esc_attr( $css_class );?>">
	<div class="pricing-header" style="background-color: <?php echo esc_attr( $main_color ); ?>">
		<h4 class="title-pricing"><?php echo esc_html( $title ); ?></h4>
		<div class="price"><?php echo esc_html( $price ); ?></div>
	</div>
	<div class="pricing-feature">
		<ul>
			<?php  
				$lists = vc_param_group_parse_atts( $group_feild );
				if ( !empty($lists) ) :
					foreach ($lists as $key => $value) :
						$text 	= $value['list_field'];
						$num 	= $value['text_number'];
			?>	
				<li><?php echo esc_html($text) ?>
					<span class="number_price"><?php echo esc_html($num) ?></span>
				</li>
			<?php 	endforeach; 
				endif;
			?>
		</ul>
	</div>
	<div class="pricing-button">
		<a class="cp-button button-style3" href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>" style="background-color: <?php echo esc_attr( $main_color ); ?>">
			<?php echo esc_html( $link['title']) ?>	
		</a>
	</div>
</div>