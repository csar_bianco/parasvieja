<?php
if( isset($atts) ){
	extract ( $atts );
}
$link_default = array(
    'url'    => '',
    'title'  => '',
    'target' => '',
);

if (function_exists('vc_build_link')):
    $link = vc_build_link($link);
else:
    $link = $link_default;
endif;

$args = array(
    'a' => array(
        'href' => array(),
        'title' => array()
    ),
    'br' => array(),
    'em' => array(),
    'strong' => array(),
	);
?>

<div class="cp-pricing pricing-color2 <?php echo esc_attr( $css_class );?>">
	<div class="pricing-header" style="background-color: <?php echo esc_attr( $main_color ); ?>">
		<h4 class="title-pricing">
			<a href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
				<?php echo esc_html( $title ); ?>
			</a>
		</h4>
	</div>
	<div class="pricing-content">
		<div class="price-unit">
			<div class="price-title"><?php echo esc_html__('Starting at', 'corporatepro'); ?></div>
			<div class="price">
				<span class="currency"><?php echo esc_html__('$', 'corporatepro') ?></span>
				<?php echo esc_html( $price ); ?>
			</div>
			<div class="unit"><?php echo esc_html( $per ); ?></div>
			<p class="price-desc"><?php echo esc_attr( $price_des ); ?></p>
		</div>
		<div class="pricing-desc"><?php echo wp_kses($description, $args); ?></div>
		<div class="pricing-feature">
			<ul>
				<?php  
					$lists = vc_param_group_parse_atts( $group_feild );
					if (!empty($lists)) {
						foreach ($lists as $key => $value) {
							printf('<li><i class="fa '.$value['icon_fontawesome'].'" style="color: '.$main_color.'"></i>'.$value['list_field'].'</li>');
						}	
					}	
				?>
			</ul>
		</div>
		<div class="pricing-button">
			<a class="cp-button" href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>" style="background-color: <?php echo esc_attr( $main_color ); ?>">
				<?php echo esc_html( $link['title']) ?>	
			</a>
		</div>
	</div>
</div>