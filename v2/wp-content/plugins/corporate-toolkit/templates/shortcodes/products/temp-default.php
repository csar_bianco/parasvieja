<?php
if( $atts ){
    extract ( $atts );
}

$category_slug = array();
if( $taxonomy ){
	$category_slug = explode(',', $taxonomy);
}
$categorys = get_terms( array(
    'taxonomy' => 'product_cat',
    'hide_empty' => false,
    'slug'=> $category_slug
) );

$atts_data = json_encode($atts);
?>
<?php 
remove_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_rating',5);
remove_action('woocommerce_before_shop_loop_item_title','corporatepro_shop_loop_item_contdown',20);
?>

<div class="<?php echo esc_attr( $css_class );?> cp-project project-style4 cp-portfolio pf-gap30 pd-right">
	<div class="portfolio_fillter portfolio_fillter-2 project-fillter">
		<div data-filter="*" class="item-fillter fillter-active">All</div>
		<?php if( $categorys  && count( $categorys ) > 0 ):?>
			<?php foreach( $categorys  as $category):?>
				<div data-filter=".product_cat-<?php echo esc_attr( $category->slug);?>" class="item-fillter"><?php echo esc_html( $category->name);?><span class="number"><?php echo esc_html( $category->count);?></span></div>
			<?php endforeach;?>
		<?php endif;?>	
	</div>
	<?php if( $products->have_posts()): ?>
	<div class="portfolio-grid" data-layoutMode="fitRows" data-cols="3">
		<?php while ( $products->have_posts() ) : $products->the_post();  ?>
			<div <?php post_class( 'item-project item-portfolio');?>>
                <?php wc_get_template_part('product-styles/content-product-style', '2'); ?>
            </div>
		<?php endwhile;?>
	</div>
	<div class="pj-loadmore">
    	<div class="row">
    		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
    			<a href="<?php echo get_page_link( get_page_by_title( 'shop' )->ID ); ?>" class=" cp-button button-block button-larger">
					<?php esc_html_e('View All Products','corporatepro');?>
				</a>
    		</div>
    	</div>
    </div>
	<?php else:?>
		<p>
            <strong><?php esc_html_e( 'No Product', 'corporatepro' ); ?></strong>
        </p>
	<?php endif;?>
	
</div>
<?php 
add_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_rating',5);
add_action('woocommerce_before_shop_loop_item_title','corporatepro_shop_loop_item_contdown',20);
?>