<?php
if( $atts ){
    extract ( $atts );
}
wp_enqueue_script( 'prettyphoto' );
wp_enqueue_style( 'prettyphoto' );

$items = vc_param_group_parse_atts( $items );
$width 	= 0;
$height = 0;
$i = 0;
$stt  ='';
?>

<div class="<?php echo esc_attr($css_class); ?>">
	<div class="cp-project project-style9 cp-portfolio">
		<div class="portfolio-grid" data-layoutMode="masonry" data-cols="<?php echo esc_attr( $columns );?>">
        <?php foreach( $items as $item): ?>
			<?php
				$rand =  rand();

                if ( $item['size_post']     == 'normal' ) {
                    $width 	= 300;
                    $height = 300;
                } elseif ( $item['size_post'] 	== 'big' ) {
                    $width 	= 300;
                    $height = 602;
                }

                $args = array(
                    'post_type'         => 'project',
                    'p'					=> $item['ids'],
                );
                $loop = new wp_query($args);
            ?>
			<?php while ($loop->have_posts()): $loop->the_post(); ?>
				<?php
					$thumb 	= get_post_meta( get_the_ID(),'_corporatepro_images_gallery',true);

					$i++;
					if( $i < 10 ){
						$stt = '0'.$i;
					}else{
						$stt = $i;
					}
				?>
				<div class="item-project item-portfolio">
		    		<div class="pj-caption">
		    			<div class="pj-image">
		    			<?php $image_thumb = corporatepro_resize_image(get_post_thumbnail_id(get_the_ID()), null, $width, $height, true, true, false); ?>
		    				<figure>
		    					<img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
		    				</figure>
		    				<div class="pj-hover">
		    					<?php if ( ! empty( $thumb ) ) : ?>
			    					<?php foreach ($thumb as $key => $value) :  ?>
			    						<a class="hiden-tab prettyphoto pj-icon icon-view" href="<?php echo esc_url($value) ?>" data-rel="prettyPhoto[rel-<?php echo get_the_ID() ?>-<?php echo $rand ?>]">
			    						</a>
				    				<?php endforeach; ?>
			    				<?php endif; ?>
			    				<a class="prettyphoto pj-icon icon-view" href="<?php echo esc_attr($image_thumb['url']); ?>" data-rel="prettyPhoto[rel-<?php echo get_the_ID() ?>-<?php echo $rand ?>]">
		    							<i class="fa fa-search"></i>
		    						</a>
		    					<a href="<?php the_permalink() ?>" class="pj-icon icon-link"><i class="fa fa-link"></i></a>
		    				</div>
		    			</div>
		        	</div>
					<div class="pj-info">
						<h3 class="pj-title">
							<span class="number"><?php echo esc_html($stt);?></span>
							<span class="content_animate">
								<a class="text_animate" href="<?php the_permalink() ?>"><?php the_title();?></a>
							</span>
						</h3>
					</div>
		    	</div>
		    <?php endwhile; ?>
		    <?php wp_reset_query();wp_reset_postdata(); ?>
        <?php endforeach; ?>
		</div>
	</div>
	<div class="button-center">
		<a href="<?php echo esc_url( get_post_type_archive_link( 'project' ) );?>" class="cp-button button-larger bt-link"> 
			<?php echo esc_html__( 'View All', 'corporatepro' ) ?> 
			<img src="<?php echo get_template_directory_uri(). '/images/arrow-view.png' ?>" alt="">
		</a>
	</div>
</div>
