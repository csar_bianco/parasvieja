<?php
if( $atts ){
    extract ( $atts );
}
wp_enqueue_script( 'prettyphoto' );
wp_enqueue_style( 'prettyphoto' );
$rand =  rand();
?>
<div class="<?php echo esc_attr($css_class); ?>">
<?php if ($projects->have_posts()): ?>
	<div class="feature-project owl-carousel nav-style4" data-items="1" data-dots="false" data-nav="true">
	<?php 
		while ($projects->have_posts()): $projects->the_post(); 
			$check = '';
            
            if ( get_post_type( get_the_ID() ) == 'project' ) {
                $check = 'category_project';
            } elseif ( get_post_type( get_the_ID() ) == 'post' ) {
                $check = 'category';
            }

            $categories_list    = get_the_term_list( get_the_ID(),$check,'',' ');
			$image_thumb = corporatepro_resize_image(get_post_thumbnail_id(get_the_ID()), null, 585, 380, true, true, false);
			$gallery 	= get_post_meta( get_the_ID(),'_corporatepro_images_gallery',true);
			$video 		= get_post_meta( get_the_ID(),'_corporatepro_url_video',true);
	?>
		<div class="item-project">
			<div class="pj-image">
				<img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
			</div>
			<div class="pj-info">
				<h3 class="pj-title">
					<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
				</h3>
				<div class="pj-meta">
	    			<ul>
	    				<li><i class="fa fa-calendar-o"></i>
	    					<?php the_time('F j, Y') ?>
	    				</li>
	    				<li><i class="fa fa-user"></i>
		    				<a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))) ?>">
		    					<?php the_author(); ?>
		    				</a>
	    				</li>
	    				<li><i class="fa fa-folder-o"></i>
	    					<?php
                                if ($categories_list ) {
                                    printf( esc_html__('%1$s', 'corporatepro') , $categories_list);
                                }
                            ?>
	    				</li>
	    			</ul>
	    		</div>
	    		<div class="pj-excerpt">
	    			<?php echo wp_trim_words(get_the_content(),20,'...'); ?>
	    		</div>
				<a class="pj-readmore outline-button" href="<?php the_permalink() ?>">
					<?php echo esc_html__('Read more', 'corporatepro') ?>
				</a>
				<div class="pj-icon-bottom">
					<?php if ( !empty($video) ) : ?>
						<span class="cp-video-popup">
							<a href="<?php echo esc_url( $video );?>" class="html5lightbox icon-play"><i class="fa fa-play"></i></a>
						</span>
					<?php endif; ?>

					<?php if ( !empty($gallery) ) : ?>
						<?php foreach ($gallery as $key => $value) :  ?>
							<a class="hiden-tab prettyphoto pj-icon icon-view" href="<?php echo esc_url( $value ) ?>" data-rel="prettyPhoto[rel-<?php echo get_the_ID() ?>-<?php echo $rand ?>]">
							</a>
						<?php endforeach; ?>
						<a href="<?php echo esc_attr($image_thumb['url']); ?>" class="icon-photo prettyphoto" data-rel="prettyPhoto[rel-<?php echo get_the_ID() ?>-<?php echo $rand ?>]">
							<i class="fa fa-camera"></i>
						</a>
					<?php endif; ?>
					<?php
					$url_twitter = esc_url_raw('http://twitter.com/home/?status='.get_the_title().'-'.get_the_permalink().'');
					$url_fb = esc_url_raw('http://www.facebook.com/sharer.php?u='.get_the_permalink().'&amp;t='.get_the_title().'')
					?>
					<div class="icon-share"><i class="fa fa-share"></i>
						<div class="sub_iconshare">
							<a href="<?php echo esc_url($url_fb);?>" target="_blank">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="<?php echo esc_url($url_twitter);?>" target="_blank">
								<i class="fa fa-twitter"></i>
							</a>
							<a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php $url = wp_get_attachment_url( get_post_thumbnail_id($projects->ID) ); echo $url; ?>" target="_blank">
								<i class="fa fa-linkedin"></i>
							</a>
							<a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" target="_blank">
								<i class="fa fa-google-plus"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
	</div>
<?php endif;?>
<?php wp_reset_query() ?>
</div>