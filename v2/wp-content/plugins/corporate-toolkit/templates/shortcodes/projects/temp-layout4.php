<?php
if ( $atts ) {
	extract( $atts );
}
?>
<?php
$html       = '';
$count      = 0;
$total      = 0;
$loop_array = array();
$all        = array();
$counts     = array();

foreach ( $category_project as $value ) :
	$args         = array(
		'post_type'      => 'project',
		'post_status'    => 'publish',
		'posts_per_page' => $ppp,
		'orderby'        => $orderby,
		'order'          => $order,
		'tax_query'      => array(
			array(
				'taxonomy' => 'category_project',
				'field'    => 'slug',
				'terms'    => $value,
			),
		),
	);
	$except       = new wp_query( $args );
	$loop_array[] = wp_list_pluck( $except->posts, 'ID' );
	$counts[]     = $except->post_count;
	foreach ( $loop_array as $item ) {
		if ( count( $item ) > 0 ) {
			foreach ( $item as $it ) {
				array_push( $all, $it );
			}
		}
    }

	$total = array_sum($counts) ;
	$html   .= '<div data-filter=".' . $value . '" class="item-fillter">
            ' . $value . '
            <span class="number">' . $counts[ $count++ ] . '</span>
            </div>';
endforeach;
?>
<div class="cp-project project-style10 cp-portfolio <?php echo esc_attr( $css_class ); ?>">
    <div class="portfolio_fillter portfolio_fillter-3 project-fillter">
        <div data-filter="*" class="item-fillter fillter-active">
			<?php echo esc_html__( 'All', 'corporatepro' ) ?>
            <span class="number"><?php echo $total ?></span>
        </div>
		<?php echo $html ?>
    </div>
    <div class="portfolio-grid" data-layoutMode="fitRows" data-cols="<?php echo esc_attr( $columns ) ?>">
		<?php
		$args_post  = array(
			'post_type'      => 'project',
			'post_status'    => 'publish',
			'posts_per_page' => -1,
			'orderby'        => $orderby,
			'order'          => $order,
			'post__in'       => array_unique($all),
		);
		$loop_query = new wp_query( $args_post );

		while ( $loop_query->have_posts() ) : $loop_query->the_post();
			$categories_list = get_the_term_list( get_the_ID(), 'category_project', '', ' ' );
			$names           = get_the_terms( $loop_query->ID, 'category_project' );
			?>
            <div class="item-project item-portfolio
            <?php
			if ( !empty( $names ) && !is_wp_error( $names ) ) {
				foreach ( $names as $name ) {
					echo $name->slug . ' ';
				}
			}
			?>
        ">
                <div class="pj-caption">
                    <div class="pj-image">
                        <figure>
							<?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id( get_the_ID() ), null, 390, 290, true, true, false ); ?>
                            <img class="img-responsive" src="<?php echo esc_attr( $image_thumb[ 'url' ] ); ?>"
                                 width="<?php echo intval( $image_thumb[ 'width' ] ) ?>"
                                 height="<?php echo intval( $image_thumb[ 'height' ] ) ?>"
                                 alt="<?php the_title() ?>">
                        </figure>
                    </div>
                    <div class="pj-info">
                        <div class="left-info">
                            <h3 class="pj-title">
                                <a href="<?php the_permalink() ?>">
									<?php the_title() ?>
                                </a>
                            </h3>
                            <span class="pj-name-of">
        					<?php
							if ( $categories_list ) {
								printf( esc_html__( '# %1$s', 'corporatepro' ), $categories_list );
							}
							?>
        				</span>
                        </div>
                        <div class="right-info">
                            <a href="<?php the_permalink() ?>" class="pj-readmore cp-button">
								<?php echo esc_html__( 'Read more', 'corporatepro' ) ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
			<?php
		endwhile;
		wp_reset_postdata();
		wp_reset_query();
		?>
    </div>
    <div class="button-center" style="padding-top: 30px">
        <a href="<?php echo esc_url( get_post_type_archive_link( 'project' ) ); ?>"
           class="cp-button button-larger bt-link">
			<?php echo esc_html__( 'View All', 'corporatepro' ) ?>
            <img src="<?php echo get_template_directory_uri() . '/images/arrow-view.png' ?>" alt="">
        </a>
    </div>
</div>