<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="<?php echo esc_attr( $css_class );?> <?php echo esc_attr($style_title) ?>">
	<h2 class="title"><?php echo esc_html($title) ?></h2>
	<span class="hr"></span>
</div>