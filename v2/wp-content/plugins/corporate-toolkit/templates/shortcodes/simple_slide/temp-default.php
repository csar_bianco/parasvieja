<?php
if( $atts ){
    extract ( $atts );
}
$items = vc_param_group_parse_atts( $items );
?>
<div class="simple-slide cp-recent-client owl-carousel" <?php echo $owl_carousel ?>>
    <?php foreach( $items as $item):?>
        <?php
        if( !empty($item['link']) ){
            $link = vc_build_link( $item['link'] );
        }else{
            $link = array('url'=>'#', 'title'=>'', 'target'=>'_self', 'rel'=>'') ;
        }
        ?>
        <a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_attr($link['target']) ?>">
            <img src="<?php echo esc_url( wp_get_attachment_url($item['bg_img']) ) ?>" alt="Slide images">
        </a>
    <?php endforeach;?>
</div>
