<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="widget widget-connect <?php echo esc_attr($no_text) ?> <?php echo esc_attr( $css_class );?>" style="text-align: <?php echo esc_attr($text_align); ?>">
	<?php if( $title ):?>
	<h3 class="title-widget widgettitle"><?php echo esc_html( $title );?></h3>
	<?php endif;?>

	<?php if( $socials && is_array($socials) && count( $socials ) > 0 ):?>
	    <ul class="social-f">
	        <?php foreach ($socials as $social):?>
	        	<li><?php corporatepro_social($social);?></li>
	        <?php endforeach;?>
	    </ul>
	<?php endif;?>
</div>