<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="widget widget-social <?php echo esc_attr( $css_class );?>" style="text-align: <?php if ($text_align != '') {echo esc_attr($text_align); } ?>">
	<?php if( $socials && is_array($socials) && count( $socials ) > 0 ):?>
	    <ul class="cp-social">
	        <?php foreach ($socials as $social):?>
	        	<li><?php corporatepro_social($social);?></li>
	        <?php endforeach;?>
	    </ul>
	<?php endif;?>
</div>