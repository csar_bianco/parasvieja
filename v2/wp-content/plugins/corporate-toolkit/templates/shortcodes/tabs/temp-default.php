<?php
if( isset($atts) ){
	extract ( $atts );
}
?>
<div class="<?php echo esc_attr( $css_class );?> cp-tab-horizontal tab-style-1">
	<?php if( $sections && is_array( $sections ) && count( $sections ) > 0 ):?>
		<ul>
			<?php foreach( $sections as $section):?>
		    <li><a href="#<?php echo esc_attr( $section['tab_id'] );?>"><?php echo esc_html( $section['title'] );?></a></li>
			<?php endforeach;?>
		</ul>
		<?php foreach( $sections as $section):?>
		    <div id="<?php echo esc_attr( $section['tab_id'] );?>">
		    	<?php echo do_shortcode( $section['content'] );?>
		    </div>
		<?php endforeach;?>
	<?php endif;?>	
</div>