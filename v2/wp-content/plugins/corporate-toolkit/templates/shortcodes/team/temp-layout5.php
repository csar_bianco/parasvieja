<?php
if( $atts ){
	extract($atts);
}
$items = vc_param_group_parse_atts( $items );
?>

<div class="ss-testimonial1 <?php echo esc_attr( $css_class );?>">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
			<?php if ( $title ) : ?>
				<div class="title-section title-style9 light">
					<h2 class="title"><?php echo esc_html( $title ) ?></h2>
					<span class="hr"></span>
				</div>
			<?php endif; ?>
			</div>
		</div>
		<div class="cp-slide-navcustom cp-testimonial testimonial-style5">
			<div class="slide-navcustom owl-carousel" data-items="1" data-dots="false" data-nav="true">
				<?php foreach( $items as $item ):?>
					<?php  
						if( !empty($item['link']) ){
							$link = vc_build_link( $item['link'] );
						}else{
							$link = array('url'=>'#', 'title'=>'', 'target'=>'_self', 'rel'=>'') ;
						}
					?>
					<div class="testimonial-item">
						<div class="row">
							<div class="col-sm-6">
								<div class="client-info">
									<div class="owl-nav cp-nav-custom"></div>
									<?php if( !empty($item['textarea'])):?>
										<div class="client-quote"><?php echo esc_html($item['textarea']);?></div>
									<?php endif; ?>

									<?php if( !empty($item['name'])):?>
										<a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>">
											<h4 class="client-name"><?php echo esc_html($item['name']);?></h4>
										</a>
									<?php endif; ?>

									<?php if( !empty($item['position'])):?>
										<div class="client-pos"><?php echo esc_html($item['position']);?></div>
									<?php endif; ?>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="avatar-client">
									<?php $image_thumb = corporatepro_resize_image( $item['avatar'], null, 570, 600, true, true, false ); ?>
                    				<img  src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="">
								</div>
							</div>
						</div>
					</div>
				<?php endforeach;?>	
			</div>
		</div>
	</div>
</div>