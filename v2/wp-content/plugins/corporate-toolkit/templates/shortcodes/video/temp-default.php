<?php
if( isset($atts) ){
	extract ( $atts );
}
?>
<div class="cp-video-popup <?php echo esc_attr( $css_class );?>">
	<figure><?php echo wp_get_attachment_image( $videobackground ,'full');?></figure>
	<a href="<?php echo esc_url( $videolink );?>" class="html5lightbox icon-video-popup"><i class="fa fa-play"></i></a>
</div>