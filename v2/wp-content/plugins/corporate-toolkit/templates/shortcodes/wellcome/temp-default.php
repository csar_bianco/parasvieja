<?php
if( $atts ){
	extract($atts);
}
if( !empty($link) ){
	$link = vc_build_link( $link );
}else{
	$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
}
?>
<div class="ss-wellcome1 <?php echo esc_attr( $css_class );?>">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="block-wellcome">
					<?php if ( $title ) : ?>
						<div class="title-section title-style9 light">
							<h2 class="title"><?php echo esc_html( $title ) ?></h2>
							<span class="hr"></span>
						</div>
					<?php endif; ?>
					<?php if ( $description) : ?>
						<div class="text-wellcome">
							<?php echo esc_html( $description) ?>
						</div>
					<?php endif; ?>
					<?php if ($link['url'] != '') : ?>
						<a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>" class="outline-button">
							<?php echo esc_html($link['title']) ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="wellcome-slide owl-carousel nav-style2" data-items=1 data-autoplay="auto" data-nav="true" data-dots="false">
					<?php $args_images = explode(',', $bg_wellcome) ?>
					<?php foreach ($args_images as $key => $value) : ?>
						<div class="item-slide">
							<figure>
								<?php $image_thumb = corporatepro_resize_image( $value, null, 570, 380, true, true, false ); ?>
		            			<img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
		            		</figure>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>