<?php
if( $atts ){
    extract ( $atts );
}
?>
<div class="<?php echo esc_attr( $css_class );?> bg-parallax">
	<div class="ss-column">
		<?php if( $title ):?>
		<div class="title-section title-style1 text-center light">
			<span class="icon-title"><i class="fa fa-home"></i></span>
			<h2 class="title"><?php echo esc_html( $title );?></h2>
		</div>
		<?php endif;?>	
		<div></div>
		<div></div>
		<?php if( $corporatepro_whatwedo_item && is_array( $corporatepro_whatwedo_item ) && count( $corporatepro_whatwedo_item ) > 0 ):?>
		<div class="cp-service service-style-1">
			<span class="number">01</span>
			<?php if( $corporatepro_whatwedo_item[0]['title']):?>
				<?php
				$link = '#';
				if( isset($corporatepro_whatwedo_item[0]['link']) && $corporatepro_whatwedo_item[0]['link']!="" ){
					$link = $corporatepro_whatwedo_item[0]['link'];
				}
				?>
			<h4 class="title-service"><a href="<?php echo esc_url( $link);?>"><?php echo esc_html( $corporatepro_whatwedo_item[0]['title'] );?></a></h4>
			<?php endif;?>
			<?php if( $corporatepro_whatwedo_item[0]['short_description']):?>
			<div class="desc-service"><?php echo balanceTags( $corporatepro_whatwedo_item[0]['short_description']);?></div>
			<?php endif;?>
		</div>
		<?php endif;?>
	</div>
	<div class="ss-column">
		<div></div>
		<?php if( $corporatepro_whatwedo_item && is_array( $corporatepro_whatwedo_item ) && count( $corporatepro_whatwedo_item ) > 1 ):?>
		<?php $i = 0; $stt = ''; ?>		
		<?php foreach( $corporatepro_whatwedo_item as $item ): $i++;?>
			<?php
			if( $i < 10 ){
				$stt = '0'.$i;
			}else{
				$stt = $i;
			}
			$link = '#';
				if( isset($item['link']) && $item['link']!="" ){
					$link = $item['link'];
				}
			?>
			<?php if( $i > 1 ):?>
			<div class="cp-service service-style-1">
				<span class="number"><?php echo esc_html( $stt );?></span>
				<?php if( $item['title']):?>
				<h4 class="title-service"><a href="<?php echo esc_url( $link);?>"><?php echo esc_html( $item['title'] );?></a></h4>
				<?php endif;?>
				<?php if( $item['short_description']):?>
				<div class="desc-service"><?php echo balanceTags( $item['short_description']);?></div>
				<?php endif;?>
			</div>
			<?php endif;?>
		<?php endforeach;?>
		<?php endif;?>
	</div>
	<div class="ss-column colum-end">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
</div>