<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package corporatepro
 */
?>
<?php get_header(); ?>
<?php
/* Project Style */
$corporatepro_project_list_style    = corporatepro_get_option('opt_project_list_style','v1');
$corporatepro_check_project_banner  = corporatepro_get_option('opt_project_show_gallery','1');
$corporatepro_project_sidebar       = corporatepro_get_option('opt_project_used_sidebar','project-widget-area');

/* Project Layout */
$corporatepro_project_layout = corporatepro_get_option('opt_project_layout','full');

$corporatepro_container_class = array('main-container');

if( $corporatepro_project_layout == 'full'){
    $corporatepro_container_class[] = 'no-sidebar';
}else{
    $corporatepro_container_class[] = 'sidebar-'.$corporatepro_project_layout;
}
$corporatepro_content_class = array();
$corporatepro_content_class[] = 'main-content';
if( ($corporatepro_project_layout == 'full') || ($corporatepro_project_list_style == 'v7') ){
    $corporatepro_content_class[] ='col-sm-12';
}else{
    $corporatepro_content_class[] = 'col-md-9 col-sm-8';
}

$true_shop_slidebar_class = array();
$true_shop_slidebar_class[] = 'sidebar';
if( ($corporatepro_project_layout != 'full') ){
    $true_shop_slidebar_class[] = 'col-md-3 col-sm-4';
}
$title = sprintf( __( '%s','corporatepro' ), post_type_archive_title( '', false ) );
?>
<?php if ($corporatepro_check_project_banner == '1') : ?>
    <?php get_template_part('template-parts/project','banner');?>
<?php endif; ?>

<div class="<?php echo esc_attr( implode(' ', $corporatepro_container_class) ); ?>">
    <div class="container <?php if ($corporatepro_project_list_style == 'v7') { echo 'container-full-width'; } ?>">
        <div class="row">
            <div class="<?php echo esc_attr( implode(' ', $corporatepro_content_class) );?>" data-layout="<?php echo $corporatepro_project_list_style ?>">
                <?php if ( $corporatepro_project_list_style == 'v1' || $corporatepro_project_list_style == 'v3' ) : ?>
                    <?php elseif ( $corporatepro_project_list_style == 'v7' ) : ?>
                    <div class="container">
                        <div class="page-title">
                            <h2><?php echo $title; ?></h2>
                        </div>
                    </div>
                    <?php else : ?>
                    <div class="page-title">
                        <h2><?php echo $title; ?></h2>
                    </div>
                <?php endif; ?>
                <?php get_template_part('templates/projects/project',$corporatepro_project_list_style);?>
            </div>
            <?php if( $corporatepro_project_layout != "full"  ):?>
                <?php if (($corporatepro_project_list_style != 'v7')) : ?>
                <div class="<?php echo esc_attr( implode(' ', $true_shop_slidebar_class) );?>">
                    <?php dynamic_sidebar( $corporatepro_project_sidebar ); ?>
                </div>
                <?php endif;?>
            <?php endif;?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
