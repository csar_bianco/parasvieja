<?php corporatepro_get_footer();?>
<a href="#" class="back_to_top" title="<?php echo esc_attr(esc_html__('Scroll to Top','maxstoreplus'));?>">
    <i class="fa fa-angle-up"></i>
</a>
<?php wp_footer(); ?>
</body>
</html>