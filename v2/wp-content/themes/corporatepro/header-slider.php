<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package Wordpress
 * @subpackage koolshop
 * @since koolshop 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="header header-style5">
	<div class="cp-main-menu cp-nav-menusidebar">
		<span class="icon-menu"><i class="fa fa-bars"></i></span>
		<nav class="navigation">
			<?php 
				wp_nav_menu( array(
					'menu'            => 'primary',
					'theme_location'  => 'primary',
					'depth'           => 3,
					'container'       => '',
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => '',
					'fallback_cb'     => 'koolshop_bootstrap_navwalker::fallback',
					'walker'          => new koolshop_bootstrap_navwalker()
	            ));
	        ?>
		</nav>
	</div>
</header>
