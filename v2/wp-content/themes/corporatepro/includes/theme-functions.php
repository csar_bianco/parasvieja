<?php
/* Session Start*/
if ( !function_exists( 'corporatepro_StartSession' ) ) {
	function corporatepro_StartSession()
	{
		if ( !session_id() ) {
			session_start();
		}
	}
}
add_action( 'init', 'corporatepro_StartSession', 1 );

function corporatepro_init()
{
	global $corporatepro;

	define( 'CORPORATEPRO_THEME_VERSION', '1.0' );
	if ( isset( $corporatepro[ 'opt_enable_dev_mode' ] ) && $corporatepro[ 'opt_enable_dev_mode' ] == 1 ) {
		define( 'CORPORATEPRO_DEV_MODE', true );
	} else {
		define( 'CORPORATEPRO_DEV_MODE', false );
	}
}

add_action( 'init', 'corporatepro_init' );


//GET DEMO OPTIONS
add_action( 'wp', 'corporate_get_theme_options_json', 20 );
if ( !function_exists( 'corporate_get_theme_options_json' ) ) {
	function corporate_get_theme_options_json()
	{
		global $corporatepro;

		if ( !is_front_page() && defined( 'CORPORATEPRO_DEV_MODE' ) && CORPORATEPRO_DEV_MODE == true ) {
			$page_slug = get_post_field( 'post_name', get_post() );
			if ( is_page( $page_slug ) ) {
				$option_files    = get_template_directory() . '/includes/settings/theme-options/' . $page_slug . '.json';
				$option_file_url = get_template_directory_uri() . '/includes/settings/theme-options/' . $page_slug . '.json';
				if ( file_exists( $option_files ) ) {
					$option_content = wp_remote_get( $option_file_url );
					$option_content = $option_content[ 'body' ];
					if ( !empty( $option_content ) ) {
						$options_configs = json_decode( $option_content, true );
						if ( is_array( $options_configs ) && !empty( $options_configs ) ) {
							$corporatepro = $options_configs;
						}
					}
				}
			}
		}
	}
}

/* Get Option */
if ( !function_exists( 'corporatepro_get_option' ) ) {
	function corporatepro_get_option( $option = false, $default = false )
	{
		global $corporatepro;
		if ( isset( $_GET[ $option ] ) ) {
			return $_GET[ $option ];
		}
		if ( isset( $corporatepro[ $option ] ) && $corporatepro[ $option ] != '' ) {
			return $corporatepro[ $option ];
		} else {
			return $default;
		}
	}
}

if ( !function_exists( 'corporatepro_resize_image' ) ) {
	/**
	 * @param int $attach_id
	 * @param string $img_url
	 * @param int $width
	 * @param int $height
	 * @param bool $crop
	 * @param bool $place_hold Using place hold image if the image does not exist
	 * @param bool $use_real_img_hold Using real image for holder if the image does not exist
	 * @param string $solid_img_color Solid placehold image color (not text color). Random color if null
	 *
	 * @since 1.0
	 * @return array
	 */
	function corporatepro_resize_image( $attach_id = null, $img_url = null, $width, $height, $crop = false, $place_hold = true, $use_real_img_hold = true, $solid_img_color = null )
	{

		// If is singular and has post thumbnail and $attach_id is null, so we get post thumbnail id automatic (there is a bug, don't use)
		//  if ( is_singular() && !$attach_id && !$img_url ) {
		//   if ( has_post_thumbnail() && !post_password_required() ) {
		//    $attach_id = get_post_thumbnail_id();
		//   }
		//  }

		// this is an attachment, so we have the ID
		$image_src = array();
		if ( $attach_id ) {
			$image_src        = wp_get_attachment_image_src( $attach_id, 'full' );
			$actual_file_path = get_attached_file( $attach_id );
			// this is not an attachment, let's use the image url
		} else {
			if ( $img_url ) {
				$file_path        = str_replace( get_site_url(), ABSPATH, $img_url );
				$actual_file_path = rtrim( $file_path, '/' );
				if ( !file_exists( $actual_file_path ) ) {
					$file_path        = parse_url( $img_url );
					$actual_file_path = rtrim( ABSPATH, '/' ) . $file_path[ 'path' ];
				}
				if ( file_exists( $actual_file_path ) ) {
					$orig_size      = getimagesize( $actual_file_path );
					$image_src[ 0 ] = $img_url;
					$image_src[ 1 ] = $orig_size[ 0 ];
					$image_src[ 2 ] = $orig_size[ 1 ];
				} else {
					$image_src[ 0 ] = '';
					$image_src[ 1 ] = 0;
					$image_src[ 2 ] = 0;
				}
			}
		}
		if ( !empty( $actual_file_path ) && file_exists( $actual_file_path ) ) {
			$file_info = pathinfo( $actual_file_path );
			$extension = '.' . $file_info[ 'extension' ];

			// the image path without the extension
			$no_ext_path = $file_info[ 'dirname' ] . '/' . $file_info[ 'filename' ];

			$cropped_img_path = $no_ext_path . '-' . $width . 'x' . $height . $extension;

			// checking if the file size is larger than the target size
			// if it is smaller or the same size, stop right here and return
			if ( $image_src[ 1 ] > $width || $image_src[ 2 ] > $height ) {

				// the file is larger, check if the resized version already exists (for $crop = true but will also work for $crop = false if the sizes match)
				if ( file_exists( $cropped_img_path ) ) {
					$cropped_img_url = str_replace( basename( $image_src[ 0 ] ), basename( $cropped_img_path ), $image_src[ 0 ] );
					$vt_image        = array( 'url' => $cropped_img_url, 'width' => $width, 'height' => $height, );

					return $vt_image;
				}

				// $crop = false
				if ( $crop == false ) {
					// calculate the size proportionaly
					$proportional_size = wp_constrain_dimensions( $image_src[ 1 ], $image_src[ 2 ], $width, $height );
					$resized_img_path  = $no_ext_path . '-' . $proportional_size[ 0 ] . 'x' . $proportional_size[ 1 ] . $extension;

					// checking if the file already exists
					if ( file_exists( $resized_img_path ) ) {
						$resized_img_url = str_replace( basename( $image_src[ 0 ] ), basename( $resized_img_path ), $image_src[ 0 ] );

						$vt_image = array( 'url' => $resized_img_url, 'width' => $proportional_size[ 0 ], 'height' => $proportional_size[ 1 ], );

						return $vt_image;
					}
				}

				// no cache files - let's finally resize it
				$img_editor = wp_get_image_editor( $actual_file_path );

				if ( is_wp_error( $img_editor ) || is_wp_error( $img_editor->resize( $width, $height, $crop ) ) ) {
					return array( 'url' => '', 'width' => '', 'height' => '', );
				}

				$new_img_path = $img_editor->generate_filename();

				if ( is_wp_error( $img_editor->save( $new_img_path ) ) ) {
					return array( 'url' => '', 'width' => '', 'height' => '', );
				}
				if ( !is_string( $new_img_path ) ) {
					return array( 'url' => '', 'width' => '', 'height' => '', );
				}

				$new_img_size = getimagesize( $new_img_path );
				$new_img      = str_replace( basename( $image_src[ 0 ] ), basename( $new_img_path ), $image_src[ 0 ] );

				// resized output
				$vt_image = array( 'url' => $new_img, 'width' => $new_img_size[ 0 ], 'height' => $new_img_size[ 1 ], );

				return $vt_image;
			}

			// default output - without resizing
			$vt_image = array( 'url' => $image_src[ 0 ], 'width' => $image_src[ 1 ], 'height' => $image_src[ 2 ], );

			return $vt_image;
		} else {
			if ( $place_hold ) {
				$width  = intval( $width );
				$height = intval( $height );

				// Real image place hold (https://unsplash.it/)
				if ( $use_real_img_hold ) {
					$random_time = time() + rand( 1, 100000 );
					$vt_image    = array( 'url' => 'https://unsplash.it/' . $width . '/' . $height . '?random&time=' . $random_time, 'width' => $width, 'height' => $height, );
				} else {
					$color = $solid_img_color;
					if ( is_null( $color ) || trim( $color ) == '' ) {

						// Show no image (gray)
						//$vt_image = array( 'url' => corporatepro_no_image( array( 'width' => $width, 'height' => $height ) ), 'width' => $width, 'height' => $height, );
						$vt_image = array( 'url' => 'http://placehold.it/' . $width . 'x' . $height, 'width' => $width, 'height' => $height, );
					} else {
						if ( $color == 'transparent' ) { // Show no image transparent
							//$vt_image = array( 'url' => corporatepro_no_image( array( 'width' => $width, 'height' => $height ), false, true ), 'width' => $width, 'height' => $height, );
							$vt_image = array( 'url' => 'http://placehold.it/' . $width . 'x' . $height, 'width' => $width, 'height' => $height, );
						} else { // No image with color from placehold.it
							$vt_image = array( 'url' => 'http://placehold.it/' . $width . 'x' . $height, 'width' => $width, 'height' => $height, );
						}
					}
				}

				return $vt_image;
			}
		}

		return false;
	}
}
if ( !function_exists( 'corporatepro_no_image' ) ) {
	/**
	 * No image generator
	 *
	 * @since 1.0
	 *
	 * @param $size : array, image size
	 * @param $echo : bool, echo or return no image url
	 **/
	function corporatepro_no_image( $size = array( 'width' => 500, 'height' => 500 ), $echo = false, $transparent = false )
	{

		$noimage_dir = get_template_directory();
		$noimage_uri = get_template_directory_uri();

		$suffix = ( $transparent ) ? '_transparent' : '';

		if ( !is_array( $size ) || empty( $size ) ):
			$size = array( 'width' => 500, 'height' => 500 );
		endif;

		if ( !is_numeric( $size[ 'width' ] ) && $size[ 'width' ] == '' || $size[ 'width' ] == null ):
			$size[ 'width' ] = 'auto';
		endif;

		if ( !is_numeric( $size[ 'height' ] ) && $size[ 'height' ] == '' || $size[ 'height' ] == null ):
			$size[ 'height' ] = 'auto';
		endif;

		// base image must be exist
		$img_base_fullpath = $noimage_dir . '/images/noimage/no_image' . $suffix . '.png';
		$no_image_src      = $noimage_uri . '/images/noimage/no_image' . $suffix . '.png';


		// Check no image exist or not
		if ( !file_exists( $noimage_dir . '/images/noimage/no_image' . $suffix . '-' . $size[ 'width' ] . 'x' . $size[ 'height' ] . '.png' ) && is_writable( $noimage_dir . '/assets/images/noimage/' ) ):

			$no_image = wp_get_image_editor( $img_base_fullpath );

			if ( !is_wp_error( $no_image ) ):
				$no_image->resize( $size[ 'width' ], $size[ 'height' ], true );
				$no_image_name = $no_image->generate_filename( $size[ 'width' ] . 'x' . $size[ 'height' ], $noimage_dir . '/images/noimage/', null );
				$no_image->save( $no_image_name );
			endif;

		endif;

		// Check no image exist after resize
		$noimage_path_exist_after_resize = $noimage_dir . '/images/noimage/no_image' . $suffix . '-' . $size[ 'width' ] . 'x' . $size[ 'height' ] . '.png';

		if ( file_exists( $noimage_path_exist_after_resize ) ):
			$no_image_src = $noimage_uri . '/images/noimage/no_image' . $suffix . '-' . $size[ 'width' ] . 'x' . $size[ 'height' ] . '.png';
		endif;

		if ( $echo ):
			echo esc_url( $no_image_src );
		else:
			return esc_url( $no_image_src );
		endif;

	}
}


// POST THUMBNAIL
if ( !function_exists( 'corporatepro_post_thumbnail' ) ) {
	function corporatepro_post_thumbnail()
	{

		$blog_list_style = corporatepro_get_option( 'opt_blog_list_style', 'standard' );


		$thumb_w = 1170;
		$thumb_h = 854;
		$crop    = true;
		if ( $blog_list_style == 'standard' && $blog_list_style == 'list' ) {
			$thumb_w = 680;
			$thumb_h = 496;
		}
		if ( $blog_list_style == 'grid' ) {
			$thumb_w = 370;
			$thumb_h = 370;
		}
		if ( $blog_list_style == 'grid2' ) {
			$thumb_w = 420;
			$thumb_h = 210;
		}
		if ( $blog_list_style == 'list2' ) {
			$thumb_w = 1170;
			$thumb_h = 585;
		}
		if ( $blog_list_style == 'list3' ) {
			$thumb_w = 700;
			$thumb_h = 400;
		}
		if ( $blog_list_style == 'masonry' || $blog_list_style == 'masonry2' ) {
			$thumb_w = 400;
			$thumb_h = 400;
			$crop    = false;
		}

		if ( is_single() ) {

		}
		$image = corporatepro_resize_image( get_post_thumbnail_id(), null, $thumb_w, $thumb_h, $crop, true, false );
		?>
        <img width="<?php echo esc_attr( $image[ 'width' ] ); ?>" height="<?php echo esc_attr( $image[ 'height' ] ); ?>"
             class="attachment-post-thumbnail wp-post-image" src="<?php echo esc_attr( $image[ 'url' ] ) ?>"
             alt="<?php the_title(); ?>"/>
		<?php
	}
}
/* Get Header */
if ( !function_exists( 'corporatepro_get_header' ) ) {
	function corporatepro_get_header()
	{
		$opt_header_layout = corporatepro_get_option( 'opt_header_layout', 'style-01' );
		ob_start();
		get_template_part( 'templates/headers/header', $opt_header_layout );
		$html = ob_get_clean();
		echo $html;
	}
}
/* Get Logo*/
if ( !function_exists( 'corporatepro_get_logo' ) ) {
	function corporatepro_get_logo()
	{
		$opt_general_logo = corporatepro_get_option( "opt_general_logo", array() );
		if ( is_array( $opt_general_logo ) ) {
			if ( isset( $opt_general_logo[ 'url' ] ) && $opt_general_logo[ 'url' ] != "" ) {
				$html = '<a href="' . esc_url( get_home_url() ) . '"><img alt="' . esc_attr( get_bloginfo( 'name' ) ) . '" src="' . esc_url( $opt_general_logo[ 'url' ] ) . '" class="_rw" /></a>';
			} else {
				$html = '<a href="' . esc_url( get_home_url() ) . '"><img alt="' . esc_attr( get_bloginfo( 'name' ) ) . '" src="' . esc_url( get_template_directory_uri() . '/images/logo.png' ) . '" class="_rw" /></a>';
			}
		}

		echo apply_filters( 'corporatepro_site_logo', $html );
	}
}

/* Get search form */
if ( !function_exists( 'corporatepro_get_search_form' ) ) {
	function corporatepro_get_search_form()
	{
		get_template_part( 'template-parts/search', 'form' );
	}
}

if ( !function_exists( 'corporatepro_paging_nav' ) ) :
	/**
	 * Display navigation to next/previous set of posts when applicable.
	 *
	 * @since Boutique 1.0
	 *
	 * @global WP_Query $wp_query WordPress Query object.
	 * @global WP_Rewrite $wp_rewrite WordPress Rewrite object.
	 */
	function corporatepro_paging_nav()
	{
		global $wp_query, $wp_rewrite;
		$opt_blog_loadmore = corporatepro_get_option( 'opt_blog_loadmore', '0' );
		$opt_style_post    = corporatepro_get_option( 'opt_blog_list_style' );

		if ( $opt_blog_loadmore == '1' ) {
			?>
			<?php if ( $opt_style_post == 'masonry' || $opt_style_post == 'masonry2' ) : ?>
                <div id="pj-loadmore" class="pj-loadmore">
                    <div id="loadmore-posts-result"></div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                            <a id="cp-loadmore-masonry-posts" href="#"
                               class="cp-button button-block button-larger button-loadmore"><?php esc_html_e( 'Load more item', 'corporatepro' ); ?></a>
                        </div>
                    </div>
                </div>
			<?php else : ?>
                <div id="pj-loadmore" class="pj-loadmore">
                    <div id="loadmore-posts-result"></div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                            <a id="cp-button-loadmore-posts" href="#"
                               class="cp-button button-block button-larger button-loadmore"><?php esc_html_e( 'Load more item', 'corporatepro' ); ?></a>
                        </div>
                    </div>
                </div>
			<?php endif; ?>
			<?php
		} else {
			// Don't print empty markup if there's only one page.
			if ( $wp_query->max_num_pages < 2 ) {
				return;
			}
			echo get_the_posts_pagination( array(
				'screen_reader_text' => '&nbsp;',
				'before_page_number' => '',
			)
			);
		}

	}
endif;

if ( !function_exists( 'corporatepro_project_paging_nav' ) ) :
	/**
	 * Display navigation to next/previous set of posts when applicable.
	 *
	 * @since Boutique 1.0
	 *
	 * @global WP_Query $wp_query WordPress Query object.
	 * @global WP_Rewrite $wp_rewrite WordPress Rewrite object.
	 */
	function corporatepro_project_paging_nav()
	{
		global $wp_query, $wp_rewrite;
		$opt_project_loadmore = corporatepro_get_option( 'opt_project_loadmore', '0' );
		$opt_style_project    = corporatepro_get_option( 'opt_project_list_style' );

		if ( $opt_project_loadmore == '1' ) {
			?>
			<?php if ( $opt_style_project == 'v1' || $opt_style_project == 'v3' ) : ?>
                <div id="pj-loadmore" class="pj-loadmore">
                    <div id="loadmore-posts-result"></div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                            <a id="cp-loadmore-masonry" href="#"
                               class="cp-button button-block button-larger button-loadmore"><?php esc_html_e( 'Load more item', 'corporatepro' ); ?></a>
                        </div>
                    </div>
                </div>
			<?php elseif ( $opt_style_project == 'v7' ) : ?>
                <div id="pj-loadmore" class="pj-loadmore">
                    <div id="loadmore-posts-result"></div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                            <a id="cp-button-loadmore-posts" href="#"
                               class="cp-button button-block button-larger button-loadmore"><?php esc_html_e( 'Load more item', 'corporatepro' ); ?></a>
                        </div>
                    </div>
                </div>
			<?php else : ?>
                <div id="pj-loadmore" class="pj-loadmore">
                    <div id="loadmore-post-result"></div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                            <a id="cp-button-loadmore-project" href="#"
                               class="cp-button button-block button-larger button-loadmore"><?php esc_html_e( 'Load more item', 'corporatepro' ); ?></a>
                        </div>
                    </div>
                </div>
			<?php endif; ?>
			<?php
		} else {
			// Don't print empty markup if there's only one page.
			if ( $wp_query->max_num_pages < 2 ) {
				return;
			}
			echo get_the_posts_pagination( array(
				'screen_reader_text' => '&nbsp;',
				'before_page_number' => '',
			)
			);
		}

	}
endif;

add_filter( 'navigation_markup_template', 'corporatepro_navigation_markup_template', 2, 99 );
if ( !function_exists( 'corporatepro_navigation_markup_template' ) ) {
	function corporatepro_navigation_markup_template( $template, $class )
	{
		$template = '
        <div class="cp-pagination text-left" role="navigation">
            %3$s
        </div>';

		return $template;
	}
}

if ( !function_exists( 'corporatepro_custom_comment' ) ) {
	function corporatepro_custom_comment( $comment, $args, $depth )
	{
		$GLOBALS[ 'comment' ] = $comment;
		extract( $args, EXTR_SKIP );
		if ( 'div' == $args[ 'style' ] ) {
			$tag       = 'div';
			$add_below = 'comment item';
		} else {
			$tag       = 'li';
			$add_below = 'div-comment';
		}

		?>
        <<?php echo esc_attr( $tag ); ?><?php comment_class( 'comment-item' ) ?> id="comment-<?php comment_ID() ?>">
		<?php if ( 'div' != $args[ 'style' ] ) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
        <div class="comment-content">
            <h5 class="author-name"><?php echo get_comment_author(); ?> - </h5>
			<?php get_comment_author_link(); ?>
            <span class="author-pos"><?php printf( esc_html__( '%1$s at %2$s', 'corporatepro' ), get_comment_date( 'M j' ), get_comment_time() ); ?></span>
			<?php if ( $comment->comment_approved == '0' ) : ?>
                <div>
                    <em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'corporatepro' ); ?></em>
                </div>
			<?php endif; ?>
            <div class="comment-text"><?php comment_text(); ?></div>
            <div class="reply">
				<?php edit_comment_link( esc_html__( 'Edit', 'corporatepro' ), '  ', '' ); ?>
				<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args[ 'max_depth' ] ) ) ); ?>
            </div>
        </div>
		<?php if ( 'div' != $args[ 'style' ] ) : ?>
        </div>
	<?php endif; ?>
		<?php
	}
}

if ( !function_exists( 'corporatepro_full_size_gallery' ) ) {
	function corporatepro_full_size_gallery( $out )
	{
		$out[ 'size' ] = 'full';  // Edit this to your needs! (thumbnail, medium, large, ...)

		return $out;
	}
}

if ( !function_exists( 'corporatepro_get_footer' ) ) {
	function corporatepro_get_footer()
	{
		$opt_footer_style   = corporatepro_get_option( 'opt_footer_style', '' );
		$opt_template_style = corporatepro_get_post_meta( $opt_footer_style, '_corporatepro_template_style', '' );
		ob_start();
		$query = new WP_Query( array( 'p' => $opt_footer_style, 'post_type' => 'footer', 'posts_per_page' => 1 ) );
		if ( $query->have_posts() ):
			while ( $query->have_posts() ): $query->the_post(); ?>
				<?php if ( $opt_template_style == 'default' ): ?>
                    <footer class="footer footer-style1">
                        <div class="container">
							<?php the_content(); ?>
                        </div>
                    </footer>
				<?php else: ?>
					<?php get_template_part( 'templates/footers/footer', $opt_template_style ); ?>
				<?php endif; ?>
			<?php endwhile;
		endif;
		wp_reset_postdata();
		echo ob_get_clean();
	}
}
if ( !function_exists( 'corporatepro_get_post_meta' ) ) {
	/**
	 * Function get post meta
	 *
	 * @since corporatepro 1.0
	 * @author Kutethemes
	 */
	function corporatepro_get_post_meta( $post_id, $key, $default = "" )
	{
		$meta = get_post_meta( $post_id, $key, true );
		if ( $meta ) {
			return $meta;
		}

		return $default;
	}
}

if ( !function_exists( 'corporatepro_get_all_social' ) ) {
	function corporatepro_get_all_social()
	{
		$socials = array(
			'opt_twitter_link'     => array(
				'name' => 'Twitter',
				'id'   => 'opt_twitter_link',
				'icon' => '<i class="fa fa-twitter"></i>',
			),
			'opt_fb_link'          => array(
				'name' => 'Facebook',
				'id'   => 'opt_fb_link',
				'icon' => '<i class="fa fa-facebook"></i>',
			),
			'opt_google_plus_link' => array(
				'name' => 'Google plus',
				'id'   => 'opt_google_plus_link',
				'icon' => '<i class="fa fa-google-plus" aria-hidden="true"></i>',
			),
			'opt_dribbble_link'    => array(
				'name' => 'Dribbble',
				'id'   => 'opt_dribbble_link',
				'icon' => '<i class="fa fa-dribbble" aria-hidden="true"></i>',
			),
			'opt_behance_link'     => array(
				'name' => 'Behance',
				'id'   => 'opt_behance_link',
				'icon' => '<i class="fa fa-behance" aria-hidden="true"></i>',
			),
			'opt_tumblr_link'      => array(
				'name' => 'Tumblr',
				'id'   => 'opt_tumblr_link',
				'icon' => '<i class="fa fa-tumblr" aria-hidden="true"></i>',
			),
			'opt_instagram_link'   => array(
				'name' => 'Instagram',
				'id'   => 'opt_instagram_link',
				'icon' => '<i class="fa fa-instagram" aria-hidden="true"></i>',
			),
			'opt_pinterest_link'   => array(
				'name' => 'Pinterest',
				'id'   => 'opt_pinterest_link',
				'icon' => '<i class="fa fa-pinterest" aria-hidden="true"></i>',
			),
			'opt_youtube_link'     => array(
				'name' => 'Youtube',
				'id'   => 'opt_youtube_link',
				'icon' => '<i class="fa fa-youtube" aria-hidden="true"></i>',
			),
			'opt_vimeo_link'       => array(
				'name' => 'Vimeo',
				'id'   => 'opt_vimeo_link',
				'icon' => '<i class="fa fa-vimeo" aria-hidden="true"></i>',
			),
			'opt_linkedin_link'    => array(
				'name' => 'Linkedin',
				'id'   => 'opt_linkedin_link',
				'icon' => '<i class="fa fa-linkedin" aria-hidden="true"></i>',
			),
			'opt_rss_link'         => array(
				'name' => 'RSS',
				'id'   => 'opt_rss_link',
				'icon' => '<i class="fa fa-rss" aria-hidden="true"></i>',
			),
		);

		return $socials;
	}
}
if ( !function_exists( 'corporatepro_social' ) ) {
	function corporatepro_social( $social = '' )
	{
		$all_social  = corporatepro_get_all_social();
		$social_link = corporatepro_get_option( $social, '' );
		$social_icon = $all_social[ $social ][ 'icon' ];
		$social_name = $all_social[ $social ][ 'name' ];
		echo balanceTags( '<a class="' . $social_name . '" target="_blank" href="' . esc_url( $social_link ) . '" title ="' . esc_attr( $social_name ) . '" >' . $social_icon . '<span class="text">' . $social_name . '</span></a>' );
	}
}
if ( !function_exists( 'corporatepro_rev_slide_options_for_redux' ) ) {
	function corporatepro_rev_slide_options_for_redux()
	{
		$corporatepro_herosection_revolutions = array( '' => esc_html__( '--- Choose Revolution Slider ---', 'corporatepro' ) );
		if ( class_exists( 'RevSlider' ) ) {
			global $wpdb;
			if ( shortcode_exists( 'rev_slider' ) ) {
				$rev_sql  = $wpdb->prepare(
					"SELECT *
                FROM {$wpdb->prefix}revslider_sliders
                WHERE %d", 1
				);
				$rev_rows = $wpdb->get_results( $rev_sql );
				if ( count( $rev_rows ) > 0 ) {
					foreach ( $rev_rows as $rev_row ):
						$corporatepro_herosection_revolutions[ $rev_row->alias ] = $rev_row->title;
					endforeach;
				}
			}
		}

		return $corporatepro_herosection_revolutions;
	}
}

class Team_Post_Type_Metaboxes
{
	public function init()
	{
		add_action( 'add_meta_boxes', array( $this, 'ratio_img_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 10, 2 );
	}

	/**
	 * Register the metaboxes to be used for the team post type
	 *
	 * @since 0.1.0
	 */
	public function ratio_img_meta_boxes()
	{
		add_meta_box( "ratio_image_fields", "Select Image Ratio", array( $this, 'render_meta_boxes' ), "post", "side", "high", null );
		add_meta_box( "ratio_image_fields", "Select Image Ratio", array( $this, 'render_meta_boxes' ), "portfolio", "side", "high", null );
	}

	/**
	 * The HTML for the fields
	 *
	 * @since 0.1.0
	 */
	function render_meta_boxes( $post )
	{
		$meta     = get_post_custom( $post->ID );
		$selected = isset( $meta[ 'image_ratio_post_select' ] ) ? esc_attr( $meta[ 'image_ratio_post_select' ][ 0 ] ) : 'img1x1';
		wp_nonce_field( basename( __FILE__ ), 'ratio_image_fields' ); ?>

        <p>
            <label
                    for="image_ratio_post_select"><?php echo esc_html__( 'Select size image.', 'corporatepro' ); ?></label>
            <br>
            <select name="image_ratio_post_select" id="image_ratio_post_select">
                <option
                        value="img1x1" <?php selected( $selected, 'img1x1' ); ?>><?php echo esc_html__( 'Ratio 1:1', 'corporatepro' ); ?></option>
                <option
                        value="img2x1" <?php selected( $selected, 'img2x1' ); ?>><?php echo esc_html__( 'Ratio 2:1', 'corporatepro' ); ?></option>
                <option
                        value="img2x3" <?php selected( $selected, 'img2x3' ); ?>><?php echo esc_html__( 'Ratio 2:3', 'corporatepro' ); ?></option>
                <option
                        value="img3x2" <?php selected( $selected, 'img3x2' ); ?>><?php echo esc_html__( 'Ratio 3:2', 'corporatepro' ); ?></option>
            </select>
        </p>
	<?php }

	/**
	 * Save metaboxes
	 *
	 * @since 0.1.0
	 */
	function save_meta_boxes( $post_id )
	{
		global $post;
		// Verify nonce
		if ( !isset( $_POST[ 'ratio_image_fields' ] ) || !wp_verify_nonce( $_POST[ 'ratio_image_fields' ], basename( __FILE__ ) ) ) {
			return $post_id;
		}
		// Check Autosave
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) || isset( $_REQUEST[ 'bulk_edit' ] ) ) {
			return $post_id;
		}
		// Don't save if only a revision
		if ( isset( $post->post_type ) && $post->post_type == 'revision' ) {
			return $post_id;
		}
		// Check permissions
		if ( !current_user_can( 'edit_post', $post->ID ) ) {
			return $post_id;
		}
		$meta[ 'image_ratio_post_select' ] = ( isset( $_POST[ 'image_ratio_post_select' ] ) ? esc_textarea( $_POST[ 'image_ratio_post_select' ] ) : '' );
		foreach ( $meta as $key => $value ) {
			update_post_meta( $post->ID, $key, $value );
		}
	}
}

// Initialize metaboxes
$post_type_metaboxes = new Team_Post_Type_Metaboxes;
$post_type_metaboxes->init();

if ( class_exists( 'Vc_Manager' ) ) {
	function change_vc_row()
	{
		$args = array(
			array(
				"type"       => "checkbox",
				"group"      => "Additions",
				"class"      => "",
				"heading"    => __( 'Setting background: ', 'corporatepro' ),
				"param_name" => "half_background",
				"value"      => array(
					__( 'Yes', 'corporatepro' ) => "yes",
				),
			),
			array(
				'type'       => 'dropdown',
				"group"      => "Additions",
				'value'      => array(
					__( '50%', 'maxstoreplus' )  => '',
					__( '60%', 'maxstoreplus' )  => '60',
					__( '70%', 'maxstoreplus' )  => '70',
					__( '80%', 'maxstoreplus' )  => '80',
					__( '90%', 'maxstoreplus' )  => '90',
					__( '100%', 'maxstoreplus' ) => '100',
				),
				'std'        => '',
				'heading'    => __( 'Width Background', 'maxstoreplus' ),
				'param_name' => 'width_bg',
				'dependency' => array(
					'element' => 'half_background',
					'value'   => array( 'yes' ),
				),
			),
			array(
				"type"       => "checkbox",
				"group"      => "Additions",
				"class"      => "",
				"heading"    => __( 'Using Parallax?: ', 'corporatepro' ),
				"param_name" => "parallax_theme",
				"value"      => array(
					__( 'Yes', 'corporatepro' ) => "bg-parallax",
				),
			),
			array(
				"type"       => "checkbox",
				"group"      => "Additions",
				"class"      => "",
				"heading"    => __( 'Using Overlay? ', 'corporatepro' ),
				"param_name" => "overlay",
				"value"      => array(
					__( 'Yes', 'corporatepro' ) => "cp-overlay",
				),
			),
			array(
				'type'        => 'colorpicker',
				"group"       => "Additions",
				"class"       => "",
				'heading'     => __( 'Color Overlay: ', 'corporatepro' ),
				'param_name'  => 'color_overlay',
				'value'       => 'rgba(0,49,67,0.5)',
				'dependency'  => array(
					'element' => 'overlay',
					'value'   => array( 'cp-overlay' ),
				),
				'description' => __( 'Select Overlay color.', 'corporatepro' ),
			),
			array(
				"type"       => "checkbox",
				"group"      => "Additions",
				"class"      => "",
				"heading"    => __( 'Using scroll-button?: ', 'corporatepro' ),
				"param_name" => "scroll_btn",
				"value"      => array(
					__( 'Yes', 'corporatepro' ) => "scroll-slide-f scroll-button",
				),
			),
		);
		foreach ( $args as $value ) {
			vc_add_param( "vc_row", $value );
			vc_add_param( "vc_section", $value );
		}
	}

	change_vc_row();
	get_template_part( 'vc_templates/vc_row.php' );
	get_template_part( 'vc_templates/vc_section.php' );
}
