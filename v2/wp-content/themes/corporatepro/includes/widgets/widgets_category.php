<?php
// widgets categories
class Widgets_categories_Post extends WP_Widget
{
	function __construct() {
		$widget_ops = array( 'classname' => '', 'description' => 'Categories post ...' );
		$control_ops = array( 'width' => 400, 'height' => 0 );
		parent::__construct(
			'Widgets_categories_Post',
			esc_html__( 'Corporate - Project Categories', 'corporatepro' ),
			$widget_ops, $control_ops
		);
	}

	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );

		echo $before_widget;
		$title = empty( $instance['title'] ) ? '' : apply_filters( 'widget_title', $instance['title'] );
		$order = empty( $instance['order'] ) ? 'ASC' : strtoupper( $instance['order'] ); // ASC OR DESC
		
        // <!-- Widget Categories -->
        $terms = get_terms( array(
		    'taxonomy' 		=> 'category_project',
		    'order' 		=> $order,
		    'count'			=> false,
		));
		?>
			<div class="widget widget_categories">
				<h3 class="title-widget"><?php echo $title ?></h3>
				<ul>
					<?php  
						foreach ( $terms as $term ) {
 
						    // The $term is an object, so we don't need to specify the $taxonomy.
						    $term_link = get_term_link( $term );
						    
						    // If there was an error, continue to the next term.
						    if ( is_wp_error( $term_link ) ) {
						        continue;
						    }
						 
						    // We successfully got a link. Print it out.
						    echo '<li class="cat-item">
							    <a href="' . esc_url( $term_link ) . '">' . $term->name . '</a>
							    <span class="number">' . $term->count . '</span>
						    </li>';
						}	
					?>
					<!-- <li class="cat-item"><a href="#">Investment</a><span class="number">09</span></li> -->
				</ul>
			</div>
		<?php
        // <!--End Widget Categories -->

		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['order'] = strtoupper( $new_instance['order'] );

		return $instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array)$instance, array( 'title' => 'categories', 'order' => 'ASC' ) );
		$title = strip_tags( $instance['title'] );
		$order = strtoupper( $instance['order'] );

		?>
		<p>
			<label
				for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title', 'corporatepro' ); ?>
				:</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
			       name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
			       value="<?php echo esc_attr( $title ); ?>"/>
		</p>
		<p>
			<label
				for="<?php echo $this->get_field_id( 'order' ); ?>"><?php esc_html_e( 'Order', 'corporatepro' ); ?>
				:</label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'order' ); ?>"
			        name="<?php echo $this->get_field_name( 'order' ); ?>">
			    <option <?php selected( $order == 'ASC' ); ?>
					value="ASC"><?php esc_html_e( 'ASC', 'corporatepro' ); ?></option>
				<option <?php selected( $order == 'DESC' ); ?>
					value="DESC"><?php esc_html_e( 'DESC', 'corporatepro' ); ?></option>
			</select>
		</p>
		<?php
	}
}
add_action('widgets_init', create_function('', 'return register_widget("Widgets_categories_Post");'));