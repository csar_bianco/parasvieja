<?php
/**
 * Register the widget with WordPress
 */
add_action( 'widgets_init', function(){
  register_widget( 'faqs_widgets' );
});

class faqs_widgets extends WP_Widget {

  	/**
   * Mandatory constructor needs to call the parent constructor with the
   * following params: id (if false, one will be generated automatically),
   * the title of the widget (can be translated, of course), and last, params
   * to further configure the widget.
   * see https://codex.wordpress.org/Widgets_API for more info
   */
  	public function __construct() {
	    parent::__construct(
	      	false,
	      	__('Corporate - FAQs','corporatepro'),
	      	array( 'description' => 'FAQs Widget' )
	    );
  	}

  	/**
   	* Renders the widget to the visitors
   	*/
  	public function widget( $args, $instance ) {

    $header = apply_filters( 'widget_title', empty( $instance['header'] ) ? '' : $instance['header'], $instance, $this->id_base ); ?>
		<div class="widget widget-accordion accordion-sidebar">
			<h3 class="title-widget"><?php echo $header; ?></h3>
			<div class="cp-acordion" data-tab-active="0">
			<?php foreach ( $instance['faqs'] as $faq ): ?>
				<h3><?php echo $faq['question']; ?></h3>
				<div class="acordion-content">
					<p><?php echo $faq['answer']; ?></p>
				</div>
		    <?php endforeach; ?>
			</div>
		</div>

    <?php
  	}

  /**
   * Sanitizes the widget input before saving the data
   */
  public function update( $new_instance, $old_instance ) {

    $instance               = array();
    $instance['header']     = wp_kses_post( $new_instance['header'] );
    $instance['faqs'] 		= $new_instance['faqs'];

    return $instance;

  }

  /**
   * The most important function, used to show the widget form in the wp-admin
   */
  public function form( $instance ) {

    $header = empty( $instance['header'] ) ? 'FAQs' : $instance['header'];

    $faqs = isset( $instance['faqs'] )
      ? array_values( $instance['faqs'] )
      : array( array( 'id' => 1, 'answer' => '', 'question' => '' ) );

    ?>

    <!-- segment #1 -->
    <p>
      	<label for="<?php echo $this->get_field_id( 'header' ); ?>"><?php echo esc_html__('Title:','corporatepro') ?></label>
      	<input class="widefat" id="<?php echo $this->get_field_id( 'header' ); ?>" name="<?php echo $this->get_field_name( 'header' ); ?>" type="text" value="<?php esc_attr( $header ); ?>" />
    </p>

    <!-- segment #2 -->
    <script type="text/template" id="js-faq-<?php echo $this->id; ?>">
    	<p>
	        <label for="<?php echo $this->get_field_id( 'faqs' ); ?>-<%- id %>-question">
	        	<?php echo esc_html__('Question:','corporatepro') ?>
	        </label>
	        <input class="widefat" id="<?php echo $this->get_field_id( 'faqs' ); ?>-<%- id %>-question" name="<?php echo $this->get_field_name( 'faqs' ); ?>[<%- id %>][question]" type="text" value="<%- question %>" />
      	</p>
      	<p>
	        <label for="<?php echo $this->get_field_id( 'faqs' ); ?>-<%- id %>-answer">
	        	<?php echo esc_html__('Answer:','corporatepro') ?>
	        </label>
	        <textarea rows="4" class="widefat" id="<?php echo $this->get_field_id( 'faqs' ); ?>-<%- id %>-answer" name="<?php echo $this->get_field_name( 'faqs' ); ?>[<%- id %>][answer]"><%- answer %></textarea>
      	</p>
      	<p>
        <input name="<?php echo $this->get_field_name( 'faqs' ); ?>[<%- id %>][id]" type="hidden" value="<%- id %>" />
	        <button class="js-remove-faq"><span class="dashicons dashicons-dismiss"></span>
	        <?php echo esc_html__('Remove FAQs','corporatepro') ?>
	        </button>
      	</p>
    </script>

    <!-- segment #3 -->
    <div id="js-faqs-<?php echo $this->id; ?>">
      <div id="js-faqs-list" style="padding: 0px 15px; background: #fafafa;"></div>
      <p>
        <botton class="button" id="js-faqs-add"><?php echo esc_html__('Add New FAQs','corporatepro') ?></botton>
      </p>
    </div>

    <!-- segment #4 -->
    <script type="text/javascript">
      	var faqsJSON = <?php echo json_encode( $faqs ) ?>;
      	myWidgets.repopulatefaqs( '<?php echo $this->id; ?>', faqsJSON );
    </script>

    <?php
  }

}
