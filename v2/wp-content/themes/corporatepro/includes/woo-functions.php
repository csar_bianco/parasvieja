<?php
/*Remove woocommerce_template_loop_product_link_open */
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
/*Remove each style one by one*/
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
add_action( 'woocommerce_shop_loop_item_title', 'corporatepro_template_loop_product_title', 10 );

/*Custom product thumb*/
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'corporatepro_template_loop_product_thumbnail', 10 );

// Remove the sorting dropdown from Woocommerce
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
// Remove the result count from WooCommerce
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
add_action( 'woocommerce_before_shop_loop', 'corporatepro_shop_control', 10 );
add_action( 'corporatepro_before_main_content', 'corporatepro_shop_banners', 1 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

/* Category */
remove_action( 'woocommerce_before_subcategory_title', 'woocommerce_template_loop_category_link_open', 10 );
remove_action( 'woocommerce_after_subcategory', 'woocommerce_template_loop_category_link_close', 10 );

remove_action( 'woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail', 10 );
add_action( 'woocommerce_before_subcategory_title', 'corporatepro_woocommerce_subcategory_thumbnail', 10 );

remove_action( 'woocommerce_shop_loop_subcategory_title', 'woocommerce_template_loop_category_title', 10 );
add_action( 'woocommerce_shop_loop_subcategory_title', 'corporatepro_woocommerce_template_loop_category_title', 10 );
add_action( 'woocommerce_shop_loop_subcategory_title', 'corporatepro_woocommerce_template_loop_category_button', 20 );

/*Custom product per page*/
add_filter( 'loop_shop_per_page', 'corporatepro_loop_shop_per_page', 20 );
add_filter( 'woof_products_query', 'corporatepro_woof_products_query', 20 );


/* SINGLE PRODUCT */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 21 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 15 );


remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'woocommerce_after_single_product', 'woocommerce_upsell_display', 15 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
add_action( 'woocommerce_after_single_product', 'woocommerce_output_related_products', 20 );


if ( !function_exists( 'corporatepro_template_loop_product_title' ) ) {
	function corporatepro_template_loop_product_title()
	{
		$title_class = array( 'title-product pj-title' );
		?>
        <h3 class="<?php echo esc_attr( implode( ' ', $title_class ) ); ?>"><a
                    href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php
	}
}

if ( !function_exists( 'corporatepro_template_loop_product_thumbnail' ) ) {
	function corporatepro_template_loop_product_thumbnail()
	{
		global $product;
		$kt_enable_lazy = corporatepro_get_option( 'kt_enable_lazy', 'no' );

		$thumb_inner_class = array( 'thumb-inner' );
		/*GET SIZE IMAGE SETTING*/
		$w    = 400;
		$h    = 400;
		$crop = true;
		$size = wc_get_image_size( 'shop_catalog' );
		if ( $size ) {
			$w = $size[ 'width' ];
			$h = $size[ 'height' ];
			if ( !$size[ 'crop' ] ) {
				$crop = false;
			}
		}
		$w = apply_filters( 'koolshop_shop_pruduct_thumb_width', $w );
		$h = apply_filters( 'koolshop_shop_pruduct_thumb_height', $h );

		ob_start();
		?>
        <div class="<?php echo esc_attr( implode( ' ', $thumb_inner_class ) ); ?>">
            <a class="thumb-link" href="<?php the_permalink(); ?>">
				<?php
				$image_thumb = corporatepro_resize_image( get_post_thumbnail_id( $product->get_id() ), null, $w, $h, $crop, true, false );
				?>
				<?php if ( $kt_enable_lazy == 'yes' ): ?>
                    <img data-original="<?php echo esc_attr( $image_thumb[ 'url' ] ) ?>"
                         data-src="<?php echo esc_attr( $image_thumb[ 'url' ] ) ?>"
                         width="<?php echo esc_attr( $image_thumb[ 'width' ] ); ?>"
                         height="<?php echo esc_attr( $image_thumb[ 'height' ] ); ?>"
                         class="attachment-post-thumbnail wp-post-image kt-lazy owl-lazy"
                         src="<?php echo esc_url( get_template_directory_uri() . '/images/blank_1x1.gif' ); ?>" alt=""/>
				<?php else: ?>
                    <img width="<?php echo esc_attr( $image_thumb[ 'width' ] ); ?>"
                         height="<?php echo esc_attr( $image_thumb[ 'height' ] ); ?>"
                         class="attachment-post-thumbnail wp-post-image"
                         src="<?php echo esc_attr( $image_thumb[ 'url' ] ) ?>" alt=""/>
				<?php endif; ?>
            </a>
        </div>
		<?php
		echo ob_get_clean();
	}
}

if ( !function_exists( 'corporatepro_woocommerce_subcategory_thumbnail' ) ) {
	function corporatepro_woocommerce_subcategory_thumbnail( $category )
	{
		/*GET SIZE IMAGE SETTING*/
		$w    = 470;
		$h    = 400;
		$crop = true;

		$w            = apply_filters( 'koolshop_shop_category_thumb_width', $w );
		$h            = apply_filters( 'koolshop_shop_category_thumb_height', $h );
		$thumbnail_id = get_term_meta( $category->term_id, 'thumbnail_id', true );
		$image_thumb  = corporatepro_resize_image( $thumbnail_id, null, $w, $h, $crop, true, false );
		?>
        <div class="img-cat">
            <figure>
                <img width="<?php echo esc_attr( $image_thumb[ 'width' ] ); ?>"
                     height="<?php echo esc_attr( $image_thumb[ 'height' ] ); ?>"
                     class="attachment-post-thumbnail wp-post-image"
                     src="<?php echo esc_attr( $image_thumb[ 'url' ] ) ?>" alt=""/>
            </figure>
        </div>
		<?php
	}
}

add_action( 'woocommerce_before_shop_loop_item_title', 'corporatepro_shop_loop_item_contdown', 20 );

if ( !function_exists( 'corporatepro_shop_loop_item_contdown' ) ) {
	function corporatepro_shop_loop_item_contdown()
	{
		global $product;
		$date = corporatepro_get_max_date_sale( $product->get_id() );
		if ( $date > 0 ):
			$time = date( "Y/m/d H:i:s", $date );
			?>
            <div class="cp-countdown" data-time="<?php echo esc_attr( $time ); ?>">
                <span class="icon"><i class="fa fa-clock-o"></i></span>
                <span class="hour"></span> :
                <span class="minute"></span> :
                <span class="second"></span>&nbsp;
                <span class="text"><?php esc_html_e( 'Left', 'corporatepro' ); ?></span>
            </div>
		<?php endif;
	}
}
// GET DATE SALE
if ( !function_exists( 'corporatepro_get_max_date_sale' ) ) {
	function corporatepro_get_max_date_sale( $product_id )
	{
		$time = 0;
		// Get variations
		$args          = array(
			'post_type'   => 'product_variation',
			'post_status' => array( 'private', 'publish' ),
			'numberposts' => -1,
			'orderby'     => 'menu_order',
			'order'       => 'asc',
			'post_parent' => $product_id,
		);
		$variations    = get_posts( $args );
		$variation_ids = array();
		if ( $variations ) {
			foreach ( $variations as $variation ) {
				$variation_ids[] = $variation->ID;
			}
		}
		$sale_price_dates_to = false;

		if ( !empty( $variation_ids ) ) {
			global $wpdb;
			$sale_price_dates_to = $wpdb->get_var( "
                SELECT
                meta_value
                FROM $wpdb->postmeta
                WHERE meta_key = '_sale_price_dates_to' and post_id IN(" . join( ',', $variation_ids ) . ")
                ORDER BY meta_value DESC
                LIMIT 1
            "
			);

			if ( $sale_price_dates_to != '' ) {
				return $sale_price_dates_to;
			}
		}

		if ( !$sale_price_dates_to ) {
			$sale_price_dates_to = get_post_meta( $product_id, '_sale_price_dates_to', true );

			if ( $sale_price_dates_to == '' ) {
				$sale_price_dates_to = '0';
			}

			return $sale_price_dates_to;
		}
	}
}

/*Custom hook quick view*/
if ( class_exists( 'YITH_WCQV_Frontend' ) ) {
	// Class frontend
	$enable           = get_option( 'yith-wcqv-enable' ) == 'yes' ? true : false;
	$enable_on_mobile = get_option( 'yith-wcqv-enable-mobile' ) == 'yes' ? true : false;
	// Class frontend
	if ( ( !wp_is_mobile() && $enable ) || ( wp_is_mobile() && $enable_on_mobile && $enable ) ) {
		remove_action( 'woocommerce_after_shop_loop_item', array( YITH_WCQV_Frontend::get_instance(), 'yith_add_quick_view_button' ), 15 );
		add_action( 'corporatepro_function_shop_loop_item_quickview', array( YITH_WCQV_Frontend::get_instance(), 'yith_add_quick_view_button' ), 5 );
	}
}

/* Compare */
if ( class_exists( 'YITH_Woocompare' ) && get_option( 'yith_woocompare_compare_button_in_products_list' ) == 'yes' ) {
	global $yith_woocompare;
	$is_ajax = ( defined( 'DOING_AJAX' ) && DOING_AJAX );
	if ( $yith_woocompare->is_frontend() || $is_ajax ) {
		if ( $is_ajax ) {
			if ( !class_exists( 'YITH_Woocompare_Frontend' ) ) {
				if ( file_exists( YITH_WOOCOMPARE_DIR . 'includes/class.yith-woocompare-frontend.php' ) ) {
					require_once( YITH_WOOCOMPARE_DIR . 'includes/class.yith-woocompare-frontend.php' );
				}
			}
			$yith_woocompare->obj = new YITH_Woocompare_Frontend();
		}
		/* Remove button */
		remove_action( 'woocommerce_after_shop_loop_item', array( $yith_woocompare->obj, 'add_compare_link' ), 20 );
		/* Add compare button */
		if ( !function_exists( 'corporatepro_wc_loop_product_compare_btn' ) ) {
			function corporatepro_wc_loop_product_compare_btn()
			{
				if ( shortcode_exists( 'yith_compare_button' ) ) {
					echo do_shortcode( '[yith_compare_button product_id="' . get_the_ID() . '"]' );
				} // End if ( shortcode_exists( 'yith_compare_button' ) )
				else {
					if ( class_exists( 'YITH_Woocompare_Frontend' ) ) {
						$YITH_Woocompare_Frontend = new YITH_Woocompare_Frontend();
						echo do_shortcode( '[yith_compare_button product_id="' . get_the_ID() . '"]' );
					}
				}
			}
		}
		add_action( 'corporatepro_function_shop_loop_item_compare', 'corporatepro_wc_loop_product_compare_btn', 1 );
	}

}
/*Wishlist*/
if ( class_exists( 'YITH_WCWL' ) && get_option( 'yith_wcwl_enabled' ) == 'yes' ) {
	if ( !function_exists( 'corporatepro_wc_loop_product_wishlist_btn' ) ) {
		function corporatepro_wc_loop_product_wishlist_btn()
		{
			if ( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) {
				echo do_shortcode( '[yith_wcwl_add_to_wishlist product_id="' . get_the_ID() . '"]' );
			}
		}
	}
	add_action( 'corporatepro_function_shop_loop_item_wishlist', 'corporatepro_wc_loop_product_wishlist_btn', 1 );
}

/* Shop control*/
if ( !function_exists( 'corporatepro_shop_control' ) ) {
	function corporatepro_shop_control()
	{
		$woocommerce_shop_page_display = get_option( 'woocommerce_shop_page_display' );
		if ( is_shop() && $woocommerce_shop_page_display = 'subcategories' ) {
			return;
		}
		get_template_part( 'template-parts/shop', 'control' );

	}
}
if ( !function_exists( 'corporatepro_shop_view_more' ) ) {
	function corporatepro_shop_view_more()
	{
		$shop_display_mode = corporatepro_get_option( 'woo_shop_list_style', 'grid' );
		if ( isset( $_SESSION[ 'shop_display_mode' ] ) ) {
			$shop_display_mode = $_SESSION[ 'shop_display_mode' ];
		}

		?>
        <ul class="display-product-option">
            <li data-mode="grid"
                class="display-mode view-as-grid <?php if ( $shop_display_mode == "grid" ): ?>selected<?php endif; ?>"></li>
            <li data-mode="list"
                class="display-mode view-as-list <?php if ( $shop_display_mode == "list" ): ?>selected<?php endif; ?>"></li>
        </ul>
		<?php
	}
}

if ( !function_exists( 'corporatepro_shop_banners' ) ) {
	function corporatepro_shop_banners()
	{
		get_template_part( 'template-parts/shop', 'banners' );
	}
}

add_filter( 'woocommerce_show_page_title', 'corporatepro_hide_page_title' );
/**
 * corporatepro_hide_page_title
 *
 * Removes the "shop" title on the main shop page
 *
 * @access      public
 * @since       1.0
 * @return      void
 */
if ( !function_exists( 'corporatepro_hide_page_title' ) ) {
	function corporatepro_hide_page_title()
	{
		return false;
	}
}

if ( !function_exists( 'corporatepro_fronted_set_products_view_style' ) ) {
	function corporatepro_fronted_set_products_view_style()
	{
		check_ajax_referer( 'corporatepro_ajax_fontend', 'security' );
		$mode                            = $_POST[ 'mode' ];
		$_SESSION[ 'shop_display_mode' ] = $mode;
		die();
	}
}
add_action( 'wp_ajax_corporatepro_fronted_set_products_view_style', 'corporatepro_fronted_set_products_view_style' );
add_action( 'wp_ajax_nopriv_corporatepro_fronted_set_products_view_style', 'corporatepro_fronted_set_products_view_style' );


if ( !function_exists( 'corporatepro_woocommerce_template_loop_category_title' ) ) {
	function corporatepro_woocommerce_template_loop_category_title( $category )
	{
		?>
        <h3>
            <a href="<?php echo esc_url( get_term_link( $category, 'product_cat' ) ); ?>">
				<?php
				echo $category->name;
				if ( $category->count > 0 )
					echo apply_filters( 'woocommerce_subcategory_count_html', ' <span class="number-item">(' . $category->count . ' items)</span>', $category );
				?>

            </a>
        </h3>
		<?php
	}
}
if ( !function_exists( 'corporatepro_woocommerce_template_loop_category_button' ) ) {
	function corporatepro_woocommerce_template_loop_category_button( $category )
	{
		?>
        <a class="outline-button link-cat"
           href="<?php echo esc_url( get_term_link( $category, 'product_cat' ) ); ?>"><?php esc_html_e( 'brow store', 'corporatepro' ); ?></a>
		<?php
	}
}

if ( !function_exists( 'corporatepro_loop_shop_per_page' ) ) {
	function corporatepro_loop_shop_per_page()
	{
		$woo_products_perpage = corporatepro_get_option( 'woo_products_perpage', '12' );

		return $woo_products_perpage;
	}
}

if ( !function_exists( 'corporatepro_woof_products_query' ) ) {
	function corporatepro_woof_products_query( $wr )
	{
		$woo_products_perpage   = corporatepro_get_option( 'woo_products_perpage', '12' );
		$wr[ 'posts_per_page' ] = $woo_products_perpage;

		return $wr;
	}
}

add_filter( 'woocommerce_product_tabs', 'corporatepro_rename_tabs', 98 );
function corporatepro_rename_tabs( $tabs )
{
	if ( isset( $tabs[ 'additional_information' ] ) && $tabs[ 'additional_information' ] ) {
		$tabs[ 'additional_information' ][ 'title' ] = __( 'Information', 'corporatepro' );  // Rename the additional information tab
	}

	return $tabs;
}

/* Custom avtar size*/
add_filter( 'woocommerce_review_gravatar_size', 'corporatepro_woocommerce_review_gravatar_size', 10, 1 );
if ( !function_exists( 'corporatepro_woocommerce_review_gravatar_size' ) ) {
	function corporatepro_woocommerce_review_gravatar_size( $size )
	{
		return 140;
	}
}

if ( !function_exists( 'corporatepro_fronted_load_products' ) ) {
	function corporatepro_fronted_load_products()
	{
		$atts = $_POST[ 'atts' ];
		$page = $_POST[ 'page' ];
		die();
	}
}
add_action( 'wp_ajax_nopriv_corporatepro_fronted_load_products', 'corporatepro_fronted_load_products' );
add_action( 'wp_ajax_corporatepro_fronted_load_products', 'corporatepro_fronted_load_products' );

add_filter( 'add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

function woocommerce_header_add_to_cart_fragment( $fragments )
{
	global $woocommerce;

	ob_start();

	?>
    <a href="<?php echo wc_get_cart_url(); ?>" class="icon-text header-shoppingcart">
        <i class="fa fa-shopping-basket"></i>
        <span class="cart-number-items"><?php echo WC()->cart->get_cart_contents_count() ?></span>
		<?php echo esc_html__( 'my cart', 'corporatepro' ) ?>
    </a>
	<?php

	$fragments[ 'a.header-shoppingcart' ] = ob_get_clean();

	return $fragments;

}