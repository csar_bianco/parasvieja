<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package corporatepro
 */
?>
<?php get_header(); ?>
<?php

/* Blog Layout */
$corporatepro_blog_layout = corporatepro_get_option('opt_blog_layout','left');
$corporatepro_check_blog_banner = corporatepro_get_option('opt_blog_show_gallery','1');
$corporatepro_container_class = array('main-container');

if( $corporatepro_blog_layout == 'full'){
    $corporatepro_container_class[] = 'no-sidebar';
}else{
    $corporatepro_container_class[] = 'sidebar-'.$corporatepro_blog_layout;
}
$corporatepro_content_class = array();
$corporatepro_content_class[] = 'main-content';
if( $corporatepro_blog_layout == 'full' ){
    $corporatepro_content_class[] ='col-sm-12';
}else{
    $corporatepro_content_class[] = 'col-md-9 col-sm-8';
}

$true_shop_slidebar_class = array();
$true_shop_slidebar_class[] = 'sidebar';
if( $corporatepro_blog_layout != 'full'){
    $true_shop_slidebar_class[] = 'col-md-3 col-sm-4';
}

/* Blog Style */
$corporatepro_blog_list_style = corporatepro_get_option('opt_blog_list_style','standard');
?>
<?php if ($corporatepro_check_blog_banner == '1') : ?>
    <?php get_template_part('template-parts/blog','banner');?>
<?php endif; ?>
<div class="<?php echo esc_attr( implode(' ', $corporatepro_container_class) );?>">
    <div class="container">
        <div class="row">
            <div class="<?php echo esc_attr( implode(' ', $corporatepro_content_class) );?>" data-layout="<?php echo $corporatepro_blog_list_style ?>">
            	<?php if ( is_home() && ! is_front_page() ) : ?>
                    <div class="page-title">
                        <h2><?php single_post_title(); ?></h2>
                    </div>
                <?php endif; ?>
                <?php get_template_part('templates/blogs/blog',$corporatepro_blog_list_style);?>
            </div>
            <?php if( $corporatepro_blog_layout != "full" ):?>
                <div class="<?php echo esc_attr( implode(' ', $true_shop_slidebar_class) );?>">
                    <?php get_sidebar();?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
