var myWidgets = myWidgets || {};

// Model for a single faq
myWidgets.faq = Backbone.Model.extend({
  defaults: { 'answer':  '', 'question': '' }
});

// Single view, responsible for rendering and manipulation of each single faq
myWidgets.faqView = Backbone.View.extend( {

  className: 'faq-widget-child',

  events: {
    'click .js-remove-faq': 'destroy'
  },

  initialize: function ( params ) {

    this.template = params.template;
    this.model.on( 'change', this.render, this );

    return this;

  },

  render: function () {

    this.$el.html( this.template( this.model.attributes ) );

    return this;

  },

  destroy: function ( ev ) {

    ev.preventDefault();

    this.remove();
    this.model.trigger( 'destroy' );

  },
} );

// The list view, responsible for manipulating the array of faqs
myWidgets.faqsView = Backbone.View.extend( {

  events: {
    'click #js-faqs-add': 'addNew'
  },

  initialize: function ( params ) {

    this.widgetId = params.id;

    // cached reference to the element in the DOM
    this.$faqs = this.$( '#js-faqs-list' );

    // collection of faqs, local to each instance of myWidgets.faqsView
    this.faqs = new Backbone.Collection( [], {
      model: myWidgets.faq
    } );

    // listen to adding of the new faqs
    this.listenTo( this.faqs, 'add', this.appendOne );

    return this;

  },

  addNew: function ( ev ) {

    ev.preventDefault();

    // default, if there is no faqs added yet
    var faqId = 0;

    if ( ! this.faqs.isEmpty() ) {
      var faqsWithMaxId = this.faqs.max( function ( faq ) {
        return faq.id;
      } );

      faqId = parseInt( faqsWithMaxId.id, 10 ) + 1;
    }

    var model = myWidgets.faq;

    this.faqs.add( new model( { id: faqId } ) );

    return this;

  },

  appendOne: function ( faq ) {

    var renderedfaq = new myWidgets.faqView( {
      model:    faq,
      template: _.template( jQuery( '#js-faq-' + this.widgetId ).html() ),
    } ).render();

    this.$faqs.append( renderedfaq.el );

    return this;

  }

} );


myWidgets.repopulatefaqs = function ( id, JSON ) {

  var faqsView = new myWidgets.faqsView( {
    id: id,
    el: '#js-faqs-' + id,
  } );
  
  faqsView.faqs.add( JSON );
};

