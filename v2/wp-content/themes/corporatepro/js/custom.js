(function ($) {

    "use strict"; // Start of use strict

    var wf   = document.createElement('script');
    wf.src   = '//ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type  = 'text/javascript';
    wf.async = 'true';
    var s    = document.getElementsByTagName('script')[ 0 ];
    s.parentNode.insertBefore(wf, s);

    if ( $('.cp-google-maps').length == 1 && corporatepro_global.gmap_api_key != '' ) {
        var wfm  = document.createElement('script');
        wfm.src  = '//maps.googleapis.com/maps/api/js?key=' + corporatepro_global.gmap_api_key;
        wfm.type = 'text/javascript';
        var sm   = document.getElementsByTagName('script')[ 0 ];
        sm.parentNode.insertBefore(wfm, sm);
    }

    // The number of the next page to load (/page/x/)
    var pageNum = parseInt(corporatepro_loadmore_posts.startPage) + 1;

    // The maximum number of pages the current query can return
    var max = parseInt(corporatepro_loadmore_posts.maxPages);

    // The link of the next page of posts
    var nextLink = corporatepro_loadmore_posts.nextLink;

    function kt_masonry($masonry) {
        var t = $masonry.attr("data-cols");
        if ( t == "1" ) {
            var n = $masonry.width();
            var r = 1;
            return r
        }
        if ( t == "2" ) {
            var n = $masonry.width();
            var r = 2;
            if ( n < 600 ) r = 1;
            return r
        } else if ( t == "3" ) {
            var n = $masonry.width();
            var r = 3;
            if ( n < 600 ) r = 1;
            else if ( n >= 600 && n < 768 ) r = 2;
            else if ( n >= 768 && n < 992 ) r = 3;
            else if ( n >= 992 ) r = 3;
            return r
        } else if ( t == "4" ) {
            var n = $masonry.width();
            var r = 4;
            if ( n < 600 ) r = 1;
            else if ( n >= 600 && n < 768 ) r = 2;
            else if ( n >= 768 && n < 992 ) r = 3;
            else if ( n >= 992 ) r = 4;
            return r
        } else if ( t == "5" ) {
            var n = $masonry.width();
            var r = 5;
            if ( n < 600 ) r = 1;
            else if ( n >= 600 && n < 768 ) r = 2;
            else if ( n >= 768 && n < 992 ) r = 3;
            else if ( n >= 992 && n < 1140 ) r = 4;
            else if ( n >= 1140 ) r = 5;
            return r
        } else if ( t == "6" ) {
            var n = $masonry.width();
            var r = 5;
            if ( n < 600 ) r = 1;
            else if ( n >= 600 && n < 768 ) r = 2;
            else if ( n >= 768 && n < 992 ) r = 3;
            else if ( n >= 992 && n < 1160 ) r = 4;
            else if ( n >= 1160 ) r = 6;
            return r
        } else if ( t == "8" ) {
            var n = $masonry.width();
            var r = 5;
            if ( n < 600 ) r = 1;
            else if ( n >= 600 && n < 768 ) r = 2;
            else if ( n >= 768 && n < 992 ) r = 3;
            else if ( n >= 992 && n < 1160 ) r = 4;
            else if ( n >= 1160 ) r = 8;
            return r
        }
    }

    function cp_s($masonry) {
        var t = kt_masonry($masonry);
        var n = $masonry.width();
        var r = n / t;
        r     = Math.floor(r);
        $masonry.find(".item-portfolio").each(function (t) {
            $(this).css({
                width: r + "px"
            });
        });
    }

    function cp_masonry() {
        $('.cp-portfolio').each(function () {
            var $masonry    = $(this).find('.portfolio-grid');
            var $layoutMode = $masonry.attr('data-layoutMode');
            cp_s($masonry);
            // init Isotope
            var $grid = $masonry.isotope({
                itemSelector: '.item-portfolio',
                layoutMode: $layoutMode,
                itemPositionDataEnabled: true
            });

            $grid.imagesLoaded().always(function () {
                $grid.isotope({
                    itemSelector: '.item-portfolio',
                    layoutMode: $layoutMode,
                    itemPositionDataEnabled: true
                });
            });

            $(this).find('.portfolio_fillter .item-fillter').on('click', function () {
                var $filterValue = $(this).attr('data-filter');
                $grid.isotope({
                    filter: $filterValue
                });
                $(this).closest('.cp-portfolio').find('.portfolio_fillter .item-fillter').removeClass('fillter-active');
                $(this).addClass('fillter-active');
            });

        });
    }

    /* ---------------------------------------------
     Owl carousel
     --------------------------------------------- */
    function init_carousel() {
        if ( $('.woocommerce-product-gallery').length == 1 ) {
            $('.woocommerce-product-gallery').each(function () {
                var _config        = [];
                var _owl           = $(this);
                _config.navText    = [ '<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>' ];
                _config.nav        = true;
                _config.margin     = 10;
                _config.dots       = false;
                _config.responsive = corporatepro_global.data_reponsive;
                _owl.find('.flex-control-thumbs').owlCarousel(_config);
            })
        }
        $('.owl-carousel').each(function () {
            var config     = $(this).data();
            config.navText = [ '<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>' ];
            if ( $(this).hasClass('nav-style3') || $(this).hasClass('nav-style5') || $(this).hasClass('nav-style7') ) {
                config.navText = [ '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>' ];
            } else if ( $(this).hasClass('nav-style4') ) {
                config.navText = [ 'Prev', 'Next' ];
            } else if ( $(this).hasClass('nav-style6') ) {
                config.navText = [ '<i class="fa fa-caret-left"></i>', '<i class="fa fa-caret-right"></i>' ];
            }

            if ( $(this).hasClass('dots-custom') ) {
                var _parent          = $(this).closest('.slide-dotcustom');
                var _dotsContainer   = _parent.find('.cp-dots-custom');
                config.dotsContainer = _dotsContainer;
            }
            if ( $(this).hasClass('slide-navcustom') ) {
                var _parent         = $(this).closest('.cp-slide-navcustom');
                var _navContainer   = _parent.find('.cp-nav-custom');
                config.navContainer = _navContainer;
            }

            var animateOut = $(this).data('animateout');
            var animateIn  = $(this).data('animatein');
            if ( typeof animateOut != 'undefined' ) {
                config.animateOut = animateOut;
            }

            if ( typeof animateIn != 'undefined' ) {
                config.animateIn = animateIn;
            }

            var owl = $(this);
            owl.owlCarousel(config);


        });
    }

    /* ---------------------------------------------
     Woocommerce Quantily
     --------------------------------------------- */
    function corporatepro_woo_quantily() {
        $('body').on('click', '.quantity .plus', function () {
            var obj_qty  = $(this).closest('.quantity').find('input.qty'),
                val_qty  = parseInt(obj_qty.val()),
                min_qty  = parseInt(obj_qty.data('min')),
                max_qty  = parseInt(obj_qty.data('max')),
                step_qty = parseInt(obj_qty.data('step'));


            val_qty = val_qty + step_qty;
            if ( max_qty && val_qty > max_qty ) {
                val_qty = max_qty;
            }

            obj_qty.val(val_qty);
            obj_qty.trigger("change");
            return false;
        });

        $('body').on('click', '.quantity .minus', function () {
            var obj_qty  = $(this).closest('.quantity').find('input.qty'),
                val_qty  = parseInt(obj_qty.val()),
                min_qty  = parseInt(obj_qty.data('min')),
                max_qty  = parseInt(obj_qty.data('max')),
                step_qty = parseInt(obj_qty.data('step'));
            val_qty      = val_qty - step_qty;


            if ( min_qty && val_qty < min_qty ) {
                val_qty = min_qty;
            }
            if ( !min_qty && val_qty < 0 ) {
                val_qty = 0;
            }
            obj_qty.val(val_qty);
            obj_qty.trigger("change");
            return false;
        });
    }

    function number_dots() {
        $('.slide-dotcustom').each(function () {
            var n     = $(this).find('.cp-dots-custom .owl-dot').length;
            var $dots = $(this).find('.cp-dots-custom .owl-dot');

            for ( var i = 0; i <= n; i++ ) {
                var _number;
                if ( i < 9 ) {
                    _number = '0' + (i + 1);
                } else {
                    _number = i + 1;
                }
                if ( $(this).hasClass('dot-style5') ) {
                    $dots.eq(i).html('<span class="number">' + _number + '</span><span class="line"></span>');
                }
                if ( $(this).hasClass('dot-style6') ) {
                    $dots.eq(i).html('<span class="line"></span><span class="number">' + _number + '</span>');
                }
                if ( $(this).hasClass('dots-number') ) {
                    $dots.eq(i).html('<span>' + _number + '</span>');
                }
                if ( $(this).hasClass('dots-slide') ) {
                    $dots.eq(i).html('<span class="line"></span><span class="dot"></span>');
                }
            }
        });
    }

    function dot_owl() {
        $('.slide-dotcustom.dots-slide').each(function () {
            var $dots_slide = $(this).find('.cp-dots-custom');
            var $dots       = $(this).find('.cp-dots-custom .owl-dot');
            var $position   = 0;
            $dots.each(function (i, e) {
                if ( $(this).hasClass('active') ) {
                    $position = i;
                }
            });

            for ( var c = 0; c <= $position; c++ ) {
                $dots.eq(c).addClass('cp-active');
            }

            var owl   = $(this).find('.owl-carousel');
            var $this = $(this);
            owl.on('changed.owl.carousel', function (event) {
                var dotActive = $this.find('.cp-dots-custom .owl-dot.active');
                var index     = $dots.index(dotActive);
                $dots_slide.find('.owl-dot').removeClass('cp-active');
                for ( var c = 0; c <= index; c++ ) {
                    $dots.eq(c).addClass('cp-active');
                }

            });
        });
    }

    /* ---------------------------------------------
     Full Height
     --------------------------------------------- */
    function getCurrentScroll() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }

    function cp_height_full() {
        (function ($) {
            var heightoff = 0;
            // if($('#wpadminbar').length){
            //     heightoff = $('#wpadminbar').outerHeight();
            // }
            var heightHeader = $('.main-header').outerHeight();
            var heightSlide;
            if ( $('.header').hasClass('trans') ) {
                heightSlide = $(window).outerHeight() - heightoff;
            } else {
                heightSlide = $(window).outerHeight() - heightoff - heightHeader;
            }
            $(".full-height").css("height", heightSlide);
            $(".min-full-height").css("min-height", heightSlide);
        })(jQuery);
    }

    //HEIGHT ANIMATE COULUMN
    function cp_height_equal() {
        $('.cp-height-equal').each(function () {
            var $this = $(this);
            if ( $this.find('.height-equal').length ) {
                $this.find('.height-equal').css({
                    'height': 'auto'
                });
                var elem_height = 0;
                $this.find('.height-equal').each(function () {
                    var this_elem_h = $(this).outerHeight();
                    if ( elem_height < this_elem_h ) {
                        elem_height = this_elem_h;
                    }
                });
                $this.find('.height-equal').outerHeight(elem_height);
            }
        });
    }

    //Bg Parallax
    function parallaxInit() {
        //Mobile Detect
        var testMobile;
        var isMobile = {
            Android: function () {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };

        testMobile = isMobile.any();
        if ( testMobile == null ) {
            $('.bg-parallax').each(function () {
                $(this).parallax('50%', 0.3);
            });
        }

        if ( isMobile.iOS() ) {
            $('.bg-parallax').each(function () {
                $(this).css({
                    "background-size": "auto 100%",
                    "background-attachment": "scroll"
                });
            });
        }
    }

    //Menu fixed
    function cp_sticky() {
        if ( corporatepro_global.opt_enable_main_menu_sticky == 0 ) {
            return;
        }
        var widthDevice = $(window).width();
        var _mainHeader = $('.main-header');
        if ( !$('.header').hasClass('header-style5') ) {
            var _headerOffset = $('.main-header').offset().top;
            if ( widthDevice > 768 ) {
                if ( _headerOffset == 0 ) {
                    _mainHeader.addClass('header-fixed');
                } else {
                    $(window).scroll(function () {
                        var scroll = getCurrentScroll();
                        if ( scroll >= _headerOffset ) {
                            _mainHeader.addClass('header-fixed');
                        } else {
                            _mainHeader.removeClass('header-fixed');
                        }
                    });
                }
            } else {
                _mainHeader.removeClass('header-fixed');
            }
        }

        var heightHeader = _mainHeader.outerHeight();
    }

    // Padding ss
    $(window).load(function () {
        var widthW       = $(window).width();
        var widthContent = $('.container').outerWidth();
        var $padding     = (widthW - widthContent) / 2 + 15;
        $('.ss-table').each(function () {
            var $ssImg = $(this).find('.image-ss');
            var $imgSS;
            if ( $ssImg.hasClass('cp-slide-img') ) {
                $imgSS = $(this).find('.owl-item.active .item-slide').attr('data-bg');
                $ssImg.css({
                    'background-image': 'url(' + $imgSS + ')'
                });
                $ssImg.on('changed.owl.carousel', function (event) {
                    var index = event.item.index;
                    $imgSS    = $(this).find('.owl-item').eq(index).find('.item-slide').attr('data-bg');
                    $ssImg.animate({
                        opacity: '0.7'
                    }, 500, function () {
                        $ssImg.css({
                            'background-image': 'url(' + $imgSS + ')'
                        }).animate({
                            'opacity': 1
                        });
                    });
                });
            } else {
                $imgSS = $(this).find('.image-ss').attr('data-bg');
                $ssImg.css({
                    'background-image': 'url(' + $imgSS + ')'
                });
            }

            $(this).find('.right-content').css("padding-right", $padding);
            $(this).find('.left-content').css("padding-left", $padding);
        });

        //Position dots slide 9
        $('.slide-home9.dot-style5 .cp-dots-custom').css("right", $padding);

        //Prallax img
        parallaxInit();

        $(".portfolio_fillter .item-fillter.fillter-active").trigger("click");

    });

    $(document).ready(function () {
        //ACORDION
        $(".cp-acordion").each(function () {
            var $this       = $(this);
            var $tab_active = parseInt($this.attr('data-tab-active'));
            var $icon       = {"header": "fa fa-caret-up", "activeHeader": "fa fa-caret-down"};
            if ( $(this).hasClass('accordion-style3') ) {
                $icon = {"header": "fa fa-angle-up", "activeHeader": "fa fa-angle-down"};
            } else if ( $(this).hasClass('accordion-style6') || $(this).hasClass('accordion-style7') ) {
                $icon = {"header": "fa fa-plus", "activeHeader": "fa fa-minus"};
            }
            $(this).accordion({
                icons: $icon,
                active: $tab_active,
                heightStyle: "content",
                collapsible: true
            });
        });

        //Tab
        $('.cp-tab').each(function () {
            $(this).tabs();
        });

        //SIDE NEW PROJECT
        $('.cp-new-project').each(function () {
            var owl = $(this).find('.list-project');
            owl.owlCarousel(
                {
                    margin: 0,
                    autoplay: false,
                    dots: true,
                    loop: false,
                    items: 1,
                    nav: false,
                }
            );

            owl.trigger('next.owl.carousel');

            owl.on('changed.owl.carousel', function (event) {
                var index    = event.item.index;
                var $item    = owl.find('.owl-item').eq(index);
                var caption  = $item.find('.pj-info').html();
                var t        = owl.closest('.cp-new-project').find('.pj-info-widget');
                var animated = t.data('animated');
                t.html(caption).addClass('animated ' + animated).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $(this).removeClass('animated ' + animated);
                });

            });
            owl.trigger('prev.owl.carousel');

        });

        //Skill bar
        $('.cp-skillbar').each(function () {
            var $bg_skill    = $(this).attr('data-bgskill');
            var $bg_skillbar = $(this).attr('data-bgskillbar');
            var $percent     = $(this).attr('data-percent');
            $(this).find('.skillbar-bar').animate({
                'width': $percent + '%'
            }, 2000);
            if ( $bg_skillbar != '' ) {
                $(this).find('.skill-bar-bg').css('background', $bg_skillbar);
                if ( $(this).hasClass('skillbar-style3') ) {
                    $(this).find('.skill-bar-percent').css('color', $bg_skillbar);
                }
            }
            if ( $bg_skill != '' ) {
                $(this).find('.skillbar-bar').css('background', $bg_skill);
                if ( $(this).hasClass('skillbar-style2') ) {
                    $(this).find('.percent-skill').css('background', $bg_skill);
                }
            }
        });

        //BACK TO TOP
        $('a.backtotop').on('click', function () {
            $('html, body').animate({scrollTop: 0}, 800);
            return false;
        });
        $('a.back_to_top').on('click', function () {
            $('html, body').animate({scrollTop: 0}, 800);
            return false;
        });

        //SLIDE HOME
        $('.item-homeslide').each(function () {
            var bgImg = $(this).data('background');
            if ( bgImg != '' ) {
                $(this).css({
                    'background-image': 'url(' + bgImg + ')'
                });
            }
        });

        //Pricing image
        $('.pricing-image').each(function () {
            var bgImg = $(this).find('.pricing-header').data('background');
            if ( bgImg != '' ) {
                $(this).find('.pricing-header').css({
                    'background-image': 'url(' + bgImg + ')'
                });
            }
        });

        //Title charator
        $('.title-section.title-style3').each(function () {
            var $title = $(this).find('.title');

            var $word       = $title.html().trim();
            var $title_html = '';
            for ( var c = 0; c < $word.length; c++ ) {
                if ( $word[ c ].trim() == '' ) {
                    $title_html += '<span class="empty">&nbsp;</span>';
                } else {
                    $title_html += '<span class="charator">' + $word[ c ] + '</span>';
                }
            }

            $title.html($title_html);
        })

        // Tourist
        $('.cp-tour').each(function () {
            var $width   = $(this).data('width');
            var $imgTour = $(this).data('image-tour');
            $(this).css("width", $width + 'px');
            $(this).find('.image-tour').css('background-image', 'url(' + $imgTour + ')');
        });

        // Select 2
        // $('select').select2();
        if ( $('.product-detail .product-info select').length > 0 ) {
            $('.product-detail .product-info select').chosen();
        }

        //Time countdown
        $('.cp-countdown').each(function () {

            var _this = $(this);
            var _date = _this.attr('data-time');

            _this.countdown(_date, function (event) {
                var _hour   = event.offset.totalDays * 24 + event.offset.hours;
                var _minute = event.strftime('%-M');
                var _second = event.strftime('%-S');

                _this.find('.hour').html(_hour + 'h');
                _this.find('.minute').html(_minute + 'm');
                _this.find('.second').html(_second + 's');

            });
        });

        //Funfact
        $('.cp-counter').each(function () {
            var count_element = $(this).find('.number').attr('data-number');
            var $this         = $(this);
            if ( count_element != '' ) {
                $({countNum: 0}).animate({countNum: count_element}, {
                    duration: 2500,
                    easing: 'linear',
                    step: function () {
                        if ( $this.hasClass('counter-style4') ) {
                            $this.find('.number').text(Math.floor(this.countNum).toString());
                        } else {
                            $this.find('.number').text(Math.floor(this.countNum).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                        }
                    },
                    complete: function () {
                        if ( $this.hasClass('counter-style4') ) {
                            $this.find('.number').text(this.countNum.toString());
                        } else {
                            $this.find('.number').text(this.countNum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                        }
                    }
                });
            }
            ;
        });

        /*--------------------------------------------
         Page scroll
         ----------------------------------------------*/
        if ( $('.cp-slidepage').length ) {
            //Scroll button
            $('.scroll-button').on('click', function () {
                var _parentSection = $(this).closest('.section-slide');
                var _index         = $('.cp-slidepage').find('.section-slide').index(_parentSection) + 1;
                var _tagetSection  = $('.section-slide').eq(_index).offset().top;
                $('html,body').animate({scrollTop: _tagetSection}, 'slow');
            });

            //Pagination scroll
            var lastId;
            $(window).scroll(function () {

                var $fromTop      = $(this).scrollTop();//getCurrentScroll();
                var $sectionItems = $('.cp-slidepage .section-slide');
                var $current;

                $sectionItems.each(function () {
                    if ( $(this).offset().top - 300 < $fromTop ) {
                        $current = $(this);
                    }
                });

                var index = $('.cp-slidepage .section-slide').index($current) ? $('.cp-slidepage .section-slide').index($current) : 0;
                if ( lastId !== index ) {
                    lastId = index;
                    $('.pagination-slide .menu-item').removeClass('active');
                    $('.pagination-slide .menu-item').eq(index).addClass('active');
                }

                //Positon nav and pagination
                var $topSS2 = $('.cp-slidepage .section-slide').eq(1).offset().top - 300;
                if ( $topSS2 < $fromTop ) {
                    $('.header-style5 .cp-nav-menusidebar').css('top', 20 + '%')
                } else {
                    $('.header-style5 .cp-nav-menusidebar').css('top', 35 + '%')
                }

            });


            //Pagination click
            $('.pagination-slide .menu-item:first').addClass('active');
            if ( $('.pagination-slide .menu-item').length ) {
                var $dots = $('.pagination-slide .menu-item');
                $dots.on('click', function (e) {
                    e.preventDefault();
                    $(document).off('scroll');
                    var _index = $('.pagination-slide').find('.menu-item').index($(this));
                    if ( $('.section-slide').eq(_index).length ) {
                        var _tagetSection = $('.section-slide').eq(_index).offset().top;
                        $('html,body').stop().animate({scrollTop: _tagetSection}, 'slow');
                    }

                    $dots.removeClass('active');
                    $(this).addClass('active');

                });
            }

            //Menu navbar
            $('.icon-menu').on('click', function () {
                $(this).toggleClass('active');
                $(this).parent('.cp-nav-menusidebar').find('.navigation').slideToggle();
            });

        }
        /*--------------------------------------------------*/

        //Service slide
        if ( ('.cp-service-slide').length ) {
            $('.cp-service-slide .item-service .service-base').on('click', function (event) {
                event.stopPropagation();
                $('.cp-service-slide .service-full').slideUp();
                $(this).closest('.item-service').find('.service-full').slideDown();
            });
            $('body').on('click', function () {
                $('.cp-service-slide .service-full').slideUp();
            });
        }

        //Service2 active
        $(document).on('mouseenter', '.service-style-2', function () {
            $('.service-style-2').removeClass('active');
            $(this).addClass('active');
        });

        //Home Feaured Project 2
        if ( $('.cp-feature-project-slide').length > 0 ) {
            $('.cp-feature-project-slide').each(function () {
                $(this).find('.slide-feature-project').owlCarousel({
                    items: 1,
                    dots: true,
                    nav: false,
                    responsive: {
                        0: {
                            items: 1,
                        },
                        600: {
                            items: 2,
                        },
                        1000: {
                            items: 3,
                        }
                    }
                });
            });

            var first = $('.cp-feature-project-slide .owl-theme .owl-controls .owl-dot').first().index() + 1;
            var last  = $('.cp-feature-project-slide .owl-theme .owl-controls .owl-dot').last().index() + 1;
            //Range with fixed maximum
            $(".control-project-slider #slider-range-max").slider({
                range: 'max',
                min: first,
                max: last,
                value: first,

                slide: function (event, ui) {
                    $('.cp-feature-project-slide').find('.slide-feature-project').trigger('to.owl.carousel', ui.value - 1);
                }
            });
        }

        //Progress cicrle
        if ( $('.cp-pie-charts').length ) {
            $('.cp-pie-charts').each(function () {
                var _size = $(this).width();
                $(this).asPieProgress({
                    namespace: 'pie_progress',
                    size: _size,
                });
                $(this).asPieProgress('start');
            })
        }
        if ( $('.pie-chart-3').length ) {
            $('.pie-chart-3').each(function () {
                var _numberPie = $(this).data('number');
                if ( _numberPie != '' ) {
                    $(this).find('.piechart-number').countTo({
                        from: 0,
                        to: _numberPie,
                        speed: 1500,
                        refreshInterval: 50,
                    })
                }
                ;
            });
        }

        //Show submenu
        if ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            // on Mobile
            $('.cp-main-menu .menu-item-has-children .caret').on('click', function () {
                $(this).parent().toggleClass('show-submenu');
                $(this).parent().find('.sub-menu li').removeClass("show-submenu");
            });
        } else {
            $(document).on('mouseenter', '.cp-main-menu .menu-item', function () {
                $(this).addClass('show-submenu');
            }).on('mouseleave', '.cp-main-menu .menu-item', function () {
                $(this).removeClass('show-submenu');
            });
        }

        //Menu mobile
        $('.header-style8 .cp-main-menu').hide();
        $('.header-style8 .menu-togole').removeClass('active');
        $('.menu-togole').click(function () {
            $(this).toggleClass('active');
            $('.cp-main-menu').slideToggle(200);
        });
        $('.search-togole').on('click', function () {
            $(this).toggleClass('active');
            $('.header-search').slideToggle(200);
        });
        $('.header-style8 .menu-togole').on('click', function () {
            cp_height_equal();
        });


        // Latest blog masonry
        $('.grid-masonry').each(function () {
            var $layoutMode = $(this).attr('data-layoutmode');
            $(this).isotope({
                resizable: false,
                itemSelector: '.grid',
                layoutMode: $layoutMode,
                transitionDuration: '0.6s',
                packery: {
                    gutter: 0
                },
            }).isotope('layout');
        });

        /*--------------------------------------------
         SHOP
         ----------------------------------------------*/

        //Banner slide shop
        if ( $('.slide-banner').length ) {
            $('.slide-banner').find('.banner-item').each(function () {
                var _imgBanner = $(this).data('bg');
                $(this).css('background-image', 'url(' + _imgBanner + ')');
            })
        }

        //Product Gallery
        if ( $('.pr-gallery-vertical').length > 0 ) {
            $('.pr-gallery-vertical .main-slide').bxSlider({
                pagerCustom: '.pr-gallery-vertical #thumb-slide',
                nextText: '<span class="fa fa-angle-down"></span>',
                prevText: '<span class="fa fa-angle-up"></span>'
            });
        }

        //Service shop
        if ( $('.cp-service-shop').length ) {
            $('.cp-service-shop .item-service').hover(function () {
                var _itemnumber = $(this).data('item');
                $(this).closest('.cp-service-shop').find('.service-hover[data-item="' + _itemnumber + '"]').fadeIn(600);
            }, function () {
                var _itemnumber = $(this).data('item');
                $(this).closest('.cp-service-shop').find('.service-hover[data-item="' + _itemnumber + '"]').hide();
            });
        }

        //Menu sidebar
        $('.touch-menu-sidebar').on('click', function () {
            $('body').addClass('cp-sidebar');
        });
        $('.close-touch, .body-ovelay').on("click", function () {
            $('body').removeClass('cp-sidebar');
        })

        $('.cp-left-nav .menu-item-has-children .caret').on("click", function () {

            var $li_current = $(this).closest('.menu-item-has-children'),
                $li_parrent = $('.cp-left-nav .menu-item-has-children');
            $li_current.find('.sub-menu').slideToggle(300)
            e.preventDefault();
            e.stopPropagation();
        });

        // Search box shop
        $('.touch-search').on('click', function () {
            $('.cp-search-box-shop').fadeIn(500);
        });
        $('.cp-search-box-shop .close-touch,.cp-search-box-shop .cp-overlay').on('click', function () {
            $('.cp-search-box-shop').fadeOut(200);
        });

        /*------------------END SHOP -----------------*/

        //Time countdown

        $('.cp-time-countdown').each(function () {
            var _dateEnd = $(this).data('date');
            _dateEnd     = Date.parse(_dateEnd) / 1000;

            var _dataNow = new Date();
            _dataNow     = _dataNow.getTime() / 1000;

            $(this).final_countdown({
                'start': 1362139200,
                'end': _dateEnd,
                'now': _dataNow,
                seconds: {
                    borderColor: '#ff4949',
                    borderWidth: '2'
                },
                minutes: {
                    borderColor: '#ff4949',
                    borderWidth: '2'
                },
                hours: {
                    borderColor: '#ff4949',
                    borderWidth: '2'
                },
                days: {
                    borderColor: '#ff4949',
                    borderWidth: '2'
                }
            });
        })

        //Menu home 5 maxheight mobile
        var $widthDevice  = $(window).width();
        var $heightDevice = $(window).height();
        if ( $widthDevice < 768 ) {
            var $maxheight = $heightDevice - 80;
            $('.cp-nav-menusidebar .navigation').css("max-height", $maxheight);
        }

        // View grid list product
        $(document).on('click', '.display-mode', function () {
            var mode        = $(this).data('mode');
            var current_url = window.location.href;
            current_url     = current_url.replace("#", "");
            var data        = {
                action: 'corporatepro_fronted_set_products_view_style',
                security: corporatepro_ajax_fontend.security,
                mode: mode
            };
            $.post(corporatepro_ajax_fontend.ajaxurl, data, function (response) {
                window.location.replace(current_url);
            })
            return false;
        });

        /*Loadmore posts */

        $(document).on('click', '#cp-button-loadmore-posts', function () {
            var result_div = $('#loadmore-posts-result');
            // Are there more posts to load?
            if ( pageNum <= max ) {

                // Show that we're working.
                $(this).addClass('loading');
                var t = $(this);

                $(result_div).load(nextLink + ' .blog-item', function (response, status, xhr) {
                        if ( status == "success" ) {
                            // Update page number and nextLink.
                            pageNum++;
                            nextLink = nextLink.replace(/\/page\/[0-9]*/, '/page/' + pageNum);
                            // Update the button message.
                            if ( pageNum <= max ) {
                                t.removeClass('loading');
                            } else {
                                $('#pj-loadmore').remove();
                            }
                            var result = $(result_div).html();
                            $('.cp-blog .cp-blog-content').append(result);

                            $(result_div).html('');
                            init_carousel();
                            cp_masonry();

                        }
                    }
                );
            } else {
                $('#cp-button-loadmore-posts a').append('.');
            }

            return false;
        });

        /*Loadmore project */

        $('#cp-button-loadmore-project').click(function () {

            // get post ID in array
            var except_post_ids = Array();
            $('.cp-project-content').find('.item-project').each(function () {
                var post_id = $(this).attr('id').replace('post-', '');
                except_post_ids.push(post_id);
            });
            // get post ID in array

            var pro_layout = $('.main-content').attr('data-layout');

            var data = {
                action: 'corporatepro_loadmore_project',
                security: corporatepro_ajax_fontend.security,
                except_post_ids: except_post_ids,
                pro_layout: pro_layout,
            };

            $('#cp-button-loadmore-project').addClass('loading');

            $.post(corporatepro_ajax_fontend.ajaxurl, data, function (response) {
                var items = $('' + response[ 'html' ] + '');

                if ( $.trim(response[ 'success' ]) == 'ok' ) {

                    $(".cp-project-content").append(items);

                } else {
                    $('.cp-project-content').append('<p class="return-message bg-success">Not ok</p>');
                }

                $('#cp-button-loadmore-project').removeClass('loading');

                init_carousel();

            });

            return false;
        });


        /*Loadmore product */
        $(document).on('click', '.loadmore-products', function () {
            var atts = $(this).data('atts');
            var page = parseInt($(this).attr('page'));
            $(this).addClass('loadding');
            var data = {
                action: 'corporatepro_fronted_load_products',
                security: corporatepro_ajax_fontend.security,
                atts: atts,
                page: page
            }
            var t    = $(this);
            $.post(corporatepro_ajax_fontend.ajaxurl, data, function (response) {
                console.log(response);
            })
            return false;
        })

    });

    /* ---------------------------------------------
     Scripts scroll
     --------------------------------------------- */
    $(window).scroll(function () {
        if ( $(window).scrollTop() > 100 ) {
            $('.back_to_top').show(800);
        } else {
            $('.back_to_top').hide(800);
        }
    });

    $(window).on("debouncedresize", function (e) {
        //owl carousel
        setTimeout(function () {
            number_dots();
        }, 200)
        //full height
        cp_height_full();
        //Menu fixed
        cp_sticky();
        //Padding ss
        var widthW       = $(window).width();
        var widthContent = $('.container').outerWidth();
        var $padding     = (widthW - widthContent) / 2;
        $('.ss-table').each(function () {
            $(this).find('.right-content').css("padding-right", $padding);
            $(this).find('.left-content').css("padding-left", $padding);
        });
        //Position dots slide 9
        $('.slide-home9.dot-style5 .cp-dots-custom').css("right", $padding);

        //Menu home 5 maxheight mobile
        var $widthDevice  = $(window).width();
        var $heightDevice = $(window).height();
        if ( $widthDevice < 768 ) {
            var $maxheight = $heightDevice - 80;
            $('.cp-nav-menusidebar .navigation').css("max-height", $maxheight);
        }

    });
    // after ajax call
    $(document).ajaxComplete(function (event, xhr, settings) {

    });
    $(document).on('click', '.icon-share', function () {
        $('.sub_iconshare').slideToggle('slow');
    });


    /* Loadmore Masonry Project */

    $('#cp-loadmore-masonry').click(function () {

        var ids_category = Array();
        $('.portfolio_fillter').find('.fillter-active').each(function () {
            var cat_id = $(this).attr('data-filter').replace('.', '');
            ids_category.push(cat_id);
        });
        // get post ID in array
        var except_post_ids = Array();
        $('.cp-loadmore-content').find('.item-project').each(function () {
            var post_id = $(this).attr('id').replace('post-', '');
            except_post_ids.push(post_id);
        });
        // get post ID in array

        var pro_layout = $('.main-content').attr('data-layout');

        var data = {
            action: 'corporatepro_loadmore_masonry_project',
            security: corporatepro_ajax_fontend.security,
            except_post_ids: except_post_ids,
            ids_category: ids_category,
            pro_layout: pro_layout,
        };

        $('#cp-loadmore-masonry').addClass('loading');

        $.post(corporatepro_ajax_fontend.ajaxurl, data, function (response) {
            var items = $('' + response[ 'html' ] + '');

            if ( $.trim(response[ 'success' ]) == 'ok' ) {

                items.imagesLoaded(function () {
                    $(".cp-loadmore-content").append(items).isotope('appended', items, true);
                    cp_masonry();
                });

            } else {
                $('.cp-loadmore-content').append('<p class="return-message bg-success">Not ok</p>');
            }

            $('#cp-loadmore-masonry').removeClass('loading');

            init_carousel();

        });

        return false;
    })


    /* Loadmore Masonry Post */

    $('#cp-loadmore-masonry-posts').click(function () {

        // get post ID in array
        var except_post_ids = Array();
        $('.portfolio-grid').find('.blog-item').each(function () {
            var post_id = $(this).attr('id').replace('post-', '');
            except_post_ids.push(post_id);
        });
        // get post ID in array
        var dt_layout = $('.main-content').attr('data-layout');

        var data = {
            action: 'corporatepro_loadmore_masonry_blog',
            security: corporatepro_ajax_fontend.security,
            except_post_ids: except_post_ids,
            dt_layout: dt_layout,
        };

        $('#cp-loadmore-masonry-posts').addClass('loading');

        $.post(corporatepro_ajax_fontend.ajaxurl, data, function (response) {
            var items = $('' + response[ 'html' ] + '');

            if ( $.trim(response[ 'success' ]) == 'ok' ) {

                items.imagesLoaded(function () {
                    $(".portfolio-grid").append(items).isotope('appended', items, true);
                    cp_masonry();
                    init_carousel();
                });

            } else {
                $('.portfolio-grid').append('<p class="return-message bg-success">Not ok</p>');
            }

            $('#cp-loadmore-masonry-posts').removeClass('loading');

        });

        return false;
    })

    function remove_() {
        $('.widget_product_categories .accordion-style2>li').each(function () {
            $('span.count:lt(1)', this).remove()
        });
        $('.project-style-v3').each(function () {
            $('.cp-countdown', this).remove()
        });
    }

    function cp_google_maps() {
        if ( $('.cp-google-maps').length == 1 && corporatepro_global.gmap_api_key != '' ) {
            $('.cp-google-maps').each(function () {
                var $this            = $(this),
                    $id              = $this.attr('id'),
                    $title_maps      = $this.attr('data-title_maps'),
                    $phone           = $this.attr('data-phone'),
                    $email           = $this.attr('data-email'),
                    $zoom            = parseInt($this.attr('data-zoom')),
                    $latitude        = $this.attr('data-latitude'),
                    $longitude       = $this.attr('data-longitude'),
                    $address         = $this.attr('data-address'),
                    $map_type        = $this.attr('data-map-type'),
                    $pin_icon        = $this.attr('data-pin-icon'),
                    $modify_coloring = $this.attr('data-modify-coloring') === "true" ? true : false,
                    $saturation      = $this.attr('data-saturation'),
                    $hue             = $this.attr('data-hue'),
                    $map_style       = $this.data('map-style'),
                    $styles;

                if ( $modify_coloring == true ) {
                    var $styles = [
                        {
                            stylers: [
                                {hue: $hue},
                                {invert_lightness: false},
                                {saturation: $saturation},
                                {lightness: 1},
                                {
                                    featureType: "landscape.man_made",
                                    stylers: [ {
                                        visibility: "on"
                                    } ]
                                }
                            ]
                        }, {
                            featureType: 'water',
                            elementType: 'geometry',
                            stylers: [
                                {color: '#46bcec'}
                            ]
                        }
                    ];
                }
                var map;
                var bounds     = new google.maps.LatLngBounds();
                var mapOptions = {
                    zoom: $zoom,
                    panControl: true,
                    zoomControl: true,
                    mapTypeControl: true,
                    scaleControl: true,
                    draggable: true,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId[ $map_type ],
                    styles: $styles
                };

                map = new google.maps.Map(document.getElementById($id), mapOptions);
                map.setTilt(45);

                // Multiple Markers
                var markers           = [];
                var infoWindowContent = [];

                if ( $latitude != '' && $longitude != '' ) {
                    markers[ 0 ]           = [ $address, $latitude, $longitude ];
                    infoWindowContent[ 0 ] = [ $address ];
                }

                var infoWindow = new google.maps.InfoWindow(), marker, i;

                for ( i = 0; i < markers.length; i++ ) {
                    var position = new google.maps.LatLng(markers[ i ][ 1 ], markers[ i ][ 2 ]);
                    bounds.extend(position);
                    marker = new google.maps.Marker({
                        position: position,
                        map: map,
                        title: markers[ i ][ 0 ],
                        icon: $pin_icon
                    });
                    if ( $map_style == '1' ) {

                        if ( infoWindowContent[ i ][ 0 ].length > 1 ) {
                            infoWindow.setContent(
                                '<div style="color:#000; background-color:#fff; padding:5px; width:350px;line-height: 25px" class="cp-map-info">' +
                                '<h4>' + $title_maps + '</h4>' +
                                '<i class="fa fa-map-marker"></i>&nbsp;' + $address + '<br/>' +
                                '<i class="fa fa-phone"></i>&nbsp;' + $phone + '<br/> ' +
                                '<i class="fa fa-envelope"></i><a href="mailto:' + $email + '">&nbsp;' + $email + '</a><br/> ' +
                                '</div>'
                            );
                        }

                        infoWindow.open(map, marker);

                    }
                    if ( $map_style == '2' ) {
                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                if ( infoWindowContent[ i ][ 0 ].length > 1 ) {
                                    infoWindow.setContent(
                                        '<div style="color:#000; background-color:#fff; padding:5px; width:350px;line-height: 25px" class="cp-map-info">' +
                                        '<h4>' + $title_maps + '</h4>' +
                                        '<i class="fa fa-map-marker"></i>&nbsp;' + $address + '<br/>' +
                                        '<i class="fa fa-phone"></i>&nbsp;' + $phone + '<br/> ' +
                                        '<i class="fa fa-envelope"></i><a href="mailto:' + $email + '">&nbsp;' + $email + '</a><br/> ' +
                                        '</div>'
                                    );
                                }

                                infoWindow.open(map, marker);
                            }
                        })(marker, i));
                    }

                    map.fitBounds(bounds);
                }

                var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
                    this.setZoom($zoom);
                    google.maps.event.removeListener(boundsListener);
                });
            });
        }
    }

    $(window).load(function () {
    });
    /* ---------------------------------------------
     Scripts resize
     --------------------------------------------- */
    $(window).resize(function () {
        init_carousel();
        cp_masonry();
        cp_height_equal();
    });

    /* ---------------------------------------------
     Scripts scroll
     --------------------------------------------- */
    $(window).scroll(function () {

    });

    $(document).ready(function () {
        dot_owl();
        init_carousel();
    });

    window.onload = function () {
        WebFont.load({
            google: {
                families: [
                    'Poppins:400,300,500,600,700',
                    'Great+Vibes',
                ]
            }
        });
        cp_google_maps();
        number_dots();
        remove_();
        corporatepro_woo_quantily();
        cp_sticky();
        parallaxInit();
        $('input, textarea').placeholder();
        cp_height_equal();
        cp_masonry();
    }
    //document.onload = cp_masonry;
})(jQuery); // End of use strict

