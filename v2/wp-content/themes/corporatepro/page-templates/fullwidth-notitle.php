<?php
/**
 * Template Name: Full Width No Title
 *
 * @package WordPress
 * @subpackage corporatepro
 * @since corporatepro 1.0.0
 */
get_header();
?>
    <div class="fullwidth-template">
        <?php get_template_part('template-parts/page','banner');?>
        <div class="container">
            <div class="main-content">
                <?php
                if( have_posts()){
                    while( have_posts()){
                        the_post();
                        ?>
                        <div class="page-main-content">
                            <?php the_content();?>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
<?php
get_footer();
