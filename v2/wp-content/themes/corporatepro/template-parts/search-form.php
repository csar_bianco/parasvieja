<?php  
$header_layout = corporatepro_get_option('opt_header_layout');
?>
<?php if (($header_layout == 'style-08') || ($header_layout == 'style-05') || ($header_layout == 'style-11')) : ?>
	<span class="close-touch"><i class="fa fa-close"></i></span>
	<form method="get" action="<?php echo esc_url( home_url( '/' ) ) ?>" class="form-search">
        <input name="s" placeholder="<?php esc_html_e('Search...','corporatepro');?>" type="search">
        <span>
            <button class="search-submit" type="submit">
            	<i class="fa fa-search"></i>
            </button>
        </span>
	</form>
<?php else : ?>
	<a href="javascript:void(0)" class="search-togole"><i class="fa fa-search"></i></a>
	<div class="header-search">
		<form method="get" action="<?php echo esc_url( home_url( '/' ) ) ?>" class="form-search">
	        <input name="s" placeholder="<?php esc_html_e('Search...','corporatepro');?>" type="search">
	        <span>
	            <button class="search-submit" type="submit">
	            	<i class="fa fa-search"></i>
	            </button>
	        </span>
		</form>
	</div>
<?php endif; ?>