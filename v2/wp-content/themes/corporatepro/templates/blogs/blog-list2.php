<?php
if( have_posts()){
	?>
	<div class="cp-blog blog-style7">
		<div class="cp-blog-content">
        <?php
		while( have_posts()){
			the_post();
			?>
            <div <?php post_class('blog-item');?>>
                <div class="post-item-info">
                    <?php get_template_part('templates/blogs/post','fomats');?>
                    <div class="main-content-post">
	        			<h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
	        			<?php if( has_tag() ):?>
	        			<div class="tag-pots">
	        				<span><i class="fa fa-tag"></i><?php esc_html_e('Tags','corporatepro');?>:</span>
	        				<?php the_tags( '',''); ?> 
	        			</div>
	        			<?php endif;?>
	        			<div class="post-excerpt"><?php the_excerpt();?></div>
	        			<a class="post-readmore outline-button" href="<?php the_permalink();?>"><?php esc_html_e('Read more','corporatepro')?></a>
	        		</div>
                </div>
            </div>
			<?php
		}
		?>
		</div>
	</div>
	<?php
	corporatepro_paging_nav();
}else{
	get_template_part( 'content', 'none' );
}