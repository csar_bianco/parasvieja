<?php
$opt_blog_about_author = corporatepro_get_option('opt_blog_about_author',0);
?>
<div class="cp-blog-single blog-single-2">
    <?php
    while( have_posts()){
        the_post();
        ?>
		<?php wpb_set_post_views(get_the_ID()); ?>
        <div <?php post_class('blog-item');?> >
			<h1 class="post-title"><?php the_title();?></h1>

			<?php get_template_part('templates/blogs/post','fomats');?>
			<ul class="meta-post meta-style4">
				<?php if( !is_sticky() ): ?>
				<li class="date"><i class="fa fa-calendar-o"></i><?php echo get_the_date('M j');?></li>
				<?php endif;?>
				<li class="comment-count">
					<i class="fa fa-comment-o"></i>
					<?php comments_number(
	                    esc_html__('0', 'corporatepro'),
	                    esc_html__('1', 'corporatepro'),
	                    esc_html__('%', 'corporatepro')
	                );
	                ?>
				</li>
				<li class="author"><i class="fa fa-user"></i><?php the_author_link(); ?></li>
			</ul>
			<div class="post-content">
				<?php the_content();?>
			</div>
			<?php if( has_tag() ):?>
			<div class="tag-pots">
				<span><i class="fa fa-tag"></i><?php esc_html_e('Tags','corporatepro');?>:</span>
				<?php the_tags( '',''); ?> 
			</div>
			<?php endif;?>
		</div>
        <?php
    }
    ?>
    <?php if( $opt_blog_about_author == 1 ):?>
    <?php get_template_part('templates/blogs/blog','bio');?>
	<?php endif;?>
</div>
<?php
wp_link_pages( array(
    'before'      => '<div class="page-links">',
    'after'       => '</div>',
    'link_before' => '<span>',
    'link_after'  => '</span>',
    'pagelink'    => '%',
    'separator'   => '',
) );
?>