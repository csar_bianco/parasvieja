<?php
/*
 Name:	Footer style 04
 */
?>
<!-- Footer -->
<footer class="footer footer-style4">
	<div class="container">
		<?php the_content();?>
	</div>
</footer>
<!-- /Footer -->