<?php
/*
 Name:	Footer style 05
 */
?>
<!-- Footer -->
<footer class="footer footer-style6">
	<div class="container">
		<?php the_content();?>
	</div>
	<a href="javascript:void(0)" class="backtotop"><i class="fa fa-angle-up"></i></a>
</footer>
<!-- /Footer -->