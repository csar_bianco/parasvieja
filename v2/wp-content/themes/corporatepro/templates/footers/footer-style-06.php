<?php
/*
 Name:	Footer style 06
 */
?>
<!-- Footer -->
<footer class="footer footer-style-7">
	<div class="main-footer">
		<div class="container">
			<?php the_content();?>
		</div>
	</div>
</footer>
<!-- /Footer -->