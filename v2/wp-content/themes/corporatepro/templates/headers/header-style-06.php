<?php
/*
Name:	Header style 06
*/
?>
<!-- Header -->
<header class="header header-style6">
	<div class="topbar">
		<div class="container">
			<div class="left-topbar">
				<div class="contact-support">
					<?php 
        				wp_nav_menu( array(
        					'menu'            => 'top-bar-left-menu',
        					'theme_location'  => 'top-bar-left-menu',
        					'container'       => '',
        					'container_class' => '',
        					'container_id'    => '',
        					'menu_class'      => '',
        					'fallback_cb'     => 'koolshop_bootstrap_navwalker::fallback',
        					'walker'          => new koolshop_bootstrap_navwalker()
                        ));
                    ?>
				</div>
			</div>
			<div class="right-topbar">
				<div class="account-link">
					<?php 
        				wp_nav_menu( array(
        					'menu'            => 'top-bar-right-menu',
        					'theme_location'  => 'top-bar-right-menu',
        					'container'       => '',
        					'container_class' => '',
        					'container_id'    => '',
        					'menu_class'      => '',
        					'fallback_cb'     => 'koolshop_bootstrap_navwalker::fallback',
        					'walker'          => new koolshop_bootstrap_navwalker()
                        ));
                    ?>
				</div>
			</div>
		</div>
	</div>
	<div class="main-header">
		<div class="container">
			<div class="header-content">
				<div class="logo">
					<?php corporatepro_get_logo();?>
				</div>
				<a href="javascript:void(0)" class="menu-togole"><i class="fa fa-bars"></i></a>
				<div class="cp-main-menu">
					<nav class="navigation">
						<?php 
	        				wp_nav_menu( array(
	        					'menu'            => 'primary',
	        					'theme_location'  => 'primary',
	        					'depth'           => 3,
	        					'container'       => '',
	        					'container_class' => '',
	        					'container_id'    => '',
	        					'menu_class'      => '',
	        					'fallback_cb'     => 'koolshop_bootstrap_navwalker::fallback',
	        					'walker'          => new koolshop_bootstrap_navwalker()
	                        ));
	                    ?>
					</nav>
				</div>
				<!-- Search form -->
				<?php corporatepro_get_search_form();?>
				<!-- ./Search form -->
			</div>
		</div>
	</div>
</header>
<!-- /Header -->