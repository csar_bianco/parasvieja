<?php
/*
Name:	Header style 07
*/
$slide_rv = corporatepro_get_option('opt_header_slider');
?>
<!-- Header -->
<header class="header header-style7 trans">
	<div class="main-header cp-menu-bottom">
		<div class="container">
			<div class="header-content">
				<div class="logo">
					<?php corporatepro_get_logo();?>
				</div>
				<a href="javascript:void(0)" class="menu-togole"><i class="fa fa-bars"></i></a>
				<div class="cp-main-menu">
					<nav class="navigation">
						<?php 
	        				wp_nav_menu( array(
	        					'menu'            => 'primary',
	        					'theme_location'  => 'primary',
	        					'container'       => '',
	        					'container_class' => '',
	        					'container_id'    => '',
	        					'menu_class'      => '',
	        					'fallback_cb'     => 'koolshop_bootstrap_navwalker::fallback',
	        					'walker'          => new koolshop_bootstrap_navwalker()
	                        ));
	                     ?>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<div class="cp-slide-home slide-home7">
		<?php echo do_shortcode( '[rev_slider alias="'.$slide_rv.'"]' ); ?>
	</div>
</header>
<!-- /Header -->