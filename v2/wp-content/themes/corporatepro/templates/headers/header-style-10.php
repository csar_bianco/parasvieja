<?php
/*
Name:	Header style 09
*/
?>
<!-- Header -->
<header class="header header-style8 trans">
	<div class="main-header">
		<div class="container">
			<div class="header-content">
				<div class="logo">
					<?php corporatepro_get_logo();?>
				</div>
				<div class="cp-navigation cp-nav-menu2">
					<a href="javascript:void(0)" class="menu-togole active">
						<i class="fa fa-bars"></i>
						<i class="fa fa-close"></i>
					</a>
					<div class="cp-main-menu" style="display: block;">
						<nav class="navigation">
							<?php 
								wp_nav_menu( array(
									'menu'            => 'primary',
									'theme_location'  => 'primary',
									'container'       => '',
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => '',
									'fallback_cb'     => 'koolshop_bootstrap_navwalker::fallback',
									'walker'          => new koolshop_bootstrap_navwalker()
					            ));
					        ?>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- Header -->
