<?php $names = get_the_terms($post->ID, 'category_project'); ?>
<div id="<?php echo 'post-' . get_the_ID() ?>" class="item-project item-portfolio
    <?php
if (!empty($names) && !is_wp_error($names)) {
    foreach ($names as $name) {
        echo $name->slug . ' ';
    }
}
?>
">
    <div class="pj-caption">
        <div class="pj-image">
            <figure>
                <?php $image_thumb = corporatepro_resize_image(get_post_thumbnail_id(get_the_ID()), null, 270, 270, true, true, false); ?>
                <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>"
                     width="<?php echo intval($image_thumb['width']) ?>"
                     height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
            </figure>
            <div class="pj-hover">
                <a href="<?php the_permalink(); ?>" class="pj-icon"><i class="fa fa-link"></i></a>
            </div>
        </div>
    </div>
    <div class="pj-info">
        <h3 class="pj-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    </div>
</div>