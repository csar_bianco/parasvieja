<div class="cp-project-single pj-single-2">
	<?php
        while (have_posts()) : the_post();
            $categories_list = get_the_term_list($post->ID,'category_project','',' ');
            $names = get_the_terms($post->ID,'category_project');
            $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, 960, 550, true, true, false );
            $value = get_post_meta( get_the_ID(),'_corporatepro_images_gallery',true); 
            $url = get_post_meta( get_the_ID(),'_corporatepro_url_file',true);
            $title_url = get_post_meta( get_the_ID(),'_corporatepro_title_url',true);
    ?>
	<div class="item-porject">
		<div class="page-title pj-title">
			<h2><?php the_title(); ?></h2>
		</div>
		<div class="pj-content">
			<div class="pj-top-content">
				<div class="pj-caption">
                    <?php 
                        if ( ! empty( $value ) ) {
                    ?>
                        <div class="pj-image owl-carousel nav-style1" data-nav='true' data-items="1" data-dots="false">
                            <figure>
                                <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
                            </figure>
                            <?php foreach ($value as $key => $item) { ?>
                                <?php $gallery_thumb  = corporatepro_resize_image( null, $item, 960, 550, true, true, false ); ?>
                                <figure>
                                    <img src="<?php echo esc_attr($gallery_thumb['url']); ?>" width="<?php echo intval($gallery_thumb['width']) ?>" height="<?php echo intval($gallery_thumb['height']) ?>"  alt="<?php the_title() ?>">
                                </figure>
                            <?php } ?>
                        </div>
                    <?php   } else { ?>
                        <div class="pj-image">
                            <figure>
                                <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
                            </figure>
                        </div>
                    <?php   } ?>

                    <?php if ( ! empty( $url ) ) { ?>
                        <a class="pj-bt-download" href="<?php echo $url; ?>">
	                        <i class="fa fa-file-pdf-o"></i>
	                        <?php echo $title_url ?>
                        </a>
                    <?php  } ?>
				</div>
				<div class="pj-info">
					<div class="pj-excerpt"><?php the_title() ?></div>
					<div class="pj-meta">
	                    <ul>
	                        <li><i class="fa fa-calendar-o"></i><?php the_time('M j, Y') ?></li>
	                        <li><i class="fa fa-user"></i>
	                            <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))) ?>">
	                                <?php the_author(); ?>
	                            </a>
	                        </li>
	                        <li><i class="fa fa-folder-o"></i>
	                            <?php
	                                if ($categories_list ) {
	                                    printf( esc_html__('%1$s', 'corporatepro') , $categories_list);
	                                }
	                            ?>
	                        </li>
	                    </ul>
	                </div>
				</div>
			</div>
			<div class="pj-desc">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<?php
        endwhile; wp_reset_postdata();
    ?>
</div>