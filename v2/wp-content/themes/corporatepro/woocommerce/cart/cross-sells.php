<?php
/**
 * Cross-sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cross-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     4.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product, $woocommerce_loop;

if ( ! $cross_sells ) {
	return;
}

$woocommerce_loop['name']    = 'cross-sells';
$woocommerce_loop['columns'] = apply_filters( 'woocommerce_cross_sells_columns', $columns );

$woo_cross_sell_lg_items = corporatepro_get_option('woo_cross_sell_lg_items',3);
$woo_cross_sell_md_items = corporatepro_get_option('woo_cross_sell_md_items',3);
$woo_cross_sell_sm_items = corporatepro_get_option('woo_cross_sell_sm_items',2);
$woo_cross_sell_xs_items = corporatepro_get_option('woo_cross_sell_xs_items',2);
$woo_cross_sell_ts_items = corporatepro_get_option('woo_cross_sell_ts_items',1);

$data_reponsive = array(
	'0'=>array(
		'items'=>$woo_cross_sell_ts_items
	),
	'480'=>array(
		'items'=>$woo_cross_sell_xs_items
	),
	'768'=>array(
		'items'=>$woo_cross_sell_sm_items
	),
	'992'=>array(
		'items'=>$woo_cross_sell_md_items
	),
	'1200'=>array(
		'items'=>$woo_cross_sell_lg_items
	),
);

$data_reponsive = json_encode($data_reponsive);
$loop = 'false';

if ( $cross_sells ) : ?>
	<div class="col-sm-6">
		<div class="cross-sells">
			<?php
			if( count($cross_sells) > $woo_cross_sell_lg_items){
				$loop = 'true';
			}
			$woo_cross_sell_title = corporatepro_get_option('woo_cross_sell_title','You may be interested in...');
			$classes = array('product-item product');
			?>

			<h4><?php echo esc_html($woo_cross_sell_title); ?></h4>
			<?php woocommerce_product_loop_start(); ?>
				<div class="product-grid product-slide owl-carousel nav-style4" data-margin="30" data-nav="false" data-dots="false" data-loop="<?php echo esc_attr($loop);?>" data-responsive='<?php echo esc_attr($data_reponsive);?>'>
	                <?php foreach ( $cross_sells as $cross_sell ) : ?>
						<?php
						$post_object = get_post( $cross_sell->get_id() );

						setup_postdata( $GLOBALS['post'] =& $post_object );
                        ?>
                        <div <?php post_class(  ); ?>>
                        <?php wc_get_template_part( 'product-styles/content-product', 'style-1' ); ?>
                        </div>
	                <?php endforeach; ?>
				</div>
			<?php woocommerce_product_loop_end(); ?>
		</div>
	</div>

<?php endif;

wp_reset_query();
