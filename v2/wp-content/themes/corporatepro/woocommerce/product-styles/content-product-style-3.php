<?php
/**
 * woocommerce_before_shop_loop_item hook.
 *
 * @hooked woocommerce_template_loop_product_link_open - 10
 */
do_action( 'woocommerce_before_shop_loop_item' );
?>
<div class="item-project project-style-v3">
	<div class="pj-caption">
		<div class="pj-image">
			<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
			?>
		</div>
		<div class="pj-button">
			<?php
				do_action('corporatepro_function_shop_loop_item_wishlist');
				do_action('corporatepro_function_shop_loop_item_compare');
				do_action('corporatepro_function_shop_loop_item_quickview');
			?>
		</div>
		<div class="pj-info">
			<?php
			/**
			 * woocommerce_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_template_loop_product_title - 10
			 */
			do_action( 'woocommerce_shop_loop_item_title' );
			?>
		</div>
	</div>
</div>