<?php
remove_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title',10);
remove_action('woocommerce_before_shop_loop_item_title','corporatepro_shop_loop_item_contdown',20);
global $product;

if ( get_option( 'woocommerce_enable_review_rating' ) === 'no' )
    return;
?>
<?php
/**
 * woocommerce_before_shop_loop_item hook.
 *
 * @hooked woocommerce_template_loop_product_link_open - 10
 */
do_action( 'woocommerce_before_shop_loop_item' );
?>
<div class="product-inner">
	<div class="product-thumb">
		<?php
		/**
		 * woocommerce_before_shop_loop_item_title hook.
		 *
		 * @hooked woocommerce_show_product_loop_sale_flash - 10
		 * @hooked woocommerce_template_loop_product_thumbnail - 10
		 */
		do_action( 'woocommerce_before_shop_loop_item_title' );
		?>
		<div class="hover-thumb">
			<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item_title' );
			do_action( 'corporatepro_function_shop_loop_item_quickview' );
			?>
		</div>
	</div>
	<div class="info-product">
		<?php
		echo '<h3 class="title-product pj-title"><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
		?>
        <?php if ( $rating_html = wc_get_rating_html( $product->get_average_rating() ) ) : ?>
            <?php echo $rating_html; ?>
        <?php else : ?>
            <div class="box-rating"></div>
        <?php endif; ?>
		<div class="product-excerpt"><?php echo wp_trim_words(get_the_excerpt(),10,'...'); ?></div>
		<div class="product-button">
			<?php
			/**
			 * woocommerce_after_shop_loop_item hook.
			 *
			 * @hooked woocommerce_template_loop_product_link_close - 5
			 * @hooked woocommerce_template_loop_add_to_cart - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item' );
			do_action('corporatepro_function_shop_loop_item_wishlist');
			do_action('corporatepro_function_shop_loop_item_compare');
			?>
		</div>
		
	</div>
</div>
<?php
add_action('woocommerce_shop_loop_item_title','corporatepro_template_loop_product_title',10);
add_action('woocommerce_before_shop_loop_item_title','corporatepro_shop_loop_item_contdown',20);
?>