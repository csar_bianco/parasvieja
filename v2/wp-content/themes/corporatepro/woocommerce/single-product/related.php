<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     4.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product, $woocommerce_loop;

if ( empty( $product ) || ! $product->exists() ) {
	return;
}

if ( ! $related_products ) {
	return;
}

$woocommerce_loop['name']    = 'related';
$woocommerce_loop['columns'] = apply_filters( 'woocommerce_related_products_columns', $columns );

$woo_related_product_lg_items = corporatepro_get_option('woo_related_product_lg_items',3);
$woo_related_product_md_items = corporatepro_get_option('woo_related_product_md_items',3);
$woo_related_product_sm_items = corporatepro_get_option('woo_related_product_sm_items',2);
$woo_related_product_xs_items = corporatepro_get_option('woo_related_product_xs_items',2);
$woo_related_product_ts_items = corporatepro_get_option('woo_related_product_ts_items',1);

$data_reponsive = array(
	'0'=>array(
		'items'=>$woo_related_product_ts_items
	),
	'480'=>array(
		'items'=>$woo_related_product_xs_items
	),
	'768'=>array(
		'items'=>$woo_related_product_sm_items
	),
	'992'=>array(
		'items'=>$woo_related_product_md_items
	),
	'1200'=>array(
		'items'=>$woo_related_product_lg_items
	),
);
$data_reponsive = json_encode($data_reponsive);
$loop = 'false';


if ( $related_products ) : ?>
	<div class="related products cp-product-latest">
		<?php
		$kt_related_products_title = corporatepro_get_option('woo_related_products_title','Related Products');
		$classes = array('product-item product');
		if (count($related_products) > $woo_related_product_lg_items ) {
			$loop = 'true';
		}
		?>

		<h4><?php echo esc_html($kt_related_products_title); ?></h4>
		<?php woocommerce_product_loop_start(); ?>
			<div class="product-grid product-slide owl-carousel nav-style4" data-margin="30" data-nav="true" data-dots="false" data-loop="<?php echo esc_attr($loop);?>" data-responsive='<?php echo esc_attr($data_reponsive);?>'>
                <?php foreach ( $related_products as $related_product ) : ?>
                    <?php
                        $post_object = get_post( $related_product->get_id() );

                        setup_postdata( $GLOBALS['post'] =& $post_object );
                    ?>
                    <div <?php post_class(  ); ?>>
                    <?php wc_get_template_part( 'product-styles/content-product', 'style-1' ); ?>
                    </div>

                <?php endforeach; ?>
		</div>
		<?php woocommerce_product_loop_end(); ?>
	</div>

<?php endif;

wp_reset_postdata();
