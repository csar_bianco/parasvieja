��          �      �       0     1  -   B     p     �     �     �  !   �  &   �  "   
  �   -     �  ;   �  8       >  4   O     �     �     �     �  !   �  6     3   9  �   m       ;   8                  	      
                                  Brice Capobianco Default - without underline & justify buttons Donate to this plugin &#187; Editor style More b*web Plugins Re-add justify only Re-add text underline and justify Re-add underline & justify + rearrange Re-add underline & justify buttons This very simple plugin re-adds the Editor text underline, text justify and rearrange buttons as they were before WordPress 4.7. https://www.b-website.com/ https://www.b-website.com/re-add-text-underline-and-justify PO-Revision-Date: 2018-06-01 18:39:41+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Plugins - Re-add text underline and justify - Stable (latest release)
 Brice Capobianco Por defecto - sin botones de subrayado y justificado Dona a este plugin &#187; Estilos del editor Más plugins de b*web Añadir solo justificado Re-add text underline and justify Volver a añadir subrayado y justificado + reorganizar Volver a añadir botones de subrayado y justificado Este plugin realmente sencillo vuelve a añadir al editor el subrayado de texto y el justificado total de texto y reorganiza los botones a como estaban antes de WordPress 4.7. https://www.b-website.com/ https://www.b-website.com/re-add-text-underline-and-justify 