<?php

if (azul_top('plugin_status') == 'enable') {
    add_action('get_header', 'azul_coming_soon_page');
}

function azul_coming_soon_page() {

	function access(){
		$current_user = wp_get_current_user();
		if (is_user_logged_in()) {
			if (azul_top('site_access') == "enable") {
		   		echo '';
			} else {
		    	require_once(azul_product_info('product_dir') . '/themes/theme.php');
		        die();
		    }
		} else {
			require_once(azul_product_info('product_dir') . '/themes/theme.php');
		    die();
		}
	}
	
	if (azul_top('date_ended') == 'disable') {
	    $date = azul_top('launch_date');
	    $today = date('Y-m-d h:i:s a');
	    if (strtotime($date) < strtotime($today)){ 
    		echo '';
		} else {
		    access();
    	}
	} else {
		access();
	}
	
}