<?php
echo '
<script type="text/javascript">

$(document).ready(function(){
	"use strict";
	
	/* ---------------------------------------------------------------------- */
	/*  VIDEO VIMEO
	/* ---------------------------------------------------------------------- */
		
	$(function(){
		$(\'#fullscreen-vimeo\').bgVimeoVideo({
			videoId: '.azul_top('video_internal_vimeo').',';
			if ( azul_top('video_sound_vimeo') == 'enable' ){
				echo 'videoVolume: 5';
			} else {
				echo 'videoVolume: 0'; 
			}
	    echo '
	    });
	});
		
});

</script>';
?>
