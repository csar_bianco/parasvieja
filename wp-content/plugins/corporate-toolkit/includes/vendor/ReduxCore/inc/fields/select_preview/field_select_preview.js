/* global confirm, redux, redux_change */
(function($) {
    "use strict";
    jQuery(document).ready(function() {

        $(document).on('change','.redux-select-images',function(){
            var url = $(this).find(':selected').data('preview');
            $(this).closest('.container-select-preview').find('.preview img').attr('src',url);
        })
    });
})(jQuery);

