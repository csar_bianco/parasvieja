<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_aboutbox_settings' );
function corporatepro_aboutbox_settings() {
    $socials = array();
    $all_socials = corporatepro_get_all_social();
    if( $all_socials ){
        foreach ($all_socials as $key =>  $social)
            $socials[$social['name']] = $key;
    }
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'OUR VISION',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'aboutbox/default.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'trueshop' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
        array(
            "type"        => "textarea",
            "heading"     => __( "Content", "corporatepro" ),
            "param_name"  => "short_description",
            "admin_label" => false
        ),
        array(
            'type' => 'vc_link',
            'heading' => __( 'URL (Link)', 'kute-toolkit' ),
            'param_name' => 'link',
            'description' => __( 'Add link.', 'kute-toolkit' ),
        ),
        array(
            "type"        => "attach_images",
            "heading"     => __( "Images", "corporatepro" ),
            "param_name"  => "images",
            "admin_label" => false
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "trueshop"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'trueshop' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'trueshop' ),
        )
    );
    $map_settings = array(
        'name'     => esc_html__( 'Corporate: About Box', 'trueshop' ),
        'base'     => 'corporatepro_aboutbox', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        'description'   =>  __( 'Display a about box.', 'trueshop' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_aboutbox( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_aboutbox', $atts ) : $atts;

    $default_atts = array(
        'layout'            => 'default',
        'title'             => '',
        'short_description' => '',
        'link'              => '',
        'images'            =>'',
        'css'               => '',
        'el_class'          => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' about-box '.$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/aboutbox/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_aboutbox', 'corporatepro_aboutbox' );
