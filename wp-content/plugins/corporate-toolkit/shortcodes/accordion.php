<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_accordion_settings' );
function corporatepro_accordion_settings() {
    $socials = array();
    $all_socials = corporatepro_get_all_social();
    if( $all_socials ){
        foreach ($all_socials as $key =>  $social)
            $socials[$social['name']] = $key;
    }
    $params  = array(
        array(
            'type'      => 'kt_select_preview',
            'heading'   => __( 'Layout', 'corporatepro' ),
            'value'     => array(
                'default'   =>array(
                    'alt'   =>'Default',
                    'img'   =>CORPORATEPRO_SHORTCODE_IMG_URL.'accordion/default.jpg'
                ),
                'layout1'   =>array(
                    'alt'   =>'Layout 01',
                    'img'   =>CORPORATEPRO_SHORTCODE_IMG_URL.'accordion/layout1.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label'   => true,
            'param_name'    => 'layout',
            'description'   => __( 'Select a layout.', 'corporatepro' ),
        ),
        array(
            'type'          => 'dropdown',
            'param_name'    => 'style_accordion',
            'value' => array(
                __( 'Style 3', 'corporatepro' ) => 'accordion-style3',
                __( 'Style 4', 'corporatepro' ) => 'accordion-style4',
                __( 'Style 9', 'corporatepro' ) => 'accordion-style8',
            ),
            'std'           => 'accordion-style3',
            'heading'       => __( 'Accordion Style', 'corporatepro' ),
            'description'   => __( 'Select style accordion.', 'corporatepro' ),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'default',
            ),
        ),
        array(
            "type"          => "param_group",
            "heading"       => __( "Items", "corporatepro" ),
            "admin_label"   => false,
            "param_name"    => "items",
            "params"        => array(
                array(
                    "type"        => "textfield",
                    "heading"     => __( "Title", "corporatepro" ),
                    "param_name"  => "title",
                    "admin_label" => false
                ), 
                array(
                    "type"        => "textarea",
                    "heading"     => __( "Description", "corporatepro" ),
                    "param_name"  => "description",
                    "admin_label" => false
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => __('Icon library', 'trueshop'),
                    'value'       => array(
                        __('Font Awesome', 'trueshop')     => 'fontawesome',
                    ),
                    'admin_label' => true,
                    'param_name'  => 'icon_lib',
                    'description' => __('Select icon library.', 'trueshop'),
                ),
                array(
                    'type'        => 'iconpicker',
                    'heading'     => __('Icon', 'trueshop'),
                    'param_name'  => 'icon_fontawesome',
                    'value'       => 'fa fa-adjust', // default value to backend editor admin_label
                    'settings'    => array(
                        'emptyIcon'    => false,
                        'iconsPerPage' => 4000,
                    ),
                    'dependency'  => array(
                        'element' => 'icon_lib',
                        'value'   => 'fontawesome',
                    ),
                    'description' => __('Select icon from library.', 'trueshop'),
                ),
                array(
                    'type' => 'vc_link',
                    'heading' => __( 'URL (Link)', 'kute-toolkit' ),
                    'param_name' => 'link',
                    'description' => __( 'Add link.', 'kute-toolkit' ),
                ),
            )
        ),
        // Control Icons END
        array(
            'type'          => 'textfield',
            'param_name'    => 'active_section',
            'heading'       => __( 'Active section', 'corporatepro' ),
            'value'         => 0,
            'description'   => __( 'Enter active section number (Note: to have all sections closed on initial load enter non-existing number).', 'corporatepro' ),
        ),
        array(
            "type"          => "textfield",
            "heading"       => __("Extra class name", "corporatepro"),
            "param_name"    => "el_class",
            "description"   => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'corporatepro' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'corporatepro' ),
        )
    );
    $map_settings = array(
        'name'          => esc_html__( 'Corporate: Accordion', 'corporatepro' ),
        'base'          => 'corporatepro_accordion', // shortcode
        'icon'          => 'icon-wpb-ui-accordion',
        'category'      => esc_html__( 'Corporate', 'corporatepro' ),
        'description'   =>  __( 'Display a accordion.', 'corporatepro' ),
        'params'        => $params,
    );

    vc_map( $map_settings );
}

function corporatepro_accordion( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_accordion', $atts ) : $atts;

    $default_atts = array(
        'layout'            => 'default',
        'items'             => '',
        'active_section'    => '',
        'css'               => '',
        'el_class'          => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' ' .$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/accordion/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_accordion', 'corporatepro_accordion' );
