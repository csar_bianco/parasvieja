<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_banner_settings' );
function corporatepro_banner_settings() {
    $socials = array();
    $all_socials = corporatepro_get_all_social();
    if( $all_socials ){
        foreach ($all_socials as $key =>  $social)
            $socials[$social['name']] = $key;
    }
    $params  = array(
        array(
            'type'      => 'kt_select_preview',
            'heading'   => __( 'Layout', 'corporatepro' ),
            'value'     => array(
                'default'   =>array(
                    'alt'   =>'Default',
                    'img'   =>CORPORATEPRO_SHORTCODE_IMG_URL.'banner/default.jpg'
                ),
                'layout1'   =>array(
                    'alt'   =>'Layout 01',
                    'img'   =>CORPORATEPRO_SHORTCODE_IMG_URL.'banner/layout1.jpg'
                ),
                'layout2'   =>array(
                    'alt'   =>'Layout 02',
                    'img'   =>CORPORATEPRO_SHORTCODE_IMG_URL.'banner/layout2.jpg'
                ),
                'layout3'   =>array(
                    'alt'   =>'Layout 03',
                    'img'   =>CORPORATEPRO_SHORTCODE_IMG_URL.'banner/layout3.jpg'
                ),
                'layout4'   =>array(
                    'alt'   =>'Layout 04',
                    'img'   =>CORPORATEPRO_SHORTCODE_IMG_URL.'banner/layout4.jpg'
                ),
                'layout5'   =>array(
                    'alt'   =>'Layout 05',
                    'img'   =>CORPORATEPRO_SHORTCODE_IMG_URL.'banner/layout5.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label'   => true,
            'param_name'    => 'layout',
            'description'   => __( 'Select a layout.', 'corporatepro' ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __( "Sticky", "corporatepro" ),
            "param_name"  => "sticky",
            "admin_label" => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout4'),
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __( "Title", "corporatepro" ),
            "param_name"  => "main_title",
            "admin_label" => false,
            'dependency'  => array(
                'element' => 'layout',
                'value_not_equal_to' => array('default')
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __( "Sub Title", "corporatepro" ),
            "param_name"  => "sub_title",
            "admin_label" => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout4'),
            ),
        ),
        array(
            "type"        => "textarea",
            "heading"     => __( "Description", "corporatepro" ),
            "param_name"  => "main_des",
            "admin_label" => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout1','layout2','layout4','layout5'),
            ),
        ),
        array(
            "type"        => "attach_image",
            "heading"     => __( "Background", "corporatepro" ),
            "param_name"  => "bg_banner",
            "admin_label" => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout3','layout2','layout4','layout5'),
            ),
        ),
        array(
            "type"        => "kt_number",
            "heading"     => __("Width", 'corporatepro'),
            "param_name"  => "width",
            "value"       => '300',
            "suffix"      => __("px", 'corporatepro'),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout2',
            ),
        ),
        array(
            "type"        => "kt_number",
            "heading"     => __("Height", 'corporatepro'),
            "param_name"  => "height",
            "value"       => '300',
            "suffix"      => __("px", 'corporatepro'),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout2',
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Option Style Banner', 'corporatepro'),
            'value'       => array(
                __('Light', 'corporatepro') => 'light',
                __('Dark', 'corporatepro')  => 'dark',
            ),
            'admin_label' => true,
            'param_name'  => 'style_ads',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout2',
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Option Style Box', 'corporatepro'),
            'value'       => array(
                __('Right', 'corporatepro') => 'box-right',
                __('Left', 'corporatepro')  => 'box-left',
            ),
            'admin_label' => true,
            'param_name'  => 'style_box',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout2',
            ),
        ),
        array(
            "type"          => "param_group",
            "heading"       => __( "Items: ", "corporatepro" ),
            "admin_label"   => false,
            "param_name"    => "list",
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('layout3','layout5'),
            ),
            "params"        => array(
                array(
                    "type"        => "textfield",
                    "heading"     => __( "Title", "corporatepro" ),
                    "param_name"  => "title",
                    "admin_label" => false
                ),
                array(
                    'type' => 'vc_link',
                    'heading' => __( 'URL (Link)', 'xshop-core' ),
                    'param_name' => 'links',
                    'description' => __( 'Add link to custom heading.', 'corporatepro' ),
                ),
            )
        ),
        array(
            "type"          => "param_group",
            "heading"       => __( "Items", "corporatepro" ),
            "admin_label"   => false,
            "param_name"    => "items",
            'description'   => __( 'Only 4 field.', 'corporatepro' ),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'default',
            ),
            "params"        => array(
                array(
                    "type"        => "textfield",
                    "heading"     => __( "Title", "corporatepro" ),
                    "param_name"  => "title",
                    "admin_label" => false
                ),
                array(
                    "type"        => "textfield",
                    "heading"     => __( "Description", "corporatepro" ),
                    "param_name"  => "description",
                    "admin_label" => false
                ),
            )
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Icon library', 'corporatepro'),
            'value'       => array(
                __('Font Awesome', 'corporatepro')     => 'fontawesome',
            ),
            'admin_label' => true,
            'param_name'  => 'icon_lib',
            'description' => __('Select icon library.', 'corporatepro'),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('default','layout4'),
            ),
        ),
        array(
            'type'        => 'iconpicker',
            'heading'     => __('Icon', 'corporatepro'),
            'param_name'  => 'icon_fontawesome',
            'value'       => 'fa fa-adjust', // default value to backend editor admin_label
            'settings'    => array(
                'emptyIcon'    => false,
                'iconsPerPage' => 4000,
            ),
            'dependency'  => array(
                'element' => 'icon_lib',
                'value'   => 'fontawesome',
            ),
            'description' => __('Select icon from library.', 'corporatepro'),
        ),
        array(
            'type' => 'vc_link',
            'heading' => __( 'URL (Link)', 'xshop-core' ),
            'param_name' => 'link',
            'description' => __( 'Add link to custom heading.', 'corporatepro' ),
            'dependency'  => array(
                'element' => 'layout',
                'value_not_equal_to' => array('layout3')
            ),
        ),
        array(
            "type"          => "textfield",
            "heading"       => __("Extra class name", "corporatepro"),
            "param_name"    => "el_class",
            "description"   => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'corporatepro' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'corporatepro' ),
        )
    );
    $map_settings = array(
        'name'          => esc_html__( 'Corporate: Banner', 'corporatepro' ),
        'base'          => 'corporatepro_banner', // shortcode
        'category'      => esc_html__( 'Corporate', 'corporatepro' ),
        'description'   =>  __( 'Display a banner.', 'corporatepro' ),
        'params'        => $params,
    );

    vc_map( $map_settings );
}

function corporatepro_banner( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_banner', $atts ) : $atts;

    $default_atts = array(
        'layout'            => 'default',
        'items'             => '',
        'sticky'            => '',
        'sub_title'         => '',
        'main_title'        => '',
        'main_des'          => '',
        'width'             => '',
        'height'            => '',
        'list'              => '',
        'bg_banner'         => '',
        'link'              => '',
        'style_ads'         => 'light',
        'style_box'         => 'box-left',
        'icon_fontawesome'  => '',
        'css'               => '',
        'el_class'          => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' '.$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/banner/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_banner', 'corporatepro_banner' );
