<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_contactinfo_settings' );
function corporatepro_contactinfo_settings() {

    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'corporatepro' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'contactinfo/default.jpg'
                ),
                'style1'=>array(
                    'alt'=>'Style 01',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'contactinfo/style1.jpg'
                ),
                'style2'=>array(
                    'alt'=>'Style 02',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'contactinfo/style2.jpg'
                ),
                'style3'=>array(
                    'alt'=>'Style 03',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'contactinfo/style3.jpg'
                ),
                'style4'=>array(
                    'alt'=>'Style 04',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'contactinfo/style4.jpg'
                ),
                'style5'=>array(
                    'alt'=>'Style 05',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'contactinfo/style5.jpg'
                ),
                'style6'=>array(
                    'alt'=>'Style 06',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'contactinfo/style6.jpg'
                ),
            ),
            'default'     =>'default',
            'admin_label' => true,
            'param_name'  => 'layout',
            'description' => __( 'Select a layout.', 'corporatepro' ),
        ),
        array(
            "type"        => "attach_image",
            "heading"     => __( "Logo Contact: ", 'corporatepro' ),
            "param_name"  => "img_logo",
            "admin_label" => true,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style3'),
            ),
        ),
        array(
            'type'        => 'textfield',
            'heading'     => __('Title', 'corporatepro'),
            'param_name'  => 'title',
            'description' => __('The title of shortcode', 'corporatepro'),
            'admin_label' => true,
            'std'         => 'Contact Me',
        ),
        array(
            'type'        => 'textfield',
            'heading'     => __('Description', 'corporatepro'),
            'param_name'  => 'description',
            'description' => __('The description of shortcode', 'corporatepro'),
            'admin_label' => true,
            'std'         => 'Praesent vbulum molestie lacus nean nonummy henerituris.',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style2', 'style3'),
            ),
        ),
        array(
            "type"        => "attach_image",
            "heading"     => __( "Background Contact: ", 'corporatepro' ),
            "param_name"  => "bg_contact",
            "admin_label" => true,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style2','style6'),
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Option style contact', 'trueshop'),
            'value'       => array(
                __('Style 1', 'trueshop')     => 'contact-info-2',
                __('Style 2', 'trueshop')     => 'contact-info-3',
            ),
            'admin_label' => true,
            'param_name'  => 'option_style',
            'std'         => 'contact-info-2',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style4'),
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Icon library', 'corporatepro'),
            'value'       => array(
                __('Font Awesome', 'corporatepro')     => 'fontawesome',
            ),
            'admin_label' => true,
            'param_name'  => 'icon_lib',
            'description' => __('Select icon library.', 'corporatepro'),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style4'),
            ),
        ),
        array(
            'type'        => 'iconpicker',
            'heading'     => __('Icon', 'corporatepro'),
            'param_name'  => 'icon_fontawesome',
            'value'       => 'fa fa-adjust', // default value to backend editor admin_label
            'settings'    => array(
                'emptyIcon'    => false,
                'iconsPerPage' => 4000,
            ),
            'dependency'  => array(
                'element' => 'icon_lib',
                'value'   => 'fontawesome',
            ),
            'description' => __('Select icon from library.', 'corporatepro'),
        ),
        array(
            "type"        => "checkbox",
            "heading"     => __( "Is Mail? ", 'corporatepro' ),
            "param_name"  => "check_mail",
            "admin_label" => true,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style4'),
            ),
        ),
        array(
            'type'          =>  'param_group',
            'heading'       =>  __( '==Info Contact==', 'trueshop' ),
            'param_name'    =>  'group_feild',
            'std'           =>  'Month',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style4'),
            ),
            // Note params is mapped inside param-group:
            'params' => array(
                array(
                    'type'          => 'textfield',
                    'value'         => '',
                    'heading'       => 'Enter your title(multiple field)',
                    'param_name'    => 'list_field',
                ),
            )
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Email", 'koolshop'),
            "param_name"  => "email",
            "admin_label" => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('default','style1','style2','style3','style5','style6'),
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Phone", 'koolshop'),
            "param_name"  => "phone",
            "admin_label" => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('default','style1','style2','style3','style5','style6'),
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Address", 'koolshop'),
            "param_name"  => "address",
            "admin_label" => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('default','style1','style2','style3','style5','style6'),
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", "corporatepro"),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__('Css', 'corporatepro'),
            'param_name' => 'css',
            'group'      => esc_html__('Design options', 'corporatepro'),
        )
    );

    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Contact Info', 'corporatepro' ),
        'base'     => 'corporatepro_contactinfo', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'corporatepro' ),
        'description'   =>  __( 'Display a contact info.', 'corporatepro' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_contactinfo( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_contactinfo', $atts ) : $atts;

    $default_atts = array(
        'layout'            => 'default',
        'phone'             => '',
        'check_mail'        => '',
        'description'       => '',
        'icon_fontawesome'  => '',
        'option_style'      => '',
        'group_feild'       => '',
        'img_logo'          => '',
        'list_field'        => '',
        'email'             => '',
        'bg_contact'        => '',
        'title'             => '',
        'address'           => '',
        'css'               => '',
        'el_class'          => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );

    $css_class = $el_class.' widget widget-contactinfo '.$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/contactinfo/temp',$layout, $template_args );
    $html = ob_get_clean();

    return  balanceTags( $html );

}

add_shortcode( 'corporatepro_contactinfo', 'corporatepro_contactinfo' );