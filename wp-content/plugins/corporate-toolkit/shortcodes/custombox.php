<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

add_action( 'vc_before_init', 'corporatepro_cutombox_settings' );
function corporatepro_cutombox_settings() {
   
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'trueshop' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Link', 'trueshop' ),
            'param_name'    =>  'link',
            'description'   =>  __( 'The link of title', 'trueshop' ),
            'admin_label'   =>  false,
            'std'           =>  '#',
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Box Background", 'koolshop' ),
            "param_name" => "boxbackground",
            "value" => '#ff4949', //Default Red color
            "description" => __( "Choose color", 'koolshop' ),
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  __( 'Icon type', 'kute-toolkit' ),
            'param_name'    =>  'icon_type',
            'value'         =>  array(
                __( 'Font icon', 'muse' ) => 'font',
                __( 'Image', 'muse' ) => 'image',
            ),
            'std'           =>  'font',
        ),
        array(
            "type"        => "attach_image",
            "heading"     => __("Icon", 'kute-toolkit'),
            "param_name"  => "icon_image",
            "admin_label" => false,
            'dependency' => array(
                'element' => 'icon_type',
                'value' => 'image',
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Icon library', 'kute-toolkit' ),
            'value' => array(
                __( 'Font Awesome', 'kute-toolkit' ) => 'fontawesome',
            ),
            'admin_label' => true,
            'param_name' => 'icon_lib',
            'description' => __( 'Select icon library.', 'kute-toolkit' ),
            'dependency' => array(
                'element' => 'icon_type',
                'value' => 'font',
            ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'kute-toolkit' ),
            'param_name' => 'icon_fontawesome',
            'value' => 'fa fa-adjust', // default value to backend editor admin_label
            'settings' => array(
                'emptyIcon' => false,
                'iconsPerPage' => 4000,
            ),
            'dependency' => array(
                'element' => 'icon_lib',
                'value' => 'fontawesome',
            ),
            'description' => __( 'Select icon from library.', 'kute-toolkit' ),
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "trueshop"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'trueshop' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'trueshop' ),
        )
    );

    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Custom Box', 'trueshop' ),
        'base'     => 'corporatepro_cutombox', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        'description'   =>  __( 'Display a custom box.', 'trueshop' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_cutombox( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_cutombox', $atts ) : $atts;

    $default_atts = array(
        'layout'           => 'default',
        'link'             => '#',
        'title'            => '',
        'icon_type'        => '',
        'icon_lib'         => '',
        'icon_fontawesome' => '',
        'icon_image'       => '',
        'boxbackground'    => '',
        'css'              => '',
        'el_class'         => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' cp-counter ' .$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    if($icon_type == 'image'){
        $icon = array();
        $icon = wp_get_attachment_image_src( $icon_image , 'full' );
    }else{
        $icon = $atts['icon_'.$icon_lib];
    }

    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
        'icon'      => $icon
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/custonbox/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_cutombox', 'corporatepro_cutombox' );
