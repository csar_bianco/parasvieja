<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_maps_settings' );
function corporatepro_maps_settings() {
    
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'corporatepro' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'maps/default.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label'   => true,
            'param_name'    => 'layout',
            'description'   => __( 'Select a layout.', 'corporatepro' ),
        ),
        array(
            "type"          => "textfield",
            "heading"       => __("Title", "corporatepro"),
            "param_name"    => "title",
            'admin_label'   => true,
            "description"   => __("title.", "corporatepro"),
            'std'           => 'Kute themes',
        ),
        array(
            "type"          => "textfield",
            "heading"       => __("Phone", "corporatepro"),
            "param_name"    => "phone",
            'admin_label'   => true,
            "description"   => __("phone.", "corporatepro"),
            'std'           => '088-465 9965 02',
        ),
        array(
            "type"          => "textfield",
            "heading"       => __("Email", "corporatepro"),
            "param_name"    => "email",
            'admin_label'   => true,
            "description"   => __("email.", "corporatepro"),
            'std'           => 'kutethemes@gmail.com',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  __( 'Maps type', 'corporatepro' ),
            'param_name'    =>  'map_type',
            'value'         =>  array(
                __( 'ROADMAP', 'corporatepro' )     => 'ROADMAP',
                __( 'SATELLITE', 'corporatepro' )   => 'SATELLITE',
                __( 'HYBRID', 'corporatepro' )      => 'HYBRID',
                __( 'TERRAIN', 'corporatepro' )     => 'TERRAIN',
            ),
            'std'           =>  'ROADMAP',
        ),
        array(
            'type'          =>  'dropdown',
            'heading'       =>  __( 'Show info content?', 'corporatepro' ),
            'param_name'    =>  'info_content',
            'value'         =>  array(
                __( 'Yes', 'corporatepro' )     => '1',
                __( 'No', 'corporatepro' )      => '2',
            ),
            'std'           =>  '1',
        ),
        array(
            "type"          => "textfield",
            "heading"       => __("Address", "corporatepro"),
            "param_name"    => "address",
            'admin_label'   => true,
            "description"   => __("address.", "corporatepro"),
            'std'           => 'Z115 TP. Thai Nguyen',
        ),
        array(
            "type"          => "textfield",
            "heading"       => __("Longitude", "corporatepro"),
            "param_name"    => "longitude",
            'admin_label'   => true,
            "description"   => __("longitude.", "corporatepro"),
            'std'           => '105.800286',
        ),
        array(
            "type"          => "textfield",
            "heading"       => __("Latitude", "corporatepro"),
            "param_name"    => "latitude",
            'admin_label'   => true,
            "description"   => __("latitude.", "corporatepro"),
            'std'           => '21.587001',
        ),
        array(
            "type"          => "textfield",
            "heading"       => __("Zoom", "corporatepro"),
            "param_name"    => "zoom",
            'admin_label'   => true,
            "description"   => __("zoom.", "corporatepro"),
            'std'           => '14',
        ),


        array(
            "type"          => "textfield",
            "heading"       => __("Extra class name", "corporatepro"),
            "param_name"    => "el_class",
            "description"   => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'corporatepro' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'corporatepro' ),
        )
    );
    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Maps', 'corporatepro' ),
        'base'     => 'corporatepro_maps', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'corporatepro' ),
        'description'   =>  __( 'Display a google maps.', 'corporatepro' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_maps( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_maps', $atts ) : $atts;

    $default_atts = array(
        'layout'        => 'default',
        'info_content'  => '',
        'icon_lib'      => '',
        'map_type'      => '',
        'email'         => '',
        'title'         => '',
        'phone'         => '',
        'address'       => '',
        'longitude'     => '',
        'latitude'      => '',
        'zoom'          => '',
        'css'           => '',
        'el_class'      => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' '.$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/maps/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_maps', 'corporatepro_maps' );
