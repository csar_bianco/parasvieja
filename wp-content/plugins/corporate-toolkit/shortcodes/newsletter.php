<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_newsletter_settings' );
function corporatepro_newsletter_settings() {

    $params  = array(
        'layout'           => array(
            'type'        => 'kt_select_preview',
            'heading'     => __('Layout', 'corporatepro'),
            'value'       => array(
                'default' => array(
                    'alt' => 'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'newsletter/default.jpg'
                ),
                'style1' => array(
                    'alt' => 'Style 01',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'newsletter/style1.jpg'
                ),
                'style2' => array(
                    'alt' => 'Style 02',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'newsletter/style2.jpg'
                ),
                'style3' => array(
                    'alt' => 'Style 03',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'newsletter/style3.jpg'
                ),
                'style4' => array(
                    'alt' => 'Style 04',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'newsletter/style4.jpg'
                ),
                'style5' => array(
                    'alt' => 'Style 05',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'newsletter/style5.jpg'
                ),
            ),
            'default'     => 'default',
            'admin_label' => true,
            'param_name'  => 'layout',
            'description' => __('Select a layout.', 'corporatepro'),
        ),
        array(
            "type"        => "attach_image",
            "heading"     => __( "Background", "corporatepro" ),
            "param_name"  => "bg_newsletter",
            "admin_label" => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style2'),
            ),
        ),
        array(
            'type'        => 'textfield',
            'heading'     => __('Title', 'corporatepro'),
            'param_name'  => 'title',
            'description' => __('The title of shortcode', 'corporatepro'),
            'admin_label' => true,
            'std'         => 'Sign Up',
        ),
        array(
            'type'        => 'textarea',
            'heading'     => __('Description', 'corporatepro'),
            'param_name'  => 'description',
            'description' => __('The description of shortcode', 'corporatepro'),
            'admin_label' => true,
            'std'         => 'Get subscriber only insights &amp; news delivered by John Doe',
        ),
        array(
            'type'        => 'textfield',
            'heading'     => __('Email in description', 'corporatepro'),
            'param_name'  => 'email_des',
            'description' => __('The Email in descriptions of shortcode', 'corporatepro'),
            'admin_label' => true,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style2'),
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Placeholder Name", 'corporatepro'),
            "param_name"  => "placeholder_name",
            "admin_label" => false,
            'std'         => 'Your Name',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style4'),
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Placeholder text", 'corporatepro'),
            "param_name"  => "placeholder_text",
            "admin_label" => false,
            'std'         => 'Email address here'
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Button text", 'corporatepro'),
            "param_name"  => "button_text",
            "admin_label" => false,
            'std'         => 'Submit'
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Extra class name", "corporatepro"),
            "param_name"  => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__('Css', 'corporatepro'),
            'param_name' => 'css',
            'group'      => esc_html__('Design options', 'corporatepro'),
        )
    );

    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Newsletter', 'corporatepro' ),
        'base'     => 'corporatepro_newsletter', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'corporatepro' ),
        'description'   =>  __( 'Display a newsletter form.', 'corporatepro' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}


function corporatepro_newsletter( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_newsletter', $atts ) : $atts;

    $default_atts = array(
        'layout'            => 'default',
        'placeholder_text'  => 'Email address here',
        'placeholder_name'  => '',
        'button_text'       => 'Submit',
        'title'             => '',
        'bg_newsletter'     => '',
        'email_des'         => '',
        'description'       => '',
        'css'               => '',
        'el_class'          => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );

    $css_class = $el_class.' corporatepro-newsletter '.$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/newsletter/temp',$layout, $template_args );
    $html = ob_get_clean();

    return  balanceTags( $html );

}

add_shortcode( 'corporatepro_newsletter', 'corporatepro_newsletter' );
