<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}
        
add_action( 'vc_before_init', 'corporatepro_pricing_settings' );
function corporatepro_pricing_settings() {
   
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'pricing/default.jpg'
                ),
                'style1'=>array(
                    'alt'=>'Style 01',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'pricing/layout1.jpg'
                ),
                'style2'=>array(
                    'alt'=>'Style 02',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'pricing/layout2.jpg'
                ),
                'style3'=>array(
                    'alt'=>'Style 03',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'pricing/layout3.jpg'
                ),
                'style4'=>array(
                    'alt'=>'Style 04',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'pricing/layout4.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        array(
            "type"        => "attach_image",
            "heading"     => __( "Background Image: ", 'xshop-core' ),
            "param_name"  => "bg_price",
            "admin_label" => true,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'style2',
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'trueshop' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  'Professional',
        ),
        array(
            'type'          =>  'textarea',
            'heading'       =>  __( 'Description', 'trueshop' ),
            'param_name'    =>  'description',
            'description'   =>  __( 'The Description of shortcode.', 'trueshop' ),
            'admin_label'   =>  true,
            'std'           =>  'Host your personal blog/site',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style4'),
            ),
        ),
        array(
            'type'        => 'colorpicker',
            'heading'     => __('Main color box: ', 'trueshop'),
            'param_name'  => 'main_color',
            'value'       => '#4baf4f', // default value to backend editor admin_label
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style1','style3', 'style4'),
            ),
            'description' => __('Select main color.', 'trueshop'),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Price', 'trueshop' ),
            'param_name'    =>  'price',
            'admin_label'   =>  true,
            'std'           =>  '59.9',
            'description' => __('the price', 'trueshop'),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style1', 'default', 'style3', 'style4'),
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Option price', 'trueshop'),
            'value'       => array(
                __('Price', 'trueshop')   => 'price',
                __('Free', 'trueshop')    => 'free',
            ),
            'admin_label' => true,
            'param_name'  => 'option_price',
            'description' => __('Select option price.', 'trueshop'),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style2'),
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Highlight Price', 'trueshop' ),
            'param_name'    =>  'hi_price',
            'admin_label'   =>  true,
            'std'           =>  '9',
            'description' => __('the highlight price', 'trueshop'),
            'dependency'  => array(
                'element' => 'option_price',
                'value'   => array('price'),
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Low Price', 'trueshop' ),
            'param_name'    =>  'lo_price',
            'admin_label'   =>  true,
            'std'           =>  '99',
            'description' => __('the low price', 'trueshop'),
            'dependency'  => array(
                'element' => 'option_price',
                'value'   => array('price'),
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Text', 'trueshop' ),
            'param_name'    =>  'text_free',
            'admin_label'   =>  true,
            'std'           =>  'free',
            'dependency'  => array(
                'element' => 'option_price',
                'value'   => array('free'),
            ),
        ),

        array(
            'type' => 'vc_link',
            'heading' => __( 'Link Sign up', 'xshop-core' ),
            'param_name' => 'link_signup',
            'description' => __( 'Add link to sign up.', 'trueshop' ),
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style2'),
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'per', 'trueshop' ),
            'param_name'    =>  'per',
            'admin_label'   =>  true,
            'std'           =>  'Month',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style1', 'default', 'style4'),
            ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Price description', 'trueshop' ),
            'param_name'    =>  'price_des',
            'admin_label'   =>  true,
            'std'           =>  'was $7.99',
            'dependency'  => array(
                'element' => 'layout',
                'value'   => array('style4'),
            ),
        ),
        array(
            'type'          =>  'param_group',
            'heading'       =>  __( 'List Price', 'trueshop' ),
            'param_name'    =>  'group_feild',
            'std'           =>  'Month',
            // Note params is mapped inside param-group:
            'params' => array(
                array(
                    'type'          => 'textfield',
                    'value'         => '',
                    'heading'       => 'Enter your title(multiple field)',
                    'param_name'    => 'list_field',
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => __('Option style list', 'trueshop'),
                    'value'       => array(
                        __('Icon', 'trueshop')     => 'icon',
                        __('Text', 'trueshop')     => 'text',
                    ),
                    'admin_label' => true,
                    'param_name'  => 'option_style',
                    'description' => __('Select icon library.', 'trueshop'),
                ),
                array(
                    'type'          => 'textfield',
                    'value'         => '',
                    'heading'       => 'Enter your text or number',
                    'param_name'    => 'text_number',
                    'dependency'  => array(
                        'element' => 'option_style',
                        'value'   => 'text',
                    ),
                ),
                array(
                    'type'        => 'dropdown',
                    'heading'     => __('Icon library', 'trueshop'),
                    'value'       => array(
                        __('Font Awesome', 'trueshop')     => 'fontawesome',
                    ),
                    'admin_label' => true,
                    'param_name'  => 'icon_lib',
                    'description' => __('Select icon library.', 'trueshop'),
                    'dependency'  => array(
                        'element' => 'option_style',
                        'value'   => 'icon',
                    ),
                ),
                array(
                    'type'        => 'iconpicker',
                    'heading'     => __('Icon', 'trueshop'),
                    'param_name'  => 'icon_fontawesome',
                    'value'       => 'fa fa-adjust', // default value to backend editor admin_label
                    'settings'    => array(
                        'emptyIcon'    => false,
                        'iconsPerPage' => 4000,
                    ),
                    'dependency'  => array(
                        'element' => 'icon_lib',
                        'value'   => 'fontawesome',
                    ),
                    'description' => __('Select icon from library.', 'trueshop'),
                ),
            )
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __('Text-align: ', 'trueshop'),
            'value'       => array(
                __('Left', 'trueshop')     => 'left',
                __('Right', 'trueshop')    => 'right',
                __('Center', 'trueshop')   => 'center',
            ),
            'std'         =>  'center',
            'admin_label' => true,
            'param_name'  => 'text_align',
            'description' => __('Text-align list.', 'trueshop'),
        ),
        array(
            'type' => 'vc_link',
            'heading' => __( 'Button (Link)', 'xshop-core' ),
            'param_name' => 'link',
            'description' => __( 'Add button.', 'trueshop' ),
        ),
        // default -------------------------------------------------------------------------------
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "trueshop"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "trueshop")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'trueshop' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'trueshop' ),
        )
    );

    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Pricing', 'trueshop' ),
        'base'     => 'corporatepro_pricing', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'trueshop' ),
        'description'   =>  __( 'Display Pricing table.', 'trueshop' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_pricing( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_pricing', $atts ) : $atts;

    $default_atts = array(
        'layout'            => 'default',
        'title'             => '',
        'description'       => '',
        'price'             => '',
        'bg_price'          => '',
        'hi_price'          => '',
        'lo_price'          => '',
        'text_free'         => '',
        'link_signup'       => '',
        'option_price'      => '',
        'price_des'         => '',
        'group_feild'       => '',
        'icon_lib'          => '',
        'list_field'        => '',
        'icon_fontawesome'  => '',
        'main_color'        => '',
        'text_align'        => '',
        'per'               => '',
        'link'              => '',
        'css'               => '',
        'el_class'          => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' cp-counter ' .$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $template_args = array(
        'atts'      => $atts,
        'css_class' => $css_class,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/pricing/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_pricing', 'corporatepro_pricing' );
