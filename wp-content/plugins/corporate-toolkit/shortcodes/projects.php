<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

add_action( 'vc_before_init', 'corporatepro_projects_settings' );
function corporatepro_projects_settings()
{
	/* CATEGORY DRROPDOW */

	$params       = array(
		array(
			'type'        => 'kt_select_preview',
			'heading'     => __( 'Layout', 'corporatepro' ),
			'value'       => array(
				'layout1' => array(
					'alt' => 'Layout 01',
					'img' => CORPORATEPRO_SHORTCODE_IMG_URL . 'projects/layout1.jpg',
				),
				'layout2' => array(
					'alt' => 'Layout 02',
					'img' => CORPORATEPRO_SHORTCODE_IMG_URL . 'projects/layout2.jpg',
				),
				'layout3' => array(
					'alt' => 'Layout 03',
					'img' => CORPORATEPRO_SHORTCODE_IMG_URL . 'projects/layout3.jpg',
				),
				'layout4' => array(
					'alt' => 'Layout 04',
					'img' => CORPORATEPRO_SHORTCODE_IMG_URL . 'projects/layout4.jpg',
				),
				'layout5' => array(
					'alt' => 'Layout 05',
					'img' => CORPORATEPRO_SHORTCODE_IMG_URL . 'projects/layout5.jpg',
				),
			),
			'default'     => 'default',
			'admin_label' => true,
			'param_name'  => 'layout',
			'description' => __( 'Select a layout.', 'corporatepro' ),
		),
		array(
			'type'        => 'textfield',
			'heading'     => __( 'Title', 'corporatepro' ),
			'param_name'  => 'title',
			'description' => __( 'The title of shortcode', 'corporatepro' ),
			'admin_label' => true,
			'std'         => '',
		),
		array(
			"type"        => "kt_taxonomy_project",
			"taxonomy"    => "category_project",
			"class"       => "",
			"heading"     => __( "Category", 'corporatepro' ),
			"param_name"  => "taxonomy",
			"value"       => '',
			'parent'      => '',
			'multiple'    => true,
			'hide_empty'  => false,
			'placeholder' => __( 'Choose category', 'corporatepro' ),
			"description" => __( "Note: If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'corporatepro' ),
			'std'         => '',
			'group'       => __( 'Project Option', 'corporatepro' ),
		),
		array(
			'type'        => 'textfield',
			'heading'     => __( 'Number post', 'corporatepro' ),
			'param_name'  => 'ppp',
			'admin_label' => true,
			'std'         => '6',
			'group'       => __( 'Project Option', 'corporatepro' ),
		),
		array(
			"type"        => "dropdown",
			"heading"     => __( "Order by", 'corporatepro' ),
			"param_name"  => "orderby",
			"value"       => array(
				'',
				__( 'Date', 'corporatepro' )          => 'date',
				__( 'ID', 'corporatepro' )            => 'ID',
				__( 'Author', 'corporatepro' )        => 'author',
				__( 'Title', 'corporatepro' )         => 'title',
				__( 'Modified', 'corporatepro' )      => 'modified',
				__( 'Random', 'corporatepro' )        => 'rand',
				__( 'Comment count', 'corporatepro' ) => 'comment_count',
				__( 'Menu order', 'corporatepro' )    => 'menu_order',
				__( 'Sale price', 'corporatepro' )    => '_sale_price',
			),
			'std'         => 'date',
			"description" => __( "Select how to sort retrieved posts.", 'corporatepro' ),
			'group'       => __( 'Project Option', 'corporatepro' ),
		),
		array(
			'type'       => 'dropdown',
			'heading'    => __( 'Order', 'corporatepro' ),
			'param_name' => 'order',
			'value'      => array(
				__( 'ASC', 'corporatepro' )  => 'ASC',
				__( 'DESC', 'corporatepro' ) => 'DESC',
			),
			'std'        => 'ASC',
			'group'      => __( 'Project Option', 'corporatepro' ),
		),
		/* group posts */

		array(
			"type"        => "param_group",
			"heading"     => __( "Items Project", "corporatepro" ),
			"admin_label" => false,
			"param_name"  => "items",
			'dependency'  => array(
				'element' => 'layout',
				'value'   => array( 'layout2' ),
			),
			"params"      => array(
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Size Post', 'corporatepro' ),
					'param_name'  => 'size_post',
					'value'       => array(
						__( 'Normal', 'corporatepro' ) => 'normal',
						__( 'Big', 'corporatepro' )    => 'big',
					),
					'description' => __( 'Choose the size posts', 'corporatepro' ),
					'std'         => '',
				),
				array(
					'type'        => 'autocomplete',
					'class'       => '',
					'heading'     => esc_html__( 'Post Name', 'alispx' ),
					'param_name'  => 'ids',
					'settings'    => array(
						'multiple'      => false,
						'sortable'      => true,
						'unique_values' => true,
						'values'        => corporatepro_get_project_data(),
					),
					'description' => __( 'Enter List of post name', 'alispx' ),
				),
			),
		),
		array(
			'type'        => 'textfield',
			'heading'     => __( 'Columns Portfolio:', 'corporatepro' ),
			'param_name'  => 'columns',
			'std'         => 3,
			'admin_label' => false,
			'dependency'  => array(
				'element' => 'layout',
				'value'   => array( 'layout1', 'layout2', 'layout4' ),
			),
		),

		// responsive ---------------------------------------------------
		array(
			"type"        => "textfield",
			"heading"     => __( "The items on desktop (Screen resolution of device >= 1200px )", 'corporatepro' ),
			"param_name"  => "layout2_lg_items",
			"value"       => "4",
			'group'       => __( 'Carousel settings', 'corporatepro' ),
			'admin_label' => false,
			"dependency"  => array(
				"element" => "layout", "value" => array( 'layout5' ),
			),
		),
		array(
			"type"        => "textfield",
			"heading"     => __( "The items on desktop (Screen resolution of device >= 992px < 1200px )", 'corporatepro' ),
			"param_name"  => "layout2_md_items",
			"value"       => "3",
			'group'       => __( 'Carousel settings', 'corporatepro' ),
			'admin_label' => false,
			"dependency"  => array(
				"element" => "layout", "value" => array( 'layout5' ),
			),
		),
		array(
			"type"        => "textfield",
			"heading"     => __( "The items on tablet (Screen resolution of device >=600px and < 992px )", 'corporatepro' ),
			"param_name"  => "layout2_sm_items",
			"value"       => "2",
			'group'       => __( 'Carousel settings', 'corporatepro' ),
			'admin_label' => false,
			"dependency"  => array(
				"element" => "layout", "value" => array( 'layout5' ),
			),
		),
		array(
			"type"        => "textfield",
			"heading"     => __( "The items on mobile landscape(Screen resolution of device <=600px)", 'corporatepro' ),
			"param_name"  => "layout2_xs_items",
			"value"       => "1",
			'group'       => __( 'Carousel settings', 'corporatepro' ),
			'admin_label' => false,
			"dependency"  => array(
				"element" => "layout", "value" => array( 'layout5' ),
			),
		),
		// responsive ---------------------------------------------------
		array(
			"type"        => "textfield",
			"heading"     => __( "Extra class name", "corporatepro" ),
			"param_name"  => "el_class",
			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro" ),
		),
		array(
			'type'       => 'css_editor',
			'heading'    => esc_html__( 'Css', 'corporatepro' ),
			'param_name' => 'css',
			'group'      => esc_html__( 'Design options', 'corporatepro' ),
		),
	);
	$map_settings = array(
		'name'        => esc_html__( 'Corporate: Projects', 'corporatepro' ),
		'base'        => 'corporatepro_projects', // shortcode
		'class'       => '',
		'category'    => esc_html__( 'Corporate', 'corporatepro' ),
		'description' => __( 'Display a list Projects.', 'corporatepro' ),
		'params'      => $params,
	);

	vc_map( $map_settings );
}

function corporatepro_projects( $atts )
{

	$atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_projects', $atts ) : $atts;

	$default_atts = array(
		'layout'           => '',
		'order'            => '',
		'orderby'          => '',
		'title'            => '',
		'link'             => '',
		'columns'          => '',
		'project_filter'   => '',
		'layout2_lg_items' => '',
		'layout2_md_items' => '',
		'layout2_sm_items' => '',
		'layout2_xs_items' => '',
		'taxonomy'         => '',
		'target'           => '',
		'items'            => '',
		'ppp'              => '',
		'css'              => '',
		'el_class'         => '',
	);

	extract( shortcode_atts( $default_atts, $atts ) );
	$css_class = $el_class . ' ' . $layout;
	if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
		$css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
	endif;

	$category_project = explode( ',', $taxonomy );

	if ( $taxonomy != '' ) {
		$args = array(
			'post_type'           => 'project',
			'post_status'         => 'publish',
			'posts_per_page'      => $ppp,
			'orderby'             => $orderby,
			'order'               => $order,
			'tax_query'           => array(
				array(
					'taxonomy' => 'category_project',
					'field'    => 'slug',
					'terms'    => $category_project,
				),
			),
		);
	} else {
		$args = array(
			'post_type'      => 'project',
			'post_status'    => 'publish',
			'posts_per_page' => $ppp,
			'orderby'        => $orderby,
		);
	}

	$projects = new wp_query( $args );

	$template_args = array(
		'atts'             => $atts,
		'css_class'        => $css_class,
		'projects'         => $projects,
		'category_project' => $category_project,
	);

	ob_start();

	corporatepro_get_template_part( 'shortcodes/projects/temp', $layout, $template_args );

	$html = ob_get_clean();

	return balanceTags( $html );

}

add_shortcode( 'corporatepro_projects', 'corporatepro_projects' );
