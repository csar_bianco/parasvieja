<?php
if ( ! defined( 'ABSPATH' ) ) {
    die( '-1' );
}

add_action( 'vc_before_init', 'corporatepro_tabs_settings' );
function corporatepro_tabs_settings() {
    $socials = array();
    $all_socials = corporatepro_get_all_social();
    if( $all_socials ){
        foreach ($all_socials as $key =>  $social)
            $socials[$social['name']] = $key;
    }
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'trueshop' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'tabs/default.jpg'
                ),
                'style1'=>array(
                    'alt'=>'Style 01',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'tabs/style1.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'trueshop' ),
        ),
        array(
            'type' => 'textfield',
            'param_name' => 'active_section',
            'heading' => __( 'Active section', 'js_composer' ),
            'value' => 1,
            'description' => __( 'Enter active section number (Note: to have all sections closed on initial load enter non-existing number).', 'js_composer' ),
        ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' ),
        ),
        array(
            'type' => 'css_editor',
            'heading' => __( 'CSS box', 'js_composer' ),
            'param_name' => 'css',
            'group' => __( 'Design Options', 'js_composer' ),
        ),
    );

    $map_settings = array(
        'name' => __( 'Corporate: Tabs', 'js_composer' ),
        'base' => 'corporatepro_tabs',
        'icon' => 'icon-wpb-ui-tab-content',
        'is_container' => true,
        'show_settings_on_create' => false,
        'as_parent' => array(
            'only' => 'vc_tta_section',
        ),
        'category' => __( 'Corporate', 'js_composer' ),
        'description' => __( 'Tabbed content', 'js_composer' ),
        'params'   => $params,
        'js_view' => 'VcBackendTtaTabsView',
        'custom_markup' => '
        <div class="vc_tta-container" data-vc-action="collapse">
            <div class="vc_general vc_tta vc_tta-tabs vc_tta-color-backend-tabs-white vc_tta-style-flat vc_tta-shape-rounded vc_tta-spacing-1 vc_tta-tabs-position-top vc_tta-controls-align-left">
                <div class="vc_tta-tabs-container">'
                               . '<ul class="vc_tta-tabs-list">'
                               . '<li class="vc_tta-tab" data-vc-tab data-vc-target-model-id="{{ model_id }}" data-element_type="vc_tta_section"><a href="javascript:;" data-vc-tabs data-vc-container=".vc_tta" data-vc-target="[data-model-id=\'{{ model_id }}\']" data-vc-target-model-id="{{ model_id }}"><span class="vc_tta-title-text">{{ section_title }}</span></a></li>'
                               . '</ul>
                </div>
                <div class="vc_tta-panels vc_clearfix {{container-class}}">
                  {{ content }}
                </div>
            </div>
        </div>',
        'default_content' => '
        [vc_tta_section title="' . sprintf( '%s %d', __( 'Tab', 'js_composer' ), 1 ) . '"][/vc_tta_section]
        [vc_tta_section title="' . sprintf( '%s %d', __( 'Tab', 'js_composer' ), 2 ) . '"][/vc_tta_section]
        ',
        'admin_enqueue_js' => array(
            vc_asset_url( 'lib/vc_tabs/vc-tabs.min.js' ),
        ),

    );

    vc_map( $map_settings );
}

VcShortcodeAutoloader::getInstance()->includeClass( 'WPBakeryShortCode_VC_Tta_Accordion' );

class WPBakeryShortCode_corporatepro_tabs extends WPBakeryShortCode_VC_Tta_Accordion {
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_tabs', $atts ) : $atts;
        
        $default_atts = array(
            'layout'         => 'default',
            'active_section' => '1',
            'css'            => '',
            'el_class'       => ''
        );
        extract( shortcode_atts( $default_atts, $atts ) );

        $css_class = 'cp-tab '.$el_class.' '.$layout;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;
        // Get all url link
        $sections = corporatepro_get_all_attributes('vc_tta_section', $content);

        $template_args = array(
            'atts'      => $atts,
            'css_class' => $css_class,
            'content'  => $content,
            'sections' => $sections

        );
        ob_start();
        corporatepro_get_template_part('shortcodes/tabs/temp',$layout, $template_args );
        $html = ob_get_clean();

        return balanceTags( $html );
    }
}
