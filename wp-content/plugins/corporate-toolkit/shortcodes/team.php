<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}


add_action( 'vc_before_init', 'corporatepro_team_settings' );
function corporatepro_team_settings() {
    $socials = array();
    $all_socials = corporatepro_get_all_social();
    if( $all_socials ){
        foreach ($all_socials as $key =>  $social)
            $socials[$social['name']] = $key;
    }
    $params  = array(
        array(
            'type' => 'kt_select_preview',
            'heading' => __( 'Layout', 'corporatepro' ),
            'value' => array(
                'default'=>array(
                    'alt'=>'Default',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'team/default.jpg'
                ),
                'layout1'=>array(
                    'alt'=>'Layout 01',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'team/layout1.jpg'
                ),
                'layout2'=>array(
                    'alt'=>'Layout 02',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'team/layout2.jpg'
                ),
                'layout3'=>array(
                    'alt'=>'Layout 03',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'team/layout3.jpg'
                ),
                'layout4'=>array(
                    'alt'=>'Layout 04',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'team/layout4.jpg'
                ),
                'layout5'=>array(
                    'alt'=>'Layout 05',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'team/layout5.jpg'
                ),
                'layout6'=>array(
                    'alt'=>'Layout 06',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'team/layout6.jpg'
                ),
                'layout7'=>array(
                    'alt'=>'Layout 07',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'team/layout7.jpg'
                ),
                'layout8'=>array(
                    'alt'=>'Layout 08',
                    'img'=>CORPORATEPRO_SHORTCODE_IMG_URL.'team/layout8.jpg'
                ),
            ),
            'default'       =>'default',
            'admin_label' => true,
            'param_name' => 'layout',
            'description' => __( 'Select a layout.', 'corporatepro' ),
        ),
        array(
            'type'          =>  'textfield',
            'heading'       =>  __( 'Title', 'corporatepro' ),
            'param_name'    =>  'title',
            'description'   =>  __( 'The title of shortcode', 'corporatepro' ),
            'admin_label'   =>  true,
            'std'           =>  '',
        ),
        array(
        "type"          => "param_group",
            "heading"       => __( "Items", "corporatepro" ),
            "admin_label"   => false,
            "param_name"    => "items",
            "params"        => array(
                array(
                    "type"        => "attach_image",
                    "heading"     => __( "Avatar", "corporatepro" ),
                    "param_name"  => "avatar",
                    "admin_label" => false
                ),
                array(
                    "type"        => "textfield",
                    "heading"     => __( "Name", "corporatepro" ),
                    "param_name"  => "name",
                    "admin_label" => false
                ),
                array(
                    "type"        => "textfield",
                    "heading"     => __( "Position", "corporatepro" ),
                    "param_name"  => "position",
                    "admin_label" => false
                ),
                array(
                    "type"        => "textarea",
                    "heading"     => __( "Content", "corporatepro" ),
                    "param_name"  => "textarea",
                    "admin_label" => false
                ),
                array(
                    'type'        => 'checkbox',
                    'heading'     => __( 'Display on', 'corporatepro' ),
                    'param_name'  => 'use_socials',
                    'class'         => 'checkbox-display-block',
                    'value'       => $socials,
                ),
                array(
                    'type' => 'vc_link',
                    'heading' => __( 'URL (Link)', 'kute-toolkit' ),
                    'param_name' => 'link',
                    'description' => __( 'Add link.', 'kute-toolkit' ),
                ),
                
            )
        ),
        // responsive ---------------------------------------------------
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'Yes', 'corporatepro' ) => 'true',
                __( 'No', 'corporatepro' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => __( 'AutoPlay', 'corporatepro' ),
            'param_name'  => 'owl_autoplay',
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout7',
            ),
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'No', 'corporatepro' )  => 'false',
                __( 'Yes', 'corporatepro' ) => 'true'
            ),
            'std'         => false,
            'heading'     => __( 'Navigation', 'corporatepro' ),
            'param_name'  => 'owl_navigation',
            'description' => __( "Show buton 'next' and 'prev' buttons.", 'corporatepro' ),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout7',
            ),
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'No', 'corporatepro' )  => 'false',
                __( 'Yes', 'corporatepro' ) => 'true'
            ),
            'std'         => true,
            'heading'     => __( 'Dots', 'corporatepro' ),
            'param_name'  => 'owl_dots',
            'description' => __( "Show dots.", 'corporatepro' ),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout7',
            ),
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                __( 'Yes', 'corporatepro' ) => 'true',
                __( 'No', 'corporatepro' )  => 'false'
            ),
            'std'         => false,
            'heading'     => __( 'Loop', 'corporatepro' ),
            'param_name'  => 'owl_loop',
            'description' => __( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'corporatepro' ),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout7',
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Slide Speed", 'corporatepro'),
            "param_name"  => "owl_slidespeed",
            "value"       => "200",
            "suffix"      => __("milliseconds", 'corporatepro'),
            "description" => __('Slide speed in milliseconds', 'corporatepro'),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout7',
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("Margin", 'corporatepro'),
            "param_name"  => "owl_margin",
            "value"       => "20",
            "description" => __('Distance( or space) between 2 item', 'corporatepro'),
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout7',
            ),
        ),

        array(
            "type"        => "textfield",
            "heading"     => __("The items on desktop (Screen resolution of device >= 1200px )", 'corporatepro'),
            "param_name"  => "owl_lg_items",
            "value"       => "3",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout7',
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on desktop (Screen resolution of device >= 992px < 1200px )", 'corporatepro'),
            "param_name"  => "owl_md_items",
            "value"       => "3",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout7',
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on tablet (Screen resolution of device >=768px and < 992px )", 'corporatepro'),
            "param_name"  => "owl_sm_items",
            "value"       => "3",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout7',
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on mobile landscape(Screen resolution of device >=480px and < 768px)", 'corporatepro'),
            "param_name"  => "owl_xs_items",
            "value"       => "2",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout7',
            ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => __("The items on mobile (Screen resolution of device < 480px)", 'corporatepro'),
            "param_name"  => "owl_ts_items",
            "value"       => "1",
            'group'       => __( 'Carousel settings', 'corporatepro' ),
            'admin_label' => false,
            'dependency'  => array(
                'element' => 'layout',
                'value'   => 'layout7',
            ),
        ),
        // responsive ---------------------------------------------------

        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "corporatepro"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "corporatepro")
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => esc_html__( 'Css', 'corporatepro' ),
            'param_name' => 'css',
            'group'      => esc_html__( 'Design options', 'corporatepro' ),
        )
    );
    $map_settings = array(
        'name'     => esc_html__( 'Corporate: Team', 'corporatepro' ),
        'base'     => 'corporatepro_team', // shortcode
        'class'    => '',
        'category' => esc_html__( 'Corporate', 'corporatepro' ),
        'description'   =>  __( 'Display a team list.', 'corporatepro' ),
        'params'   => $params
    );

    vc_map( $map_settings );
}

function corporatepro_team( $atts ) {

    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'corporatepro_team', $atts ) : $atts;

    $default_atts = array(
        'layout'            => 'default',
        'title'             => '',
        'link'              => '',
        'items'             => '',
        'owl_autoplay'      => false,
        'owl_navigation'    => false,
        'owl_dots'          => true,
        'owl_loop'          => false,
        'owl_slidespeed'    => 200,
        'owl_margin'        => 20,
        'owl_lg_items'      => 3,
        'owl_md_items'      => 3,
        'owl_sm_items'      => 3,
        'owl_xs_items'      => 2,
        'owl_ts_items'      => 1,
        'css'               => '',
        'el_class'          => ''
    );

    extract( shortcode_atts( $default_atts, $atts ) );
    $css_class = $el_class.' ss-ourteam '.$layout;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;

    $owl_settings = '';

    $owl_settings = corporatepro_generate_carousel_data_attributes('owl_', $atts);

    $template_args = array(
        'atts'          => $atts,
        'css_class'     => $css_class,
        'owl_carousel'  => $owl_settings,
    );

    ob_start();
    corporatepro_get_template_part('shortcodes/team/temp',$layout, $template_args );
    $html = ob_get_clean();

    return balanceTags( $html );

}

add_shortcode( 'corporatepro_team', 'corporatepro_team' );
