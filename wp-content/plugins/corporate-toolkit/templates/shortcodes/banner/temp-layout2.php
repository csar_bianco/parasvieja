<?php
if( isset($atts) ){
	extract ( $atts );
}
if( $link ){
	$link = vc_build_link( $link );
}else{
	$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
}

$text_align = '';

if ( $style_ads == 'dark' ) {
	$text_align = 'text-center';
} else {
	$text_align = '';
}
$class = ''.$style_ads.' '.$style_box.'';
?>

<div class="ads-box <?php echo esc_attr( $css_class );?> <?php echo esc_attr( $class ) ?>">
	<a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>">
		<?php $image_thumb = corporatepro_resize_image( $bg_banner, null, $width, $height, true, true, false ); ?>
        <img  src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="">
	</a>
	<div class="ads-box-info <?php echo esc_attr( $text_align ) ?>">
		<?php if ( $main_title ) : ?>
			<h3><?php echo esc_html( $main_title ) ?></h3>
		<?php endif; ?>

		<?php if ( $main_des ) : ?>
			<span class="price-from"><?php echo esc_html( $main_des ) ?></span>
		<?php endif; ?>

		<a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>" class="outline-button">
			<?php echo esc_html($link['title']); ?>
		</a>
	</div>
</div>
