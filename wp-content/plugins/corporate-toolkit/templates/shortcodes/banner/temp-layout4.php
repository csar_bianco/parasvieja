<?php
if( isset($atts) ){
    extract ( $atts );
}
if( $link ){
    $link = vc_build_link( $link );
}else{
    $link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
}
?>
<div class="block-hotdeal">
    <?php if ( $sticky ) : ?>
        <span class="label-block"><?php echo esc_html($sticky) ?></span>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-6">
            <div class="hotdel-info">
                <?php if ( $main_title ) : ?>
                    <h2><?php echo esc_html( $main_title ) ?></h2>
                <?php endif; ?>

                <?php if ( $sub_title ) : ?>
                    <h3><i class="<?php echo esc_attr($icon_fontawesome) ?>"></i><?php echo esc_html( $sub_title ) ?></h3>
                <?php endif; ?>

                <?php if ( $main_des ) : ?>
                    <div class="desc">
                        <?php echo esc_html( $main_des ) ?>
                    </div>
                <?php endif; ?>

                <a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_url($link['target']) ?>" class="bt-learnmore">
                    <?php echo esc_html($link['title']) ?>
                    <i class="fa fa-caret-right"></i>
                </a>
            </div>
        </div>
        <div class="col-sm-6 text-center">
            <div class="hotdeal-img">
                <?php $image_thumb 	= corporatepro_resize_image( $bg_banner, null, 400, 398, true, true, false ); ?>
                <img style="display: inline-block" class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
            </div>
        </div>
    </div>
</div>
