<?php
if( isset($atts) ){
    extract ( $atts );
}
$lists = vc_param_group_parse_atts( $list );
?>
<div class="cp-whyus-block why-6">
    <div class="content-whyus">
        <?php if ( $main_title ) : ?>
            <h3><?php echo esc_html( $main_title ) ?></h3>
        <?php endif; ?>

        <?php if ( $main_des ) : ?>
            <p><?php echo esc_html( $main_des ) ?></p>
        <?php endif; ?>

        <ul class="list-dots">
            <?php foreach ($lists as $value) : ?>
                <?php
                if( !empty($value['links']) ){
                    $link = vc_build_link( $value['links'] );
                }else{
                    $link = array('url'=>'#', 'title'=>'', 'target'=>'_self', 'rel'=>'') ;
                }
                ?>
                <li>
                    <a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>">
                        <?php echo esc_html( $value['title'] ) ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="images-why">
        <?php $image_thumb = corporatepro_resize_image( $bg_banner, null, 210, 180, true, true, false ); ?>
        <img  src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="">
    </div>
</div>
