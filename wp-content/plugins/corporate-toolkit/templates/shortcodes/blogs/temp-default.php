<?php
if( isset($atts) ){
    extract ( $atts );
}
?>
<div class="widget widget-recent-post <?php echo esc_attr( $css_class );?>">
	<?php if (!empty($title)) : ?>
		<h3 class="title-widget"><?php echo esc_html( $title ); ?></h3>
	<?php endif; ?>
	<ul class="list-post">
	<?php if ($loop_posts->have_posts()): ?>

		<?php while ($loop_posts->have_posts()): $loop_posts->the_post(); ?>
			
			<li>
				<div class="post-img">
<?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, 70, 70, true, true, false ); ?>
<img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
				</div>
				<div class="post-info">
					<h3 class="title-post"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<ul class="meta-post">
	    				<li><i class="fa fa-clock-o"></i><?php the_time('F j'); ?></li>
	    				<li><i class="fa fa-comment-o"></i>
	    				<?php comments_number(
                            esc_html__('0', 'corporatepro'),
                            esc_html__('1', 'corporatepro'),
                            esc_html__('%', 'corporatepro')
                        );?> 
                        </li>
	    			</ul>
				</div>
			</li>
			
		<?php endwhile; ?>

	<?php endif;?>	
	<?php wp_reset_query(); ?>
	</ul>
</div>