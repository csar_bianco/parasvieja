<?php
if( isset($atts) ){
    extract ( $atts );
}
?>
<div class="<?php echo esc_attr( $css_class );?>">
	<?php if ($title) : ?>
		<div class="title-section title-style2 mgb-20">
			<h2 class="title"><?php echo esc_html($title) ?></h2>
		</div>
	<?php endif; ?>
	<?php if ($loop_posts->have_posts()): ?>
	<div class="cp-list-even even-style2">
		<?php while ($loop_posts->have_posts()): $loop_posts->the_post(); ?>
			<div class="even-item">
				<div class="even-date">
					<span class="month"><?php the_time('M') ?></span>
					<span class="date"><?php the_time('j') ?></span>
				</div>
				<div class="even-info">
					<h3 class="even-title">
						<a href="<?php the_permalink() ?>">
							<?php the_title() ?>
						</a>
					</h3>
					<div class="even-desc">
						<?php echo wp_trim_words( get_the_content(),12,'...') ?>
					</div>
					<a class="readmore" href="<?php the_permalink() ?>">
						<?php echo esc_html__('Read more', 'corporatepro') ?>
					</a>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
	<?php endif;?>	
	<?php wp_reset_query(); ?>
</div>