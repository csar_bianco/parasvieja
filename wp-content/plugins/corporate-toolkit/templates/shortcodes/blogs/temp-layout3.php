<?php
if( isset($atts) ){
    extract ( $atts );
}
$archive_year   = get_the_time( 'Y' ); 
$archive_month  = get_the_time( 'm' ); 
$archive_day    = get_the_time( 'd' );
?>
<div class="<?php echo esc_attr( $css_class );?>">
	<?php if ($loop_posts->have_posts()): ?>
		<div class="cp-latest-news latest-news-5 owl-carousel nav-style4" <?php echo $owl_carousel; ?>>
			<?php while ($loop_posts->have_posts()): $loop_posts->the_post(); ?>

				<div class="blog-item">
					<div class="post-img">
						<?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, 270, 450, true, true, false ); ?>
			            <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
					</div>
					<div class="post-info">
						<h3 class="post-title">
							<a href="<?php the_permalink() ?>">
								<?php the_title() ?>
							</a>
						</h3>
						<ul class="meta-post">
							<li><i class="fa fa-calendar-o"></i>
								<a href="<?php echo get_day_link( $archive_year, $archive_month, $archive_day ); ?>">
									<?php the_time('j M Y') ?>
								</a>
							</li>
							<li><i class="fa fa-comment-o"></i>
								<?php comments_number(
			                        esc_html__('0', 'corporatepro'),
			                        esc_html__('1', 'corporatepro'),
			                        esc_html__('%', 'corporatepro')
			                    );?>
			                </li>
						</ul>
						<div class="post-excerpt">
							<?php echo wp_trim_words( get_the_content(),20,'...') ?>
						</div>
					</div>
				</div>

			<?php endwhile; ?>
		</div>
	<?php endif;?>	
	<?php wp_reset_query(); ?>
</div>
