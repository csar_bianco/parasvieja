<?php
if( isset($atts) ){
    extract ( $atts );
}
$i = 0;
$width = 0;
$height = 0;
?>
<div class="<?php echo esc_attr( $css_class );?>">
	<?php if ($loop_posts->have_posts()): ?>
		<div class="cp-latest-news latest-news-8 owl-carousel dot-style1" <?php echo $owl_carousel; ?>>
			<?php while ($loop_posts->have_posts()): $loop_posts->the_post(); ?>
				<?php  
					$i++;
					if ($i % 2 == 0) {
						$width = 292;
						$height = 250;
					} else {
						$width = 292;
						$height = 200;
					}
				?>
				<div class="item-post">
					<div class="post-format">
	        			<figure>
	        				<?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, $width, $height, true, true, false ); ?>
			            	<img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
	        			</figure>
	        		</div>
	        		<div class="main-content-post">
	        			<h3 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
	        			<div class="post-excerpt">
	        				<?php echo wp_trim_words( get_the_content(),15,'...' ) ?>
	        			</div>
	        			<a href="<?php the_permalink() ?>" class="post-readmore cp-button button-dark">
	        				<?php echo esc_html__('Read more','corporatepro') ?>
	        			</a>
	        		</div>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif;?>	
	<?php wp_reset_query(); ?>
</div>