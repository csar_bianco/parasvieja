<?php
if( isset($atts) ){
    extract ( $atts );
}
?>

<div class="<?php echo esc_attr( $css_class );?>">
	<?php if ($loop_posts->have_posts()): ?>
		<?php 
			$i = 1;
			$add_class = '';
			$number = 3;
	        $total_posts = $loop_posts->post_count; 
	    ?>
		<div class="cp-latest-news latest-news-7 owl-carousel nav-style2" <?php echo $owl_carousel; ?>>
			<div class="list-post">
				<?php while ($loop_posts->have_posts()): $loop_posts->the_post(); ?>
					<?php 
			        
			        if ($number % 2 == 0) {
			            $numberr = $number/2;
			            if ($numberr % 2 == 0){
			                $add_class = '';
			            }
			            else {
			                $add_class = 'img-right';
			            }
			        }
			        else {
			            $numberr = ($number - 1)/2;
			            if ($numberr % 2 == 0) {
			                $add_class = '';
			            }
			            else {
			                $add_class = 'img-right';
			            }
			        }
			        $number++;
			        ?>

					<div class="item-post <?php echo esc_attr($add_class) ?>">
						<div class="post-format">
							<?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, 370, 300, true, true, false ); ?>
			            	<img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
						</div>
						<div class="info-post">
							<h3 class="title-post">
								<a href="<?php the_permalink() ?>">
									<?php the_title() ?>
								</a>
							</h3>
							<ul class="meta-post">
		        				<li><i class="fa fa-calendar-o"></i><?php the_time('F j, Y') ?></li>
		        				<li><i class="fa fa-comment-o"></i>
		        				<?php comments_number(
			                        esc_html__('0 Comments', 'corporatepro'),
			                        esc_html__('1 Comments', 'corporatepro'),
			                        esc_html__('% Comments', 'corporatepro')
			                    );?>
		        				</li>
		        			</ul>
		        			<a href="<?php the_permalink() ?>" class="outline-button">
		        				<?php echo esc_html__('Read More','corporatepro') ?>
		        			</a>
						</div>
					</div>
					<?php
		                if( $i % $owl_number_row == 0 && $i < $total_posts ){
		                    echo '</div><div class="list-post">';
		                }
		                $i++;
	                ?>
	            <?php endwhile; ?>
			</div>
		</div>
	<?php endif;?>	
	<?php wp_reset_query(); ?>
</div>