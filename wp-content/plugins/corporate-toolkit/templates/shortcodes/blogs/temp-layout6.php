<?php
if ( isset( $atts ) ) {
	extract( $atts );
}
$width  = 0;
$height = 0;
?>
<div class="<?php echo esc_attr( $css_class ); ?> cp-portfolio">
	<?php if ( $loop_posts->have_posts() ): ?>
        <div class="cp-latest-news latest-news-6 portfolio-grid" data-layoutMode="packery">
			<?php while ( $loop_posts->have_posts() ): $loop_posts->the_post(); ?>
				<?php $size_img = get_post_meta( get_the_ID(), 'image_ratio_post_select', true ); ?>
				<?php
				$class = '';
				if ( $size_img == 'img1x1' || $size_img == '' ) {
					$width  = 210;
					$height = 255;
					$class = 'grid';
				} elseif ( $size_img == 'img2x1' ) {
					$width  = 450;
					$height = 255;
					$class = 'grid grid-2x';
				} elseif ( $size_img == 'img2x3' ) {
					$width  = 690;
					$height = 354;
					$class = 'grid grid-3x';
				} elseif ( $size_img == 'img3x2' ) {
					$width  = 450;
					$height = 639;
					$class = 'grid grid-2x 3x2';
				}
				?>
				<?php
				$gallery   = get_post_meta( get_the_ID(), '_format_gallery_images', true );
				$videos    = get_post_meta( get_the_ID(), '_format_video_embed', true );
				$thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full', true );
				?>
				<?php if ( empty($gallery) && empty($videos) ) : ?>

					<?php
					$image_thumb = corporatepro_resize_image( get_post_thumbnail_id( get_the_ID() ), null, $width, $height, true, true, false );
					?>
                    <div class="item-post item-portfolio <?php echo esc_attr( $class ) ?> <?php if ( $class == 'grid' ) {
						echo 'text';
					} ?>">
                        <div class="post-format">
                            <img class="img-responsive" src="<?php echo esc_attr( $image_thumb[ 'url' ] ); ?>"
                                 width="<?php echo intval( $image_thumb[ 'width' ] ) ?>"
                                 height="<?php echo intval( $image_thumb[ 'height' ] ) ?>" alt="<?php the_title() ?>">
                        </div>
                        <div class="info-post">
                            <div class="info-text">
                                <h3 class="title-post"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>
                                <div class="excerpt-post">
									<?php echo wp_trim_words( get_the_content(), 6, '...' ) ?>
                                </div>
                            </div>
                            <ul class="meta-post meta-style4">
                                <li><i class="fa fa-calendar-o"></i><?php the_time( 'M Y' ) ?></li>
                                <li><i class="fa fa-comment-o"></i>
									<?php comments_number(
										esc_html__( '0', 'corporatepro' ),
										esc_html__( '1', 'corporatepro' ),
										esc_html__( '%', 'corporatepro' )
									); ?>
                                </li>
                                <li><i class="fa fa-user"></i>
                                    <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) ?>">
										<?php the_author() ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

				<?php elseif ( !empty($gallery) && empty($videos) ) : ?>

					<?php
					$image_thumb = corporatepro_resize_image( get_post_thumbnail_id( get_the_ID() ), null, $width, $height, true, true, false );
					?>

                    <div class="item-post item-portfolio <?php echo esc_attr( $class ) ?>">
                        <div class="post-format post-img">
                            <img class="img-responsive" src="<?php echo esc_attr( $image_thumb[ 'url' ] ); ?>"
                                 width="<?php echo intval( $image_thumb[ 'width' ] ) ?>"
                                 height="<?php echo intval( $image_thumb[ 'height' ] ) ?>" alt="<?php the_title() ?>">
                            <a href="<?php the_permalink(); ?>" class="icon-link-post">
                                <i class="fa fa-camera"></i>
                            </a>
                        </div>
                    </div>

				<?php elseif ( empty($gallery) && !empty($videos) ) : ?>

					<?php
					$image_thumb = corporatepro_resize_image( get_post_thumbnail_id( get_the_ID() ), null, $width, $height, true, true, false );
					?>
                    <div class="item-post item-portfolio <?php echo esc_attr( $class ) ?>">
                        <div class="post-format pos-video">
                            <img src="<?php echo esc_attr( $image_thumb[ 'url' ] ); ?>" alt="">
                            <a class="icon-link-post" href="<?php the_permalink(); ?>"><i class="fa fa-play"></i></a>
                        </div>
                    </div>

				<?php endif; ?>

			<?php endwhile; ?>
        </div>
	<?php endif; ?>
	<?php wp_reset_query(); ?>
</div>