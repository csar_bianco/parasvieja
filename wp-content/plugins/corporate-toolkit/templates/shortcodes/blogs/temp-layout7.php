<?php
if( isset($atts) ){
    extract ( $atts );
}
?>
<div class="ss-lastestnews-h1 <?php echo esc_attr( $css_class );?>">
    <?php if ($title) : ?>
        <h2 class="title-ss"><?php echo esc_html($title) ?></h2>
    <?php endif; ?>
    <div class="cp-latest-news latest-news-1">
        <?php while ($loop_posts->have_posts()): $loop_posts->the_post() ?>
            <div class="item-post">
            <div class="meta-latest">
                <span class="date-post"><?php the_time('j') ?></span>
                <span class="month"><?php the_time('D') ?></span>
                <span class="comment">
                    <?php comments_number(
                        esc_html__('0', 'corporatepro'),
                        esc_html__('1', 'corporatepro'),
                        esc_html__('%', 'corporatepro')
                    );?>
                </span>
            </div>
            <div class="info-post">
                <h4 class="title-post"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
                <a href="<?php the_permalink() ?>" class="outline-button">
                    <?php echo esc_html__('Read More','corporatepro') ?>
                </a>
            </div>
        </div>
        <?php endwhile; ?>
        <?php wp_reset_query(); wp_reset_postdata(); ?>
    </div>
</div>
