<?php
if( isset($atts) ){
	extract ( $atts );
}
?>
<?php
if( !empty($link) ){
	$link = vc_build_link( $link );
}else{
	$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
}
?>
<?php if($link['url']): ?>

	<div class="block-help">
		<h2><?php echo esc_html($title) ?></h2>
		<a  class="<?php echo esc_attr( $css_class );?> link-help" href="<?php echo esc_url($link['url']) ?>" <?php if($link['target']): ?> target="<?php echo esc_html($link['target']) ?>" <?php endif; ?>  <?php if($link['rel']): ?> rel="<?php echo esc_attr($link['rel']) ; ?>" <?php endif; ?>><?php echo esc_html($link['title']); ?>
			<img alt="" src="<?php echo get_template_directory_uri().'/images/arrow-view.png' ?>">
		</a>
	</div>

<?php endif; ?>