<?php
if( isset($atts) ){
	extract ( $atts );
}
?>
<?php
if( !empty($link) ){
	$link = vc_build_link( $link );
}else{
	$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
}
?>

<a  class="<?php if($style_btn) {echo esc_attr( $style_btn );} ?> <?php echo esc_attr( $css_class );?> button-icon button-style3" href="<?php echo esc_url($link['url']) ?>" <?php if($link['target']): ?> target="<?php echo esc_html($link['target']) ?>" <?php endif; ?>  <?php if($link['rel']): ?> rel="<?php echo esc_attr($link['rel']) ; ?>" <?php endif; ?>>
	<i class="<?php echo esc_attr($icon_fontawesome) ?>"></i>
	<?php echo esc_html($link['title']); ?>
</a>