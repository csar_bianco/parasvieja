<?php
if( isset($atts) ){
    extract ( $atts );
}
?>
<div class="widget widget-contactinfo <?php echo esc_attr( $css_class );?>">
    <?php if( $title ):?>
        <h3 class="title-widget"><i class="fa fa-envelope"></i><?php echo esc_html($title)?></h3>
    <?php endif;?>
    <div class="contact-info">
        <?php if( $email ):?>
        <div class="contact-text">
            <span class="title"><?php esc_html_e('E-mail','corporatepro');?></span>
            <a href="mailto:<?php echo esc_html($email)?>"><?php echo esc_html($email)?></a>
        </div>
        <?php endif;?>
        <?php if( $phone ):?>
        <div class="contact-text">
            <span class="title"><?php esc_html_e('Telephone','corporatepro');?></span>
            <span class="info"><?php echo esc_html($phone)?></span>
        </div>
        <?php endif;?>
        <?php if( $address ):?>
        <div class="contact-text">
            <span class="title"><?php esc_html_e('Address','corporatepro');?></span>
            <span class="info"><?php echo esc_html($address)?></span>
        </div>
        <?php endif;?>
    </div>
</div>