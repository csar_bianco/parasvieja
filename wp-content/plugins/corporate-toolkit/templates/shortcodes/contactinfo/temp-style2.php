<?php
if( isset($atts) ){
    extract ( $atts );
}
?>

<div class="widget widget_contactinfo <?php echo esc_attr( $css_class );?>">
	<?php if( $title ):?>
        <h3 class="title-widget"><?php echo esc_html($title)?></h3>
    <?php endif;?>
	<div class="widget-content" style="background-image: url(<?php echo esc_url( wp_get_attachment_url($atts['bg_contact']) ) ?>);">
		<?php if( $description ):?>
			<p><?php echo esc_html($description)?></p>
		<?php endif;?>
		<address>
			<?php if( $address ):?>
				<span class="address"><i class="fa fa-map-marker"></i><?php echo esc_html($address)?></span>
			<?php endif;?>
			<?php if( $phone ):?>
				<span class="phone"><i class="fa fa-phone"></i><?php echo esc_html($phone)?></span>
			<?php endif;?>
			<?php if( $email ):?>
				<a class="email" href="mailto:<?php echo esc_html($email)?>"><i class="fa fa-envelope-o"></i><?php echo esc_html($email)?></a>
			<?php endif;?>
		</address>
	</div>
</div>