<?php
if( isset($atts) ){
    extract ( $atts );
}
?>
<div class="cp-contactinfo <?php echo esc_attr( $css_class );?>">
	<div class="contact-info">
		<?php if( $email ):?>
			<div class="contact-text">
				<span class="title"><i class="fa fa-envelope"></i><?php echo esc_html__('E-mail','corporatepro') ?></span>
				<a href="mailto:<?php echo esc_html($email)?>"><?php echo esc_html($email)?></a>
			</div>
		<?php endif;?>
		<?php if( $phone ):?>
			<div class="contact-text">
				<span class="title"><i class="fa fa-phone"></i><?php echo esc_html__('Telephone','corporatepro') ?></span>
				<span class="info"><?php echo esc_html($phone)?></span>
			</div>
		<?php endif;?>
		<?php if( $address ):?>
			<div class="contact-text">
				<span class="title"><i class="fa fa-mobile-phone"></i><?php echo esc_html__('Address','corporatepro') ?></span>
				<span class="info"><?php echo esc_html($address)?></span>
			</div>
		<?php endif;?>
	</div>
</div>
