<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="<?php echo esc_attr( $css_class );?> counter-style2">
	<span class="number" data-number="<?php echo esc_attr($number)?>"><?php echo esc_html($number)?></span>
	<?php if( $title ):?>
	<span class="title"><?php echo esc_html( $title );?></span>
	<?php endif;?>
	<span class="icon-counter">
		<i class="<?php echo esc_attr($icon);?>"></i>
	</span>
</div>