<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="<?php echo esc_attr( $css_class );?> counter-style1">
	<span class="number" data-number="<?php echo esc_attr($number)?>"><?php echo esc_html($number)?></span>
	<?php if( $title ):?>
	<span class="title"><?php echo esc_html( $title );?></span>
	<?php endif;?>
</div>