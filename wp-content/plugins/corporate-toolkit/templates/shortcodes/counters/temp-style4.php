<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="<?php echo esc_attr( $css_class );?> cp-counter counter-style4">
	<span class="icon-counter"><i class="<?php echo esc_attr($icon);?>"></i></span>
	<span data-number="<?php echo esc_attr($number)?>" class="number"><?php echo esc_html($number)?></span>
	<?php if( $title ):?>
		<span class="title"><?php echo esc_html( $title );?></span>
	<?php endif;?>
</div>
