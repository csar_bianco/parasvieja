<?php
if( isset($atts) ){
	extract ( $atts );
}
?>
<div class="widget widget_funfact">
	<div class="item-funfact <?php echo esc_attr($style_counter) ?>">
		<span class="number"><?php echo esc_html($number) ?></span>
		<span class="title"><?php echo esc_html($title) ?></span>
	</div>
</div>