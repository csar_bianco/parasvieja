<?php
if( isset($atts) ){
	extract ( $atts );
}
if( !empty($link) ){
	$link = vc_build_link( $link );
}else{
	$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
}
?>

<div class="feature-style1 <?php echo esc_attr($light) ?> <?php echo esc_attr($text_align) ?> <?php echo esc_attr( $css_class );?>">
	<div class="title-feature">
	<?php if($number) : ?>
		<span class="number-feature"><?php echo esc_html($number) ?></span>
	<?php endif; ?>
	<?php if($title) : ?>
		<h4>
			<a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>">
				<?php echo esc_html($title) ?>		
			</a>
		</h4>
	<?php endif; ?>
	</div>
	<?php if($description) : ?>
		<div class="info-feature">
			<p><?php echo esc_html($description) ?></p>
		</div>
	<?php endif; ?>
</div>