<?php
if( isset($atts) ){
	extract ( $atts );
}
$link_default = array(
    'url'    => '',
    'title'  => '',
    'target' => '',
);

if (function_exists('vc_build_link')):
    $link = vc_build_link($link);
else:
    $link = $link_default;
endif;
?>

<div class="cp-service service-style-2 <?php echo esc_attr( $css_class ) ?>">
	<?php if( $icon ):?>
	<div class="icon-service">
		<i class="<?php echo esc_attr( $icon );?>"></i>
	</div>
	<?php endif;?>

	<div class="content-service">
		<?php if( $title ):?>
		<h4 class="title-service">
			<a href="<?php echo esc_url( $link['url']) ?>" <?php if($link['target']):?>target="<?php echo esc_attr( $link['target']) ?>"<?php endif;?>>
				<?php echo esc_html( $title );?>	
			</a>
		</h4>
		<?php endif;?>
		<?php if( $subtitle ):?>
		<span class="sub-title"><?php echo esc_html($subtitle );?></span>
		<?php endif;?>
		<?php if( $description ):?>
		<div class="desc-service">
			<?php echo esc_html( $description );?>
		</div>
		<?php endif;?>
	</div>
</div>