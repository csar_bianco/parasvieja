<?php
if( isset($atts) ){
	extract ( $atts );
}
$link_default = array(
    'url'    => '',
    'title'  => '',
    'target' => '',
);

if (function_exists('vc_build_link')):
    $link = vc_build_link($link);
else:
    $link = $link_default;
endif;
?>

<div class="cp-service service-style-6 <?php echo esc_attr( $css_class ) ?>">
	<div class="icon-service">
		<i class="<?php echo esc_attr( $icon );?>"></i>
	</div>
	<h4 class="title-service">
		<a href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
			<?php echo esc_html( $title );?>	
		</a>
	</h4>
	<?php if ($description != '' ) : ?>
		<div class="desc-service">
			<?php echo esc_html( $description );?>
		</div>
	<?php endif; ?>
</div>