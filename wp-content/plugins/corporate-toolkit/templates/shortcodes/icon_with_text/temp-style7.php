<?php
if( isset($atts) ){
	extract ( $atts );
}
$link_default = array(
    'url'    => '',
    'title'  => '',
    'target' => '',
);

if (function_exists('vc_build_link')):
    $link = vc_build_link($link);
else:
    $link = $link_default;
endif;
?>
<div class="list-about">
	<div class="item-ab <?php echo esc_attr( $css_class ) ?>">
		<span class="ab-icon"><i class="<?php echo esc_attr( $icon );?>"></i></span>
		<a class="ab-text" href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
			<?php echo esc_html( $title );?>
		</a>
	</div>
</div>
