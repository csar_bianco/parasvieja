<?php
if( isset($atts) ){
	extract ( $atts );
}
$link_default = array(
    'url'    => '',
    'title'  => '',
    'target' => '',
);

if (function_exists('vc_build_link')):
    $link = vc_build_link($link);
else:
    $link = $link_default;
endif;
?>

<div class="<?php echo esc_attr( $css_class ) ?>">
	<div class="cp-service service-style-4">
		<div class="icon-service"><i class="<?php echo esc_attr( $icon );?>"></i></div>
		<div class="content-service">
		<?php if ($title) : ?>
			<h4 class="title-service">
				<a href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
					<?php echo esc_html( $title );?>
				</a>
			</h4>
		<?php endif; ?>
		<?php if ($description) : ?>
			<div class="desc-service"><?php echo esc_html( $description );?></div>
		<?php endif; ?>
		<?php if (!empty($link['url'])) : ?>
			<a class="cp-button button-dark" href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
				<?php echo esc_html( $link['title']) ?>
			</a>
		<?php endif; ?>
		</div>
	</div>
</div>