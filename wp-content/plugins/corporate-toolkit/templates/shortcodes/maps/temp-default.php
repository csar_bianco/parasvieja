<?php
if( isset($atts) ){
    extract ( $atts );
}
?>
<div class="map-footer <?php echo esc_attr( $css_class );?>">
	<div class="cp-google-maps " id="az-google-maps57341d9e51968"
        data-hue			=""
        data-lightness		="1"
        data-map-style		="<?php echo esc_attr($info_content)?>"
        data-saturation		="-100"
        data-modify-coloring="true"
        data-title_maps		="<?php echo esc_html($title) ?>"
        data-phone			="<?php echo esc_html($phone) ?>"
        data-email			="<?php echo esc_html($email) ?>"
        data-address		="<?php echo esc_html($address) ?>"
        data-longitude		="<?php echo esc_html($longitude) ?>"
        data-latitude		="<?php echo esc_html($latitude) ?>" 
        data-pin-icon		=""
        data-zoom			="<?php echo esc_html($zoom) ?>"
        data-map-type		="<?php echo esc_attr($map_type) ?>">
    </div>
</div>