<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="left-footer <?php echo esc_attr( $css_class );?>">
	<div class="widget widget-newsletter newsletter-form-wrap">
		<?php if( $title ):?>
			<h3 class="title-widget"><?php echo esc_html($title);?></h3>
		<?php endif;?>
		<?php if( $description ):?>
			<p><?php echo esc_html($description);?></p>
		<?php endif;?>
		<form action="#" class="form-newsletter">
			<input name="emailaddress" type="email" value="" placeholder="<?php echo esc_attr($placeholder_text);?>">
			<span><button type="submit" class="submit-newsletter button-icon outline-button"><i class="fa fa-envelope"></i><?php echo balanceTags($button_text);?></button></span>
		</form>
	</div>
</div>