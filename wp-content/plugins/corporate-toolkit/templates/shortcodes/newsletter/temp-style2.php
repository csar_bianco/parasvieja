<?php
if( isset($atts) ){
	extract ( $atts );
}
?>
<div class="top-footer <?php echo esc_attr( $css_class );?>">
	<div class="container">
		<div class="newsletter-form-wrap widget widget-newsletter" style="background: url(<?php echo esc_url( wp_get_attachment_url($bg_newsletter)) ?>) no-repeat center center;">
			<?php if( $title ):?>
				<h3 class="title-widget"><?php echo esc_html($title);?></h3>
			<?php endif;?>
			<?php if( $description ):?>
				<p><?php echo esc_html($description);?>
					<a href="mailto:<?php echo esc_html($email_des);?>"><?php echo esc_html($email_des);?></a>
				</p>
			<?php endif;?>
			<form class="form-newsletter" action="#">
				<input name="emailaddress" type="email" placeholder="<?php echo esc_attr($placeholder_text);?>" value="">
				<span><input class="submit-newsletter" type="submit" value="<?php echo balanceTags($button_text);?>"></span>
			</form>
		</div>
	</div>
</div>