<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="newsletter-form-wrap widget widget-newsletter widget-newsletter-4 <?php echo esc_attr( $css_class );?>">
	<?php if( $title ):?>
		<h3 class="title-widget"><?php echo esc_html($title);?></h3>
	<?php endif;?>
	<?php if( $description ):?>
		<p><?php echo esc_html($description);?></p>
	<?php endif;?>
	<form action="#" class="form-newsletter">
		<input name="emailaddress" type="email" value="" placeholder="<?php echo esc_attr($placeholder_text);?>">
		<span><button type="submit" class="submit-newsletter"><i class="submit-newsletter fa fa-paper-plane-o"></i></button></span>
	</form>
</div>