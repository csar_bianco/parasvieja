<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="newsletter-form-wrap widget widget-newsletter-3 <?php echo esc_attr( $css_class );?>">
	<form action="#" class="form-newsletter">
		<?php if( $title ):?>
			<h3><?php echo esc_html($title);?></h3>
		<?php endif;?>
		<?php if( $description ):?>
			<p><?php echo esc_html($description);?></p>
		<?php endif;?>
		<input class="name_input" type="text" name="name" value="" placeholder="<?php echo esc_attr($placeholder_name);?>">
		<input class="email_input" type="email" name="emailaddress" value="" placeholder="<?php echo esc_attr($placeholder_text);?>">
		<button type="submit" class="submit-newsletter"><?php echo balanceTags($button_text);?></button>
	</form>
</div>