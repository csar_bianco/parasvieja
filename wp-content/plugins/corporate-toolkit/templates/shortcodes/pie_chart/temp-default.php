<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="<?php echo esc_attr( $css_class );?> cp-pie-charts pie-chart-1" data-barsize="10" data-goal="<?php echo esc_attr( $percent );?>" data-barcolor="<?php echo esc_attr( $barcolor );?>" data-trackcolor="<?php echo esc_attr( $trackcolor );?>">
	<div class="progress-label">
        <div class="percent-number">
        	<span class="percent"><?php echo esc_html( $percent );?></span>
        	<span class="unit">%</span>
        </div>
        <?php if( $title ):?>
        <div class="title-progress"><?php echo esc_html( $title );?></div>
    	<?php endif;?>
    </div>
</div>