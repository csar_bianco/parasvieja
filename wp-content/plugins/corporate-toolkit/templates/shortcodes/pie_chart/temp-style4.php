<?php
if( isset($atts) ){
	extract ( $atts );
}
?>
<div class="cp-block-pricing <?php echo esc_attr( $css_class );?>">
    <div class="center-block ">
        <div class="percent-price" style="background-color: <?php echo esc_attr( $boxcolor );?>;">
            <div class="percent"><?php echo esc_attr( $number );?><span class="unit"><?php echo esc_html__('%','corporatepro') ?></span></div>
            <?php if( $title ):?>
                <div class="text-desc"><?php echo esc_html( $title ) ?></div>
            <?php endif; ?>
        </div>
    </div>
</div>
