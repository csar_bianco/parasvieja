<?php
if( isset($atts) ){
	extract ( $atts );
}
$link_default = array(
    'url'    => '',
    'title'  => '',
    'target' => '',
);

if (function_exists('vc_build_link')):
    $link = vc_build_link($link);
else:
    $link = $link_default;
endif;

?>

<div class="cp-pricing pricing-color color-00bbd3 <?php echo esc_attr( $css_class );?>">
	<div class="pricing-header">
		<h4 class="title-pricing">
			<a href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
				<?php echo esc_html( $title ); ?>
			</a>
		</h4>
		<div class="price-unit" style="background-color: <?php echo esc_attr( $main_color ); ?>">
			<div class="price">
				<span class="currency"><?php echo esc_html__('$','corporatepro') ?></span>
				<?php echo esc_html( $price ); ?>
			</div>
			<div class="unit"><?php echo esc_html__('/','corporatepro') ?><?php echo esc_html( $per ); ?></div>
		</div>
	</div>
	<div class="pricing-feature" style="text-align: <?php echo esc_attr($text_align); ?>">
		<ul>
		<?php  
			$lists = vc_param_group_parse_atts( $group_feild );
			if (!empty($lists)) {
				foreach ($lists as $key => $value) {
					printf('<li><i class="fa '.$value['icon_fontawesome'].'"></i>'.$value['list_field'].'</li>');
				}	
			}	
		?>
		</ul>
	</div>
	<div class="pricing-button">
		<a class="cp-button" href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>" style="background-color: <?php echo esc_attr( $main_color ); ?>">
			<?php echo esc_html( $link['title']) ?>	
		</a>
	</div>
</div>