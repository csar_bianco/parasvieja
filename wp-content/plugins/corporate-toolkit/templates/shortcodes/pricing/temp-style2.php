<?php
if( isset($atts) ){
	extract ( $atts );
}
$link_default = array(
    'url'    => '#',
    'title'  => '',
    'target' => '',
);

if (function_exists('vc_build_link')):
    $link = vc_build_link($link);
    $link_signup = vc_build_link($link_signup);
else:
    $link = $link_default;
    $link_signup = $link_default;
endif;

?>
<div class="cp-pricing pricing-image <?php echo esc_attr( $css_class );?>">
	<div class="pricing-header" data-background="<?php echo esc_url( wp_get_attachment_url($atts['bg_price']) ) ?>">
		<?php  if ($option_price == 'free') : ?>
			<div class="price-unit">
				<div class="price"><?php echo esc_html( $text_free ); ?></div>
				<a href="<?php echo esc_url( $link_signup['url']) ?>" <?php if( $link['target'] ):?>target="<?php echo esc_attr( $link['target']) ?>"<?php endif;?> class="signup">
					<?php echo esc_html__('Sign up','corporatepro') ?>
				</a>
			</div>
		<?php else : ?>
			<div class="price-unit">
				<div class="price">
					<?php echo esc_html__('$', 'corporatepro'); ?>
					<span class="big-number"><?php echo esc_html( $hi_price ); ?></span>
					<span class="unline"><?php echo esc_html( $lo_price ); ?></span>
				</div>
				<a href="<?php echo esc_url( $link_signup['url']) ?>" <?php if( $link['target'] ):?>target="<?php echo esc_attr( $link['target']) ?>"<?php endif;?> class="signup">
					<?php echo esc_html__('Sign up','corporatepro') ?>
				</a>
			</div>
		<?php endif; ?>
	</div>
	<div class="pricing-feature">
		<h4 class="title-pricing">
			<a href="<?php echo esc_url( $link['url']) ?>" target="<?php echo esc_attr( $link['target']) ?>">
				<?php echo esc_html( $title ); ?>
			</a>
		</h4>
		<ul>
			<?php  
				$lists = vc_param_group_parse_atts( $group_feild );
				if ( !empty($lists) ) {
					foreach ($lists as $key => $value) {
						printf('<li>'.$value['list_field'].'</li>');
					}	
				}	
			?>
		</ul>
	</div>
	<div class="pricing-button">
		<a class="outline-button bt-recent" href="<?php echo esc_url( $link['url']) ?>" <?php if( $link['target'] ):?>target="<?php echo esc_attr( $link['target']) ?>"<?php endif;?>>
			<?php echo esc_html( $link['title']) ?>	
		</a>
	</div>
</div>