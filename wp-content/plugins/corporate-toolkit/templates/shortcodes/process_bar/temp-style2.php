<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="<?php echo esc_attr( $css_class );?> skillbar-style2" data-bgskillbar="<?php echo esc_attr( $bgskillbar );?>" data-bgskill="<?php echo esc_attr( $bgskill );?>" data-percent="<?php echo esc_attr( $percent );?>">
	<div class="info-skill">
		<?php if( $title ):?>
		<h4><?php echo esc_html( $title );?></h4>
		<?php endif;?>
		<?php if( $short_description ):?>
		<div class="desc-skill"><?php echo esc_html( $short_description );?></div>
		<?php endif;?>
		<span class="percent-skill"><?php echo esc_attr( $percent );?>%</span>
	</div>
	<div class="skill-bar-bg">
		<div class="skillbar-bar">
		</div>
    </div>
</div>