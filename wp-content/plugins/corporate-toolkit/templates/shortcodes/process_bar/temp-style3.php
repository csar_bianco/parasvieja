<?php
if( isset($atts) ){
	extract ( $atts );
}
$css_class .= 'skill-'.$skill_align;
?>

<div class="<?php echo esc_attr( $css_class );?> skillbar-style3" data-bgskillbar="<?php echo esc_attr( $bgskillbar );?>" data-bgskill="<?php echo esc_attr( $bgskill );?>" data-percent="<?php echo esc_attr( $percent );?>">
	<?php if( $title ):?>
	<span class="skillbar-title"><?php echo esc_html( $title );?></span>
	<?php endif;?>
	<div class="skill-bar-bg">
		<div class="skillbar-bar">
			<div class="skill-bar-percent"><?php echo esc_attr( $percent );?>%</div>
		</div>
    </div>
</div>