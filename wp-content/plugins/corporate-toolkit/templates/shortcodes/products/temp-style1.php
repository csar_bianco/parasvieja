<?php
if( $atts ){
    extract ( $atts );
}

$category_slug = array();
if( $taxonomy ){
	$category_slug = explode(',', $taxonomy);
}
$categorys = get_terms( array(
    'taxonomy' => 'product_cat',
    'hide_empty' => false,
    'slug'=> $category_slug
) );

$atts_data = json_encode($atts);
?>

<div class="products product-grid-3 row <?php echo esc_attr( $css_class );?>">
	<?php while ( $products->have_posts() ) : $products->the_post();  ?>
		<div <?php post_class( $grid_bootstrap ) ?>>
        	<?php wc_get_template_part('product-styles/content-product-style', '4'); ?>
        </div>
	<?php endwhile;?>
</div>