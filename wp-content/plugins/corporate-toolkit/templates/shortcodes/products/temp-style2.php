<?php
if( $atts ){
    extract ( $atts );
}

$category_slug = array();
if( $taxonomy ){
	$category_slug = explode(',', $taxonomy);
}
$categorys = get_terms( array(
    'taxonomy' => 'product_cat',
    'hide_empty' => false,
    'slug'=> $category_slug
) );

$atts_data = json_encode($atts);
?>
<?php if( $products->have_posts()): ?>
	<?php 
		$i = 1;
        $toal_product = $products->post_count; 
    ?>
<div class="cp-popular-works slide-dotcustom dot-style6 <?php echo esc_attr( $css_class );?>">
	<div class="owl-dots cp-dots-custom">
	</div>
	<div class="cp-works-slide owl-carousel dots-custom" <?php echo $owl_carousel; ?>>
		<div class="item-workslide">
			<?php while ( $products->have_posts() ) : $products->the_post();  ?>
				<div class="item-project">
					<div class="pj-caption">
	        			<div class="pj-image">
	        				<figure>
	        					<?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, $thumb_width, $thumb_height, true, true, false ); ?>
			            		<img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
			            	</figure>
	        				<div class="pj-hover">
	        					<div class="pf-info">
		        					<h3 class="pj-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
		        					<span class="pj-desc"><?php echo wp_trim_words(get_the_excerpt(),5,''); ?></span>
	        					</div>
	        				</div>
	        			</div>
		        	</div>
				</div>
				<?php
	                if( $i % $owl_number_row == 0 && $i < $toal_product ){
	                    echo '</div><div class="item-workslide">';
	                }
	                $i++;
                ?>
			<?php endwhile;?>
		</div>
	</div>
</div>
<?php endif; wp_reset_postdata(); wp_reset_query();?>