<?php
if( $atts ){
    extract ( $atts );
}

$category_slug = array();
if( $taxonomy ){
	$category_slug = explode(',', $taxonomy);
}
$categorys = get_terms( array(
    'taxonomy' => 'product_cat',
    'hide_empty' => false,
    'slug'=> $category_slug
) );

$atts_data = json_encode($atts);
?>
<?php 
remove_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_rating',5);
remove_action('woocommerce_before_shop_loop_item_title','corporatepro_shop_loop_item_contdown',20);
?>
<?php if( $products->have_posts()): ?>
	<?php 
		$i = 1;
        $toal_product = $products->post_count; 
    ?>
    <div class="ss-product-v10 <?php echo esc_attr( $css_class );?>">
    	<div class="product-slide product-grid-4 owl-carousel dot-style1" <?php echo $owl_carousel; ?>>
			<div class="list_product">
				<?php while ( $products->have_posts() ) : $products->the_post();  ?>
					<?php wc_get_template_part('product-styles/content-product-style', '5'); ?>
					<?php
		                if( $i % $owl_number_row == 0 && $i < $toal_product ){
		                    echo '</div><div class="list_product">';
		                }
		                $i++;
	                ?>
	            <?php endwhile;?>
			</div>
		</div>
    </div>
<?php endif; wp_reset_postdata(); wp_reset_query();?>
<?php 
add_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_rating',5);
add_action('woocommerce_before_shop_loop_item_title','corporatepro_shop_loop_item_contdown',20);
?>