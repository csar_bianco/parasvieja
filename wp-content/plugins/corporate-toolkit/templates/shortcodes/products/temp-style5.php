<?php
if( $atts ){
    extract ( $atts );
}

$category_slug = array();
if( $taxonomy ){
	$category_slug = explode(',', $taxonomy);
}
$categorys = get_terms( array(
    'taxonomy' => 'product_cat',
    'hide_empty' => false,
    'slug'=> $category_slug
) );

$atts_data = json_encode($atts);
wp_enqueue_script( 'prettyphoto' );
wp_enqueue_style( 'prettyphoto' );
$rand =  rand();
?>
<div class="cp-new-product-shopv2 cp-new-product product-grid <?php echo esc_attr( $css_class );?>">
	<?php while ( $products->have_posts() ) : $products->the_post();  ?>
	<div <?php post_class( $grid_bootstrap ) ?>>
		<?php
		/**
		 * woocommerce_before_shop_loop_item hook.
		 *
		 * @hooked woocommerce_template_loop_product_link_open - 10
		 */
		do_action( 'woocommerce_before_shop_loop_item' );

		$date = get_post_meta( get_the_ID(), '_sale_price_dates_to', true );

		if ($date != 0) {

		    $deal_date = date_i18n( 'Y/m/d g:i:s', $date );

		} else { 

			$deal_date = date_i18n( 'Y/m/d g:i:s', '0' );

		}
		global $product;

		$attachment_ids = $product->get_gallery_attachment_ids();
	?>
		<div class="product <?php echo esc_attr($show_gallery) ?>">
			<div class="product-inner">
				<div class="product-thumb">
					<?php
					/**
					 * woocommerce_before_shop_loop_item_title hook.
					 *
					 * @hooked woocommerce_show_product_loop_sale_flash - 10
					 * @hooked woocommerce_template_loop_product_thumbnail - 10
					 */
					do_action( 'woocommerce_before_shop_loop_item_title' );
					?>
					<div class="product-button">
					<?php
						do_action('corporatepro_function_shop_loop_item_wishlist');
						do_action('corporatepro_function_shop_loop_item_quickview');
						do_action('corporatepro_function_shop_loop_item_compare');
					?>
					</div>
					<?php if ($date != '') : ?>
						<div data-time="<?php echo esc_attr( $deal_date ); ?>" class="cp-countdown">
							<span class="icon"><i class="fa fa-clock-o"></i></span>
							<span class="hour"><?php echo esc_html__( 'h','corporatepro' ); ?></span> : 
							<span class="minute"><?php echo esc_html__( 'm','corporatepro' ); ?></span> : 
							<span class="second"><?php echo esc_html__( 's','corporatepro' ); ?></span>&nbsp;
							<span class="text"><?php echo esc_html__('Left','corporatepro') ?></span>
						</div>
					<?php endif; ?>
				</div>
				<div class="info-product">
					<?php
					/**
					 * woocommerce_shop_loop_item_title hook.
					 *
					 * @hooked woocommerce_template_loop_product_title - 10
					 */
					do_action( 'woocommerce_shop_loop_item_title' );
					/**
					 * woocommerce_after_shop_loop_item_title hook.
					 *
					 * @hooked woocommerce_template_loop_rating - 5
					 * @hooked woocommerce_template_loop_price - 10
					 */
					do_action( 'woocommerce_after_shop_loop_item_title' );
					/**
					 * woocommerce_after_shop_loop_item hook.
					 *
					 * @hooked woocommerce_template_loop_product_link_close - 5
					 * @hooked woocommerce_template_loop_add_to_cart - 10
					 */
					do_action( 'woocommerce_after_shop_loop_item' );
					?>
				</div>
			</div>
			<?php if (( $show_gallery != 'no') && !empty($attachment_ids) ) : ?>
				<div class="slide-brand">
					<div class="cp-recent-client owl-carousel nav-style3" <?php echo $owl_carousel; ?>>
						<?php foreach( $attachment_ids as $attachment_id ) : ?>
							<?php $image_thumb = corporatepro_resize_image( $attachment_id, null, 73, 62, true, true, false ); ?>
							<a class="prettyphoto pj-icon icon-view" href="<?php echo esc_url( wp_get_attachment_url($attachment_id) ) ?>" data-rel="prettyPhoto[rel-<?php echo get_the_ID() ?>-<?php echo $rand ?>]">
					            <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
							</a>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php endwhile;?>
</div>
