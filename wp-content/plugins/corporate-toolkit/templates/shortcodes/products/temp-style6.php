<?php
if( $atts ){
    extract ( $atts );
}
?>

<div class="cp-feature-project-slide <?php echo esc_attr($css_class); ?>">
	<div class="slide-feature-project">
		<?php while ($products->have_posts()) : $products->the_post() ?>
			
			<?php wc_get_template_part('product-styles/content-product-style', '3'); ?>

		<?php endwhile; wp_reset_postdata(); wp_reset_query();?>
	</div>
	<div class="control-project-slider">
        <div id="slider-range-max"></div>
    </div>
</div>