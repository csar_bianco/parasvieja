<?php
if( $atts ){
    extract ( $atts );
}
?>
<?php if ( $projects->have_posts() ) : ?>
<div class="cp-project project-style3 cp-portfolio <?php echo esc_attr($css_class); ?>">
	<?php if ($projects->have_posts()): ?>
		<div class="portfolio-grid" data-layoutMode="fitRows" data-cols="<?php echo esc_attr( $columns );?>">
		<?php
		$i = 0;
		$stt  ='';
		?>
		<?php while ($projects->have_posts()): $projects->the_post(); ?>
			<?php
			$i++;
			if( $i < 10 ){
				$stt = '0'.$i;
			}else{
				$stt = $i;
			}
			?>
			<div <?php post_class('item-project item-portfolio');?> data-wow-delay="0.4s">
        		<div class="pj-caption">
                    <?php $image_thumb = corporatepro_resize_image(get_post_thumbnail_id(get_the_ID()), null, 305, 305, true, true, false); ?>
        			<div class="pj-image">
        				<figure>
        					<img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
        				</figure>
        				<div class="pj-hover">
        					<a href="<?php the_permalink() ?>" class="pj-icon"><i class="fa fa-link"></i></a>
        				</div>
        			</div>
        		</div>
        		<div class="pj-info">
    				<h3 class="pj-title"><a href="<?php the_permalink() ?>"><?php echo esc_html($stt);?> - <?php the_title();?></a></h3>
    			</div>
        	</div>
		<?php endwhile; ?>
		</div>
	<?php endif;?>	
	<div class="pj-loadmore">
    	<a href="<?php echo esc_url( get_post_type_archive_link( 'project' ) );?>" class="cp-button button-medium"><?php esc_html_e('View all Project','corporatepro');?></a>
    </div>
</div>
<?php endif; ?>