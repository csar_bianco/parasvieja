<?php
if( $atts ){
    extract ( $atts );
}
$responsive = '{"0":{"items":'.$layout2_xs_items.'},"600":{"items":'.$layout2_sm_items.'},"992":{"items":'.$layout2_md_items.'},"1200":{"items":'.$layout2_lg_items.'}}';
?>
<div class="cp-project-slide cp-slide-navcustom <?php echo esc_attr($css_class); ?>">
	<div class="title-slide">
		<?php if ($title) : ?>
			<h3><?php echo esc_html($title) ?></h3>
		<?php endif; ?>
		<div class="owl-nav cp-nav-custom"></div>
		<a href="<?php echo esc_url( get_post_type_archive_link( 'project' ) );?>" class="bt-learnmore bt-link">
            <?php echo esc_html__('View all','corporatepro') ?>
            <i class="fa fa-caret-right"></i>
        </a>
	</div>
	<div class="owl-carousel slide-navcustom nav-style6" data-nav="true" data-dots="false" data-margin="30" data-responsive='<?php echo esc_attr($responsive) ?>'>
		<?php while ($projects->have_posts()) : $projects->the_post() ?>
			<?php 
				$check = '';
				
				if ( get_post_type( get_the_ID() ) == 'project' ) {
					$check = 'category_project';
				} elseif ( get_post_type( get_the_ID() ) == 'post' ) {
					$check = 'category';
				}

				$categories_list 	= get_the_term_list( get_the_ID(),$check,'',' ');
				$the_flash 			= get_post_meta( get_the_ID(),'_corporatepro_flash',true);

			?>

			<div class="item-project">
				<div class="pj-image">
					<figure>
						<?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, 270, 203, true, true, false ); ?>
			            <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
					</figure>
					<?php if ($the_flash != '') : ?>
						<span class="label-block <?php echo esc_html($the_flash) ?>"><?php echo esc_html($the_flash) ?></span>
					<?php endif; ?>
				</div>
				<div class="pj-info">
					<h3 class="pj-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
					<div class="pj-meta">
	        			<ul>
	        				<li><i class="fa fa-user"></i>
	        					<a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))) ?>">
	        						<?php the_author() ?>
	        					</a>
	        				</li>
	        				<li><i class="fa fa-folder-o"></i>
	        					<?php
	                                if ($categories_list ) {
	                                    printf( esc_html__('%1$s', 'corporatepro') , $categories_list);
	                                }
                            	?>
                            </li>
	        			</ul>
	        		</div>
				</div>
			</div>
		<?php endwhile; wp_reset_postdata(); wp_reset_query();?>
	</div>
</div>