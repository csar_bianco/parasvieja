<?php
if( isset($atts) ){
	extract ( $atts );
}
?>
<div class="<?php echo esc_attr( $css_class );?> title-style2" style="color: <?php echo esc_attr( $textcolor ) ;?>;">
	<span class="hr"></span>
	<?php if( $title ):?>
	<h2 style="color: <?php echo esc_attr( $textcolor ) ;?>;" class="title"><?php echo esc_html( $title ); ?></h2>
	<?php endif;?>
</div>