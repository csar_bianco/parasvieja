<?php
if( isset($atts) ){
	extract ( $atts );
}
?>

<div class="title-section title-style3 <?php echo esc_attr($light_bg) ?> <?php echo esc_attr( $css_class );?>">
	<span class="hr"></span>
	<h2 class="title" style="color: <?php echo esc_attr( $textcolor ) ;?>;">
	<?php 
		$arr = str_split($title);
		foreach ($arr as $value) {
			printf($value);
		}
	?>
	</h2>
	<?php if ($description) : ?>
		<p class="title-desc"><?php echo esc_attr( $description ) ;?></p>
	<?php endif; ?>
</div>