<?php
if( isset($atts) ){
	extract ( $atts );
}

if( !empty($link) ){
    $link = vc_build_link( $link );
}else{
    $link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
}
?>
<div class="<?php echo esc_attr( $css_class );?>">
	<div class="logo">
        <a href="<?php echo esc_url($link['url']) ?>" <?php if($link['target']):?>target="<?php echo esc_attr( $link['target']) ?>"<?php endif;?>>
            <img class="img-responsive" src="<?php echo esc_url( wp_get_attachment_url($bg_title)) ?>" alt="">
        </a>
	</div>
	<?php if ($number) : ?>
	<div class="divider divider-sm divider-number">
		<div class="divider-content">
			<span style="color: <?php echo esc_attr($textcolor) ?>">
				<?php echo esc_html($number); ?>
			</span>
		</div>
	</div>
	<?php endif; ?>
	<?php if ($title) : ?>
	<div class="title-section title-style5 light text-center">
		<h2 class="title" style="color: <?php echo esc_attr($textcolor) ?>">
			<?php echo esc_html($title) ?>
		</h2>
		<?php if ($hr_title) : ?>
			<span class="<?php echo esc_attr($hr_title) ?>"></span>
		<?php endif; ?>
		<?php if ($description) : ?>
			<div class="title-desc"><?php echo wp_kses($description, $args_text); ?></div>
		<?php endif; ?>
	</div>
	<?php endif; ?>
</div>
