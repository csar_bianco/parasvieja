<?php
if( $atts ){
	extract($atts);
}
$items = vc_param_group_parse_atts( $items );
$i_1 = 0;
$i_2 = 0;
?>
<div class="cp-service-shop <?php echo esc_attr( $css_class );?>">
	<div class="col-sm-6 col-md-5  list-service">
		<?php foreach( $items as $item ):?>
			<?php
				if( !empty($item['link']) ){
	    			$link = vc_build_link( $item['link'] );
	    		}else{
	    			$link = array('url'=>'#', 'title'=>'', 'target'=>'_self', 'rel'=>'') ;
	    		}
			?>
			<div class="item-service" data-item="<?php echo $i_1++ ?>">
				<div class="icon-service"><i class="<?php echo esc_attr( $item['icon_fontawesome'] ) ?>"></i></div>
				<div class="service-info">
					<?php if( !empty($item['title'])):?>
						<h3 class="title-service">
							<a href="<?php echo esc_url($link['url']) ?>" <?php if( $link['target'] ):?> target="<?php echo esc_html($link['target']) ?>" <?php endif;?>>
								<?php echo esc_html($item['title']);?>
							</a>
						</h3>
					<?php endif;?>
					<?php if( !empty($item['description'])):?>
						<div class="desc-service"><?php echo esc_html($item['description']);?></div>
					<?php endif;?>
				</div>
			</div>
		<?php endforeach;?>	
	</div>
	<div class="col-md-6 col-sm-6 col-md-offset-1 list-service-hover">
		<?php foreach( $items as $item ):?>
			<div class="service-hover" data-item="<?php echo $i_2++ ?>">
				<?php $image_thumb = corporatepro_resize_image( $item['img'], null, 339, 336, true, true, false ); ?>
                <img  src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="">
			</div>
		<?php endforeach;?>	
	</div>
</div>