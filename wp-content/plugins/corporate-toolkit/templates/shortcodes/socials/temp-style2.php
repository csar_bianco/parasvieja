<?php
if( isset($atts) ){
	extract ( $atts );
}
?>
<div class="top-footer <?php echo esc_attr( $css_class );?>">
	<?php if( $socials && is_array($socials) && count( $socials ) > 0 ):?>
		<ul class="social-footer">
			<?php foreach ($socials as $social):?>
	        	<li><?php corporatepro_social($social);?></li>
	        <?php endforeach;?>
		</ul>
	<?php endif;?>
</div>