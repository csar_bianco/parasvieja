<?php
if( $atts ){
	extract($atts);
}
$items = vc_param_group_parse_atts( $items );
?>
<div class="<?php echo esc_attr( $css_class );?>">
	<?php if( $title ):?>
		<div class="title-section title-style2">
			<span class="hr"></span>
			<h2 class="title"><?php echo esc_html( $title );?></h2>
		</div>
	<?php endif;?>	
	<?php if( count( $items ) > 0 ):?>
	<?php
	$i = 1;
	$total_item = count( $items );
	?>	
	<div class="cp-ourteam owl-carousel nav-style5" data-items="1" data-nav="true" data-dots="false">
		<div class="list-ourteam">
		<?php foreach( $items as $item ):?>
			<?php
			if( !empty($item['link']) ){
    			$link = vc_build_link( $item['link'] );
    		}else{
    			$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
    		}
			?>
			<div class="cp-team team-style2">

				<div class="avatar">
					<?php $image_thumb = corporatepro_resize_image( $item['avatar'], null, 120, 120, true, true, false ); ?>
                    <img  src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="">
				</div>
				<div class="team-info">
					<?php if( !empty($item['name'])):?>
					<h4 class="name"><?php echo esc_html($item['name']);?></h4>
					<?php endif;?>
					<?php if( !empty($item['position'])) :?>
					<span class="pos"><?php echo esc_html($item['position']);?></span>
					<?php endif;?>
					<?php if( !empty($item['textarea'])) :?>
					<div class="desc-team"><?php echo esc_html($item['textarea']);?></div>
					<?php endif;?>
					<?php if( !empty($link['url'])) : ?>
	                    <a class="bt-more-icon readmore" href="<?php echo esc_url($link['url']) ?>" <?php if($link['target']):?>target="<?php echo esc_html($link['target']) ?>"<?php endif;?> <?php if($link['rel']): ?>rel="<?php echo esc_attr($link['rel']) ; ?>" <?php endif;?>>
	                    	<i class="fa fa-long-arrow-right"></i>
	                    	<?php echo esc_html($link['title']); ?>
	                    </a>
	                <?php endif; ?>
				</div>
			</div>
			
		<?php
		if( ($i%2 == 0) && ( $i < $total_item ) ){
			echo '</div><div class="list-ourteam">';
		}
		$i++;
		?>
		<?php endforeach;?>	
		</div>
	</div>
	<?php endif;?>
</div>