<?php
if( $atts ){
	extract($atts);
}
$items = vc_param_group_parse_atts( $items );
?>
<div class="<?php echo esc_attr( $css_class );?>">
	<div class="slide-team-4 owl-carousel dot-style3" data-dots="true" data-items="1">
		<?php foreach( $items as $item ):?>
			<?php
				if( !empty($item['link']) ){
					$link = vc_build_link( $item['link'] );
				}else{
					$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
				}
			?>
			<div class="cp-team team-style4">
			 	<div class="avatar">
			 		<a href="<?php echo esc_url($link['url']) ?>" <?php if($link['target']): ?> target="<?php echo esc_html($link['target']) ?>" <?php endif; ?>  <?php if($link['rel']): ?> rel="<?php echo esc_attr($link['rel']) ; ?>" <?php endif; ?>>
			 			<?php $image_thumb = corporatepro_resize_image( $item['avatar'], null, 170, 173, true, true, false ); ?>
                    	<img  src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="">
			 		</a>
			 	</div>
			 	<div class="info-team">
			 		<?php if( !empty($item['name'])):?>
				 		<h4 class="name">
				 			<a href="<?php echo esc_url($link['url']) ?>" <?php if($link['target']): ?> target="<?php echo esc_html($link['target']) ?>" <?php endif; ?>  <?php if($link['rel']): ?> rel="<?php echo esc_attr($link['rel']) ; ?>" <?php endif; ?>>
				 				<?php echo esc_html($item['name']);?>
				 			</a>
				 		</h4>
				 	<?php endif;?>
				 	<?php if( !empty($item['textarea'])):?>
			 			<div class="desc-team"><?php echo esc_html($item['textarea']);?></div>
			 		<?php endif;?>
			 		<?php if( !empty($link['url'])): ?>
			 			<a href="<?php echo esc_url($link['url']) ?>" <?php if($link['target']): ?> target="<?php echo esc_html($link['target']) ?>" <?php endif; ?>  <?php if($link['rel']): ?> rel="<?php echo esc_attr($link['rel']) ; ?>" <?php endif; ?>>
			 				<img src="<?php echo get_template_directory_uri(). '/images/arrow-view.png' ?>" alt="">
			 			</a>
			 		<?php endif; ?>
			 	</div>
			</div>
		<?php endforeach;?>	
	</div>
</div>
