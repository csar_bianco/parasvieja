<?php
if( $atts ){
	extract($atts);
}
$items = vc_param_group_parse_atts( $items );
?>
<div class="<?php echo esc_attr( $css_class );?> cp-testimonial testimonial-style4 nav-style4 owl-carousel" data-dots="false" data-nav="true" data-items="1">
	<?php foreach( $items as $item ):?>
		<?php
			if( !empty($item['link']) ){
				$link = vc_build_link( $item['link'] );
			}else{
				$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
			}
		?>
		<div class="testimonial-item">
			<div class="avatar-client">
				<?php $image_thumb = corporatepro_resize_image( $item['avatar'], null, 220, 220, true, true, false ); ?>
                <img  src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="">
			</div>
			<?php if( !empty($item['name'])):?>
				<h4 class="client-name">
					<a href="<?php echo esc_url($link['url']) ?>" <?php if($link['target']): ?> target="<?php echo esc_html($link['target']) ?>" <?php endif; ?>  <?php if($link['rel']): ?> rel="<?php echo esc_attr($link['rel']) ; ?>" <?php endif; ?>>
		 				<?php echo esc_html($item['name']);?>
		 			</a>
				</h4>
			<?php endif;?>
			<?php if( !empty($item['textarea'])):?>
	 			<div class="client-quote"><?php echo esc_html($item['textarea']);?></div>
	 		<?php endif;?>
		</div>
	<?php endforeach;?>	
</div>