<?php
if( $atts ){
	extract($atts);
}
$items = vc_param_group_parse_atts( $items );
?>
<div class="cp-member <?php echo esc_attr( $css_class );?>">
	<?php foreach( $items as $item ):?>
		<?php
			if( !empty($item['link']) ){
				$link = vc_build_link( $item['link'] );
			}else{
				$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
			}
		?>
		<div class="avatar">
			<?php $image_thumb = corporatepro_resize_image( $item['avatar'], null, 130, 130, true, true, false ); ?>
            <img  src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="">
		</div>
		<div class="info-member">
			<?php if( !empty($item['name'])):?>
				<h3 class="title-member">
					<a href="<?php echo esc_url($link['url']) ?>" <?php if($link['target']): ?> target="<?php echo esc_html($link['target']) ?>" <?php endif; ?>  <?php if($link['rel']): ?> rel="<?php echo esc_attr($link['rel']) ; ?>" <?php endif; ?>>
						<?php echo esc_html($item['name']);?>
					</a>
				</h3>
			<?php endif;?>
			<?php if( !empty($item['textarea'])):?>
				<div class="desc-member"><?php echo esc_html($item['textarea']);?></div>
			<?php endif; ?>
			<a href="<?php echo esc_url($link['url']) ?>" <?php if($link['target']): ?> target="<?php echo esc_html($link['target']) ?>" <?php endif; ?>  <?php if($link['rel']): ?> rel="<?php echo esc_attr($link['rel']) ; ?>" <?php endif; ?> class="outline-button">
				<?php echo esc_html($link['title']) ?>
			</a>
		</div>
	<?php endforeach;?>	
</div>