<?php
if( $atts ){
	extract($atts);
}
$items = vc_param_group_parse_atts( $items );
?>
<?php foreach( $items as $item ):?>
	<?php
		if( !empty($item['link']) ){
			$link = vc_build_link( $item['link'] );
		}else{
			$link = array('url'=>'#', 'title'=>'', 'target'=>'_self', 'rel'=>'') ;
		}
		$social = explode(',', $item['use_socials']);
	?>
	<div class="cp-team team-style6 <?php echo esc_attr( $css_class );?>">
		<div class="avatar">
			<?php $image_thumb = corporatepro_resize_image( $item['avatar'], null, 140, 140, true, true, false ); ?>
	        <img  src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="">
		</div>
		<div class="team-right">
			<div class="animated-hover">
				<div class="team-info">
					<?php if( !empty($item['name'])):?>
						<a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>">
							<h4 class="name"><?php echo esc_html($item['name']);?></h4>
						</a>
					<?php endif; ?>

					<?php if( !empty($item['position'])):?>
						<span class="pos"><?php echo esc_html($item['position']);?></span>
					<?php endif; ?>
					
					<span class="hr"></span>
					<ul class="social-team">
						<?php foreach ($social as $key => $value) : ?>	
							<li><?php corporatepro_social($value);?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php endforeach;?>	