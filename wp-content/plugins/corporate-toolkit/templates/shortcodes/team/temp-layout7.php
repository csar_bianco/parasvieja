<?php
if( $atts ){
	extract($atts);
}
$items = vc_param_group_parse_atts( $items );
?>

<div class="cp-testimonial testimonial-style6 slide-dotcustom dot-style6 light <?php echo esc_attr( $css_class );?>">
	<div class="owl-dots cp-dots-custom">
	</div>
	<div class="testimonial-slide owl-carousel dots-custom" <?php echo $owl_carousel; ?>>
	<?php foreach( $items as $item ):?>
		<?php
			if( !empty($item['link']) ){
				$link = vc_build_link( $item['link'] );
			}else{
				$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
			}
		?>
		<div class="testimonial-item">
			<div class="avatar-client">
				<?php $image_thumb = corporatepro_resize_image( $item['avatar'], null, 100, 100, true, true, false ); ?>
	        	<img  src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="">
			</div>
			<div class="client-info">
				<?php if( !empty($item['name'])):?>
					<a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>">
						<h4 class="client-name"><?php echo esc_html($item['name']);?></h4>
					</a>
				<?php endif; ?>

				<?php if( !empty($item['position'])):?>
					<div class="client-pos"><?php echo esc_html($item['position']);?></div>
				<?php endif; ?>

				<?php if( !empty($item['textarea'])):?>
					<div class="client-quote"><?php echo esc_html($item['textarea']);?></div>
				<?php endif; ?>
			</div>
		</div>
	<?php endforeach;?>	
	</div>
</div>