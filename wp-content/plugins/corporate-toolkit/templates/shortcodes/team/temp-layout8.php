<?php
if( $atts ){
	extract($atts);
}
$items = vc_param_group_parse_atts( $items );
?>
<?php foreach( $items as $item ):?>
	<?php
		if( !empty($item['link']) ){
			$link = vc_build_link( $item['link'] );
		}else{
			$link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
		}
		$social = explode(',', $item['use_socials']);
	?>
	<div class="cp-team team-style5">
		<div class="avatar">
			<?php $image_thumb = corporatepro_resize_image( $item['avatar'], null, 170, 170, true, true, false ); ?>
	        <img  src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="">
			<a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>" class="link-more">
				<?php echo esc_html__('more','corporatepro') ?>
			</a>
		</div>
		<div class="info-team">
			<?php if( !empty($item['name'])):?>
				<a href="<?php echo esc_url($link['url']) ?>" target="<?php echo esc_html($link['target']) ?>">
					<h4 class="name"><?php echo esc_html($item['name']);?></h4>
				</a>
			<?php endif; ?>

			<ul class="social-team">
				<?php foreach ($social as $key => $value) : ?>	
					<li><?php corporatepro_social($value);?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
<?php endforeach;?>	