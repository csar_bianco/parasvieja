	<div class="view-content">
<h3>Deportes</h3>

<table border="0" CELLSPACING=0 CELLPADDING=0>
<tr>

<?php
$azar = rand(1,3); 
?>

<?php switch($azar): 
case 1: ?>
	<td>
		<div class="views-row views-row-3 views-row-odd views-row-last">
				<div class="views-row-inner">
					<div class="views-field-field-image-fid">
						<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="imagen deporte" src="http://www.paralimpico.cl/wp-content/uploads/2013/04/tenisilla.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Tenis en silla de rueda</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">La principal diferencia con el tenis convencional, es que la pelota puede dar dos botes, 
						siempre y cuando el primero haya sido dentro de los l&iacute;mites establecidos por la cancha.</span>
						</p>
						<p></p>
						<a href="/?page_id=89"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;">Perfil Deporte</a>
					</div>
				</div>
			</div>
		</div>

	</td>
	<td>
		<div class="views-row views-row-first">
			<div class="views-row-inner">
				<div class="views-field-field-image-fid">
					<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="Imagen deporte" src="/images/depo_basket.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>B&aacute;squetbol en silla de ruedas</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">El B&aacute;squetbol en silla de ruedas fue uno de los deportes fundadores del programa Paral&iacute;mpico en Roma en 1960.
						Hoy en d&iacute;a, sigue siendo uno de los deportes m&aacute;s populares en los Juegos Paral&iacute;mpicos.</span></p>
						<p></p>
						<a href="/?page_id=68"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deporte</a>
					</div>
				</div>
			</div>
		</div>

	</td>
	<td>
		<div class="views-row views-row-2 views-row-even">
			<div class="views-row-inner">
				<div class="views-field-field-image-fid">
					<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="Kurt Fearnley" src="/images/depo_pesas.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Levantamiento de pesas</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">
						<span style="TEXT-ALIGN: justify">El levantamiento de pesas o “powerlifting”, es por excelencia una prueba de fuerza máxima de brazos. 
						Los deportistas tienen tres intentos, y el ganador es el que logra levantar correctamente la mayor cantidad de peso.</span>
						</span>
						</p>
						<p></p>
						<a href="/?page_id=72"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deporte</a>
					</div>
				</div>
			</div>
		</div>
	</td>
<?php break; ?>

<?php case 2: ?>
	<td>
		<div class="views-row views-row-first">
			<div class="views-row-inner">
				<div class="views-field-field-image-fid">
					<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="Imagen deporte" src="/images/depo_bochas.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Bochas</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">Bochas o “Boccia”, hizo su debut como deporte Paral&iacute;mpico en los Juegos de Barcelona el año 1992, 
						como deporte exclusivo para deportistas con par&aacute;lisis cerebral. </span></p>
						<p></p>
						<a href="/?page_id=75"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deporte</a>
					</div>
				</div>
			</div>
		</div>
	</td>
	<td>
		<div class="views-row views-row-3 views-row-odd views-row-last">
				<div class="views-row-inner">
					<div class="views-field-field-image-fid">
						<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="imagen deporte" src="http://www.paralimpico.cl/wp-content/uploads/2013/04/tenisilla.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Tenis en silla de rueda</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">La principal diferencia con el tenis convencional, es que la pelota puede dar dos botes, siempre y cuando el primero haya 
						sido dentro de los l&iacute;mites establecidos por la cancha.</span>
						</p>
						<p></p>
						<a href="/?page_id=89"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;">Perfil Deporte</a>
					</div>
				</div>
			</div>
		</div>
	</td>
	<td>
		<div class="views-row views-row-first">
			<div class="views-row-inner">
				<div class="views-field-field-image-fid">
					<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="Imagen deporte" src="/images/depo_atletismo.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Atletismo</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">El atletismo ha formado parte del programa Paral&iacute;mpico desde los primeros Juegos en Roma el a&ntilde;o 1960.</span></p>
						<p></p>
						<a href="/?page_id=38"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deporte</a>
					</div>
				</div>
			</div>
		</div>
	</td>
<?php break; ?>

<?php case 3: ?>
	<td>
		<div class="views-row views-row-first">
			<div class="views-row-inner">
				<div class="views-field-field-image-fid">
					<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="Imagen deporte" src="/images/depo_natacion.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Nataci&oacute;n</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">Desde los primeros Juegos Paral&iacute;mpicos en Roma el año 1960, la nataci&oacute;n ha sido siempre uno de los deportes 
						m&aacute;s populares. </span></p>
						<p></p>
						<a href="/?page_id=86"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deporte</a>
					</div>
				</div>
			</div>
		</div>
	</td>
	<td>
		<div class="views-row views-row-first">
			<div class="views-row-inner">
				<div class="views-field-field-image-fid">
					<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="Imagen deporte" src="/images/depo_atletismo.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Atletismo</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">El atletismo ha formado parte del programa Paral&iacute;mpico desde los primeros Juegos en Roma el a&ntilde;o 1960.</span></p>
						<p></p>
						<a href="/?page_id=38"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deporte</a>
					</div>
				</div>
			</div>
		</div>
	</td>
	<td>
		<div class="views-row views-row-3 views-row-odd views-row-last">
				<div class="views-row-inner">
					<div class="views-field-field-image-fid">
						<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="imagen deporte" src="/images/depo_tenismesa.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Tenis de Mesa</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">El tenis de mesa fue unos de los deportes pioneros y esta en el programa Paral&iacute;mpico desde los primeros 
						Juegos en Roma 1960. Los deportistas compiten en clases de pie y clases en silla de ruedas.</span>
						</p>
						<p></p>
						<a href="/?page_id=91"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;">Perfil Deporte</a>
					</div>
				</div>
			</div>
		</div>
	</td>
<?php break; ?>
<?php endswitch; ?>

</tr></table>
	</div>