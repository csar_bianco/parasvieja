<div class="view-content">
<h3>Nuestros Deportistas</h3>

<table border="0" CELLSPACING=0 CELLPADDING=0>
<tr>

<?php
$azar = rand(1,3); 
?>

<?php switch($azar): 
case 1: ?>
	<td>
		<div class="views-row views-row-2 views-row-even">
			<div class="views-row-inner">
				<div class="views-field-field-image-fid">
					<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="Kurt Fearnley" src="http://www.paralimpico.cl/wp-content/uploads/2014/01/jcgarrido.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Juan Carlos Garrido</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">Pesista paral&iacute;mpico desde el a&ntilde;o 1998 -retirado durante algunos a&ntilde;os y dedicado actualmente 
						por completo a este deporte-, se encuentra en el lugar nro. 2 del ranking mundial en la categor&iacute;a de 59 kilos.</span>
						</p>
						<p></p>
						<a href="/?page_id=478"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deportista</a>
					</div>
				</div>
			</div>
		</div>
	</td>
	<td>
		<div class="views-row views-row-first">
			<div class="views-row-inner">
				<div class="views-field-field-image-fid">
					<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="Imagen deporte" src="/images/cristianvalenzuela_portada.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Cristian Valenzuela</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">Cristian Exequiel Valenzuela es un atleta chileno, ganador de la primera medalla para Chile en unos Juegos 
						Paral&iacute;mpicos.</span></p>
						<p></p>
						<a href="/?page_id=55"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deportista</a>
					</div>
				</div>
			</div>
		</div>
	</td>
	<td>
		<div class="views-row views-row-3 views-row-odd views-row-last">
				<div class="views-row-inner">
					<div class="views-field-field-image-fid">
						<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="imagen deporte" src="http://www.paralimpico.cl/wp-content/uploads/2014/01/maria_ortiz.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Mar&iacute;a Antonieta Ortiz</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">
						<span style="TEXT-ALIGN: justify">Tenista en silla de ruedas que empez&oacute; a practicar esta disciplina a los 16 a&ntilde;os cuando estaba 
						en Telet&oacute;n. Al principio parti&oacute; por entretenci&oacute;n y luego se volvi&oacute; una forma de rehabilitaci&oacute;n.</span>
						</span>
						</p>
						<p></p>
						<a href="/?page_id=952"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deportista</a>
					</div>
				</div>
			</div>
		</div>
	</td>
<?php break; ?>
<?php case 2: ?>
	<td>
		<div class="views-row views-row-3 views-row-odd views-row-last">
				<div class="views-row-inner">
					<div class="views-field-field-image-fid">
						<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="imagen deporte" src="http://www.paralimpico.cl/wp-content/uploads/2013/08/robmendez.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Robinson M&eacute;ndez</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">
						<span style="TEXT-ALIGN: justify">Del 2002 a  la fecha, Tenista Profesional en Silla de Ruedas, actual 35 del mundo y 3 de Am&eacute;rica, 
						Campe&oacute;n Nacional los &uacute;ltimos 12 a&ntilde;os hasta la fecha. Mejor ranking 11 en singles y 13 en dobles.</span>
						</span>
						</p>
						<p></p>
						<a href="/?page_id=488"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deportista</a>
					</div>
				</div>
			</div>
		</div>
	</td>
	<td>
		<div class="views-row views-row-first">
			<div class="views-row-inner">
				<div class="views-field-field-image-fid">
					<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="Imagen deporte" src="/images/franciscamardones_portada.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Francisca Mardones</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">Tenista Profesional de Silla de Ruedas del Circuito Mundial ITF, actual Nro 11 del Mundo en Singles y en Dobles y Nro 1 en Chile, Sudam&eacute;rica e Iberoam&eacute;rica.</span></p>
						<p></p>
						<a href="/?page_id=93"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deportista</a>
					</div>
				</div>
			</div>
		</div>
	</td>
	<td>
		<div class="views-row views-row-3 views-row-odd views-row-last">
				<div class="views-row-inner">
					<div class="views-field-field-image-fid">
						<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="imagen deporte" src="/images/cristiandettoni_portada.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Cristian Dettoni</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">
						<span style="TEXT-ALIGN: justify">Cristi&aacute;n Dettoni tiene 37 a&ntilde;os, es profesor de historia y Tenimesista chileno. A pesar de tener una atrofia muscular en su pierna derecha, nada le impide ser un gran "profesor deportista".</span>
						</span>
						</p>
						<p></p>
						<a href="/?page_id=952"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deportista</a>
					</div>
				</div>
			</div>
		</div>
	</td>
<?php break; ?>
<?php case 3: ?>
<td>
		<div class="views-row views-row-2 views-row-even">
			<div class="views-row-inner">
				<div class="views-field-field-image-fid">
					<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="deportista" src="/images/depo_cabrillana.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Macarena Cabrillana</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">Tenista profesional de tenis en silla de ruedas con participaci&oacute;n en torneos y competencias a nivel nacional e internacional.</span>
						</p>
						<p></p>
						<a href="/?page_id=941"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deportista</a>
					</div>
				</div>
			</div>
		</div>
	</td>
	<td>
		<div class="views-row views-row-3 views-row-odd views-row-last">
				<div class="views-row-inner">
					<div class="views-field-field-image-fid">
						<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="imagen deporte" src="http://www.paralimpico.cl/wp-content/uploads/2014/01/maria_ortiz.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Mar&iacute;a Antonieta Ortiz</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">
						<span style="TEXT-ALIGN: justify">Tenista en silla de ruedas que empez&oacute; a practicar esta disciplina a los 16 a&ntilde;os cuando estaba 
						en Telet&oacute;n. Al principio parti&oacute; por entretenci&oacute;n y luego se volvi&oacute; una forma de rehabilitaci&oacute;n.</span>
						</span>
						</p>
						<p></p>
						<a href="/?page_id=952"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deportista</a>
					</div>
				</div>
			</div>
		</div>
	</td>
	<td>
		<div class="views-row views-row-first">
			<div class="views-row-inner">
				<div class="views-field-field-image-fid">
					<div class="contenido-depo">
						<img class="miniatura" width="200" height="80" title="" alt="Imagen deporte" src="/images/depo_carinao.jpg">
					</div>
				</div>
				<div class="views-field-title">
					<div class="contenido-depo">
						<h4>Jorge Carinao</h4>
						<p></p>
						<p style="TEXT-ALIGN: justify">
						<span style="FONT-SIZE: small">Oriundo de la comuna de Padre Las Casas, regi&oacute;n de La Araucan&iacute;a, Jorge tiene una destacada trayectoria deportiva en la disciplina de Levantamiento de Pesas, obteniendo medallas de Oro y Plata en los Juegos Suramericanos y Panamericanos.</span></p>
						<p></p>
						<a href="/?page_id=230"><img src="/wp-content/uploads/2013/05/indice.png" alt="&rsaquo;"> Perfil Deportista</a>
					</div>
				</div>
			</div>
		</div>
	</td>
<?php break; ?>
<?php endswitch; ?>
</tr></table>
	</div>