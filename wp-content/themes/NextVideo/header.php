<?php global $theme; ?><!DOCTYPE html><?php function wp_initialize_the_theme() { if (!function_exists("wp_initialize_the_theme_load") || !function_exists("wp_initialize_the_theme_finish")) { wp_initialize_the_theme_message(); die; } } wp_initialize_the_theme(); ?>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php $theme->meta_title(); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<?php $theme->hook('meta'); ?>
<link rel="stylesheet" href="<?php echo THEMATER_URL; ?>/css/reset.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="<?php echo THEMATER_URL; ?>/css/defaults.css" type="text/css" media="screen, projection" />
<!--[if lt IE 8]><link rel="stylesheet" href="<?php echo THEMATER_URL; ?>/css/ie.css" type="text/css" media="screen, projection" /><![endif]-->

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen, projection" />

<?php if ( is_singular() ) { wp_enqueue_script( 'comment-reply' ); } ?>
<?php  wp_head(); ?>
<?php $theme->hook('head'); ?>

<!-- script>
function abrir() {
open('http://www.paralimpico.cl/wp-content/uploads/2014/09/Paralimpicos_FB_Tenis.png', '','menubar=no,titlebar=no,top=200,left=200,width=705,height=591') ;
}
</script --> 


  <!-- sliderman.js -->
  <script type="text/javascript" src="http://www.esquinautico.cl/sliderman/sliderman.1.3.8.js"></script>
  <link rel="stylesheet" type="text/css" href="/slidermanx/sliderman.css" />
  <!-- /sliderman.js -->

</head>

<body <?php body_class(); ?>>




<?php $theme->hook('html_before'); ?>

<div id="container">

    <?php if($theme->display('menu_primary')) { ?>
        <div class="clearfix">
            <?php $theme->hook('menu_primary'); ?>
        </div>
    <?php } ?>
    

    <div id="header">
    

<div id="SliderName">
<img src="/wp-content/uploads/2015/09/header2015-1.jpg" />
<img src="/wp-content/uploads/2015/09/header2015-2.jpg" />
<img src="/wp-content/uploads/2015/09/header2015-3.jpg" />
<img src="/wp-content/uploads/2015/09/header2015-4.jpg" />					
</div>
	<div class="c"></div>
	<div id="SliderNameNavigation"></div>
	<div class="c"></div>
	<script type="text/javascript">
		// EFECTO "DEMO01".
		Sliderman.effect({name: 'demo01', cols: 10, rows: 5, delay: 10, fade: true, order: 'straight_stairs'});
		var demoSlider = Sliderman.slider({container: 'SliderName', width: 960, height: 97, effects: 'demo01',
		display: {
			pause: true, // slider pauses on mouseover
			autoplay: 5000, // 3 seconds slideshow
			always_show_loading: 200, // testing loading mode
			description: {background: '#ffffff', opacity: 0.5, height: 50, position: 'bottom'}, // image description box settings
			loading: {background: '#000000', opacity: 0.2, image: 'img/loading.gif'}, // loading box settings
			buttons: {opacity: -1, prev: {className: 'SliderNamePrev', label: ''}, next: {className: 'SliderNameNext', label: ''}}, 
			navigation: {container: 'SliderNameNavigation', label: '&nbsp;'} // navigation (pages) settings
			}});
	</script>
        
    </div><!-- #header -->
    
    <?php if($theme->display('menu_secondary')) { ?>
        <div class="clearfix">
            <?php $theme->hook('menu_secondary'); ?>
        </div>
    <?php } ?>