<?php
/**
 * Template Name: Paginas
*/
global $theme; get_header(); ?>

    <div id="main">
        <?php $theme->hook('main_before'); ?>
        <div id="content">
            <?php $theme->hook('content_before'); ?>
            <?php 
                if (have_posts()) : while (have_posts()) : the_post();
                    /**
                     * Find the post formatting for the pages in the post-page.php file
                     */
                    get_template_part('post', 'page');
                  endwhile;
                else :
                    get_template_part('post', 'noresults');
                endif; 
            ?>
            
            <div class="sitemap">
                <div class="clearfix">
                    <div class="alignleft ssitemap-col">
                        <ul class="sitemap-list">
                            <?php wp_list_pages('child_of=8&sort_column=post_title&title_li='); ?>
                        </ul>
                    </div>
                </div>
            </div>
            
            <?php $theme->hook('content_after'); ?>
        </div><!-- #content -->
        <?php get_sidebars(); ?>
        <?php $theme->hook('main_after'); ?>
    </div><!-- #main -->
<?php get_footer(); ?>