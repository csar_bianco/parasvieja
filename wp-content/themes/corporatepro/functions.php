<?php
if ( !isset( $content_width ) ) $content_width = 900;
if ( !class_exists( 'Coporatepro_Functions' ) ) {
	class Coporatepro_Functions
	{
		/**
		 * Instance of the class.
		 *
		 * @since   1.0.0
		 *
		 * @var   object
		 */
		protected static $instance = null;

		/**
		 * Initialize the plugin by setting localization and loading public scripts
		 * and styles.
		 *
		 * @since    1.0.0
		 */
		public function __construct()
		{
			add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
			add_action( 'widgets_init', array( $this, 'widgets_init' ) );
			add_action( 'init', array( $this, 'add_muti_widgets' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'widgets_enqueue_scripts' ) );
			add_filter( 'body_class', array( $this, 'body_class' ) );
			add_filter( 'get_default_comment_status', array( $this, 'open_default_comments_for_page' ), 10, 3 );

			$this->includes();
		}

		public static function get_instance()
		{

			// If the single instance hasn't been set yet, set it now.
			if ( null == self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;

		}

		function theme_setup()
		{
			load_theme_textdomain( 'corporatepro', get_template_directory() . '/languages' );

			add_theme_support( 'automatic-feed-links' );

			add_theme_support( 'title-tag' );

			add_theme_support( 'post-thumbnails' );

			register_nav_menus(
				array(
					'primary'            => esc_html__( 'Primary Menu', 'corporatepro' ),
					'top-bar-left-menu'  => esc_html__( 'Top Left Menu', 'corporatepro' ),
					'top-bar-right-menu' => esc_html__( 'Top Right Menu', 'corporatepro' ),
					'top-bar-shop-menu'  => esc_html__( 'Top Shop Menu', 'corporatepro' ),
					'slide-menu'         => esc_html__( 'Slide Menu', 'corporatepro' ),
				)
			);

			add_theme_support( 'html5', array(
					'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
				)
			);

			add_theme_support( 'post-formats', array(
					'image', 'video', 'gallery',
				)
			);
			if ( class_exists( 'WooCommerce' ) ) {
				/*Support woocommerce*/
				add_theme_support( 'woocommerce' );
				add_theme_support( 'wc-product-gallery-lightbox' );
				add_theme_support( 'wc-product-gallery-slider' );
				add_theme_support( 'wc-product-gallery-zoom' );
			}
		}

		function add_muti_widgets()
		{
			$opt_multi_slidebars = corporatepro_get_option( 'opt_multi_slidebars', '' );

			if ( is_array( $opt_multi_slidebars ) && count( $opt_multi_slidebars ) > 0 ) {
				foreach ( $opt_multi_slidebars as $value ) {
					if ( $value && $value != '' ) {
						register_sidebar( array(
								'name'          => $value,
								'id'            => 'kt-custom-sidebar-' . sanitize_key( $value ),
								'before_widget' => '<div id="%1$s" class="widget block-sidebar %2$s">',
								'after_widget'  => '</div>',
								'before_title'  => '<div class="title-widget widgettitle"><strong>',
								'after_title'   => '</strong></div>',
							)
						);
					}
				}
			}
		}

		/**
		 * Register widget area.
		 *
		 * @since corporatepro 1.0
		 *
		 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
		 */
		function widgets_init()
		{

			register_sidebar( array(
					'name'          => esc_html__( 'Widget Area', 'corporatepro' ),
					'id'            => 'widget-area',
					'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'corporatepro' ),
					'before_widget' => '<div id="%1$s" class="widget block-sidebar %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<div class="title-widget widgettitle"><strong>',
					'after_title'   => '</strong></div>',
				)
			);
			register_sidebar( array(
					'name'          => esc_html__( 'Project Widget Area', 'corporatepro' ),
					'id'            => 'project-widget-area',
					'description'   => esc_html__( 'Add widgets here to appear in your project sidebar.', 'corporatepro' ),
					'before_widget' => '<div id="%1$s" class="widget block-sidebar %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<div class="title-widget widgettitle"><strong>',
					'after_title'   => '</strong></div>',
				)
			);
			register_sidebar( array(
					'name'          => esc_html__( 'Shop Widget Area', 'corporatepro' ),
					'id'            => 'shop-widget-area',
					'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'corporatepro' ),
					'before_widget' => '<div id="%1$s" class="widget block-sidebar %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<div class="title-widget widgettitle"><strong>',
					'after_title'   => '</strong></div>',
				)
			);

			register_sidebar( array(
					'name'          => esc_html__( 'Shop Single Widget Area', 'corporatepro' ),
					'id'            => 'shop-single-widget-area',
					'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'corporatepro' ),
					'before_widget' => '<div id="%1$s" class="widget block-sidebar %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<div class="title-widget widgettitle"><strong>',
					'after_title'   => '</strong></div>',
				)
			);
		}

		/**
		 * Enqueue scripts and styles.
		 *
		 * @since theme-name 1.0
		 */
		function scripts()
		{
			global $wp_query;

			wp_enqueue_style( 'bootstrap', trailingslashit( get_template_directory_uri() ) . 'css/bootstrap.min.css', array(), false );
			wp_enqueue_style( 'jquery_ui', trailingslashit( get_template_directory_uri() ) . 'css/jquery-ui.css', array(), false );
			wp_enqueue_style( 'font_awesome', trailingslashit( get_template_directory_uri() ) . 'css/font-awesome.min.css', array(), false );
			wp_enqueue_style( 'owl_carousel', trailingslashit( get_template_directory_uri() ) . 'css/owl.carousel.css', array(), false );
			wp_enqueue_style( 'select2', trailingslashit( get_template_directory_uri() ) . 'css/select2.min.css', array(), false );
			wp_enqueue_style( 'animate', trailingslashit( get_template_directory_uri() ) . 'css/animate.css', array(), false );
			wp_enqueue_style( 'chosen_css', trailingslashit( get_template_directory_uri() ) . 'css/chosen.css', array(), false );
			wp_enqueue_style( 'corporatepro_style', trailingslashit( get_template_directory_uri() ) . 'css/style.css', array(), false );
			wp_enqueue_style( 'corporatepro_woocommerce', trailingslashit( get_template_directory_uri() ) . 'css/woocommerce.css', array(), false );
			/*Load our main stylesheet.*/
			wp_enqueue_style( 'corporatepro_main_style', get_stylesheet_uri() );

			/*Load js */
			wp_enqueue_script( 'jquery-ui-accordion' );
			wp_enqueue_script( 'jquery-ui-slider' );
			wp_enqueue_script( 'jquery-ui-tabs' );
			wp_enqueue_script( 'jquery-ui-core' );
			wp_enqueue_script( 'jquery-ui-autocomplete' );
			wp_enqueue_script( 'imagesloaded' );
			wp_enqueue_script( 'owl_carousel', trailingslashit( get_template_directory_uri() ) . '/js/owl.carousel.min.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'debouncedresize', trailingslashit( get_template_directory_uri() ) . '/js/jquery.debouncedresize.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'isotope', trailingslashit( get_template_directory_uri() ) . '/js/isotope.pkgd.min.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'masonry', trailingslashit( get_template_directory_uri() ) . '/js/masonry.pkgd.min.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'packery_mode', trailingslashit( get_template_directory_uri() ) . '/js/packery-mode.pkgd.min.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'parallax', trailingslashit( get_template_directory_uri() ) . '/js/jquery.parallax-1.1.3.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'html5lightbox', trailingslashit( get_template_directory_uri() ) . '/js/html5lightbox.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'select2', trailingslashit( get_template_directory_uri() ) . '/js/select2.min.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'countdown', trailingslashit( get_template_directory_uri() ) . '/js/jquery.countdown.min.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'countTo', trailingslashit( get_template_directory_uri() ) . '/js/jquery.countTo.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'asPieProgress', trailingslashit( get_template_directory_uri() ) . '/js/jquery-asPieProgress.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'bxslider', trailingslashit( get_template_directory_uri() ) . '/js/jquery.bxslider.min.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'kinetic', trailingslashit( get_template_directory_uri() ) . '/js/kinetic.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'final_countdown', trailingslashit( get_template_directory_uri() ) . '/js/jquery.final-countdown.min.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'placeholder', trailingslashit( get_template_directory_uri() ) . '/js/jquery.placeholder.min.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'chosen_js', trailingslashit( get_template_directory_uri() ) . '/js/chosen.jquery.min.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_script( 'corporatepro-scripts', trailingslashit( get_template_directory_uri() ) . '/js/custom.js', array( 'jquery' ), '1.0', true );


			// What page are we on and what is the pages limit?
			$max   = $wp_query->max_num_pages;
			$paged = ( get_query_var( 'paged' ) > 1 ) ? get_query_var( 'paged' ) : 1;

			// Add some parameters for the JavaScript
			wp_localize_script(
				'corporatepro-scripts',
				'corporatepro_loadmore_posts',
				array(
					'startPage' => $paged,
					'maxPages'  => $max,
					'nextLink'  => next_posts( $max, false ),
				)
			);

			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}
			wp_localize_script( 'corporatepro-scripts', 'corporatepro_ajax_fontend', array(
					'ajaxurl'  => admin_url( 'admin-ajax.php' ),
					'security' => wp_create_nonce( 'corporatepro_ajax_fontend' ),
				)
			);
			$woo_single_product_thumbnail_lg_items = corporatepro_get_option( 'woo_single_product_thumbnail_lg_items', 4 );
			$woo_single_product_thumbnail_md_items = corporatepro_get_option( 'woo_single_product_thumbnail_md_items', 4 );
			$woo_single_product_thumbnail_sm_items = corporatepro_get_option( 'woo_single_product_thumbnail_sm_items', 3 );
			$woo_single_product_thumbnail_xs_items = corporatepro_get_option( 'woo_single_product_thumbnail_xs_items', 2 );
			$woo_single_product_thumbnail_ts_items = corporatepro_get_option( 'woo_single_product_thumbnail_ts_items', 1 );

			$data_reponsive              = array(
				'0'    => array(
					'items' => $woo_single_product_thumbnail_ts_items,
				),
				'480'  => array(
					'items' => $woo_single_product_thumbnail_xs_items,
				),
				'768'  => array(
					'items' => $woo_single_product_thumbnail_sm_items,
				),
				'992'  => array(
					'items' => $woo_single_product_thumbnail_md_items,
				),
				'1200' => array(
					'items' => $woo_single_product_thumbnail_lg_items,
				),
			);
			$map_api_key                 = corporatepro_get_option( 'google_key', '' );
			$opt_enable_main_menu_sticky = corporatepro_get_option( 'opt_enable_main_menu_sticky', 1 );
			wp_localize_script( 'corporatepro-scripts', 'corporatepro_global', array(
					'opt_enable_main_menu_sticky' => $opt_enable_main_menu_sticky,
					'query'                       => $wp_query->query,
					'gmap_api_key'                => trim( $map_api_key ),
					'data_reponsive'              => $data_reponsive,
				)
			);

		}

		function widgets_enqueue_scripts()
		{
			wp_enqueue_script(
				'admin-script', get_template_directory_uri() . '/js/admin-widgets.js',
				array( 'jquery', 'underscore', 'backbone' )
			);
			wp_enqueue_style( 'chosen_admin_css', trailingslashit( get_template_directory_uri() ) . 'css/chosen.css', array(), false );
			wp_enqueue_script( 'chosen_admin_js', trailingslashit( get_template_directory_uri() ) . '/js/chosen.jquery.min.js', array( 'jquery' ), '1.0', true );
			wp_enqueue_style( 'admin-style', trailingslashit( get_template_directory_uri() ) . 'css/admin-style.css', array(), false );
		}

		public function body_class( $classes )
		{
			$classes[] = 'body_test_class';

			return $classes;
		}

		/**
		 * Filter whether comments are open for a given post type.
		 *
		 * @param string $status Default status for the given post type,
		 *                             either 'open' or 'closed'.
		 * @param string $post_type Post type. Default is `post`.
		 * @param string $comment_type Type of comment. Default is `comment`.
		 * @return string (Maybe) filtered default status for the given post type.
		 */
		public function open_default_comments_for_page( $status, $post_type, $comment_type )
		{
			if ( 'page' == $post_type ) {
				return 'open';
			}

			return $status;
		}

		public function includes()
		{
			require_once( trailingslashit( get_template_directory() ) . 'includes/class/class-tgm-plugin-activation.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/class/breadcrumbs.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/class/corporatepro-breadcrumbs.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/nav/nav.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/theme-functions.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/custom-css.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/settings/theme-options.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/settings/meta-boxs.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/widgets/widgets_category.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/widgets/widgets_latest_post.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/widgets/widgets_faqs.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/widgets/widgets_author.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/widgets/widgets_tab_post.php' );

			require_once( trailingslashit( get_template_directory() ) . 'includes/settings/templates.php' );
			require_once( trailingslashit( get_template_directory() ) . 'includes/settings/plugins_load.php' );
			if ( class_exists( 'WooCommerce' ) ) {
				require_once get_template_directory() . '/includes/woo-functions.php';
				require_once( trailingslashit( get_template_directory() ) . 'includes/widgets/Widgets_Top_Rated_Products.php' );
				require_once( trailingslashit( get_template_directory() ) . 'includes/widgets/widgets_product_categories.php' );
			}
		}
	}

	new Coporatepro_Functions();
}

/* Loadmore Masonry Project */

function corporatepro_loadmore_masonry_project()
{

	$response        = array(
		'html'    => '',
		'message' => '',
		'success' => 'no',
	);
	$except_post_ids = isset( $_POST[ 'except_post_ids' ] ) ? $_POST[ 'except_post_ids' ] : '';
	$ids_category    = isset( $_POST[ 'ids_category' ] ) ? $_POST[ 'ids_category' ] : '';
	$pro_layout      = isset( $_POST[ 'pro_layout' ] ) ? $_POST[ 'pro_layout' ] : '';

	$ids = '';

	if ( $ids_category == '*' ) {
		$ids = '';
	} else {
		$ids = $ids_category[ 0 ];
	}
	if ( $ids != '*' ) {
		$args = array(
			'post_type'    => 'project',
			'showposts'    => 3,
			'post_status'  => 'publish',
			'post__not_in' => $except_post_ids,
			'tax_query'    => array(
				array(
					'taxonomy' => 'category_project',
					'field'    => 'slug',
					'terms'    => $ids,
				),
			),
		);
	} else {
		$args = array(
			'post_type'    => 'project',
			'showposts'    => 3,
			'post__not_in' => $except_post_ids,
		);
	}

	$loop = new wp_query( $args );
	ob_start();

	while ( $loop->have_posts() ) : $loop->the_post();

		get_template_part( 'templates/projects/project-formats/project-content', $pro_layout );

	endwhile;
	wp_reset_postdata();

	$response[ 'html' ] = ob_get_clean();

	$response[ 'success' ] = 'ok';


	wp_send_json( $response );
	die();
}

add_action( 'wp_ajax_corporatepro_loadmore_masonry_project', 'corporatepro_loadmore_masonry_project' );
add_action( 'wp_ajax_nopriv_corporatepro_loadmore_masonry_project', 'corporatepro_loadmore_masonry_project' );

/* Loadmore Masonry Post */

function corporatepro_loadmore_masonry_blog()
{

	$response        = array(
		'html'    => '',
		'message' => '',
		'success' => 'no',
	);
	$except_post_ids = isset( $_POST[ 'except_post_ids' ] ) ? $_POST[ 'except_post_ids' ] : '';
	$dt_layout       = isset( $_POST[ 'dt_layout' ] ) ? $_POST[ 'dt_layout' ] : '';

	$args = array(
		'post_type'    => 'post',
		'showposts'    => 3,
		'post__not_in' => $except_post_ids,
		'post_status'  => 'publish',
	);

	$loop = new wp_query( $args );
	ob_start();

	while ( $loop->have_posts() ) : $loop->the_post();

		get_template_part( 'templates/blogs/blog-content', $dt_layout );

	endwhile;
	wp_reset_postdata();

	$response[ 'html' ] = ob_get_clean();

	$response[ 'success' ] = 'ok';


	wp_send_json( $response );
	die();
}

add_action( 'wp_ajax_corporatepro_loadmore_masonry_blog', 'corporatepro_loadmore_masonry_blog' );
add_action( 'wp_ajax_nopriv_corporatepro_loadmore_masonry_blog', 'corporatepro_loadmore_masonry_blog' );

function corporatepro_loadmore_project()
{

	$response        = array(
		'html'    => '',
		'message' => '',
		'success' => 'no',
	);
	$except_post_ids = isset( $_POST[ 'except_post_ids' ] ) ? $_POST[ 'except_post_ids' ] : '';
	$pro_layout      = isset( $_POST[ 'pro_layout' ] ) ? $_POST[ 'pro_layout' ] : '';

	$args = array(
		'post_type'    => 'project',
		'showposts'    => 3,
		'post__not_in' => $except_post_ids,
		'post_status'  => 'publish',
	);

	$loop = new wp_query( $args );
	ob_start();

	while ( $loop->have_posts() ) : $loop->the_post();

		get_template_part( 'templates/projects/project-formats/project-content', $pro_layout );

	endwhile;
	wp_reset_postdata();

	$response[ 'html' ] = ob_get_clean();

	$response[ 'success' ] = 'ok';


	wp_send_json( $response );
	die();
}

add_action( 'wp_ajax_corporatepro_loadmore_project', 'corporatepro_loadmore_project' );
add_action( 'wp_ajax_nopriv_corporatepro_loadmore_project', 'corporatepro_loadmore_project' );

function wpb_set_post_views( $postID )
{
	if ( get_post_type( $postID ) == 'post' || get_post_type( $postID ) == 'project' ) {
		$count_key = 'wpb_post_views_count';
		$count     = get_post_meta( $postID, $count_key, true );
		if ( $count == '' ) {
			$count = 0;
			delete_post_meta( $postID, $count_key );
			add_post_meta( $postID, $count_key, '0' );
		} else {
			$count++;
			update_post_meta( $postID, $count_key, $count );
		}
	}
}
