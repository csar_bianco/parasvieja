<?php

/**
 * Customize css for corporatepro theme
 **/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

if ( !function_exists( 'corporatepro_get_custom_css' ) ) {
	function corporatepro_get_custom_css()
	{
		global $corporatepro;
		$opt_general_accent_color = corporatepro_get_option( 'opt_general_accent_color', array( 'color' => '#ff4949', 'alpha' => 1 ) );

		$base_color      = isset( $opt_general_accent_color ) ? $opt_general_accent_color : array( 'color' => '#ff4949', 'alpha' => 1 );
		$root_base_color = $base_color[ 'color' ];
		if ( !isset( $base_color[ 'alpha' ] ) ) {
			$base_color[ 'alpha' ] = 1;
		}
		$base_color = corporatepro_color_hex2rgba( $base_color[ 'color' ], $base_color[ 'alpha' ] );
		$css        = '';
		$css        .= '
			.product a.add-to-cart:hover::before,
            body a,
            .meta-post li .fa,
            .tag-pots span .fa,
            .breadcrumb a:hover,
            .tag-pots a:hover,
            .cp-pagination .page-number:hover, .cp-pagination .page-number.current-page, .cp-pagination .page-numbers:hover, .cp-pagination .page-numbers.current,
            .post-title a:hover,
            .format-video .icon-video:hover,
            .meta-style2 li,
            .product .price,
            .product .title-product a:hover,
            .product-detail .thumbnails .owl-prev:hover,
            .product-detail .thumbnails .owl-next:hover,
            .tab-style-1 .ui-tabs-nav li.ui-tabs-active, .tab-style-1 .ui-tabs-nav li:hover,
            .pj-title a:hover,
            .pj-meta ul li .fa,
            .item-project .pj-icon:hover,
            .cp-acordion .ui-accordion-header.ui-accordion-header-active:after,
            .cp-acordion .ui-accordion-header .ui-icon,
            .widget ul li a:hover,
            .portfolio_fillter .item-fillter.fillter-active, .portfolio_fillter .item-fillter:hover,
            .portfolio_fillter .item-fillter.fillter-active, .portfolio_fillter .item-fillter:hover,
            .cp-main-menu .sub-menu li a:hover,
            .project-style7 .pj-readmore:hover,
            .pj-meta ul li a:hover,
            .nav-style5 .owl-nav > div:hover,
            a.outline-button.bt-recent,
            .pricing-default .price,
            div.iconbox-style1 .iconbox,
            outline-button bt-recent,
            .iconbox-style1:hover .title-iconbox,
            .cp-iconbox .title-iconbox a:hover,
            .iconbox-style3 .iconbox,
            .iconbox-style3:hover .title-iconbox,
            .iconbox-style4 .iconbox,
            .widget-new-project .pj-readmore:hover,
            .nav-style2 .owl-nav > div:hover,
            .cp-pricing .title-pricing > a:hover,
            .service-style-2 .icon-service,
            .cp-video-popup .icon-video-popup:hover, .icon-video:hover,
            .product-item-style-2 .yith-wcqv-button:hover, 
            .product-item-style-2 .compare:hover, 
            .product-item-style-2 .yith-wcwl-add-to-wishlist > div a:hover,
            .footer-style3 .hotline .fa,
            .widget-connect ul li .fa,
            .widget-newsletter-3 button[type="submit"]:hover,
            .widget-contactinfo-2 .contact-phone .info .fa,
            .footer-style-7 .widget_nav_menu li:hover,
            .widget-contactinfo-2 .contactinfo .fa,
            .footer-style10 .main-footer .widget_nav_menu ul li::before,
            .widget-newsletter-4 .form-newsletter button,
            .footer-style10 .copyright-text a,
            .cp-partner .info-partner .desc a:hover,
            .cp-latest-news .item-post .title-post a:hover,
            .latest-news-2 .post-standard .link-more a,
            .footer-style-2 .copyright a:hover,
            .widget-newsletter-2 button[type="submit"],
            .cp-team .name a:hover,
            .recent-color,
            .button-icon.outline-button .fa,
            .accordion-style3 .ui-accordion-header.ui-accordion-header-active,
            .feature-style2:hover h4,
            .even-style2 .even-item .readmore:hover,
            .even-style2 .even-item .even-title a:hover,
            .cp-service-image .titles-service a:hover,
            .service-image-2 .titles-service a,
            .service-image-2 .link-service:hover,
            .cp-service .title-service > a:hover,
            .meta-post li a:hover,
            .cp-contactinfo .contact-text .fa,
            .cp-nav-menusidebar .icon-menu,
            .cp-nav-menusidebar .navigation ul li a:hover,
            .cp-service-slide .icon-service,
            .bt-learnmore.bt-link:hover,
            .bt-learnmore.bt-link,
            .bt-learnmore.bt-link:hover .fa,
            .cp-contactinfo .contact-text .fa,
            .topbar .fa,
            .topbar a:hover,
            .slide-home6 .cp-slide .bt-learnmore,
            .bt-learnmore:hover .fa,
            .nav-style6 .owl-nav > div,
            .cp-project-slide .cp-nav-custom > div:hover,
            .cp-project-slide .pj-meta li:hover,
            .block-hotdeal .hotdel-info h2,
            .block-hotdeal .hotdel-info h3 .fa,
            .header-style7 .cp-main-menu .navigation > ul > li:hover > a, 
            .header-style7 .cp-main-menu .navigation > ul > li.show-submenu > a .header-style7 .cp-main-menu .navigation > ul > li > a:hover,
            .cp-icon-box .icon,
            .statistic-box .number,
            .cp-feature-project-slide .yith-wcwl-add-to-wishlist:hover,
            .cp-feature-project-slide .button.yith-wcqv-button:hover,
            .cp-block-pricing .box-text h4 > a:hover,
            .cp-feature-project-slide .compare-button:hover,
            .cp-feature-project-slide .product .compare,
            .feature-style1 .title-feature h4 a:hover,
            .cp-member .info-member .title-member > a:hover,
            .accordion-style8 .ui-accordion-header,
            .product-grid-3 .product .product-button a:hover,
            .product-grid-3 .product .product-button .add_to_wishlist:hover,
            .product-grid-3 .product .product-button .compare:hover,
            .product-grid-4 .product .add-to-cart:hover,
            .service-style-4 .icon-service,
            .wellcome-slide.nav-style2 .owl-nav > div:hover,
            .block-wellcome .outline-button:hover, 
            .block-wellcome .outline-button:focus,
            .team-style6 .social-team li a:hover,
            .team-style6 .name:hover,
            .product-slide.product-grid-2.nav-style2 .owl-nav > div:hover,
            .ads-box.ads-style2 .ads-box-info .outline-button:hover, .ads-box.ads-style2 .ads-box-info .outline-button:focus,
            .ads-box.light .ads-box-info .outline-button:hover, .ads-box.light .ads-box-info .outline-button:focus,
            .testimonial-style6 .client-name,
            .header-style10 .icon-text,
            .cp-service-shop .item-service .icon-service,
            .cp-service-shop .item-service:hover .title-service,
            .cp-brand-spot .list-brand li a:hover,
            .header-style10 .cp-main-menu .navigation > ul > li.show-submenu > a, 
            .header-style10 .cp-main-menu .navigation > ul > li > a:hover,
            .latest-news-7.nav-style2 .owl-nav > div:hover,
            .accordion-style3 .ui-accordion-header.ui-accordion-header-active,
            .button-icon.outline-button .fa,
            .testimonial-style4 .testimonial-item .client-name a:hover,
            .cp-service-slide .title-service:hover,
            .cp-contact-info.contact-info-3.style4 a:hover,
            .cp-left-nav ul li a:hover,
            .product-grid-3 .product .hover-thumb .button.yith-wcqv-button:hover,
            .header-style10 .menu-togole,
            .header .cp-main-menu .caret,
            .accordion-style3 .ui-accordion-header.ui-accordion-header-active,
            .button-icon.outline-button:focus .fa,
            .recent-color,
            .feature-style2:hover h4,
            .cp-list-even.even-style2 .even-title a,
            .footer-style3 .hotline .fa
            {
                color:' . $base_color . ';
            }
            .breadcrumb a::after,
            .outline-button:hover, 
            .outline-button:focus,
            .meta-style4,
            .block-date-post,
            .blog-style2 .post-title:after,
            .cp-button, input[type="submit"], button, .button,
            .blog-style3 .blog-item .main-content-post::before,
            .comment-reply-link:hover,
            .comment-edit-link:hover,
            .comment-reply-link:focus,
            .comment-edit-link:focus,
            .page-links a:hover,
            .product-grid .product-button,
            .product .onsale,
            .added_to_cart:hover,
            .added_to_cart:focus,
            .product .cp-countdown,
            .product .product-button,
            .nav-style4 .owl-nav > div:hover,
            .cp-new-project .pj-info-widget,
            .cp-new-project .owl-controls,
            .cp-acordion .ui-accordion-header.ui-accordion-header-active:after,
            .widget ul li.cat-item .number,
            .portfolio_fillter .item-fillter .number,
            .portfolio_fillter .item-fillter:after,
            .cp-main-menu .navigation > ul > li > a:after,
            .project-style5 .item-project .pj-info::before,
            .project-style6 .pj-meta,
            .project-style7 .item-project .pj-info,
            .project-style7 .item-project .pj-info,
            .project-style7 .pj-image .owl-controls .owl-dots .owl-dot:hover,
            .owl-controls .owl-dots .owl-dot.active,
            .summary  .yith-wcwl-add-to-wishlist>div a,
            .summary .compare,
            .footer .widget-connect li a:hover > .fa,
            .page-links>span,
            .title-style2 .hr,
            .bt-more-icon,
            .service-style-1:hover,
            .title-style1 .icon-title:after,
            .service-style-1 .title-service:after,
            .list-dots2 li:hover:before,
            .iconbox-style1:hover .iconbox,
            .iconbox-style2 .iconbox,
            .iconbox-style4:hover .iconbox,
            .pj-single-2 .pj-bt-download,
            .pricing-image .pricing-header:after,
            .bt-more-icon,
            .service-style-2::before,
            .project-style4 .item-project  .add-to-cart:hover,
            .contact-info-3 .icon-contact,
            .contact-info-2 .icon-contact:hover,
            .cp-social li a:hover,
            .title-style3 .hr,
            .project-style9 .pj-title .number,
            .project-style9 .item-project .pj-icon:hover,
            .latest-news-2 .post-standard .link-more::before,
            .latest-news-2 .post-standard .link-more .icon-before,
            .block-ab3 .custom-bt,
            .list-icon .icon,
            .accordion-style3 .ui-accordion-header.ui-accordion-header-active .ui-icon,
            .feature-style2 .icon-feature,
            .even-style2 .even-item .even-date, .latest-news-3 .item-post .post-date,
            .feature-project.nav-style4 .owl-nav > div:hover,
            .pj-icon-bottom > a:hover,
            .testimonial-style4.nav-style4 .owl-nav > div:hover,
            .latest-news-5.nav-style4 .owl-nav > div:hover,
            .portfolio_fillter-3 .item-fillter.fillter-active, 
            .portfolio_fillter-3 .item-fillter:hover,
            .project-style10 .pj-readmore:hover,
            .scroll-button > span,
            .pj-icon-bottom .icon-share .sub_iconshare a:hover,
            .pj-icon-bottom .icon-share:hover,
            .pj-icon-bottom .icon-play:hover,
            .cp-nav-menusidebar .icon-menu:hover, .cp-nav-menusidebar .icon-menu.active,
            .bt-learnmore.bt-link .fa,
            .pj-icon-bottom .icon-play:hover,
            .header-style6 .form-search .search-submit,
            .slide-home6 .cp-slide .bt-learnmore .fa,
            .slide-home6 .cp-slide .bt-learnmore:hover,
            .cp-project-slide .cp-nav-custom > div,
            .cp-whyus-block::after,
            .list-dots li::before,
            .bt-learnmore,
            .contact-info-2 .icon-contact:hover,
            .ss-statistics .row-statistics.odd::before, 
            .ss-statistics .row-statistics.even::after,
            .statistic-box.light .number,
            .feature-style1 .number-feature,
            .header-style9 .main-header,
            .cp-tab.tab-style-2 .ui-tabs-nav li a::before,
            .accordion-style8 .ui-accordion-header .ui-icon::before,
            .accordion-style8 .ui-accordion-header .ui-icon::after,
            .product-grid-3 .product .hover-thumb .price,
            .product-grid-3 .product .product-button a:hover,
            .dot-style6 .owl-dot.active .line,
            .cp-popular-works .item-project .pj-hover,
            .product-grid-4 .product-button .add_to_wishlist:hover, 
            .product-grid-4 .product-button .compare.button:hover, 
            .product-grid-4 .product-button .yith-wcwl-wishlistexistsbrowse:hover,
            .product-grid-4 .product .product-button > a:hover,
            .cp-button.button-dark:hover, .cp-button.button-dark:focus,
            .title-style9 .hr,
            .widget_funfact .item-funfact.download,
            .product-grid-2 .product-button a:hover,
            .product-grid-2 .product .hover-thumb .button:hover,
            .ads-box.dark .ads-box-info .outline-button:hover, .ads-box.dark .ads-box-info .outline-button:focus,
            .product-brand .nav-style3 .owl-nav > div:hover,
            .testimonial-style6 .avatar-client::before,
            .team-style5 .social-team li a:hover,
            .team-style5 .link-more,
            .cp-service-shop .item-service:hover .icon-service,
            .cp-service-shop .item-service .icon-service::after,
            .backtotop,
            .form-search .search-submit:hover,
            .ss-product-v10.style3 .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse:hover,
            .cp-service.service-style-1:hover,
            .simple-slide .owl-nav div,
            a.back_to_top
            {
                background-color:' . $base_color . ';
            }
            .outline-button:hover, 
            .outline-button:focus,
            .comment-reply-link:hover,
            .comment-edit-link:hover,
            .comment-reply-link:focus,
            .comment-edit-link:focus,
            .cp-pagination .page-number.next-page:hover, 
            .cp-pagination .page-number.prev-page:hover, 
            .cp-pagination .page-numbers.next:hover, 
            .cp-pagination .page-numbers.prev:hover,
            .product-grid .product:hover .product-inner,
            .added_to_cart:hover,
            .added_to_cart:focus,
            .product-list .product .product-thumb:after,
            .tab-style-1 .ui-tabs-nav li.ui-tabs-active, .tab-style-1 .ui-tabs-nav li:hover,
            .nav-style4 .owl-nav > div:hover,
            .owl-controls .owl-dots .owl-dot,
            .cp-acordion .ui-accordion-header.ui-accordion-header-active,
            .cp-acordion .ui-accordion-content,
            .cp-acordion .ui-accordion-header.ui-accordion-header-active,
            .project-style7 .pj-image .owl-controls .owl-dots .owl-dot:hover,
            .ourvision-text .outline-button:hover,
            .ss-ourvision,
            div .outline-button.bt-recent,
            .iconbox-style1:hover .iconbox,
            .iconbox-style2 .iconbox::after,
            .iconbox-style3:hover .iconbox,
            .project-style4 .item-project  .add-to-cart:hover,
            .product-item-style-2 .yith-wcqv-button:hover, 
            .product-item-style-2 .compare:hover, 
            .product-item-style-2 .yith-wcwl-add-to-wishlist > div a:hover,
            .contact-info-3 .icon-contact::after,
            .contact-info-2 .icon-contact:hover,
            .latest-news-2 .post-standard .link-more .icon-before::after,
            .feature-style2 .icon-feature::after,
            .even-style2 .even-item .even-date::after,
            .feature-project.nav-style4 .owl-nav > div:hover,
            .testimonial-style4.nav-style4 .owl-nav > div:hover,
            .latest-news-5.nav-style4 .owl-nav > div:hover,
            .bt-learnmore.bt-link .fa,
            .cp-project-slide .cp-nav-custom > div,
            .cp-whyus-block,
            .cp-feature-project-slide .yith-wcwl-add-to-wishlist:hover,
            .cp-feature-project-slide .button.yith-wcqv-button:hover,
            .cp-feature-project-slide .compare-button:hover,
            .feature-style1 .number-feature,
            .tab-style-2 .ui-tabs-nav li.ui-tabs-active, .tab-style-2 .ui-tabs-nav li:hover,
            .product-grid-4 .product-button .add_to_wishlist:hover, 
            .product-grid-4 .product-button .compare.button:hover, 
            .product-grid-4 .product-button .yith-wcwl-wishlistexistsbrowse:hover,
            .product-grid-4 .product .product-button > a:hover,
            .team-style6 .team-info,
            .team-style6 .team-info::after,
            .product-grid-2 .product .info-product::after,
            .product-grid-2 .product-button a:hover,
            .ads-box.dark .ads-box-info .outline-button:hover, .ads-box.dark .ads-box-info .outline-button:focus,
            .product-grid .product-brand:hover .slide-brand,
            .cp-service-shop .item-service .icon-service,
            .cp-contact-info .icon-contact::after
            {
                border-color:' . $base_color . ';
            }
            .project-style3 .item-project:hover .pj-title
            {
                background-color:' . $base_color . ' !important;
            }
        ';
		$custom_css = isset( $corporatepro[ 'opt_general_css_code' ] ) ? $corporatepro[ 'opt_general_css_code' ] : '';

		$css .= $custom_css;

		return $css;
	}
}

if ( !function_exists( 'corporatepro_footer_custom_css' ) ) {
	function corporatepro_footer_custom_css()
	{
		$opt_footer_style = corporatepro_get_option( 'opt_footer_style', '' );

		$shortcodes_custom_css = get_post_meta( $opt_footer_style, '_wpb_shortcodes_custom_css', true );

		return $shortcodes_custom_css;
	}
}

if ( !function_exists( 'corporatepro_custom_css' ) ) {
	function corporatepro_custom_css()
	{
		$css = corporatepro_get_custom_css();
		$css .= corporatepro_footer_custom_css();
		$css .= corporatepro_get_option( 'opt_general_css_code', '' );

		wp_add_inline_style( 'corporatepro_style', $css );
	}
}

add_action( 'wp_enqueue_scripts', 'corporatepro_custom_css', 999 );


if ( !function_exists( 'corporatepro_custom_js' ) ) {
	function corporatepro_custom_js()
	{
		$js = corporatepro_get_option( 'opt_general_js_code', '' );
		wp_add_inline_script( 'corporatepro-scripts', $js );
	}
}
add_action( 'wp_enqueue_scripts', 'corporatepro_custom_js' );

if ( !function_exists( 'corporatepro_color_hex2rgba' ) ) {
	function corporatepro_color_hex2rgba( $hex, $alpha = 1 )
	{
		$hex = str_replace( "#", "", $hex );

		if ( strlen( $hex ) == 3 ) {
			$r = hexdec( substr( $hex, 0, 1 ) . substr( $hex, 0, 1 ) );
			$g = hexdec( substr( $hex, 1, 1 ) . substr( $hex, 1, 1 ) );
			$b = hexdec( substr( $hex, 2, 1 ) . substr( $hex, 2, 1 ) );
		} else {
			$r = hexdec( substr( $hex, 0, 2 ) );
			$g = hexdec( substr( $hex, 2, 2 ) );
			$b = hexdec( substr( $hex, 4, 2 ) );
		}
		$rgb = array( $r, $g, $b );

		return 'rgba( ' . implode( ', ', $rgb ) . ', ' . $alpha . ' )'; // returns the rgb values separated by commas
	}
}

if ( !function_exists( 'corporatepro_color_rgb2hex' ) ) {
	function corporatepro_color_rgb2hex( $rgb )
	{
		$hex = '#';
		$hex .= str_pad( dechex( $rgb[ 0 ] ), 2, '0', STR_PAD_LEFT );
		$hex .= str_pad( dechex( $rgb[ 1 ] ), 2, '0', STR_PAD_LEFT );
		$hex .= str_pad( dechex( $rgb[ 2 ] ), 2, '0', STR_PAD_LEFT );

		return $hex; // returns the hex value including the number sign (#)
	}
}
