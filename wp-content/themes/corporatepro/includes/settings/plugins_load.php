<?php

if( !class_exists('Corporatepro_PluginLoad')){
    class Corporatepro_PluginLoad{
        public $plugins = array();
        public $config = array();

        public function __construct() {
            $this->plugins();
            $this->config();
            if( !class_exists('TGM_Plugin_Activation')){
                return false;
            }

            if( function_exists('tgmpa')){
                tgmpa( $this->plugins, $this->config );
            }

        }

        public  function plugins(){

            $this->plugins = array(
                array(
                    'name'                  => 'Corporate Toolkit', // The plugin name
                    'slug'                  => 'corporate-toolkit', // The plugin slug (typically the folder name)
                    'source'                => 'http://kutethemes.net/wordpress/plugins/corporate-toolkit.zip', // The plugin source
                    'required'              => true, // If false, the plugin is only 'recommended' instead of required
                    'version'               => '1.0.7', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                    'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                    'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                    'external_url'          => '', // If set, overrides default API URL and points to an external URL
                ),
                array(
                    'name'                  => 'Corporate Importer', // The plugin name
                    'slug'                  => 'corporate-importer', // The plugin slug (typically the folder name)
                    'source'                => 'http://kutethemes.net/wordpress/plugins/corporate-importer.zip', // The plugin source
                    'required'              => true, // If false, the plugin is only 'recommended' instead of required
                    'version'               => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                    'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                    'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                    'external_url'          => '', // If set, overrides default API URL and points to an external URL
                ),
                array(
                    'name'                  => 'Revolution Slider', // The plugin name
                    'slug'                  => 'revslider', // The plugin slug (typically the folder name)
                    'source'                => 'http://kutethemes.net/wordpress/plugins/revslider.zip', // The plugin source
                    'required'              => true, // If false, the plugin is only 'recommended' instead of required
                    'version'               => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                    'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                    'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                    'external_url'          => '', // If set, overrides default API URL and points to an external URL
                ),
                array(
                    'name'               => 'WPBakery Visual Composer', // The plugin name
                    'slug'               => 'js_composer', // The plugin slug (typically the folder name)
                    'source'             => 'http://kutethemes.net/wordpress/plugins/js_composer.zip', // The plugin source
                    'required'           => true, // If false, the plugin is only 'recommended' instead of required
                    'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                    'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                    'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                    'external_url'       => '', // If set, overrides default API URL and points to an external URL
                    'image'              => get_template_directory_uri().'/famework/assets/images/visual-composer.png'
                ),
                array(
                    'name'      => 'WooCommerce',
                    'slug'      => 'woocommerce',
                    'required'  => false,
                ),
                array(
                    'name'      => 'YITH WooCommerce Compare',
                    'slug'      => 'yith-woocommerce-compare',
                    'required'  => false,
                ),
                array(
                    'name' => 'YITH WooCommerce Wishlist', // The plugin name
                    'slug' => 'yith-woocommerce-wishlist', // The plugin slug (typically the folder name)
                    'required' => false, // If false, the plugin is only 'recommended' instead of required
                ),
                array(
                    'name' => 'YITH WooCommerce Quick View', // The plugin name
                    'slug' => 'yith-woocommerce-quick-view', // The plugin slug (typically the folder name)
                    'required' => false, // If false, the plugin is only 'recommended' instead of required
                ),
                array(
                    'name'                  => 'Vafpress Post Formats UI', // The plugin name
                    'slug'                  => 'vafpress-post-formats-ui-develop', // The plugin slug (typically the folder name)
                    'source'                => 'http://kutethemes.net/wordpress/plugins/vafpress-post-formats-ui-develop.zip', // The plugin source
                    'required'              => true, // If false, the plugin is only 'recommended' instead of required
                    'version'               => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                    'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                    'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                    'external_url'          => '', // If set, overrides default API URL and points to an external URL
                ),
            );
        }

        public function config(){
            $this->config =  array(
                'id'           => 'linda',                 // Unique ID for hashing notices for multiple instances of TGMPA.
                'default_path' => '',                      // Default absolute path to bundled plugins.
                'menu'         => 'linda-install-plugins', // Menu slug.
                'parent_slug'  => 'themes.php',            // Parent menu slug.
                'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
                'has_notices'  => true,                    // Show admin notices or not.
                'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
                'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
                'is_automatic' => true,                   // Automatically activate plugins after installation or not.
                'message'      => '',                      // Message to output right before the plugins table.
            );
        }
    }


}
if( !function_exists('Corporatepro_PluginLoad')){
    function Corporatepro_PluginLoad(){
        new  Corporatepro_PluginLoad();
    }
}
add_action( 'tgmpa_register', 'Corporatepro_PluginLoad' );