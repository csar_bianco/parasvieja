<?php
    /**
     * @author    khanh
     * @copyright 2017
     */
    class corporatepro_author extends WP_Widget
    {

        function __construct() {
            $widget_ops = array( 'classname' => 'widget-instagram', 'description' => __( 'Displays Author', 'corporatepro' ) );
            parent::__construct( 'corporatepro_author', __( 'Corporatepro - Author', 'corporatepro' ), $widget_ops );
        }

        function widget( $args, $instance ) {

            extract( $args, EXTR_SKIP );

            $title = empty( $instance['title'] ) ? '' 		: $instance['title'];
            $users = empty( $instance['users'] ) ? '' 		: intval( $instance['users'] );

            echo $before_widget;

            do_action( 'wpiw_before_widget', $instance );

            ?>
            <div class="widget widget-author">
				<h3 class="title-widget"><?php echo esc_html($title) ?></h3>
				<?php 
					$author_obj = get_user_meta( $users );
				?>
				<div class="cp-author-info">
					<div class="avatar-social">
						<div class="avatar-author">
							<?php echo get_avatar( $users, 210 ); ?>
						</div>
						<div class="social-author">
							<?php if (!empty($author_obj['twitter'][0]) ) : ?>
								<a href="<?php echo esc_url( $author_obj['twitter'][0] ) ?>" class="twitter">
									<i class="fa fa-twitter"></i>
								</a>
							<?php endif; ?>
							<?php if (!empty($author_obj['google'][0]) ) : ?>
								<a href="<?php echo esc_url( $author_obj['google'][0] ) ?>" class="google-plus">
									<i class="fa fa-google-plus"></i>
								</a>
							<?php endif; ?>
							<?php if ( !empty($author_obj['facebook'][0]) ) : ?>
								<a href="<?php echo esc_url( $author_obj['facebook'][0] ) ?>" class="facebook">
									<i class="fa fa-facebook"></i>
								</a>
							<?php endif; ?>
						</div>
					</div>
					<div class="author-name">
						<?php
							if ( $author_obj['first_name'][0] != '' ) {
								echo '<h4>'.esc_html( $author_obj['first_name'][0] ).'</h4>';
							} else {
								echo '<h4>'.esc_html( $author_obj['nickname'][0] ).'</h4>';
							}	 
						?>	
						
						<?php
							if ( !empty($author_obj['company'][0]) ) {
								echo '<span>'.esc_html( $author_obj['company'][0] ).'</span>';
							}
						?>
					</div>
				</div>
			</div>
			<?php
            do_action( 'wpiw_after_widget', $instance );

            echo $after_widget;
        }

        function form( $instance ) {
            $instance = wp_parse_args(
            	(array)$instance,
                array(
                    'title'          	=> __( 'Author', 'corporatepro' ),
                    'users'           	=> ''
                )
            );
            $title = esc_attr( $instance['title'] );
            $users = intval( $instance['users'] );
            ?>
            <p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title', 'corporatepro' ); ?>:
                    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                           name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text"
                           value="<?php echo esc_attr( $title ); ?>"/>
                </label>
            </p>

            <p><label
                    for="<?php echo esc_attr( $this->get_field_id( 'users' ) ); ?>"><?php _e( 'List User', 'corporatepro' ); ?>
                    :</label>
                <select id="<?php echo esc_attr( $this->get_field_id( 'users' ) ); ?>"
                        name="<?php echo esc_attr( $this->get_field_name( 'users' ) ); ?>" class="widefat">
	                    <?php 
	                    	$blogusers = get_users();
	        				// Array of stdClass objects.
							foreach ( $blogusers as $user ) {
							    echo '<option value="' . esc_attr( $user->ID ) . '" '.selected( $user->ID , $users ).'>' . esc_html( $user->display_name ) . '</option>';
							} 
						?>
                </select>
            </p>
            <?php

        }

        function update( $new_instance, $old_instance ) {
            $instance = $old_instance;
            $instance['title'] = strip_tags( $new_instance['title'] );
            $instance['users'] = intval( $new_instance['users'] );

            return $instance;
        }

    }
add_action('widgets_init', create_function('', 'return register_widget("corporatepro_author");'));