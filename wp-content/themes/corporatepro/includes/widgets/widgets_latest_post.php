<?php
// widgets categories
class Widgets_latest_Post extends WP_Widget
{
	function __construct() {
		$widget_ops 	= array( 'classname' => '', 'description' => 'Categories post ...' );
		$control_ops 	= array( 'width' => 400, 'height' => 0 );
		parent::__construct(
			'Widgets_latest_Post',
			esc_html__( 'Corporate - Project Latest Post', 'corporatepro' ),
			$widget_ops, $control_ops
		);
	}

	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );

		echo $before_widget;
		$title 		= empty( $instance['title'] ) ? '' : apply_filters( 'widget_title', $instance['title'] );
		$cat 		= empty( $instance['cat'] ) ? '' : strip_tags( $instance['cat'] );
		$number 	= empty( $instance['number'] ) ? '' : strip_tags( $instance['number'] );

        // <!-- Widget Latest Post -->
    ?>
        <div class="widget widget-new-project">
			<h3 class="title-widget"><?php echo $title ?></h3>
			<div class="cp-new-project">
				<div class="list-project">
					<?php 
						$args = array(
							'post_type'			=> 'project',
							'category_project'	=> $cat,
							'posts_per_page'	=> $number,
						);
								
						$loop = new wp_query($args);
								
						while ($loop->have_posts()) : $loop->the_post();
					?>
						<div class="item-project">
							<div class="pj-image">
								<figure>
			                        <?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, 270, 270, true, true, false ); ?>
			                        <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
                    			</figure>
							</div>
							<div class="pj-info">
								<h3 class="pj-title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>
								<a href="<?php the_permalink() ?>" class="pj-readmore outline-button">
									<?php echo esc_html__('Read more','corporatepro') ?>
								</a>
							</div>
						</div>
					<?php
			            endwhile; wp_reset_postdata();
			        ?>
				</div>
				<div class="pj-info-widget pj-info"></div>
			</div>
		</div>
	<?php
        // <!--End Widget Latest Post -->

		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance 			= $old_instance;
		$instance['title'] 		= strip_tags( $new_instance['title'] );
		$instance['cat'] 		= strip_tags( $new_instance['cat'] );
		$instance['number'] 	= strip_tags( $new_instance['number'] );

		return $instance;
	}

	function form( $instance ) {
		$instance 	= wp_parse_args( (array)$instance, array( 'title' => 'categories', 'cat' => '', 'number' => '3' ) );
		$title 			= strip_tags( $instance['title'] );
		$cat 			= strip_tags( $instance['cat'] );
		$number 		= strip_tags( $instance['number'] );

		?>
		<p>
			<label
				for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title', 'corporatepro' ); ?>
				:</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
			       name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
			       value="<?php echo esc_attr( $title ); ?>"/>
		</p>
		<p>
			<label
				for="<?php echo $this->get_field_id( 'cat' ); ?>"><?php esc_html_e( 'Category', 'corporatepro' ); ?>
				:</label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'cat' ); ?>"
			        name="<?php echo $this->get_field_name( 'cat' ); ?>">

			    <option value=""><?php esc_html_e( '==All==', 'corporatepro' ); ?></option>
			    <?php
		            $terms = get_terms( 'category_project' );
		            if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
		                foreach ( $terms as $term ) {
		                    echo '
								<option '. selected( $cat == $term->slug ) .'
									value="'. $term->slug .'">'. $term->name .'</option>
		                    ';
		                }
		            }
	            ?>
			</select>
		</p>
		<p>
			<label
				for="<?php echo $this->get_field_id( 'number' ); ?>"><?php esc_html_e( 'Number shows', 'corporatepro' ); ?>
				:</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>"
			       name="<?php echo $this->get_field_name( 'number' ); ?>" type="text"
			       value="<?php echo esc_attr( $number ); ?>"/>
		</p>
		<?php
	}
}
add_action('widgets_init', create_function('', 'return register_widget("Widgets_latest_Post");'));