<?php
    /**
     * @author    khanh
     * @copyright 2017
     */
    class corporatepro_tab_post extends WP_Widget
    {

        function __construct() {
            $widget_ops = array( 'classname' => 'widget-instagram', 'description' => __( 'Displays Author', 'corporatepro' ) );
            parent::__construct( 'corporatepro_tab_post', __( 'Corporatepro - Tab Post', 'corporatepro' ), $widget_ops );
        }

        function widget( $args, $instance ) {

            extract( $args, EXTR_SKIP );

            $tab_1          = empty( $instance['tab_1'] ) ? ''          : $instance['tab_1'];
            $tab_2          = empty( $instance['tab_2'] ) ? '' 		    : $instance['tab_2'];
            $number_post    = empty( $instance['number_post'] ) ? ''    : intval( $instance['number_post'] );

            echo $before_widget;

            do_action( 'wpiw_before_widget', $instance );

            ?>

            <div class="widget widget-tab">
                <div class="cp-tab">
                    <ul>
                        <li><a href="#tabs-1"><?php echo esc_html( $tab_1 ) ?></a></li>
                        <li><a href="#tabs-2"><?php echo esc_html( $tab_2 ) ?></a></li>
                    </ul>
                    <div id="tabs-1">
                        <ul class="list-post">
                        <?php  
                            $args = array(
                                'post_type'     => 'post',
                                'ignore_sticky_posts' => 1,
                                'posts_per_page'     => $number_post,
                                'post_status'   => 'publish',
                                'meta_key'      => 'wpb_post_views_count',
                                'orderby'       => 'meta_value_num', 
                            );

                            $loop = new wp_query($args);

                            while ( $loop->have_posts()) : $loop->the_post();
                        ?>
                                <li>
                                    <div class="post-img">
                                        <?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, 70, 70, true, true, false ); ?>
                                        <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
                                    </div>
                                    <div class="post-info">
                                        <h3 class="title-post"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                                        <ul class="meta-post">
                                            <li><i class="fa fa-calendar-o"></i><?php the_time( 'M j' ) ?></li>
                                            <li><i class="fa fa-comment-o"></i>
                                                <?php comments_number(
                                                    esc_html__('0', 'corporatepro'),
                                                    esc_html__('1', 'corporatepro'),
                                                    esc_html__('%', 'corporatepro')
                                                );?>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                        <?php
                            endwhile; wp_reset_postdata();
                        ?>
                        </ul>
                    </div>
                    <div id="tabs-2">
                        <ul class="list-post">
                        <?php  
                            $arg = array(
                                'post_type'     => 'post',
                                'posts_per_page'     => $number_post,
                                'post_status'   => 'publish',
                            );

                            $loops = new wp_query($arg);

                            while ( $loops->have_posts()) : $loops->the_post();
                        ?>
                                <li>
                                    <div class="post-img">
                                        <?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, 70, 70, true, true, false ); ?>
                                        <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
                                    </div>
                                    <div class="post-info">
                                        <h3 class="title-post"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                                        <ul class="meta-post">
                                            <li><i class="fa fa-calendar-o"></i><?php the_time( 'M j' ) ?></li>
                                            <li><i class="fa fa-comment-o"></i>
                                                <?php comments_number(
                                                    esc_html__('0', 'corporatepro'),
                                                    esc_html__('1', 'corporatepro'),
                                                    esc_html__('%', 'corporatepro')
                                                );?>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                        <?php
                            endwhile; wp_reset_postdata();
                        ?>
                        </ul>
                    </div>
                </div>
            </div>

			<?php

            do_action( 'wpiw_after_widget', $instance );

            echo $after_widget;
        }

        function form( $instance ) {
            $instance = wp_parse_args(
            	(array)$instance,
                array(
                    'tab_1'             => __( 'popular', 'corporatepro' ),
                    'tab_2'          	=> __( 'recent', 'corporatepro' ),
                    'number_post'       => 4
                )
            );
            $tab_1 = esc_attr( $instance['tab_1'] );
            $tab_2 = esc_attr( $instance['tab_2'] );
            $number_post = intval( $instance['number_post'] );
            ?>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'tab_1' ) ); ?>"><?php _e( 'Title Tab 1', 'corporatepro' ); ?>:
                    <?php echo esc_html__('( Show most view post )','corporatepro')?>
                    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'tab_1' ) ); ?>"
                           name="<?php echo esc_attr( $this->get_field_name( 'tab_1' ) ); ?>" type="text"
                           value="<?php echo esc_attr( $tab_1 ); ?>"/>
                </label>
            </p>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'tab_2' ) ); ?>"><?php _e( 'Title Tab 2', 'corporatepro' ); ?>:
                    <?php echo esc_html__('( Show recent post )','corporatepro')?>
                    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'tab_2' ) ); ?>"
                           name="<?php echo esc_attr( $this->get_field_name( 'tab_2' ) ); ?>" type="text"
                           value="<?php echo esc_attr( $tab_2 ); ?>"/>
                </label>
            </p>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'number_post' ) ); ?>"><?php _e( 'Number posts:', 'corporatepro' ); ?>:
                    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number_post' ) ); ?>"
                           name="<?php echo esc_attr( $this->get_field_name( 'number_post' ) ); ?>" type="text"
                           value="<?php echo esc_attr( $number_post ); ?>"/>
                </label>
            </p>

            <?php

        }

        function update( $new_instance, $old_instance ) {
            $instance = $old_instance;
            $instance['tab_1']          = strip_tags( $new_instance['tab_1'] );
            $instance['tab_2']          = strip_tags( $new_instance['tab_2'] );
            $instance['number_post']    = intval( $new_instance['number_post'] );

            return $instance;
        }

    }
add_action('widgets_init', create_function('', 'return register_widget("corporatepro_tab_post");'));