<?php
/**
 * Template Name: Box
 *
 * @package WordPress
 * @subpackage corporatepro
 * @since corporatepro 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php wp_head(); ?>
</head>
<?php
?>
<body <?php body_class(); ?>>
    <div class="page-wrapper">
        <?php corporatepro_get_header(); ?>    
            <?php
            // Start the loop.
            while ( have_posts() ) : the_post();
                ?>
                <?php the_content( );?>
                <?php
                // End the loop.
            endwhile;
            ?>
    </div>
<?php get_footer(); ?>