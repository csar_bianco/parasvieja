<?php
/**
 * Template Name: Slide Page
 *
 * @package WordPress
 * @subpackage corporatepro
 * @since corporatepro 1.0
 */
get_header('slider');
?>
<div class="pagination-slide">
    <?php 
        wp_nav_menu( array(
            'menu'            => 'slide-menu',
            'theme_location'  => 'slide-menu',
            'depth'           => 3,
            'container'       => '',
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => '',
            'fallback_cb'     => 'koolshop_bootstrap_navwalker::fallback',
            'walker'          => new koolshop_bootstrap_navwalker()
        ));
    ?>
</div>
    <div class="cp-slidepage">
        <div class="container">
            <?php
            // Start the loop.
            while ( have_posts() ) : the_post();
                ?>
                <?php the_content( );?>
                <?php
                // End the loop.
            endwhile;
            ?>
        </div>
    </div>
<?php wp_footer(); ?>
</body>
</html>