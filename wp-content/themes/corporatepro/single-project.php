<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package corporatepro
 */
?>
<?php get_header(); ?>
<?php

/* project Layout */
$corporatepro_project_layout     = corporatepro_get_option('opt_project_layout','full');
$corporatepro_container_class    = array('main-container');

if( $corporatepro_project_layout == 'full'){
    $corporatepro_container_class[]      = 'no-sidebar';
    $corporatepro_single_project_style   = 'nosidebar';
}else{
    $corporatepro_container_class[]      = 'content-'.$corporatepro_project_layout;
}
$corporatepro_content_class      = array();
$corporatepro_content_class[]    = 'main-content';
if( $corporatepro_project_layout == 'full' ){
    $corporatepro_content_class[] ='col-sm-12';
}else{
    $corporatepro_content_class[] = 'col-md-9 col-sm-8';
}

$true_shop_slidebar_class   = array();
$true_shop_slidebar_class[] = 'sidebar';
if( $corporatepro_project_layout != 'full'){
    $corporatepro_single_project_style   = 'sidebar';
    $true_shop_slidebar_class[]         = 'col-md-3 col-sm-4';
}
/* Project Style */
$corporatepro_project_sidebar       = corporatepro_get_option('opt_project_used_sidebar','project-widget-area');

?>
<?php get_template_part('template-parts/project','banner');?>
<div class="<?php echo esc_attr( implode(' ', $corporatepro_container_class) );?>">
    <div class="container">
        <div class="row">
            <div class="<?php echo esc_attr( implode(' ', $corporatepro_content_class) );?>">
                <?php get_template_part('templates/projects/project',$corporatepro_single_project_style);?>
                <?php get_template_part('templates/projects/project','related');?>
            </div>
            <?php if( $corporatepro_project_layout != "full" ):?>
                <div class="<?php echo esc_attr( implode(' ', $true_shop_slidebar_class) );?>">
                    <?php dynamic_sidebar( $corporatepro_project_sidebar ); ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
