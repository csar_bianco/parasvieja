<?php
$woo_shop_gallery = corporatepro_get_option('woo_shop_gallery','');
$banner_class = array('banner-shop banner-shop');
if( $woo_shop_gallery ){
	$banner_class[]='slide-banner';
}else{
	$banner_class[]='bg-parallax';
}
$woo_shop_gallery = explode(',',$woo_shop_gallery);
?>
<div class="<?php echo esc_attr( implode(' ', $banner_class) );?>">
	<?php if( count( $woo_shop_gallery ) > 0 ):?>
		<div class="owl-carousel dot-style4" data-dots="true" data-items="1">
			<?php foreach( $woo_shop_gallery as $key => $value):?>
			<?php
			$image = wp_get_attachment_image_src( $value, 'full', false );
			?>
			<?php if( $image[0] && $image[0] !=''):?>
			<div class="banner-item" data-bg="<?php echo esc_url($image[0]);?>"></div>
			<?php endif;?>
			<?php endforeach;?>
		</div>
	<?php endif;?>	
	<div class="content-banner">
		<div class="container">
			<h2 class="title-page"><?php woocommerce_page_title();?></h2>
		</div>
	</div>
</div>