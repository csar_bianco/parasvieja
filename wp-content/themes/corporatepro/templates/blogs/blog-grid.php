<?php
$opt_blog_lg_items = corporatepro_get_option('opt_blog_lg_items',4);
$opt_blog_md_items = corporatepro_get_option('opt_blog_md_items',4);
$opt_blog_sm_items = corporatepro_get_option('opt_blog_sm_items',4);
$opt_blog_xs_items = corporatepro_get_option('opt_blog_xs_items',6);
$opt_blog_ts_items = corporatepro_get_option('opt_blog_ts_items',12);

$post_lass = 'blog-item';
$post_lass .= ' col-lg-'.$opt_blog_lg_items;
$post_lass .= ' col-md-'.$opt_blog_md_items;
$post_lass .= ' col-sm-'.$opt_blog_sm_items;
$post_lass .= ' col-xs-'.$opt_blog_xs_items;
$post_lass .= ' col-ts-'.$opt_blog_ts_items;
if( have_posts()){
	?>
	<div class="cp-blog blog-style5 row auto-clear">
		<div class="cp-blog-content">
        <?php
		while( have_posts()){
			the_post();

			?>
            <div <?php post_class( $post_lass );?>>
                <div class="post-item-info">
                    <?php get_template_part('templates/blogs/post','fomats');?>
                    <div class="main-content-post">
	        			<h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
	        			<div class="post-excerpt"><?php echo wp_trim_words(apply_filters('the_excerpt', get_the_excerpt()), 20, __('...', 'corporatepro')); ?></div>
	        			<a class="post-readmore outline-button" href="<?php the_permalink();?>"><?php esc_html_e('Read more','corporatepro')?></a>
	        		</div>
                </div>
            </div>
			<?php
		}
		?>
		</div>
	</div>
	<?php
	corporatepro_paging_nav();
}else{
	get_template_part( 'content', 'none' );
}