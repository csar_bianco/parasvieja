<?php
$opt_blog_lg_items = corporatepro_get_option('opt_blog_lg_items',4);
$opt_blog_md_items = corporatepro_get_option('opt_blog_md_items',4);
$opt_blog_sm_items = corporatepro_get_option('opt_blog_sm_items',4);
$opt_blog_xs_items = corporatepro_get_option('opt_blog_xs_items',6);
$opt_blog_ts_items = corporatepro_get_option('opt_blog_ts_items',12);

$post_lass = 'blog-item';
$post_lass .= ' col-lg-'.$opt_blog_lg_items;
$post_lass .= ' col-md-'.$opt_blog_md_items;
$post_lass .= ' col-sm-'.$opt_blog_sm_items;
$post_lass .= ' col-xs-'.$opt_blog_xs_items;
$post_lass .= ' col-ts-'.$opt_blog_ts_items;

if( have_posts()){
	?>
	<div class="cp-blog blog-style9 row auto-clear">
		<div class="cp-blog-content">
        <?php
		while( have_posts()){
			the_post();
			?>
            <div <?php post_class( $post_lass );?>>
                <div class="post-item-info">
                    <?php get_template_part('templates/blogs/post','fomats');?>
                    <div class="main-content-post">
	        			<h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
	        			<ul class="meta-post">			
	        				<li class="comment-count">
	        					<i class="fa fa-comment-o"></i>
								<?php comments_number(
	                                esc_html__('0', 'corporatepro'),
	                                esc_html__('1', 'corporatepro'),
	                                esc_html__('%', 'corporatepro')
	                            );
	                            ?>
	        				</li>
	        				<li class="author"><i class="fa fa-user"></i><?php the_author_link(); ?></li>
	        				<?php
                    		if ( is_sticky() && is_home() && ! is_paged() ) {
                    			printf( '<li class="sticky-post"><i class="fa fa-flag"></i>%s</li>', esc_html__( 'Sticky', 'corporatepro' ) );
                    		}
                    		?>
	        			</ul>
	        			<div class="post-excerpt"><?php echo wp_trim_words(apply_filters('the_excerpt', get_the_excerpt()), 35, __('...', 'corporatepro')); ?></div>
	        		</div>
                </div>
            </div>
			<?php
		}
		?>
		</div>
	</div>
	<?php
	corporatepro_paging_nav();
}else{
	get_template_part( 'content', 'none' );
}