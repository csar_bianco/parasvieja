<?php
if( have_posts()){
	?>
	<div class="cp-blog blog-style6">
		<div class="cp-blog-content">
        <?php
		while( have_posts()){
			the_post();
			?>
            <div <?php post_class('blog-item');?>>
                <div class="post-item-info">
                    <?php get_template_part('templates/blogs/post','fomats');?>
                    <div class="main-content-post">
	        			<h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
	        			<ul class="meta-post meta-style4">
	        				<?php if( !is_sticky() ): ?>
	        				<li class="date"><i class="fa fa-calendar-o"></i><?php echo get_the_date('M j');?></li>
	        				<?php endif;?>
	        				<li class="comment-count">
	        					<i class="fa fa-comment-o"></i>
								<?php comments_number(
	                                esc_html__('0', 'corporatepro'),
	                                esc_html__('1', 'corporatepro'),
	                                esc_html__('%', 'corporatepro')
	                            );
	                            ?>
	        				</li>
	        				<li class="author"><i class="fa fa-user"></i><?php the_author_link(); ?></li>
	        				<?php
                    		if ( is_sticky() && is_home() && ! is_paged() ) {
                    			printf( '<li class="sticky-post"><i class="fa fa-flag"></i>%s</li>', esc_html__( 'Sticky', 'corporatepro' ) );
                    		}
                    		?>
	        			</ul>
	        			<div class="post-excerpt">
							<?php echo wp_trim_words(apply_filters('the_excerpt', get_the_excerpt()), 35, __('...', 'corporatepro')); ?>
						</div>
	        			<a class="post-readmore outline-button" href="<?php the_permalink();?>"><?php esc_html_e('Read more','corporatepro')?></a>
	        		</div>
                </div>
            </div>
			<?php
		}
		?>
		</div>
	</div>
	<?php
	corporatepro_paging_nav();
}else{
	get_template_part( 'content', 'none' );
}