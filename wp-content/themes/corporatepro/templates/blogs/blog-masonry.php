<?php
$opt_blog_masonry_columns = corporatepro_get_option('opt_blog_masonry_columns',3);
if( have_posts()){
	?>
	<div class="cp-blog blog-style1 cp-portfolio pf-gap20">
		<div class="portfolio-grid cp-blog-content" data-layoutMode="masonry" data-cols="<?php echo esc_attr($opt_blog_masonry_columns);?>">
        <?php
		while( have_posts()){
			the_post();
			?>
            <div id="post-<?php echo get_the_ID() ?>" <?php post_class('blog-item item-portfolio');?>>
                <div class="post-item-info">
                    <?php get_template_part('templates/blogs/post','fomats');?>
                    <div class="main-content-post">
	        			<ul class="meta-post">
	        				<li class="comment-count">
	        					<i class="fa fa-comment-o"></i>
								<?php comments_number(
	                                esc_html__('0', 'corporatepro'),
	                                esc_html__('1', 'corporatepro'),
	                                esc_html__('%', 'corporatepro')
	                            );
	                            ?>
	        				</li>
	        				<li class="author"><i class="fa fa-user"></i><?php the_author_link(); ?></li>
	        			</ul>
	        			<h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
	        			<div class="post-excerpt"><?php echo wp_trim_words(apply_filters('the_excerpt', get_the_excerpt()), 20, __('...', 'corporatepro')); ?></div>
	        			<a class="post-readmore outline-button" href="<?php the_permalink();?>"><?php esc_html_e('Read more','corporatepro')?></a>
	        		</div>
                </div>
            </div>
			<?php
		}
		?>
		</div>
	</div>
	<?php
	corporatepro_paging_nav();
}else{
	get_template_part( 'content', 'none' );
}