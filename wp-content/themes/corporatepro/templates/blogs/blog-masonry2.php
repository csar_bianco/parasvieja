<?php
$opt_blog_masonry_columns = corporatepro_get_option('opt_blog_masonry_columns',3);
if( have_posts()){
	?>
	<div class="cp-blog blog-style2 cp-portfolio">
		 <div class="portfolio-grid cp-blog-content" data-layoutMode="masonry" data-cols="<?php echo esc_attr($opt_blog_masonry_columns);?>">
        <?php
		while( have_posts()){
			the_post();
			?>
            <div id="post-<?php echo get_the_ID() ?>" <?php post_class('blog-item item-portfolio');?>>
                <div class="post-item-info">
                    <?php get_template_part('templates/blogs/post','fomats');?>
                    <div class="main-content-post">
	        			<h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
	        			<div class="post-excerpt"><?php echo wp_trim_words(apply_filters('the_excerpt', get_the_excerpt()), 35, __('...', 'corporatepro')); ?></div>
	        			<ul class="meta-post meta-style2">
	        				<li class="comment-count">
	        					<i class="fa fa-comment-o"></i>
								<?php comments_number(
	                                esc_html__('0', 'corporatepro'),
	                                esc_html__('1', 'corporatepro'),
	                                esc_html__('%', 'corporatepro')
	                            );
	                            ?>
	        				</li>
	        				<li class="author"><i class="fa fa-user"></i><?php the_author_link(); ?></li>
	        			</ul>
	        		</div>
                </div>
            </div>
			<?php
		}
		?>
		</div>
		<?php corporatepro_paging_nav();?>
	</div>
	<?php
	
}else{
	get_template_part( 'content', 'none' );
}