<?php
if( have_posts()){
	?>
	<div class="cp-blog blog-style8 standard">
        <?php
		while( have_posts()){
			the_post();
			?>
            <div <?php post_class('blog-item');?>>
                <div class="post-item-info">
                    <div class="main-content-post">
	        			<h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
	        			<div class="post-content">
	        				<?php the_excerpt(); ?>
	        			</div>
	        		</div>
                </div>
            </div>
			<?php
		}
		?>
	</div>
	<?php
	corporatepro_paging_nav();
}else{
	get_template_part( 'content', 'none' );
}