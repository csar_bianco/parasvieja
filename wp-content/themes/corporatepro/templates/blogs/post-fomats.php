<?php
$content_html  = '';
$post_meta_html  = '';
$fomat_class = array('post-format');
$corporatepro_blog_list_style = corporatepro_get_option('opt_blog_list_style','standard');
if( $corporatepro_blog_list_style =='grid' && !is_single() ){
	ob_start();
	?>
	<ul class="meta-post meta-style3">
		<li><i class="fa fa-calendar-o"></i><?php echo get_the_date('M j');?></li>
		<li><i class="fa fa-comment-o"></i>
			<?php comments_number(
                esc_html__('0', 'corporatepro'),
                esc_html__('1', 'corporatepro'),
                esc_html__('%', 'corporatepro')
            );
            ?>
		</li>
		<li><i class="fa fa-user"></i><?php the_author_link(); ?></li>
	</ul>
	<?php
	$post_meta_html = ob_get_clean();
}

if( $corporatepro_blog_list_style =='list2' && !is_single() ){
	ob_start();
	?>
	<ul class="meta-post meta-style5">
		<li><i class="fa fa-calendar-o"></i><?php echo get_the_date('M j, Y');?></li>
		<li><i class="fa fa-comment-o"></i>
			<?php comments_number(
                esc_html__('0 comment', 'corporatepro'),
                esc_html__('1 comment', 'corporatepro'),
                esc_html__('% comments', 'corporatepro')
            );
            ?>
		</li>
	</ul>
	<?php
	$post_meta_html = ob_get_clean();
}

if( has_post_format('gallery') ){
	$fomat_class[] ='post-gallery slider';
	ob_start();
	?>
	<?php
	?>
	<?php 
	$blog_list_style = corporatepro_get_option('opt_blog_list_style','standard');

    $thumb_w = 1170;
    $thumb_h = 854;
    $crop    = true;
    if( $blog_list_style == 'standard' && $blog_list_style == 'list'){
        $thumb_w = 680;
        $thumb_h = 496;
    }
    if( $blog_list_style == 'grid'){
        $thumb_w = 370;
        $thumb_h = 370;
    }
    if( $blog_list_style == 'grid2'){
        $thumb_w = 420;
        $thumb_h = 210;
    }
    if( $blog_list_style == 'list2'){
        $thumb_w = 1170;
        $thumb_h = 585;
    }
    if( $blog_list_style == 'list3'){
        $thumb_w = 700;
        $thumb_h = 400;
    }
    if( $blog_list_style == 'masonry' || $blog_list_style == 'masonry2'){
        $thumb_w = 400;
        $thumb_h = 400;
    }


    ?>
	<?php $images = get_post_meta( get_the_ID(), '_format_gallery_images', true ); ?>
    <?php if ( $images ) : ?>
	<div class="post-slide owl-carousel nav-style3" data-nav="true" data-dots="false" data-items="1">
	    <?php foreach ( $images as $image_id ) : ?>
	    	<?php
	        $image_url = corporatepro_resize_image( $image_id, null, $thumb_w, $thumb_h, $crop, true );
	        $image_url = $image_url['url'];
    		$imgage_caption = get_post_field( 'post_excerpt', $image_id );
    		?>
			<img src="<?php echo esc_url($image_url); ?>" alt="" <?php if ($imgage_caption) : ?>title="<?php echo esc_attr($imgage_caption); ?>"<?php endif; ?> />
		<?php endforeach; ?>
	</div>
	<?php else:?>
		<?php corporatepro_post_thumbnail(); ?>
	<?php endif; ?>
	<?php
	$content_html = ob_get_clean();
}elseif ( has_post_format('video') ) {
	$fomat_class[] = 'post-video';
	ob_start();
	?>
	<?php if ( is_single() ) : ?>
		<?php $video = get_post_meta( get_the_ID(), '_format_video_embed', true ); ?>
		<?php if( $video ):?>
		<div class="embed-responsive embed-responsive-16by9">
		    <?php if ( wp_oembed_get($video) ) : ?>
		        <?php echo wp_oembed_get($video); ?>
		    <?php else : ?>
		        <?php echo wp_kses_post($video); ?>
		    <?php endif; ?>
		</div>
	    <?php endif; ?>
	<?php else:?>	
		<?php if ( has_post_thumbnail() ) : ?>
			<?php corporatepro_post_thumbnail(); ?>
	    	<a href="<?php the_permalink();?>" class="icon-video"><i class="fa fa-play"></i></a>
	    <?php else:?>
	    	<?php corporatepro_post_thumbnail(); ?>
	    <?php endif; ?>
	<?php endif; ?>
	<?php
	$content_html = ob_get_clean();

}else{
	$fomat_class[] = 'post-standard';
	ob_start();
	?>
	<?php if ( !is_single() ) : ?>
        <a href="<?php the_permalink(); ?>"><?php corporatepro_post_thumbnail(); ?></a>
    <?php else : ?>
        <?php the_post_thumbnail('full'); ?>
    <?php endif; ?>
	<?php
	$content_html = ob_get_clean();
}
?>

<?php if( $content_html != '' ):?>
<div class="<?php echo esc_attr( implode(' ', $fomat_class) );?>">
	<?php echo balanceTags($content_html);?>
	<?php if( $post_meta_html != '' ):?>
	<?php echo balanceTags($post_meta_html);?>
	<?php endif;?>
	<?php if(  in_array($corporatepro_blog_list_style, array('grid2','masonry') )  ):?>
		<span class="block-date-post"><i class="fa fa-calendar-o"></i><?php echo get_the_date('M j');?></span>
	<?php endif;?>	
	<?php if(  in_array($corporatepro_blog_list_style, array('masonry2') )  ):?>
		<div class="post-cat"><?php the_category(' ');?></div>
	<?php endif;?>
</div>
<?php endif;?>