<?php
/*
 Name:	Footer style 01
 */
?>
<footer class="footer footer-style1">
	<div class="container">
		<?php the_content();?>
	</div>
	
</footer>
