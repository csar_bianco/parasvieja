<?php
/*
 Name:	Footer style 07
 */
?>
<!-- Footer -->
<footer class="footer footer-style10">
	<div class="main-footer">
		<div class="container">
			<?php the_content();?>
		</div>
	</div>
</footer>
<!-- /Footer -->