<?php
/*
 Name:	Footer style 08
 */
?>
<!-- Footer -->
<footer class="footer footer-shop">
	<div class="container">
		<?php the_content();?>
	</div>
</footer>
<!-- /Footer -->