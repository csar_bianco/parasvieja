<?php
/*
Name:	Header style 03
*/
?>
<!-- Header -->
<header class="header header-style3 trans">
	<div class="main-header">
		<div class="container">
			<div class="header-content">
				<div class="logo">
					<?php corporatepro_get_logo();?>
				</div>
				<a href="javascript:void(0)" class="menu-togole"><i class="fa fa-bars"></i></a>
				<div class="cp-main-menu">
					<nav class="navigation">
						<?php 
	        				wp_nav_menu( array(
	        					'menu'            => 'primary',
	        					'theme_location'  => 'primary',
	        					'container'       => '',
	        					'container_class' => '',
	        					'container_id'    => '',
	        					'menu_class'      => '',
	        					'fallback_cb'     => 'koolshop_bootstrap_navwalker::fallback',
	        					'walker'          => new koolshop_bootstrap_navwalker()
	                        ));
	                     ?>
					</nav>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- /Header -->