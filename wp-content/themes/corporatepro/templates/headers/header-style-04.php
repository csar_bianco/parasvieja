<?php
/*
Name:	Header style 04
*/
?>
<!-- Header -->
<header class="header header-style4 trans">
	<div class="logo">
		<?php corporatepro_get_logo();?>
	</div>
	<h1 class="web-title"><?php wp_title(''); ?></h1>
	<div class="main-header">
		<div class="container">
			<div class="header-content main-header-content">
				<a href="javascript:void(0)" class="menu-togole"><i class="fa fa-bars"></i></a>
				<div class="cp-main-menu">
					<nav class="navigation">
						<?php 
	        				wp_nav_menu( array(
	        					'menu'            => 'primary',
	        					'theme_location'  => 'primary',
	        					'container'       => '',
	        					'container_class' => '',
	        					'container_id'    => '',
	        					'menu_class'      => '',
	        					'fallback_cb'     => 'koolshop_bootstrap_navwalker::fallback',
	        					'walker'          => new koolshop_bootstrap_navwalker()
	                        ));
	                    ?>
					</nav>
				</div>
				<div class="cp-account">
					<?php 
        				wp_nav_menu( array(
        					'menu'            => 'top-bar-right-menu',
        					'theme_location'  => 'top-bar-right-menu',
        					'container'       => '',
        					'container_class' => '',
        					'container_id'    => '',
        					'menu_class'      => '',
        					'fallback_cb'     => 'koolshop_bootstrap_navwalker::fallback',
        					'walker'          => new koolshop_bootstrap_navwalker()
                        ));
                    ?>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- /Header -->