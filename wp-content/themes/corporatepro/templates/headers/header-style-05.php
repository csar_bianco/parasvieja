<?php
/*
Name:	Header style 05
*/
?>
<!-- Header -->
<header class="header header-style9">
	<div class="main-header">
		<div class="container">
			<div class="header-content">
				<div class="logo">
					<?php corporatepro_get_logo();?>
				</div>
				<div class="cp-navigation">
					<a href="<?php echo wc_get_cart_url(); ?>" class="icon-text header-shoppingcart">
						<i class="fa fa-shopping-basket"></i>
						<span class="cart-number-items"><?php echo WC()->cart->get_cart_contents_count() ?></span>
					</a>
					<a href="javascript:void(0)" class="icon-text touch-search"><i class="fa fa-search"></i></a>
					<a href="javascript:void(0)" class="menu-togole"><i class="fa fa-bars"></i></a>
					<div class="cp-main-menu">
						<nav class="navigation">
							<?php 
		        				wp_nav_menu( array(
		        					'menu'            => 'primary',
		        					'theme_location'  => 'primary',
		        					'container'       => '',
		        					'container_class' => '',
		        					'container_id'    => '',
		        					'menu_class'      => '',
		        					'fallback_cb'     => 'koolshop_bootstrap_navwalker::fallback',
		        					'walker'          => new koolshop_bootstrap_navwalker()
		                        ));
		                     ?>
						</nav>
					</div>
					<div class="cp-search-box-shop">
			        	<div class="cp-overlay"></div>
			        	<div class="container">
			                <div class="cp-search">
			                	<div class="inner-search">
			                		<!-- Search form -->
									<?php corporatepro_get_search_form();?>
									<!-- ./Search form -->               
								</div>
			                </div>
		                </div>     
			        </div>
		        </div>
			</div>
		</div>
	</div>
</header>
<!-- /Header -->