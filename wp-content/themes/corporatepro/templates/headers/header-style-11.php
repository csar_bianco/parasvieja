<?php
/*
Name:	Header style 10
*/
if ( class_exists( 'WooCommerce' ) ) {
    global $woocommerce;
    $items_text = $woocommerce->cart->cart_contents_count > 1 ? esc_html__( 'items', 'corporatepro' ) : esc_html__( 'item', 'corporatepro' );
    $cart_subtotal = 0;

    if ( !WC()->cart->is_empty() ) {
        $cart_subtotal = WC()->cart->get_cart_subtotal();
    }
}
?>
<!-- Header -->
<header class="header header-shop">
    <div class="body-ovelay"></div>
    <div class="main-header">
        <div class="container">
            <div class="header-left">
                <a href="javascript:void(0)" class="icon-text touch-menu-sidebar"><i class="fa fa-bars"></i>Menu</a>
                <a href="javascript:void(0)" class="icon-text touch-search"><i class="fa fa-search"></i>Search</a>
            </div>
            <div class="logo">
                <?php corporatepro_get_logo();?>
            </div>
            <div class="header-right">
                <a href="<?php if ( class_exists( 'WooCommerce' ) ) {echo get_page_link( get_page_by_title( 'My Account' )->ID );} ?>" class="icon-text">
                    <i class="fa fa-desktop"></i>
                    <?php echo esc_html__('my account','corporatepro') ?>
                </a>
                <a href="<?php if ( class_exists( 'WooCommerce' ) ) {echo get_page_link( get_page_by_title( 'wishlist' )->ID );} ?>" class="icon-text">
                    <i class="fa fa-heart-o"></i>
                    <?php echo esc_html__('wishlist','corporatepro') ?>
                </a>
                <a href="<?php if ( class_exists( 'WooCommerce' ) ) {echo wc_get_cart_url();}  ?>" class="icon-text header-shoppingcart">
                    <i class="fa fa-shopping-basket"></i>
                    <span class="cart-number-items">
						<?php if ( class_exists( 'WooCommerce' ) ){ echo WC()->cart->get_cart_contents_count(); }?>
					</span>
                    <?php echo esc_html__('my cart','corporatepro') ?>
                </a>
            </div>
        </div>
    </div>
    <div class="cp-left-nav">
        <span class="close-touch"><i class="fa fa-close"></i></span>
        <nav class="navigation">
            <?php
            wp_nav_menu( array(
                'menu'            => 'primary',
                'theme_location'  => 'primary',
                'container'       => '',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => '',
                'fallback_cb'     => 'koolshop_bootstrap_navwalker::fallback',
                'walker'          => new koolshop_bootstrap_navwalker()
            ));
            ?>
        </nav>
    </div>
    <div class="cp-search-box-shop">
        <div class="cp-overlay"></div>
        <div class="container">
            <div class="cp-search">
                <div class="inner-search">
                    <!-- Search form -->
                    <?php corporatepro_get_search_form();?>
                    <!-- ./Search form -->
                </div>
            </div>
        </div>
    </div>
</header>
<!-- /Header -->
