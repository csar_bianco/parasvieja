<?php  
$project_related_post   = corporatepro_get_option('opt_project_related_post');
$post_per_page          = corporatepro_get_option('opt_related_project_per_page');
$item1 = corporatepro_get_option('opt_project_related_lg_items');
$item2 = corporatepro_get_option('opt_project_related_md_items');
$item3 = corporatepro_get_option('opt_project_related_sm_items');
$item4 = corporatepro_get_option('opt_project_related_xs_items');
$item5 = corporatepro_get_option('opt_project_related_ts_items');
$responsive = '';
$responsive .= '{"0":{"items":'.$item5.'},"480":{"items":'.$item4.'},"768":{"items":'.$item3.'},"992":{"items":'.$item2.'},"1200":{"items":'.$item1.'}}';
?>

<div class="pj-single-2 project_switch_<?php echo $project_related_post ?>">
    <div class="cp-project-relate">
        <h3><?php echo esc_html__('Related Projects','corporatepro') ?></h3>
        <div class="cp-project project-style1 owl-carousel nav-style2" data-nav="true" data-dots="false" data-items="<?php echo $post_per_page ?>" data-margin="30" data-responsive='<?php echo $responsive ?>'>
            <?php
                $args = array(
                    'post_type'     => 'project',
                    'showposts'     => -1
                    );
                $loop = new wp_query($args);
                while ($loop->have_posts()) : $loop->the_post();
                    $categories_list = get_the_term_list($post->ID,'category_project','',' ');
                    $names = get_the_terms($post->ID,'category_project');
            ?>
            <div class="item-project">
                <div class="pj-caption">
                    <div class="pj-image">
                        <figure>
                            <?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, 270, 270, true, true, false ); ?>
                            <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
                        </figure>
                        <div class="pj-hover">
                            <a href="<?php the_permalink(); ?>" class="pj-icon"><i class="fa fa-link"></i></a>
                        </div>
                    </div>
                    <div class="pj-meta">
                        <ul>
                            <li><i class="fa fa-calendar-o"></i><?php the_time('M j, Y') ?></li>
                            <li><i class="fa fa-user"></i>
                                <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))) ?>">
                                    <?php the_author(); ?>
                                </a>
                            </li>
                            <li><i class="fa fa-folder-o"></i>
                                <?php
                                    if ($categories_list ) {
                                        printf( esc_html__('%1$s', 'corporatepro') , $categories_list);
                                    }
                                ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="pj-info">
                    <h3 class="pj-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <a href="<?php the_permalink(); ?>" class="pj-readmore outline-button">
                        <?php echo esc_html__('Read more','corporatepro') ?>
                    </a>
                </div>
            </div>
            <?php
                endwhile; wp_reset_postdata();
            ?>
        </div>
    </div>
</div>
