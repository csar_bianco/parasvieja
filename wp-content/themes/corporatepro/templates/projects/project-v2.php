<?php
/**
 * Created by PhpStorm.
 * User: hoangkhanh
 * Date: 12/24/2016
 * Time: 1:53 PM
 */

$ppp = corporatepro_get_option('opt-posts_per_page');
?>
<?php
    if( have_posts()){
    $corporatepro_project_number = corporatepro_get_option('opt_number_project_items');
?>

<div class="cp-loadmore-main">
    <div class="cp-project project-style2 cp-project-content">
        <?php
        while (have_posts()) : the_post();
            $categories_list = get_the_term_list(get_the_ID(),'category_project','',' ');
        ?>

            <div id="<?php echo 'post-'.get_the_ID() ?>" class="item-project">
                <div class="pj-caption">
                    <div class="pj-image">
                        <figure>
                            <?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, 270, 270, true, true, false ); ?>
                            <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
                        </figure>
                        <div class="pj-hover">
                            <a href="<?php the_permalink(); ?>" class="pj-icon"><i class="fa fa-link"></i></a>
                        </div>
                    </div>
                </div>
                <div class="pj-info">
                    <h3 class="pj-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <div class="pj-meta">
                        <ul>
                            <li><i class="fa fa-calendar-o"></i><?php the_time('M j, Y') ?></li>
                            <li><i class="fa fa-user"></i>
                                <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))) ?>">
                                    <?php the_author(); ?>
                                </a>
                            </li>
                            <li><i class="fa fa-folder-o"></i>
                                <?php
                                    if ($categories_list ) {
                                        printf( esc_html__('%1$s', 'corporatepro') , $categories_list);
                                    }
                                ?>
                            </li>
                        </ul>
                    </div>
                    <div class="pj-excerpt">
                        <?php echo wp_trim_words(get_the_content( ), 30 , "..." ); ?>
                    </div>
                    <a href="<?php the_permalink(); ?>" class="pj-readmore outline-button"><?php echo esc_html__('Read more','corporatepro') ?></a>
                </div>
            </div>

        <?php
            endwhile;
        ?>
    </div>
    <?php corporatepro_project_paging_nav(); ?>
</div>
<?php
    }else{
        echo '<h1 style="text-align: center"> Nothing to show ...!</h1>';
    }
?>
