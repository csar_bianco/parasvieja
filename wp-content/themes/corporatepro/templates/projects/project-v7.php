<?php
/**
 * Created by PhpStorm.
 * User: hoangkhanh
 * Date: 12/24/2016
 * Time: 2:39 PM
 */

if( have_posts()){
    $ppp = corporatepro_get_option('opt-posts_per_page');
?>
<div class="cp-project project-style7 cp-blog">
    <div class="project-grid cp-blog-content">
        <?php
            $i = 1;
            while (have_posts()) : the_post();
            $categories_list = get_the_term_list($post->ID,'category_project','',' ');
            $i++;
            $value          = get_post_meta( get_the_ID(),'_corporatepro_images_gallery',true);
            $image_thumb    = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()), null, 674, 386, true, true, false );
        ?>
        <?php
            if ($i % 2 == 0) { ?>
                <div id="<?php echo 'post-'.get_the_ID() ?>" class="item-project blog-item">
                    <div class="pj-caption">
                        <?php 
                            if ( ! empty( $value ) ) {
                        ?>
                            <div class="pj-image owl-carousel" data-items="1" data-dots="true">
                                <figure>
                                    <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
                                </figure>
                                <?php foreach ($value as $item) { ?>
                                    <?php $gallery_thumb  = corporatepro_resize_image( null, $item, 674, 386, true, true, false ); ?>
                                    <figure>
                                        <img class="img-responsive" src="<?php echo esc_url($gallery_thumb['url']); ?>" width="<?php echo intval($gallery_thumb['width']) ?>" height="<?php echo intval($gallery_thumb['height']) ?>"  alt="<?php the_title() ?>">
                                    </figure>
                                <?php } ?>
                            </div>
                        <?php   } else { ?>
                            <div class="pj-image">
                                <figure>
                                    <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
                                </figure>
                            </div>
                        <?php   } ?>
                    </div>
                    <div class="bg-pj-info">
                        <div class="pj-info">
                            <h3 class="pj-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <div class="pj-meta">
                                <ul>
                                    <li><i class="fa fa-calendar-o"></i><?php the_time('M j, Y') ?></li>
                                    <li><i class="fa fa-user"></i>
                                        <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))) ?>">
                                            <?php the_author(); ?>
                                        </a>
                                    </li>
                                    <li><i class="fa fa-folder-o"></i>
                                        <?php
                                        if ($categories_list ) {
                                            printf( esc_html__('%1$s', 'corporatepro') , $categories_list);
                                        }
                                        ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="pj-excerpt">
                                <?php echo wp_trim_words(get_the_content( ), 30 , "..." ); ?>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="pj-readmore outline-button"><?php echo esc_html__('Read more','corporatepro') ?></a>
                        </div>
                    </div>
                </div>
        <?php } else { ?>
                <div id="<?php echo 'post-'.get_the_ID() ?>" class="item-project blog-item">
                    <div class="bg-pj-info">
                        <div class="pj-info">
                            <h3 class="pj-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <div class="pj-meta">
                                <ul>
                                    <li><i class="fa fa-calendar-o"></i><?php the_time('M j, Y') ?></li>
                                    <li><i class="fa fa-user"></i>
                                        <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))) ?>">
                                            <?php the_author(); ?>
                                        </a>
                                    </li>
                                    <li><i class="fa fa-folder-o"></i>
                                        <?php
                                        if ($categories_list ) {
                                            printf( esc_html__('%1$s', 'corporatepro') , $categories_list);
                                        }
                                        ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="pj-excerpt">
                                <?php echo wp_trim_words(get_the_content( ), 30 , "..." ); ?>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="pj-readmore outline-button"><?php echo esc_html__('Read more','corporatepro') ?></a>
                        </div>
                    </div>
                    <div class="pj-caption">
                        <?php 
                            if ( ! empty( $value ) ) {
                        ?>
                            <div class="pj-image owl-carousel" data-items="1" data-dots="true">
                                <?php foreach ($value as $key => $item) { ?>
                                    <figure>
                                        <img src="<?php echo esc_attr($item); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>"  alt="<?php the_title() ?>">
                                    </figure>
                                <?php } ?>
                            </div>
                        <?php   } else { ?>
                            <div class="pj-image">
                                <figure>
                                    <img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
                                </figure>
                            </div>
                        <?php   } ?>
                    </div>
                </div>
        <?php } ?>
        <?php
            endwhile;
        ?>
    </div>
    <?php corporatepro_project_paging_nav(); ?>
</div>

<?php
    } else {
        echo '<h1 style="text-align: center"> Nothing to show ...!</h1>';
    }
?>