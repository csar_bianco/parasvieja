<?php
/**
 * The template for displaying product widget entries
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-widget-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product; ?>

<li class="product">
	<div class="product-thumb">
		<?php $image_thumb = corporatepro_resize_image( get_post_thumbnail_id(get_the_ID()) , null, 70, 70, true, true, false ); ?>
		<img class="img-responsive" src="<?php echo esc_attr($image_thumb['url']); ?>" width="<?php echo intval($image_thumb['width']) ?>" height="<?php echo intval($image_thumb['height']) ?>" alt="<?php the_title() ?>">
	</div>
	<div class="info-product">
        <h3 class="title-product">
            <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
        </h3>
        <?php if ( $price_html = $product->get_price_html() ) : ?>
            <span class="price pj-price"><?php echo $price_html; ?></span>
        <?php endif; ?>
		<?php if ( wc_get_rating_html( $product->get_average_rating() ) ) : ?>
			<?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
		<?php else : ?>
            <div class="box-rating"></div>
        <?php endif; ?>
	</div>
</li>
