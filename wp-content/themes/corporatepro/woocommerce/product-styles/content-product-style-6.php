<?php
/**
 * woocommerce_before_shop_loop_item hook.
 *
 * @hooked woocommerce_template_loop_product_link_open - 10
 */
do_action( 'woocommerce_before_shop_loop_item' );
?>
<div class="product">
	<div class="product-inner">
		<div class="product-thumb">
			<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
			?>
			<div class="hover-thumb">
				<?php do_action('corporatepro_function_shop_loop_item_quickview'); ?>
			</div>
		</div>
		<div class="info-product">
			<?php  
			/**
			 * woocommerce_shop_loop_item_title hook.
			 *
			 * @hooked woocommerce_template_loop_product_title - 10
			 */
			do_action( 'woocommerce_shop_loop_item_title' );
			?>
			<span class="price">
				<?php 
				/**
				 * woocommerce_after_shop_loop_item_title hook.
				 *
				 * @hooked woocommerce_template_loop_rating - 5
				 * @hooked woocommerce_template_loop_price - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item_title' );
				?>
			</span>
			<div class="product-button">
				<?php  
				do_action('corporatepro_function_shop_loop_item_wishlist');
				/**
				 * woocommerce_after_shop_loop_item hook.
				 *
				 * @hooked woocommerce_template_loop_product_link_close - 5
				 * @hooked woocommerce_template_loop_add_to_cart - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item' );
				do_action('corporatepro_function_shop_loop_item_compare');
				?>
			</div>
			<div class="product-excerpt"><?php echo wp_trim_words(get_the_excerpt(),10,'...'); ?></div>
		</div>
	</div>
</div>