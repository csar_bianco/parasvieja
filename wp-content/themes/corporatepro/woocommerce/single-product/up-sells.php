<?php
/**
 * Single Product Up-Sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/up-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     4.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product, $woocommerce_loop;

if ( ! $upsells ) {
	return;
}

$woocommerce_loop['name']    = 'up-sells';
$woocommerce_loop['columns'] = apply_filters( 'woocommerce_up_sells_columns', $columns );

$woo_up_sells_lg_items = corporatepro_get_option('woo_up_sells_lg_items',3);
$woo_up_sells_md_items = corporatepro_get_option('woo_up_sells_md_items',3);
$woo_up_sells_sm_items = corporatepro_get_option('woo_up_sells_sm_items',2);
$woo_up_sells_xs_items = corporatepro_get_option('woo_up_sells_xs_items',2);
$woo_up_sells_ts_items = corporatepro_get_option('woo_up_sells_ts_items',1);

$data_reponsive = array(
	'0'=>array(
		'items'=>$woo_up_sells_ts_items
	),
	'480'=>array(
		'items'=>$woo_up_sells_xs_items
	),
	'768'=>array(
		'items'=>$woo_up_sells_sm_items
	),
	'992'=>array(
		'items'=>$woo_up_sells_md_items
	),
	'1200'=>array(
		'items'=>$woo_up_sells_lg_items
	),
);

$data_reponsive = json_encode($data_reponsive);
$loop = 'false';

if ( $upsells ) : ?>

	<div class="up-sells upsells products cp-product-latest">
		<?php
		$woo_up_sells_title = corporatepro_get_option('woo_up_sells_title','You may also like');
		$classes = array('product-item product');
		if (count($upsells) > $woo_up_sells_lg_items ) {
			$loop = 'true';
        }
		?>

		<h4><?php echo esc_html($woo_up_sells_title); ?></h4>
		<?php woocommerce_product_loop_start(); ?>
			<div class="product-grid product-slide owl-carousel nav-style4" data-margin="30" data-nav="true" data-dots="false" data-loop="<?php echo esc_attr($loop);?>" data-responsive='<?php echo esc_attr($data_reponsive);?>'>
                <?php foreach ( $upsells as $upsell ) : ?>
                    <?php
                    $post_object = get_post( $upsell->get_id() );

                    setup_postdata( $GLOBALS['post'] =& $post_object );
                    ?>
                    <div <?php post_class(  ); ?>>
                    <?php wc_get_template_part( 'product-styles/content-product', 'style-1' ); ?>
                    </div>
                <?php endforeach; ?>
			</div>
		<?php woocommerce_product_loop_end(); ?>
	</div>

<?php endif;

wp_reset_postdata();
